package com.tkilas.Utils;

import java.util.ResourceBundle;

public class EnumUtils{
	
	ResourceBundle bundle1 = ResourceBundle.getBundle("TestBundle");
	
	public enum DistrictEnum {
		CENTRO(0,"Centro"), CHAMBERI(1,"Chamberí"), CHAMARTIN(2,"Chamartín"), RETIRO(3,"Retiro"), SALAMANCA(4,"Salamanca"), TETUAN(5,"Tetuan");
		 private int code;
		 private String label;
		 private DistrictEnum(int code, String label) {
		   this.code = code;
		   this.label = label;
		 }
		 public int getCode() {
		   return code;
		 }
		 
		 public String getLabel(){
			 return label;
		 }
	}
	
	public enum LocalTypeEnum {
		PUB(0,"Pub"), DISCOTECA(1,"Discoteca"), BAR(2,"Bar/Cafeteria"), RESTAURANTE(3,"Restaurante");
		 private int code;
		 private String label;
		 private LocalTypeEnum(int code, String label) {
		   this.code = code;
		   this.label = label;
		 }
		 public int getCode() {
		   return code;
		 }
		 
		 public String getLabel(){
			 return label;
		 }
	}
	
	public enum ProductTypeLocal {
		 CERVEZA(0,"Sólo Cervezas"), COPAS(1,"Sólo Copas"), CAFE(2,"Sólo Cafes"), BOTELLAS(3,"Sólo Botellas"), ALL(4,"Todo"), 
		 CERVEZA_COPA(5,"Cervezas + Copas"),CERVEZA_COPA_BOTELLA(6,"Cervezas + Copas + Botellas"), COPAS_BOTELLAS(7,"Copas + Botellas"), CAFES_CERVEZAS(8,"Cafe + Cervezas");
		 private int code;
		 private String label;
		 private ProductTypeLocal(int code, String label) {
		   this.code = code;
		   this.label = label;
		 }
		 public int getCode() {
		   return code;
		 }
		 
		 public String getLabel(){
			 return label;
		 }
	}
	
	public enum CitiesEnum {
		 ACORUÑA(0,"A Coruña"), ALAVA(1,"Álava"), CAFE(2,"Albacete"), ALICANTE(3,"Alicante"), ALMERIA(4,"Almería"), 
		 ASTURIAS(5,"Asturias"),AVILA(6,"Ávila"), BADAJOZ(7,"Badajoz"), BARCELONA(8,"Barcelona"),
		 BURGOS(9,"Burgos"),CACERES(10,"Cáceres"),CADIZ(11,"Cádiz"), CANTABRIA(12,"Cantabria"),
		 CASTELLON(13,"Castellón"),CEUTA(14,"Ceuta"), CIUDADREAL(15,"Ciudad Real"), CORDOBA(16,"Córdoba"),
		 CUENCA(17,"Cuenca"),GIRONA(18,"Girona"), GRANADA(19,"Granada"),GUADALAJARA(20,"Guadalajara"),
		 GUIPUZCOA(21,"Guipúzcoa"),HUELVA(22,"Huelva"), HUESCA(23,"Huesca"), JAEN (24,"Jaén"), LASPALMAS(25,"Las Palmas"),
		 LARIOJA(26,"La Rioja"),LEON(27,"León"), LLEIDA(28,"Lleida"), LUGO(29,"Lugo"), MADRID(30,"Madrid"), MALAGA(31,"Málaga"),
		 MALLORCA(31,"Mallorca"),MELILLA(32,"Melilla"),MURCIA(33,"Murcia"), NAVARRA (34,"Navarra"), OURENSE(35,"Ourense"),
		 PALENCIA(36,"Palencia"), PONTEVEDRA(37,"Pontevedra"),SALAMANCA(38,"Salamanca"),SEGOVIA(39,"Segovia"),SEVILLA(40,"Sevilla"),
		 SORIA(41,"Soria"),TARRAGONA(42,"Tarragona"),TENERIFE(43,"Tenerife"), TERUEL (44,"Teruel"),TOLEDO(45,"Toledo"),
		 VALENCIA(46,"Valencia"),VALLADOLID(47,"Valladolid"), VIZCAYA (48,"Vizcaya"), ZAMORA (49,"Zamora"), ZARAGOZA(50,"Zaragoza");
		 private int code;
		 private String label;
		 private CitiesEnum(int code, String label) {
		   this.code = code;
		   this.label = label;
		 }
		 public int getCode() {
		   return code;
		 }
		 
		 public String getLabel(){
			 return label;
		 }
	}
	
	public enum ProductType{
		
		 CERVEZA(0,"Cerveza"),CAFE(1,"Café"), COPA(2,"Copa"), BOTELLA(3,"Botella");
		
		 private int code;
		 private String label;
		 private ProductType(int code, String label) {
		   this.code = code;
		   this.label = label;
		 }
		 public int getCode() {
		   return code;
		 }
		 
		 public String getLabel(){
			 return label;
		 }
	}
	
	
	public enum ProductSubtype{
		
		 BASIC(0,"Básico"),PREMIUM(1,"Premium"),VINO(2,"Vino");
		
		 private int code;
		 private String label;
		 private ProductSubtype(int code, String label) {
		   this.code = code;
		   this.label = label;
		 }
		 public int getCode() {
		   return code;
		 }
		 
		 public String getLabel(){
			 return label;
		 }
	}
	
	public enum ProductSubtypeCopa{
		
		 BASIC(0,"Básico"),PREMIUM(1,"Premium"),VINO(2,"Vino"),COCKTAIL(3,"Cocktail");
		
		 private int code;
		 private String label;
		 private ProductSubtypeCopa(int code, String label) {
		   this.code = code;
		   this.label = label;
		 }
		 public int getCode() {
		   return code;
		 }
		 
		 public String getLabel(){
			 return label;
		 }
	}
	
	
	public enum StatusClientReservation{
		
		 SINCONFIRMAR(0,"Sin confirmar"),CONFIRMED(1,"Confirmado"),DENIED(2,"Denegado");
		
		 private int code;
		 private String label;
		 private StatusClientReservation(int code, String label) {
		   this.code = code;
		   this.label = label;
		 }
		 public int getCode() {
		   return code;
		 }
		 
		 public String getLabel(){
			 return label;
		 }
	}

	
}


package com.tkilas.Utils;

public class StringTkilasUtils {

	public static final String HTML_REGISTER_LOCAL_OK = "<div id='tarifas_text_wrapper'>"+
				"<div style='top: 8%;bottom: 4%;margin: auto;position:absolute;left:0;right: 0; max-width: 600px; max-height: 500px;'>"+
					"<h1 style='text-align: center;'>¡Enhorabuena!</h1><br>"+
					"<p class='text_normal'>Finaliza ahora el registro de tu establecimiento y comienza a subir tus ofertas en este link <a href='https://migestor.tkilas.com'>https://migestor.tkilas.com</a></p><br>"+
					"<p class='text_spm'>Si ahora no puede finalizar su registro, le hemos mandado un correo electrónico para que lo haga</p><br>"+
					"<p class='text_spm'><i>No olvide revisar su carpeta de correo no deseado y si lo desea puede ponerse en contacto con nosotros en: (+34) 639 76 43 07</i></p>"+
				"</div>"+
			"</div>";
	
	
	public static final String HTML_REGISTER_LOCAL_KO = HTML_REGISTER_LOCAL_OK;

}

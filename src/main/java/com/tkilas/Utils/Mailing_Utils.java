package com.tkilas.Utils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;


public class Mailing_Utils {
	
	private final static String MAIL_COMERCIAL = "Comercial"; 
	private final static String MAIL_SUGERENCIA = "Sugerencia"; 
	private final static String MAIL_OTROS = "Otros";
	private final static String MAIL_SUBJECT_FORMAT = "---";
	
	private static final Logger logger = LoggerFactory.getLogger(Mailing_Utils.class);

	
	public final static String WELCOME_MAIL_ADRESS = "welcome@tkilas.com";
	public final static String WELCOME_MAIL_SUBJECT = "¡Bienvenido a tkilas!";
	
	public final static String INFO_MAIL_ADRESS = "info@tkilas.com";
	public final static String INFO_MAIL_SUBJECT = "Información Tkilas";
	public final static String INFO_PASSCHANGE_SUBJECT = "Solicitud cambio de contraseña";
	
	public SimpleMailMessage send_simple_mail(String from, String to, String subject, String body){
		
		logger.debug("Sending simple mail by: send_simple_mail");
		logger.debug("FROM: "+from);
		logger.debug("TO: "+to);
		logger.debug("SUBJECT: "+subject);
		logger.debug("BODY: "+body);

		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(to);
		email.setFrom(from);
		String subjectFormated= new String();
		
		
		if(subject.equals(MAIL_COMERCIAL)){
			subjectFormated = MAIL_SUBJECT_FORMAT+MAIL_COMERCIAL+MAIL_SUBJECT_FORMAT;
		}else{
			if(subject.equals(MAIL_SUGERENCIA)){
				subjectFormated = MAIL_SUBJECT_FORMAT+MAIL_SUGERENCIA+MAIL_SUBJECT_FORMAT;
			}else{
				if(subject.equals(MAIL_OTROS)){
					subjectFormated = MAIL_SUBJECT_FORMAT+MAIL_OTROS+MAIL_SUBJECT_FORMAT;
				}else{
					subjectFormated = subject;
				}
			}
		}
		

		email.setSubject(subjectFormated);
		email.setText(body);		
		return email;
	}
	
	public boolean send_HTMLFormated_mail(JavaMailSender mailSender,String from, String to,String subject, String body) {
		boolean aux = false;
		// Ahora hago testing para enviar correos
		try {
			MimeMessage message = mailSender.createMimeMessage();
			message.setSubject(subject);
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(from);
			helper.setTo(to);
			helper.setText(body, true);
			mailSender.send(message);
			aux = true;
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			logger.debug("ENVIO DE MAIL: " + e);
		}
		return aux;
	}
	
	
	public String constructHTMLFormatedMail(String nameOfUser) {
		StringBuilder body = new StringBuilder();

		body.append("<p style='width: 100%; height: 40px; background-color: #8be029; -webkit-margin-after: 0em;'>&nbsp;</p>");
		body.append("<p style='-webkit-margin-after: 0em;float: left; width: 25%; height: 25%; max-width: 120px; max-height: 120px; float:left;'><a class='clink' style='border: 1px  solid  #8be029; float: left; padding: 10px;' href='http://www.tkilas.com' target='_blank'><img id='sig-logo' style='width: 100%;' src='http://tkilas.com/resources/img/logo_landing.png' alt='tkilas logo' border='0' /></a>");
		body.append("<p style='width: 75%; margin-left: 25%; font-family: Helvetica, Arial, sans-serif; font-size: 20px; line-height: 14px; color: #8cd200;'><span id='name-input' class='txt' style='font-weight: bold; margin-left: 15px;'>¡Bienvenido a tkilas "+nameOfUser+"!<br><br>Publica tus ofertas en bebidas</span></p>");
		body.append("<p style='width: 75%;font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #969696;float:left;padding-left: 3%;text-align: justify;'><span id='name-input' class='txt' style='font-weight: bold;'>¡Llenemos tu establecimiento! Tkilas te da la bienvenida a la primera plataforma donde podrás publicar tus ofertas en bebidas."+
		"<b>Publica tus ofertas en bebidas</b> y podrás aumentar tus ingresos hasta más de un 40 %. Ya has realizado el primer paso para pertenecer a Tkilas. "+
				"Para finalizar tu registro dirígete a este <a id='name-input' class='txt' style='font-weight: bold;' href='http://migestor.tkilas.com' target='_blank'>link</a> donde podrás acceder al perfil de tu establecimiento. Ahí crearás tu perfil, subirás tus ofertas en bebidas, consultarás las reservas que se realizan en tu establecimiento y lo más importante: llenarás tu establecimiento en 24 horas.</p>");
		body.append("<p style='width: 75%;font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #969696;float:left;padding-left: 3%;text-align: justify;'><span id='name-input' class='txt' style='font-weight: bold;'>A continuación te pedimos que completes los datos que nos quedan para publicar tu establecimiento en Tkilas y podrás comenzar a subir tus ofertas en bebidas.</p>");
		body.append("<p style='width: 75%;font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #969696;float:left;padding-left: 3%;text-align: justify;'><span id='name-input' class='txt' style='font-weight: bold;'>Tkilas ¡tu bebida con descuento!. Las mejores ofertas en bebidas de tu ciudad solo en Tkilas.<br><br>Tkilas te da las gracias por confiar en su equipo.</p>");
		body.append("<p style='width: 75%;font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #969696;float:left;padding-left: 3%;text-align: justify;'><span id='name-input' class='txt' style='font-style: italic;'>Si tienes dudas puedes ponerte en contacto con nuestro equipo en:  Correo: <a id='name-input' class='txt' style='font-weight: bold;' href='mailto:info@tkilas.com' target='_blank'>info@tkilas.com</a> o por teléfono: (+34)639 76 43 07.Gracias por tu confianza.</span></p>");
		body.append("<p style='width: 100%; height: 40px; background-color: #8be029; -webkit-margin-after: 0em; float:left;'>&nbsp;</p>");

		return body.toString();
	}
	
	public String constructHTMLFormatedMailForRejection (String nameOfUser, String dia, String hour, String localName){
		StringBuilder body = new StringBuilder();

		
		body.append("<p style='width: 100%; height: 40px; background-color: #8be029; -webkit-margin-after: 0em;'>&nbsp;</p>");
		body.append("<p style='-webkit-margin-after: 0em; width: 25%; height: 25%; max-width: 120px; max-height: 120px;margin: 1% auto;'><a class='clink' style='border: 1px  solid  #8be029; float: left; padding: 10px;' href='http://www.tkilas.com' target='_blank'><img id='sig-logo' style='width: 100%;' src='http://tkilas.com/resources/img/logo_landing.png' alt='tkilas logo' border='0' /></a></p>");
		body.append("<p style='width: 75%;font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #969696;float:left;padding-left: 3%;text-align: justify;'><span id='name-input' class='txt' style='font-weight: bold;'>"+nameOfUser+", <br><br>");
		body.append("Su reserva con fecha "+dia+" a las "+hour+" en "+localName+" ha sido cancelada.<br><br>");
		body.append("Sentimos no poder atender su oferta y le animamos a que lo intente en otro momento.");
		body.append("<br><br>Reciba un cordial saludo,</p>");
		body.append("<p style='width: 75%;font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #969696;float:left;padding-left: 3%;text-align: justify;'><span id='name-input' class='txt' style='font-style: italic;'>Equipo Tkilas</span></p>");
		body.append("<p style='width: 100%; height: 40px; background-color: #8be029; -webkit-margin-after: 0em; float:left;'>&nbsp;</p>");

		
		return body.toString();
	}
	
	public String constructHTMLFormatedMailForRecoveryPass (String fullUserName, String fingerprint){
		StringBuilder body = new StringBuilder();

		
		body.append("<p style='width: 100%; height: 40px; background-color: #8be029; -webkit-margin-after: 0em;'>&nbsp;</p>");
		body.append("<p style='-webkit-margin-after: 0em; width: 25%; height: 25%; max-width: 120px; max-height: 120px;margin: 1% auto;'><a class='clink' style='border: 1px  solid  #8be029; float: left; padding: 10px;' href='http://www.tkilas.com' target='_blank'><img id='sig-logo' style='width: 100%;' src='http://tkilas.com/resources/img/logo_landing.png' alt='tkilas logo' border='0' /></a></p>");
		body.append("<p style='width: 75%;font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #969696;float:left;padding-left: 3%;text-align: justify;'><span id='name-input' class='txt' style='font-weight: bold;'>"+fullUserName+", <br><br>");
		body.append("Se ha solicitado un cambio de contraseña para la cuenta asociada a este correo.<br><br>");
		body.append("Para finalizar el proceso debe continuar <a target='_blank' href='https://www.tkilas.com/reset_password?status=0&fingerprint="+fingerprint+"'>aquí</a>.<br><br>");
		body.append("Si usted ha recibido este mail sin solicitarlo bórrelo y póngase en contacto con nosotros.");
		body.append("<br><br>Reciba un cordial saludo,</p>");
		body.append("<p style='width: 75%;font-family: Helvetica, Arial, sans-serif; font-size: 15px; color: #969696;float:left;padding-left: 3%;text-align: justify;'><span id='name-input' class='txt' style='font-style: italic;'>Equipo Tkilas</span></p>");
		body.append("<p style='width: 100%; height: 40px; background-color: #8be029; -webkit-margin-after: 0em; float:left;'>&nbsp;</p>");

		
		return body.toString();
	}

}

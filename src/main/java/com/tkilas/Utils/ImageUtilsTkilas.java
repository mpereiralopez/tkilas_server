package com.tkilas.Utils;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

public class ImageUtilsTkilas {

	
	
	 /**
		 * Resizes an image using a Graphics2D object backed by a BufferedImage.
		 * @param srcImg - source image to scale
		 * @param w - desired width
		 * @param h - desired height
		 * @return - the new resized image
		 */
		public static BufferedImage getScaledImage(BufferedImage srcImg, int w, int h){
		    BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TRANSLUCENT);
		    Graphics2D g2 = resizedImg.createGraphics();
		    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		    g2.drawImage(srcImg, 0, 0, w, h, null);
		    g2.dispose();
		    return resizedImg;
		}
}

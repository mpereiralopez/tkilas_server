package com.tkilas.Utils;

import java.beans.PropertyEditorSupport;
import java.text.DateFormat;
import java.text.ParseException;

public class CustomSqlDateEditor extends PropertyEditorSupport {

	DateFormat format = null;

	public CustomSqlDateEditor(DateFormat format) {
		super();
		this.format = format;
	}

	public String getAsText() {
		java.sql.Date value = (java.sql.Date) getValue();
		return (value != null ? this.format.format(new java.util.Date(value.getTime())) : "");
	}

	public void setAsText(String text) throws IllegalArgumentException {
		try {
			setValue(new java.sql.Date(this.format.parse(text).getTime()));
		} catch (ParseException e) {
			throw new IllegalArgumentException("Could not parse date: "+ e.getMessage());
		}
	}

}

package com.tkilas.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/about")
public class QuienesSomosController {
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView quienesSomosContentPage() {
 
		ModelAndView model = new ModelAndView();
		model.setViewName("about");
		return model;
 
	}

}

package com.tkilas.controller;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;




import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.mail.javamail.JavaMailSender;

import com.tkilas.Utils.Mailing_Utils;
import com.tkilas.Utils.StringTkilasUtils;
import com.tkilas.model.Local;
import com.tkilas.model.Role;
import com.tkilas.model.User;
import com.tkilas.model.forms.LocalForm;
import com.tkilas.service.LocalServices;
import com.tkilas.service.UserService;

@Controller
public class LocalRegisterController {
	
	private static final Logger logger = LoggerFactory.getLogger(LocalRegisterController.class);
	
	

	@Autowired
	private LocalServices localServices;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private JavaMailSender mailSender;
	
	@RequestMapping(value ="/register" , method = RequestMethod.GET)
	public String registerPage(Model model) {
        model.addAttribute("localForm", new LocalForm());
		return "register";
 
	}
	
	@RequestMapping(value ="/register/check_email_free" , method = RequestMethod.POST)
	public @ResponseBody String checkMailFree(@RequestParam(value="email",required = true) String email) {
		
		String auxStr = new String();
		boolean aux = userService.isEmailFree(email);
		if(aux){
			auxStr = "true";
		}else{
			auxStr= "false";
		}
		
		
		return  auxStr;
		
 
	}
	
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register_new_local( @ModelAttribute LocalForm localForm, Model model){
		
		User user = new User();
		user.setEmail(localForm.getEmail());
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(localForm.getUser_pass());
		
		user.setPassword(hashedPassword);
		/*try {
			user.setPassword( new String (AESUtils.encrypt(localForm.getUser_pass(), localForm.getUser_pass())));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		Role role = new Role();
		role.setIdRole(2);
		role.setName("ROLE_LOCAL");
		user.setRole(role);
		user.setUser_name(localForm.getUser_name());
		user.setUser_surname(localForm.getUser_surname());
		userService.addUser(user);
		
		/* AQUI CREO EL LOCAL */
		Local local = new Local();
		
		local.setUser(user);
		local.setUserIdUser(user.getIdUser());
		local.setLocal_name(localForm.getLocal_name());
		local.setTlf(localForm.getLocal_phone());
		local.setAddress(localForm.getLocal_address());
		local.setLocal_cp(localForm.getLocal_cp());
		local.setCity(localForm.getLocal_city());
		local.setStatus(0);
		/**FIX PARA SOLUCIONAR NULL POINTER **/
		local.setCapacity(0);
		local.setComercial("NONE");
		local.setDistrict(-1);
		BigDecimal costDefault = new BigDecimal(0);
		Float defaultCoordenates = new Float(0.0);
		local.setLocal_cost_alch_bottle(costDefault);
		local.setLocal_cost_beer(costDefault);
		local.setLocal_cost_cocktail(costDefault);
		local.setLocal_cost_coffe(costDefault);
		local.setLocal_cost_tonic(costDefault);
		local.setLocal_cost_wine_bottle(costDefault);
		local.setLocal_desc("");
		local.setLocal_latitud(defaultCoordenates);
		local.setLocal_longitud(defaultCoordenates);
		local.setLocal_type(-1);
		local.setLocal_pay_way(0);
		local.setProduct_type_local(-1);
		local.setWeb("");
		/************************************/
		localServices.addLocal(local);
		
		Mailing_Utils mail_utils = new Mailing_Utils();
		String body = mail_utils.constructHTMLFormatedMail(localForm.getUser_name());
		boolean returnValue = mail_utils.send_HTMLFormated_mail(mailSender, Mailing_Utils.WELCOME_MAIL_ADRESS, localForm.getEmail(), Mailing_Utils.WELCOME_MAIL_SUBJECT, body);
		
		if(returnValue){
			model.addAttribute("returnValue", StringTkilasUtils.HTML_REGISTER_LOCAL_OK);

		}else{
			model.addAttribute("returnValue", StringTkilasUtils.HTML_REGISTER_LOCAL_KO);
		}
		
		
		 Runnable r = new MyMailThreading(mail_utils, user, local);
		 new Thread(r).start();
		
		
		
		return "localRegisterResult";
	}
	
	
	@RequestMapping(value = "/send_comment", method = RequestMethod.POST)
	@ResponseBody
	public void sendComment(@RequestParam("body") String body, @RequestParam("to") String to, @RequestParam("from") String from,@RequestParam("subject") String subject) {
		mailSender.send(new Mailing_Utils().send_simple_mail(from, to, subject, body));  
	}
	

	private class MyMailThreading implements Runnable{
		
		@SuppressWarnings("serial")
		private List<String> listaCorreosInternos = new ArrayList<String>() {{ add("comunity@tkilas.com"); }};
		 private String subject = "Nuevo local registrado";
		 private StringBuilder body = new StringBuilder();
		 private Mailing_Utils mail_utils;
		 private User user;
		 private Local business;
		 
		public MyMailThreading(Mailing_Utils mail_utils, User user, Local local) {
			// TODO Auto-generated constructor stub
			this.mail_utils = mail_utils;
			this.user = user;
			this.business = local;
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			 body.append("Un nuevo local se ha dado de alta en el sistema con los siguientes datos: \n");
	    	 body.append("Nombre de contacto: "+user.getUser_name()+" "+user.getUser_surname()+". \n");
	    	 body.append("Email de contacto: "+user.getEmail()+". \n");
	    	 Date date = new Date(user.getCreateTime().getTime());
	         SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy' 'HH:mm:ss:S");
	    	 body.append("Fecha de alta: "+simpleDateFormat.format(date)+". \n");
	    	 body.append("Nombre del establecimiento: "+business.getLocal_name()+". \n");
	    	 body.append("Dirección del establecimiento: "+business.getAddress()+". \n");
	    	 body.append("Ciudad: "+business.getCity()+". \n");
	    	 body.append("Código postal: "+business.getLocal_cp()+". \n");

	        for(int i =0; i<listaCorreosInternos.size();i++){
				mail_utils.send_HTMLFormated_mail(mailSender, "info@tkilas.com", listaCorreosInternos.get(i), subject, body.toString());

	        }
		}
		
		
	}
	
}




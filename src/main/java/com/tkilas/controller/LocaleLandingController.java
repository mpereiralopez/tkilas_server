package com.tkilas.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tkilas.Utils.CustomSqlDateEditor;
import com.tkilas.Utils.EnumUtils;
import com.tkilas.Utils.EnumUtils.ProductTypeLocal;
import com.tkilas.Utils.ImageUtilsTkilas;
import com.tkilas.Utils.Mailing_Utils;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientHasDiscountPK;
import com.tkilas.model.ClientHasPromo;
import com.tkilas.model.Comment;
import com.tkilas.model.Discount;
import com.tkilas.model.Local;
import com.tkilas.model.LocalPayInfo;
import com.tkilas.model.Pack;
import com.tkilas.model.PackPK;
import com.tkilas.model.PacksCanceled;
import com.tkilas.model.Promo;
import com.tkilas.model.PromoPK;
import com.tkilas.model.User;
import com.tkilas.model.forms.LocalFormForUpdate;
import com.tkilas.model.forms.LocalFormUpdateIBAN;
import com.tkilas.model.forms.LocalFormUpdateLocalInfo;
import com.tkilas.model.forms.LocalFormUpdateTermsAndPayment;
import com.tkilas.service.ClientHasDiscountService;
import com.tkilas.service.ClientHasPromoService;
import com.tkilas.service.DiscountService;
import com.tkilas.service.LocalServices;
import com.tkilas.service.PackService;
import com.tkilas.service.PromoService;
import com.tkilas.service.SurveyService;
import com.tkilas.service.UserService;

@Controller
@RequestMapping(value = "/gestor")
public class LocaleLandingController implements HandlerExceptionResolver {

	private static final Logger logger = LoggerFactory.getLogger(LocaleLandingController.class);

	// 80%
	private static final double PERCENTAGE_MAX_LOCAL_CAPACITY = 0.80;

	@Autowired
	private LocalServices localServices;

	@Autowired
	private UserService userService;

	@Autowired
	private PackService packService;

	@Autowired
	private ClientHasDiscountService cHdService;

	@Autowired
	private DiscountService discountService;

	@Autowired
	private PromoService promoService;

	@Autowired
	private ClientHasPromoService clientHasPromoService;

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private SurveyService surveyService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView adminPage(HttpServletRequest request,
			@RequestParam(value = "section", required = false) String section,
			@RequestParam(value = "subsection", required = false) String subsection,
			RedirectAttributes redirectAttributes) {

		Local local = null;

		String email = request.getUserPrincipal().getName();
		//For impersonate from redirection
		User u = userService.getUser(email);
		if(u.getRole().getIdRole() == 1){
			ModelAndView mav2 = new ModelAndView();
			mav2.setViewName("redirect:/admin?section=viewall");
			return mav2;
		}
		
		
		
		local = localServices.getLocalByUserName(email);

		ModelAndView model = new ModelAndView();
		model.addObject("local_info", local);
		// Compruebo el estado de los terminos y condiciones
		if (local.getTerms_accpeted() == 1) {
			if (section == null || section.length() == 0) {
				// section = "dashboard";
				ModelAndView mav2 = new ModelAndView();
				mav2.setViewName("redirect:/gestor?section=dashboard");
				return mav2;
			}

			if (section.equals("dashboard")) {
				if (local.getStatus() == 0) {

					model.addObject("template", 1);
				} else {
					Date today = new Date(System.currentTimeMillis());
					SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
					List<Pack> packsActived = packService.getPackListOfLocal(local.getUserIdUser(), dt1.format(today));

					model.addObject("local_offers", packsActived);

					/* Metemos cosas en sesión para evitar problemas con enums */

					Double localForum = local.getCapacity() * PERCENTAGE_MAX_LOCAL_CAPACITY;

					request.getSession().setAttribute("local_capacity", localForum.intValue());
					request.getSession().setAttribute("localTypeEnum", EnumUtils.LocalTypeEnum.values());
					request.getSession().setAttribute("productTypeLocal", EnumUtils.ProductTypeLocal.values());
					request.getSession().setAttribute("citiesEnum", EnumUtils.CitiesEnum.values());
					request.getSession().setAttribute("productType", EnumUtils.ProductType.values());
					request.getSession().setAttribute("productSubtype", EnumUtils.ProductSubtype.values());
					request.getSession().setAttribute("productSubtypeCopa", EnumUtils.ProductSubtypeCopa.values());
					request.getSession().setAttribute("statusClientReservation",
							EnumUtils.StatusClientReservation.values());
					request.getSession().setAttribute("localId", local.getUserIdUser());

					List<ClientHasDiscount> listaClientesConDescuento = cHdService.getClientsOfDiscountByLocalId(
							local.getUserIdUser(), dt1.format(today));
					List<ClientHasPromo> listaClientesConPromo = clientHasPromoService
							.getClientsOfPromoByLocalIdAndDate(local.getUserIdUser(), dt1.format(today));
					List<PacksCanceled> listaCancelados = packService.getListOfCanceledClients(local.getUserIdUser(),
							dt1.format(today));
					if (listaCancelados.size() > 0)
						model.addObject("client_canceled", listaCancelados);
					model.addObject("listaClientesConDescuento", listaClientesConDescuento);
					model.addObject("listaClientesConPromo", listaClientesConPromo);
					int totalReservedForDay = 0;
					for (int i = 0; i < listaClientesConDescuento.size(); i++)
						totalReservedForDay = totalReservedForDay + listaClientesConDescuento.get(i).getSize();
					for (int i = 0; i < listaClientesConPromo.size(); i++)
						totalReservedForDay = totalReservedForDay + listaClientesConPromo.get(i).getSize();
					for (int i = 0; i < listaCancelados.size(); i++)
						totalReservedForDay = totalReservedForDay + listaCancelados.get(i).getSize();

					model.addObject("totalReservedForDay", totalReservedForDay);

					model.addObject("template", 0);
					Pack pack = packService.getPackOfLocalDay(local.getUserIdUser(), dt1.format(today));
					if (pack != null)
						
						model.addObject("pack", pack);

				}
			}

			if (section.equals("profile")) {
				model.addObject("template", 2);
				if (subsection == null) {
					subsection = new String("personal");
				}
				if (subsection.equals("personal")) {
					model.addObject("subTemplate", 0);
					request.getSession().setAttribute("citiesEnum", EnumUtils.CitiesEnum.values());

					model.addObject("localFormUpdate", new LocalFormForUpdate());
				}

				if (subsection.equals("business")) {
					model.addObject("subTemplate", 1);
					request.getSession().setAttribute("localTypeEnum", EnumUtils.LocalTypeEnum.values());
					model.addObject("localFormUpdateLocalInfo", new LocalFormUpdateLocalInfo());

				}
			}
			if (local.getStatus() != 0 && section.equals("comments")) {
				model.addObject("template", 3);
				Date today = new Date(System.currentTimeMillis());
				SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
				List<Comment> listaComentarios = surveyService.getCommentsOfLocalAndDate(local.getUserIdUser(),
						dt1.format(today), 0);
				model.addObject("listaComentarios", listaComentarios);

			}
			model.setViewName("business/gestor");
		} else {
			// No estan aceptadas acepto
			model.addObject("isAccepted", true);
			model.addObject("template", 2);
			request.getSession().setAttribute("citiesEnum", EnumUtils.CitiesEnum.values());
			request.getSession().setAttribute("localTypeEnum", EnumUtils.LocalTypeEnum.values());

			model.addObject("local_info", local);
			if (request.getSession().getAttribute("step") == null) {
				request.getSession().setAttribute("step", 1);
			}

			switch ((int) request.getSession().getAttribute("step")) {
			case 1:
				model.addObject("subTemplate", 0);
				model.addObject("localFormUpdate", new LocalFormForUpdate());
				model.setViewName("business/gestor");
				break;

			case 2:
				model.addObject("subTemplate", 1);
				model.addObject("localFormUpdateLocalInfo", new LocalFormUpdateLocalInfo());
				model.setViewName("business/gestor");
				break;

			case 3:
				model.addObject("subTemplate", 2);
				LocalPayInfo localPayInfo = localServices.getLocalPayInfoByUserId(local.getUserIdUser());
				model.addObject("localPayInfo", localPayInfo);
				model.addObject("localFormUpdateTermsAndPayment", new LocalFormUpdateTermsAndPayment());
				model.setViewName("business/gestor");
				break;

			case 4:
				model.addObject("subTemplate", 3);
				model.addObject("localFormUpdateIBAN", new LocalFormUpdateIBAN());
				model.setViewName("business/gestor");
				break;

			default:
				break;
			}

		}
		return model;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/update_contact_info")
	public ModelAndView update_personal_info(HttpServletRequest request,
			@ModelAttribute LocalFormForUpdate localFormUpdate, RedirectAttributes redirectAttributes) {
		System.out.println("update_personal_info de local con id " + localFormUpdate.getUser_id());
		Local local = localServices.getLocalById(localFormUpdate.getUser_id());
		User user = local.getUser();
		// user.setEmail(localFormUpdate.getEmail_in_profile());
		user.setUser_name(localFormUpdate.getUser_name());
		user.setUser_surname(localFormUpdate.getUser_surname());
		userService.editUser(user);

		/* AQUI CREO EL LOCAL */
		local.setUser(user);
		local.setUserIdUser(user.getIdUser());
		local.setLocal_name(localFormUpdate.getLocal_name());
		local.setTlf(localFormUpdate.getLocal_phone());
		local.setAddress(localFormUpdate.getLocal_address());
		local.setLocal_cp(localFormUpdate.getLocal_cp());
		local.setCity(localFormUpdate.getLocal_city());
		local.setWeb(localFormUpdate.getLocal_web());
		if(local.getStatus() == 1){
			local.setStatus(2);
		}else{
			local.setStatus(0);
		}
		localServices.editLocal(local);
		ModelAndView mav = new ModelAndView();

		if (request.getSession().getAttribute("step") != null) {
			request.getSession().setAttribute("step", 2);
			mav.setViewName("redirect:/gestor");

		} else {
			redirectAttributes.addFlashAttribute("MSG_OK", "Datos actualizados correctamente");
			mav.setViewName("redirect:/gestor?section=profile&subsection=personal");
		}
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/update_local_info")
	public ModelAndView update_local_info(@ModelAttribute LocalFormUpdateLocalInfo localFormUpdateInfo, Model model,
			HttpServletRequest request, RedirectAttributes redirectAttributes) {
		logger.info("########## update_personal_info de local con LocalFormUpdateLocalInfo "
				+ localFormUpdateInfo.getLocal_id());
		Local local = localServices.getLocalById(localFormUpdateInfo.getLocal_id());
		local.setLocal_type(localFormUpdateInfo.getLocal_type());
		local.setLocal_desc(localFormUpdateInfo.getLocal_desc());
		local.setLocal_pay_way(localFormUpdateInfo.getLocal_pay_way());
		local.setCapacity(localFormUpdateInfo.getCapacity());
		local.setLocal_cost_beer(localFormUpdateInfo.getLocal_cost_beer());
		local.setLocal_cost_alch_bottle(localFormUpdateInfo.getLocal_cost_alch_bottle());
		local.setLocal_cost_cocktail(localFormUpdateInfo.getLocal_cost_cocktail());
		local.setLocal_cost_coffe(localFormUpdateInfo.getLocal_cost_coffe());
		local.setLocal_cost_tonic(localFormUpdateInfo.getLocal_cost_tonic());
		local.setLocal_cost_wine_bottle(localFormUpdateInfo.getLocal_cost_wine_bottle());
		localServices.editLocal(local);
		ModelAndView mav = new ModelAndView();

		if (request.getSession().getAttribute("step") != null) {
			request.getSession().setAttribute("step", 3);
			mav.setViewName("redirect:/gestor");

		} else {
			redirectAttributes.addFlashAttribute("MSG_OK", "Datos actualizados correctamente");
			mav.setViewName("redirect:/gestor?section=profile&subsection=business");
		}
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/update_local_terms_pay")
	public ModelAndView update_local_terms_pay(
			@ModelAttribute LocalFormUpdateTermsAndPayment localFormUpdateTermsAndPayment, Model model,
			HttpServletRequest request) {
		LocalPayInfo localPayInfo = localServices.getLocalPayInfoByUserId(localFormUpdateTermsAndPayment.getId_local());
		localPayInfo.setContact_address(localFormUpdateTermsAndPayment.getContact_address());
		localPayInfo.setContact_city(localFormUpdateTermsAndPayment.getContact_city());
		localPayInfo.setContact_cp(localFormUpdateTermsAndPayment.getContact_cp());
		localPayInfo.setCif(localFormUpdateTermsAndPayment.getCif());
		request.getSession().setAttribute("step", 4);
		return new ModelAndView("redirect:/gestor");
	}

	@RequestMapping(value = "/update_iban", method = RequestMethod.POST)
	public ModelAndView update_local_IBAN(@ModelAttribute LocalFormUpdateIBAN localFormUpdateIBAN,
			HttpServletRequest request) {
		LocalPayInfo localPayInfo = localServices.getLocalPayInfoByUserId(localFormUpdateIBAN.getId_local());
		Local local = localServices.getLocalById(localFormUpdateIBAN.getId_local());
		local.setTerms_accpeted(1);
		localServices.editLocal(local);

		if (localFormUpdateIBAN.getLocal_iban_1().trim().length() == 4
				&& localFormUpdateIBAN.getLocal_iban_2().trim().length() == 4
				&& localFormUpdateIBAN.getLocal_iban_3().trim().length() == 4
				&& localFormUpdateIBAN.getLocal_iban_4().trim().length() == 2
				&& localFormUpdateIBAN.getLocal_iban_5().trim().length() == 10) {

			String iban1 = localFormUpdateIBAN.getLocal_iban_1();
			String iban2 = localFormUpdateIBAN.getLocal_iban_2();
			String iban3 = localFormUpdateIBAN.getLocal_iban_3();
			String iban4 = localFormUpdateIBAN.getLocal_iban_4();
			String iban5 = localFormUpdateIBAN.getLocal_iban_5();

			StringBuilder completeIban = new StringBuilder();
			completeIban.append(iban1);
			completeIban.append(iban2);
			completeIban.append(iban3);
			completeIban.append(iban4);
			completeIban.append(iban5);

			localPayInfo.setContact_IBAN(completeIban.toString());
			localServices.editLocalPayInfo(localPayInfo);

		}

		request.getSession().removeAttribute("step");
		ModelAndView mav = new ModelAndView();
		mav.setViewName("redirect:/gestor");
		return mav;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/update_local_pic")
	public ModelAndView uploadFileHandler(@RequestParam("file") MultipartFile file, @RequestParam("x") int x,
			@RequestParam("y") int y, @RequestParam("w") int w, @RequestParam("h") int h,
			@RequestParam("pic_id") int pos, @RequestParam("user_id") int localId, HttpServletRequest request,
			@RequestParam(required = false, defaultValue = "") String save,
			@RequestParam(required = false, defaultValue = "") String delete, RedirectAttributes redirectAttributes,
			Model model) throws SizeLimitExceededException {

		String msg = new String();
		String url = new String();
		if (save.length() > 0) {
			// user clicked "save"
			BufferedImage originalImage;
			BufferedImage newImage = null;
			try {
				originalImage = ImageIO.read(file.getInputStream());
				originalImage = ImageUtilsTkilas.getScaledImage(originalImage, 500, 282);
				newImage = originalImage.getSubimage(x, y, w, h);
			} catch (IllegalStateException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				redirectAttributes.addFlashAttribute("MSG_ERR", "Error al guardar la imagen");
			}
			url = localServices.updateLocalPic(newImage, pos, localId, request);
			Date d = new Date();

			url += "?timestamp=" + d.getTime();
			msg = "Imagen subida correctamente";

		} else if (delete.length() > 0) {
			// user clicked "delete"
			localServices.updateLocalPic(null, pos, localId, request);
			url = null;
			msg = "Imagen eliminada correctamente";

		} else {
			throw new IllegalArgumentException("Need either approve or deny!");
		}

		switch (pos) {
		case 1:
			redirectAttributes.addFlashAttribute("MSG_PIC_UPLOAD_OK_1", msg);
			break;
		case 2:
			redirectAttributes.addFlashAttribute("MSG_PIC_UPLOAD_OK_2", msg);
			break;
		case 3:
			redirectAttributes.addFlashAttribute("MSG_PIC_UPLOAD_OK_3", msg);
			break;
		default:
			break;
		}

		redirectAttributes.addFlashAttribute("PIC_URL", url);

		return new ModelAndView("redirect:/gestor?section=profile&subsection=business");

	}

	@RequestMapping(value = "/send_comment", method = RequestMethod.POST)
	public void sendComment(@RequestParam("body") String body, @RequestParam("to") String to,
			@RequestParam("from") String from, @RequestParam("subject") String subject) {
		System.out.println("AQUI MANDO EL MAIL");
		mailSender.send(new Mailing_Utils().send_simple_mail(from, to, subject, body));
	}

	@ExceptionHandler(IOException.class)
	public String handleIOException(IOException ex, HttpServletRequest request) {
		return ClassUtils.getShortName(ex.getClass());
	}

	// update_client_reservation_status
	@RequestMapping(value = "/update_client_reservation_status", method = RequestMethod.POST)
	public ModelAndView update_client_reservation_status(@RequestParam("localId") int localId,
			@RequestParam("clientId") int clientId, @RequestParam("date") String date, @RequestParam("type") int type,
			@RequestParam("newStatus") int status, HttpServletRequest request) {

		if (date.length() == 0 || date == null) {
			Date today = new Date(System.currentTimeMillis());
			SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
			date = dt1.format(today);
		}

		int sizeAffected = 0;
		ClientHasDiscountPK pk = null;
		if (type == 0) {
			ClientHasDiscount chd = cHdService.getDiscountByClientAndLocalAndDate(clientId, localId, date);
			sizeAffected = chd.getSize();
			pk = chd.getId();
		} else {

		}

		try {
			localServices.updateClientReservationStatus(status, pk, sizeAffected, type);

			if (status == 2) {
				// Mando un mail al cliente diciendo que le han denegado la
				// oferta
				// WAITING FOR JORGE

				List<Map<String, Object>> aliasToValueMapList = localServices.getClientDataForMailing(clientId, date,
						localId, type);

				// System.out.println(aliasToValueMapList);
				// [{user_name=Miguel, hour=07:30:00, user_surname=Pereira
				// Lopez, local_name=BLA BLA}]
				Mailing_Utils mail_utils = new Mailing_Utils();
				String clientFullName = aliasToValueMapList.get(0).get("user_name") + " "
						+ aliasToValueMapList.get(0).get("user_surname");
				String body = mail_utils.constructHTMLFormatedMailForRejection(clientFullName, date,
						aliasToValueMapList.get(0).get("hour").toString(), aliasToValueMapList.get(0).get("local_name")
								.toString());
				mail_utils.send_HTMLFormated_mail(mailSender, Mailing_Utils.INFO_MAIL_ADRESS, aliasToValueMapList
						.get(0).get("email").toString(), Mailing_Utils.INFO_MAIL_SUBJECT, body);
			}

		} catch (Exception e) {
			logger.error(e.toString());
		}

		return update_reserves_list(localId, date);

	}

	@RequestMapping(value = "/delete_pack", method = RequestMethod.POST)
	public @ResponseBody String delete_pack(@RequestParam("localId") int id_local, @RequestParam("date") String date,
			HttpServletRequest request) {

		if (date.length() == 0 || date == null) {
			Date today = new Date(System.currentTimeMillis());
			SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
			date = dt1.format(today);
		}

		packService.deletePack(id_local, date);
		return date;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/upload_pack_discount_and_product")
	public ModelAndView uploadPackDiscountAndProduct(@RequestParam("localId") int localId,
			@RequestParam("dates") String[] dates, @RequestParam("time_ini") String time_ini,
			@RequestParam("time_fin") String time_fin, @RequestParam("offer_size") int offer_size,
			@RequestParam("disscount") int disscount, @RequestParam("product_type") int product_type,
			@RequestParam("product_subtype") int product_subtype, @RequestParam("product_number") int product_number,
			@RequestParam("product_pvp") float product_pvp, @RequestParam("product_max") int product_max,
			@RequestParam("product_type2") int product_type2, @RequestParam("product_subtype2") int product_subtype2,
			@RequestParam("product_number2") int product_number2, @RequestParam("product_pvp2") float product_pvp2,
			@RequestParam("product_max2") int product_max2, HttpServletRequest request,
			RedirectAttributes redirectAttributes) {
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatoDelTextoHora = new SimpleDateFormat("hh:mm:ss");

		for (int i = 0; i < dates.length; i++) {
			Date fecha = null, time_ini_formated = null, time_fin_formated = null;
			try {
				fecha = formatoDelTexto.parse(dates[i]);
				time_ini_formated = formatoDelTextoHora.parse(time_ini);
				time_fin_formated = formatoDelTextoHora.parse(time_fin);

			} catch (ParseException ex) {
				ex.printStackTrace();
			}

			Pack pack = new Pack();
			PackPK pk = new PackPK();
			pk.setLocalUserIdUser(localId);
			pk.setDate(fecha);
			pack.setId(pk);
			pack.setSize(offer_size);
			pack.setTimeIni(time_ini_formated);
			pack.setTimeFin(time_fin_formated);

			if (product_type != -1 || product_type2 != -1) {

				List<Promo> promoList = new LinkedList<Promo>();
				/**** METO EL PRODUCTO 1 ***********/
				if (product_type != -1) {
					PromoPK promoPk1 = new PromoPK();
					promoPk1.setDate(pk.getDate());
					promoPk1.setLocalUserIdUser(localId);
					promoPk1.setPromo_index(0);
					Promo promo = new Promo();
					promo.setId(promoPk1);
					promo.setPromoPvp(product_pvp);
					promo.setPromoSize(product_number);
					promo.setPromoType(product_type);
					promo.setPromoSubtype(product_subtype);
					promo.setPromoMax(product_max);
					promoList.add(promo);
				}
				/**** METO EL PRODUCTO 2 ***********/
				if (product_type2 != -1) {
					PromoPK promoPk2 = new PromoPK();
					promoPk2.setDate(pk.getDate());
					promoPk2.setLocalUserIdUser(localId);
					promoPk2.setPromo_index(1);

					Promo promo2 = new Promo();
					promo2.setId(promoPk2);
					promo2.setPromoPvp(product_pvp2);
					promo2.setPromoSize(product_number2);
					promo2.setPromoType(product_type2);
					promo2.setPromoSubtype(product_subtype2);
					promo2.setPromoMax(product_max2);
					promoList.add(promo2);
				}

				pack.setPacks(promoList);
			}

			if (disscount != -1) {
				/***** METO EL DISCOUNT *****/
				Discount dis = new Discount();
				dis.setId(pk);
				dis.setDiscount(disscount);
				pack.setDiscount(dis);
			}

			packService.addPack(pack);
		}

		ModelAndView mav = new ModelAndView();

		Date today = new Date(System.currentTimeMillis());
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
		List<Pack> packsActived = packService.getPackListOfLocal(localId, dt1.format(today));
		mav.addObject("local_offers", packsActived);
		mav.addObject("MSG_OFERTA_OK", "Oferta subida correctamente");
		mav.setViewName("business/dashboard/dashboard");
		return mav;

	}

	@RequestMapping(value = "/get_pack_info_by_date", method = RequestMethod.GET)
	public ModelAndView get_pack_info_by_date(@RequestParam("localId") int localId, @RequestParam("date") String date) {
		Pack pack = packService.getPackOfLocalDay(localId, date);
		ModelAndView mav = new ModelAndView();
		Date today = new Date(System.currentTimeMillis());
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
		List<Pack> packsActived = packService.getPackListOfLocal(localId, dt1.format(today));
		mav.addObject("fromGet", true);
		mav.addObject("local_offers", packsActived);
		mav.addObject("pack", pack);
		List<ClientHasDiscount> listaClientesConDescuento = cHdService.getClientsOfDiscountByLocalId(localId, date);
		List<ClientHasPromo> listaClientesConPromo = clientHasPromoService.getClientsOfPromoByLocalIdAndDate(localId,
				date);
		List<PacksCanceled> listaCancelados = packService.getListOfCanceledClients(localId, date);
		if (listaCancelados.size() > 0)
			mav.addObject("client_canceled", listaCancelados);
		mav.addObject("listaClientesConDescuento", listaClientesConDescuento);
		mav.addObject("listaClientesConPromo", listaClientesConPromo);
		int totalReservedForDay = 0;
		for (int i = 0; i < listaClientesConDescuento.size(); i++)
			totalReservedForDay = totalReservedForDay + listaClientesConDescuento.get(i).getSize();
		for (int i = 0; i < listaClientesConPromo.size(); i++)
			totalReservedForDay = totalReservedForDay + listaClientesConPromo.get(i).getSize();
		for (int i = 0; i < listaCancelados.size(); i++)
			totalReservedForDay = totalReservedForDay + listaCancelados.get(i).getSize();
		mav.addObject("totalReservedForDay", totalReservedForDay);
		mav.setViewName("business/dashboard/dashboard");
		return mav;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm aa");
		dateFormat.setLenient(false);
		hourFormat.setLenient(false);
		// true passed to CustomDateEditor constructor means convert empty
		// String to null
		binder.registerCustomEditor(java.util.Date.class, new CustomDateEditor(hourFormat, true));
		binder.registerCustomEditor(java.sql.Date.class, new CustomSqlDateEditor(dateFormat));

	}

	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
			Exception ex) {
		Map<String, Object> model = new HashMap<String, Object>();
		if (ex instanceof MaxUploadSizeExceededException) {
			model.put("errors", ex.getMessage());
		} else {
			model.put("errors", "Unexpected error: " + ex.getMessage());
		}
		// model.put("uploadedFile", new UploadedFile());
		return new ModelAndView("/gestor", model);
	}

	@RequestMapping(value = "/update_reserves_list", method = RequestMethod.GET)
	public ModelAndView update_reserves_list(@RequestParam("localId") int localId, @RequestParam("date") String date) {
		ModelAndView mav = new ModelAndView();

		if (date.length() == 0 || date == null) {
			Date today = new Date(System.currentTimeMillis());
			SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
			date = dt1.format(today);
		}

		List<ClientHasDiscount> listaClientesConDescuento = cHdService.getClientsOfDiscountByLocalId(localId, date);
		List<ClientHasPromo> listaClientesConPromo = clientHasPromoService.getClientsOfPromoByLocalIdAndDate(localId,
				date);
		List<PacksCanceled> listaCancelados = packService.getListOfCanceledClients(localId, date);
		if (listaCancelados.size() > 0)
			mav.addObject("client_canceled", listaCancelados);
		mav.addObject("listaClientesConDescuento", listaClientesConDescuento);
		mav.addObject("listaClientesConPromo", listaClientesConPromo);
		int totalReservedForDay = 0;
		for (int i = 0; i < listaClientesConDescuento.size(); i++)
			totalReservedForDay = totalReservedForDay + listaClientesConDescuento.get(i).getSize();
		for (int i = 0; i < listaClientesConPromo.size(); i++)
			totalReservedForDay = totalReservedForDay + listaClientesConPromo.get(i).getSize();
		for (int i = 0; i < listaCancelados.size(); i++)
			totalReservedForDay = totalReservedForDay + listaCancelados.get(i).getSize();
		Pack pack = packService.getPackOfLocalDay(localId, date);
		mav.addObject("pack", pack);
		mav.addObject("totalReservedForDay", totalReservedForDay);

		mav.setViewName("business/dashboard/reservationList");
		return mav;
	}

}

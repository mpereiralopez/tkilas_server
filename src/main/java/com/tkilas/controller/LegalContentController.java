package com.tkilas.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;



@Controller
@RequestMapping("/legal")
public class LegalContentController {
	
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView legalContentPage() {
 
		ModelAndView model = new ModelAndView();
		model.setViewName("legal");
		return model;
 
	}

}

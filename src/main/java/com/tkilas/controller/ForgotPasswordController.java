package com.tkilas.controller;


import java.sql.Timestamp;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.tkilas.Utils.AESUtils;
import com.tkilas.Utils.Mailing_Utils;
import com.tkilas.model.RecoveryPassAttemp;
import com.tkilas.model.User;
import com.tkilas.model.forms.ForgotPasswordForm;
import com.tkilas.service.RecoveryPassAttempService;
import com.tkilas.service.UserService;

@Controller
@RequestMapping("/forgot_password")
public class ForgotPasswordController {

	private static final Logger logger = LoggerFactory.getLogger(ForgotPasswordController.class);

	@Autowired
	private UserService userService;
	
	@Autowired
	private RecoveryPassAttempService recoveryPassAttempService;
	
	@Autowired
	private JavaMailSender mailSender;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView forgotPasswordPage(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "success", required = false) String success) {
 
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "true");
		}
		if (success != null) {
			model.addObject("success", "true");
		}
		model.setViewName("forgot_password");
		return model;
 
	}
	
	//Spring Security see this :
		@RequestMapping(value = "/send", method = RequestMethod.POST)
		public String startRecoveryPassword(@ModelAttribute ForgotPasswordForm forgotPasswordForm, Model model) {
	 
			String email = forgotPasswordForm.getEmail_recovery();
			User user = userService.getUser(email);
			
			if(user == null){
				return "redirect:/forgot_password?error=true";
			}else{
				String fingerPrint = AESUtils.SHA512(email);
				RecoveryPassAttemp attempAux = recoveryPassAttempService.getAttemp(fingerPrint);
				if(attempAux!=null){
					return "redirect:/forgot_password?error=true";
				}else{
					RecoveryPassAttemp attemp = new RecoveryPassAttemp();
					attemp.setEmail(user.getEmail());
					attemp.setFingerprint(fingerPrint);
					attemp.setUserIdUser(user.getIdUser());
					
					Date twoDaysAfter = new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 2);
					Timestamp tmstamp = new Timestamp(twoDaysAfter.getTime());
					attemp.setTime_max(tmstamp);
				
					recoveryPassAttempService.addAttemp(attemp);
					
					Mailing_Utils mail_utils = new Mailing_Utils();
					String body = mail_utils.constructHTMLFormatedMailForRecoveryPass(user.getUser_name()+" "+user.getUser_surname(),fingerPrint);
					mail_utils.send_HTMLFormated_mail(mailSender, Mailing_Utils.INFO_MAIL_ADRESS, email, Mailing_Utils.INFO_PASSCHANGE_SUBJECT, body);
					
					return "redirect:/forgot_password?success=true";
				}

			}
		}
}

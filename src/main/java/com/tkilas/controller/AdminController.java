package com.tkilas.controller;


import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.tkilas.Utils.EnumUtils;
import com.tkilas.Utils.ImageUtilsTkilas;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientHasPromo;
import com.tkilas.model.Comment;
import com.tkilas.model.Facturacion;
import com.tkilas.model.Local;
import com.tkilas.model.LocalPayInfo;
import com.tkilas.model.Promo;
import com.tkilas.model.TableInfo;
import com.tkilas.model.User;
import com.tkilas.model.ComercialReportRow;
import com.tkilas.model.forms.CommentForm;
import com.tkilas.model.forms.LocalFormAdmin;
import com.tkilas.service.ClientHasDiscountService;
import com.tkilas.service.ClientHasPromoService;
import com.tkilas.service.ComercialReportService;
import com.tkilas.service.DiscountService;
import com.tkilas.service.LocalServices;
import com.tkilas.service.PackService;
import com.tkilas.service.PromoService;
import com.tkilas.service.SurveyService;
import com.tkilas.service.UserService;




@Controller
@RequestMapping("/admin")
public class AdminController implements HandlerExceptionResolver{
	
	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);
 	 
	@Autowired
	private LocalServices localServices;
	
	@Autowired 
	private UserService userService;
	
	@Autowired
	private PackService packService;
	
	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private ClientHasDiscountService cHdService;

	@Autowired
	private DiscountService discountService;

	@Autowired
	private PromoService promoService;

	@Autowired
	private ClientHasPromoService clientHasPromoService;
	
	@Autowired
	private ComercialReportService comercialReportService;
	
	@Autowired
	private SurveyService surveyService;
	
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView adminPage(HttpServletRequest request,
			@RequestParam(value = "section", required = false) String section,
			 @RequestParam(value = "subsection", required = false) String subsection,
			 RedirectAttributes redirectAttributes) {
		
		ModelAndView model = new ModelAndView();

		
		if(section == null ||section.length() == 0){
			section = "validator";
		}
		
		if(section.equals("validator")){
			model.addObject("template", 0);
			List<Local> localList = localServices.getNoConfirmedLocalList();
			String newString = new String();
			for(int i = 0; i< localList.size();i++){
				newString = localList.get(i).getLocal_desc();
				newString = newString.replaceAll("[\n\r]", " ");
				localList.get(i).setLocal_desc(newString);
			}
			model.addObject("citiesEnum", EnumUtils.CitiesEnum.values());
			model.addObject("districtEnum", EnumUtils.DistrictEnum.values());
			model.addObject("localTypeEnum", EnumUtils.LocalTypeEnum.values());
			model.addObject("productTypeLocal", EnumUtils.ProductTypeLocal.values());
			model.addObject("locals", localList);
			model.addObject("localFormAdmin", new LocalFormAdmin());
		}
		
		if(section.equals("viewall")){
			model.addObject("template", 1);
			List<Local> localList = localServices.getAllLocalList();
			model.addObject("locals", localList);
			model.addObject("districtEnum", EnumUtils.DistrictEnum.values());
			model.addObject("localFormAdmin", new LocalFormAdmin());
		}
		
		if(section.equals("facturacion")){
			model.addObject("template", 2);
			List<Local> localList = localServices.getAllLocalList();
			Date today = new Date(System.currentTimeMillis());
			model.addObject("max-year",today.getYear());
			model.addObject("locals", localList);
		}
		
		if(section.equals("comercial")){
			model.addObject("template", 3);
			List<Local> localList = localServices.getAllLocalList();
			Date today = new Date(System.currentTimeMillis());
			model.addObject("max-year",today.getYear());
			model.addObject("locals", localList);
		}
		
		if(section.equals("comments")){
			model.addObject("template", 4);
			List<Comment> commentList = surveyService.getCommentsByStatus(1);
			model.addObject("commentList", commentList);
			
		}
		
		
		
		model.setViewName("adminTool/admin");
		
	return model;
	
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/validate_local")
	public ModelAndView updateLocalStatus(@ModelAttribute LocalFormAdmin localFormAdmin ,HttpServletRequest request, Map<String, Object> model, Model modelo,
			RedirectAttributes redirectAttributes){
		localServices.updateLocal(localFormAdmin);
		//Aqui pongo las directivas para mandar un mail al destinatario del form para informarle de que ya puede user la parte web.
		//SimpleMailMessage email = new SimpleMailMessage();
		//email.setTo(localForm.getEmail());
		//email.setSubject("Test desde linea de comandos");
		//email.setText("Este es el cuerpo del mail enviado desde linea de comandos");
		//mailSender.send(email);
		
		redirectAttributes.addFlashAttribute("MSG_OK", "Local valdiado correctamente"); 

			return  new ModelAndView( "redirect:/admin?section=validator"); 
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/validate_comment")
	public ModelAndView validateComment(@ModelAttribute CommentForm commentForm,RedirectAttributes redirectAttributes ){
			
		Comment comment  = surveyService.getAttemp(commentForm.getFingerprint());
		comment.setCommentBody(commentForm.getCommentBody());
		comment.setStatus(2);
		surveyService.editComment(comment);
		redirectAttributes.addFlashAttribute("MSG_OK", "Comentario valdiado correctamente");
		
		

		return  new ModelAndView( "redirect:/admin?section=comments"); 
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/update_local")
	public ModelAndView updateLocal(@ModelAttribute LocalFormAdmin localFormAdmin ,HttpServletRequest request, Map<String, Object> model, Model modelo,
			RedirectAttributes redirectAttributes){
		localServices.updateLocal(localFormAdmin);
	
		redirectAttributes.addFlashAttribute("MSG_OK", "Local actualizado correctamente"); 

			return  new ModelAndView(  "redirect:/admin?section=viewall"); 
	}
	
	
    
	@RequestMapping(method = RequestMethod.POST, value = "/update_local_pic")
	public ModelAndView uploadFileHandler (
			@RequestParam("file") MultipartFile file,
			@RequestParam("x") int x,
			@RequestParam("y") int y,
			@RequestParam("w") int w,
			@RequestParam("h") int h,
			@RequestParam("pic_id") int pos,
			@RequestParam("user_id") int localId, HttpServletRequest request,
			@RequestParam(required=false, defaultValue="") String save,
	        @RequestParam(required=false, defaultValue="") String delete,
	        RedirectAttributes redirectAttributes,Model model) throws SizeLimitExceededException, MaxUploadSizeExceededException {
	 		
	 		String msg = new String();
			String url = new String();
	 		  if (save.length()>0) {
	 		        // user clicked "save"
	 			 BufferedImage originalImage;
	 	 		BufferedImage newImage = null;
	 			try {
	 				originalImage = ImageIO.read(file.getInputStream());
	 				originalImage = ImageUtilsTkilas.getScaledImage(originalImage, 500, 282);
	 				newImage = originalImage.getSubimage(x, y, w, h);
	 			} catch (IllegalStateException | IOException e) {
	 				// TODO Auto-generated catch block
	 				e.printStackTrace();
	 				redirectAttributes.addFlashAttribute("MSG_ERR", "Error al guardar la imagen"); 
	 			}
	 			 url=  localServices.updateLocalPic(newImage, pos, localId , request);
		 		  	Date d = new Date();

	 			 url +="?timestamp="+d.getTime();
	 			 msg = "Imagen subida correctamente";

	 		    } else if (delete.length()>0) {
	 		        // user clicked "delete"
	 		    	localServices.updateLocalPic(null, pos, localId , request);
	 		    	url = null;
		 			msg = "Imagen eliminada correctamente";

	 		    } else {
	 		    	throw new IllegalArgumentException("Need either approve or deny!");
	 		    }
	 		  
	 		  	
	 		 switch (pos) {
				case 1:
					redirectAttributes.addFlashAttribute("MSG_PIC_UPLOAD_OK_1", msg); 
					break;
				case 2:
					redirectAttributes.addFlashAttribute("MSG_PIC_UPLOAD_OK_2", msg); 
					break;
				case 3:
					redirectAttributes.addFlashAttribute("MSG_PIC_UPLOAD_OK_3", msg); 
					break;
				default:
					break;
				}
	 		  	
				redirectAttributes.addFlashAttribute("PIC_URL", url); 


	 			return  new ModelAndView( "adminTool/validator/pics_selector"); 

	 		
	    }
	
	@RequestMapping(method = RequestMethod.GET, value = "/get_local_by_id")
	public ModelAndView  getLocalById(HttpServletRequest request,
			@RequestParam(value = "localId", required = true) int localId, 
			@RequestParam(value = "section", required = true) int section, Model model) {
		Local localToUpdate = localServices.getLocalById(localId);
		LocalPayInfo localPayInfo = localServices.getLocalPayInfoByUserId(localId);
		
		model.addAttribute("localToUpdate", localToUpdate);
		model.addAttribute("localPayInfo", localPayInfo);
		
		model.addAttribute("citiesEnum", EnumUtils.CitiesEnum.values());

		model.addAttribute("districtEnum", EnumUtils.DistrictEnum.values());
		model.addAttribute("localTypeEnum", EnumUtils.LocalTypeEnum.values());
		model.addAttribute("productTypeLocal", EnumUtils.ProductTypeLocal.values());
		model.addAttribute("localFormAdmin", new LocalFormAdmin());
		if(section == 0){
			return new ModelAndView("adminTool/validator/validator_form");
		}else{
			return new ModelAndView("adminTool/viewall/popup/local_info");
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/get_comment_by_fingerprint")
	public ModelAndView  get_comment_by_fingerprint(HttpServletRequest request,
			@RequestParam(value = "fingerprint", required = true) String fingerprint, Model model) {
		
		Comment comment = surveyService.getAttemp(fingerprint);
		
		model.addAttribute("comment", comment);
		model.addAttribute("userId", comment.getClient().getUserIdUser());
		model.addAttribute("localId", comment.getLocal().getUserIdUser());
		model.addAttribute("fingerprint", fingerprint);
		
		model.addAttribute("commentForm", new CommentForm());
		
		return new ModelAndView("adminTool/comments/validator_comment_form");
		
	}
	

	@Override
	public ModelAndView resolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		// TODO Auto-generated method stub
		Map<String, Object> model = new HashMap<String, Object>();
		ModelAndView mav = new ModelAndView();
        if (ex instanceof MaxUploadSizeExceededException){
            model.put("errors", ex.getMessage());
            mav.addAllObjects(model);
            mav.setViewName("redirect:/admin");
           
        } else{
            model.put("errors", "Unexpected error: " + ex.getMessage());
        }
        //model.put("uploadedFile", new UploadedFile());
        //return new ModelAndView("/admin", model);
        return mav;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/get_local_pay_info_by_name_month_year")
	public ModelAndView  get_local_pay_info_by_name_month_year(HttpServletRequest request,
			@RequestParam(value = "localId", required = true) int localId, 
			@RequestParam(value = "year", required = true) int year, 
			@RequestParam(value = "month", required = true) int month, 
			Model model) {
		
		Local localInfo = localServices.getLocalById(localId);
		LocalPayInfo localPayInfo = localServices.getLocalPayInfoByUserId(localId);
		
		Facturacion factura = null;
		
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);
		Date date = calendar.getTime();
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
		Date dateFin = null;
		if(month!=11){
			calendar.clear();
			calendar.set(Calendar.MONTH, month+1);
			calendar.set(Calendar.YEAR, year);
			dateFin = calendar.getTime();
			factura = packService.getFacturacionDataByDateAndLocalId(localId, dt1.format(date),dt1.format(dateFin));
		}else{
			calendar.clear();
			calendar.set(Calendar.MONTH, 0);
			calendar.set(Calendar.YEAR, year+1);
			dateFin = calendar.getTime();
			factura = packService.getFacturacionDataByDateAndLocalId(localId, dt1.format(date),dt1.format(dateFin));
		}
		
		model.addAttribute("local_selected", localPayInfo);
		model.addAttribute("local_selected_info", localInfo);
		model.addAttribute("factura", factura);
		
		List<ClientHasDiscount> listaDeReservasDeDescuento = cHdService.getClientsWithDisscountBetweenDates(localId, dt1.format(date), dt1.format(dateFin));
		List<ClientHasPromo> listaReservasPromo = clientHasPromoService.getClientsWithPromoBetweenDates(localId, dt1.format(date), dt1.format(dateFin));
		
		
		
		
		
		List<User> listaClientesPorDescuento = new LinkedList<User>();
		User u= null;
		for(int i=0; i<listaDeReservasDeDescuento.size();i++){
			//listaDeReservasDeDescuento.get(i).getClient().getUser().getUser_name()
			u = userService.getUser(listaDeReservasDeDescuento.get(i).getId().getClientId());
			listaClientesPorDescuento.add(i,u);
		}
		
		List<TableInfo> tableInfo  = MergeLists(listaDeReservasDeDescuento, listaReservasPromo);
		model.addAttribute("tableInfo", tableInfo);
		return new ModelAndView("adminTool/facturacion/search_results");
	
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/get_comercial_info_by_month_year")
	public ModelAndView  get_comercial_info_by_month_year(HttpServletRequest request,
			@RequestParam(value = "year", required = true) int year, 
			@RequestParam(value = "month", required = true) int month, 
			Model model) {
				
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.YEAR, year);
		Date date = calendar.getTime();
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
		Date dateFin = null;
		if(month!=11){
			calendar.clear();
			calendar.set(Calendar.MONTH, month+1);
			calendar.set(Calendar.YEAR, year);
			dateFin = calendar.getTime();			
		}else{
			calendar.clear();
			calendar.set(Calendar.MONTH, 0);
			calendar.set(Calendar.YEAR, year+1);
			dateFin = calendar.getTime();
		}
		
		List <ComercialReportRow> rows = comercialReportService.getComercialReportByFechaIniAndFechaFin(dt1.format(date), dt1.format(dateFin));
		List<Integer> registeredLocal = comercialReportService.getRegisteredLocalPerMonthAndHistoricalByDate(dt1.format(date), dt1.format(dateFin));
		List<Integer> localWithOffers = comercialReportService.getNumLocalWithOffers(dt1.format(date), dt1.format(dateFin));
		List<Integer> numReserves = comercialReportService.getNumReserves(dt1.format(date), dt1.format(dateFin));
		List<Integer> numUsuarios = comercialReportService.getNumUsuarios(dt1.format(date), dt1.format(dateFin));
		List<Double> ingresos = comercialReportService.getIngresos(dt1.format(date), dt1.format(dateFin));
		model.addAttribute("registeredLocals", registeredLocal);
		model.addAttribute("localWithOffers", localWithOffers);
		model.addAttribute("numReserves", numReserves);
		model.addAttribute("numUsuarios", numUsuarios);
		model.addAttribute("ingresos", ingresos);
		model.addAttribute("rows", rows);
		List<Double> listaTicketMedio = new LinkedList<Double>();
		Double resultTicketMedioPeriodo=0.0, resultTicketMedioAcumulado = 0.0;
		if(ingresos.get(0)!=0.0)resultTicketMedioPeriodo=ingresos.get(0)/numReserves.get(0);
		if(ingresos.get(1)!=0.0)resultTicketMedioAcumulado=ingresos.get(1)/numReserves.get(1);
		listaTicketMedio.add(0, resultTicketMedioPeriodo);
		listaTicketMedio.add(1, resultTicketMedioAcumulado);
		model.addAttribute("listaTicketMedio", listaTicketMedio);

		return new ModelAndView("adminTool/comercial/search_results");
	
	}
	
	
	
	
	private List<TableInfo> MergeLists(List<ClientHasDiscount> listaDeReservasDeDescuento, List<ClientHasPromo> listaReservasPromo) {
	  List<TableInfo> listaTableInfo = new LinkedList<TableInfo>();
	  TableInfo tableInfo = null;
	  for(int i=0;i<listaDeReservasDeDescuento.size();i++){
		  
		  double unitCost = 0.29;
		  if(listaDeReservasDeDescuento.get(i).getSize() <=4){
			  unitCost = 0.29;
		  }else{
			  if(listaDeReservasDeDescuento.get(i).getSize() >4 && listaDeReservasDeDescuento.get(i).getSize() <=9 ){
				  unitCost = 0.39;
			  }else{
				  if(listaDeReservasDeDescuento.get(i).getSize() >9 ){
					  unitCost = 0.49;
				  }
			  }
		  }
		  
		  tableInfo = new TableInfo(listaDeReservasDeDescuento.get(i).getId().getDate(), 
				  listaDeReservasDeDescuento.get(i).getClient().getUser().getUser_name(), 
				  1,
				  listaDeReservasDeDescuento.get(i).getSize(), 
				  0.00, unitCost, (listaDeReservasDeDescuento.get(i).getSize()*unitCost), 
				  (listaDeReservasDeDescuento.get(i).getSize()*unitCost*0.21), 
				  (listaDeReservasDeDescuento.get(i).getSize()*unitCost*1.21));
		  
		  listaTableInfo.add(tableInfo);
	  }
	  
	  SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
	  Promo p = null;
	  double importe = 0.0;
	  double unitCost = 0.10;
	  for(int i=0;i<listaReservasPromo.size();i++){
		  p = packService.getPormoForLocalByDate(listaReservasPromo.get(i).getId().getLocalId(),  dt1.format(listaReservasPromo.get(i).getId().getDate()));
		  importe = p.getPromoPvp()*p.getPromoSize();
		
		  tableInfo = new TableInfo(listaReservasPromo.get(i).getId().getDate(), 
				  listaReservasPromo.get(i).getClient().getUser().getUser_name(), 
				  0,
				  -1, 
				  importe, unitCost, (importe*unitCost), 
				  (importe*unitCost*0.21), 
				  (importe*unitCost*1.21));
		  
		  listaTableInfo.add(tableInfo);
	  }
	  
	  
	  
	  Collections.sort(listaTableInfo, new Comparator<TableInfo>() {
		    public int compare(TableInfo s1, TableInfo s2) {
		        return s1.getDate().compareTo(s2.getDate());
		    }
		});
	  
	  
	 
	  return listaTableInfo;
	}
	
	
	
	

}

package com.tkilas.controller;

import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.http.util.LangUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.tkilas.Utils.Mailing_Utils;
import com.tkilas.Utils.StringTkilasUtils;
import com.tkilas.model.RecoveryPassAttemp;
import com.tkilas.model.User;
import com.tkilas.model.forms.RecoveryPasswordForm;
import com.tkilas.service.RecoveryPassAttempService;
import com.tkilas.service.UserService;

@Controller
@RequestMapping("/reset_password")
public class ResetPasswordController {

	private static final Logger logger = LoggerFactory.getLogger(ResetPasswordController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private MessageSource message;
		
	@Autowired
	private RecoveryPassAttempService recoveryPassAttempService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView forgotPasswordPage(HttpServletRequest request,Locale locale,
			@RequestParam(value="fingerprint",required = false) String fingerprint, 
			@RequestParam(value = "error", required = false) String error,
			@RequestParam(value="status",required = false) String status) {
		ModelAndView model = new ModelAndView();
		String errorMSG= new String();

		model.addObject("status", status);
		if(status.equals("0")){
			RecoveryPassAttemp attemp = recoveryPassAttempService.getAttemp(fingerprint);
			request.getSession().setAttribute("fingerprint", fingerprint);
			if(attemp.getTime_max().after(new Date(System.currentTimeMillis()))){
				if(!attemp.isIs_done()){
					model.addObject("userId", attemp.getUserIdUser());
				}else{
					errorMSG = message.getMessage ("password.recovery.error.retry", null,locale);
					model.addObject("error",errorMSG );
				}
			}else{
				errorMSG = message.getMessage ("password.recovery.error.timeout", null,locale);
				model.addObject("error", errorMSG);
			}
		}else{
			if(status.equals("2")){
				errorMSG = message.getMessage ("password.recovery.error.timeout", null,locale);
				model.addObject("error", errorMSG);
			}
		}
		
		
		model.setViewName("reset_password");
		return model;
	}
	
	//Spring Security see this :
		@RequestMapping(value = "/send", method = RequestMethod.POST)
		public String startRecoveryPassword(HttpServletRequest request, @ModelAttribute RecoveryPasswordForm recoveryPasswordForm) {
	 		
			User user = userService.getUser(recoveryPasswordForm.getUserId());
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			String hashedPassword = passwordEncoder.encode(recoveryPasswordForm.getPassword());
			user.setPassword(hashedPassword);
			String fingerPrint = request.getSession().getAttribute("fingerprint").toString();
			userService.editUser(user);
			boolean returnValue = recoveryPassAttempService.updateStatusAttempByUserId(user.getIdUser(), fingerPrint);
			if(returnValue){
				return "redirect:/reset_password?status=1";
			}else{
				return "redirect:/reset_password?status=2";
			}

		
		}
}

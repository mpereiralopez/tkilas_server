package com.tkilas.controller;

import java.sql.Timestamp;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.tkilas.model.Comment;
import com.tkilas.model.forms.CommentForm;
import com.tkilas.service.SurveyService;
import com.tkilas.service.UserService;

@Controller
@RequestMapping("/survey")
public class SurveyController {
	
	@Autowired
	private SurveyService surveyService;
	
	@Autowired
	private MessageSource message;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView forgotPasswordPage(HttpServletRequest request,Locale locale,
			@RequestParam(value="fingerprint",required = true) String fingerprint, 
			@RequestParam(value = "error", required = false) String error) {
		ModelAndView model = new ModelAndView();
		String errorMSG= new String();

		Comment attemp = surveyService.getAttemp(fingerprint);
		
		if(attemp!=null){
			model.addObject("status", attemp.getStatus());

			//Solo puedo rellenar si su estado es 0  
			if(attemp.getStatus()==0){
					model.addObject("userId", attemp.getClient().getUserIdUser());
					model.addObject("localId", attemp.getLocal().getUserIdUser());
					model.addObject("fingerprint", fingerprint);
					model.addObject("userName", attemp.getClient().getUser().getUser_name()+" "+attemp.getClient().getUser().getUser_surname());
					model.addObject("localName", attemp.getLocal().getLocal_name());

					

			}else{
				//Si es 1 o 2 error
				/*if(attemp.getStatus() == 1){
					errorMSG = message.getMessage ("password.recovery.error.retry", null,locale);
					model.addObject("error",errorMSG );
				}else{
					errorMSG = message.getMessage ("password.recovery.error.retry", null,locale);
					model.addObject("error",errorMSG );
				}*/
					
			}
			
		}else{
			//Fallo gordo porque no encuentra nada
			System.out.println("Error al recuperar intento de comentar");
		}
		
			
		
		
		
		model.setViewName("survey/survey");
		return model;
	}
	
	@RequestMapping(value = "/send", method = RequestMethod.POST)
	public String sendComment(HttpServletRequest request, @ModelAttribute CommentForm commentForm) {
 		
		
		Comment comment  = surveyService.getAttemp(commentForm.getFingerprint());
		comment.setAmbient(commentForm.getAmbient());
		comment.setDrinks(commentForm.getDrinks());
		comment.setTrate(commentForm.getTrate());
		comment.setGlobal(commentForm.getGlobal());
		
		float v1= commentForm.getAmbient();
		float v2= commentForm.getDrinks();
		float v3=commentForm.getTrate();
		float v4=commentForm.getGlobal();
		
		Float mean = (float) ((v1+v2+v3+v4)/4);
		comment.setMean(mean);
		comment.setCommentBody(commentForm.getCommentBody());
		comment.setStatus(1);
		comment.setResponsTime(new Timestamp(System.currentTimeMillis()));
		
		surveyService.editComment(comment);
		
		//boolean returnValue = recoveryPassAttempService.updateStatusAttempByUserId(user.getIdUser(), fingerPrint);
		

		return "redirect:/survey?fingerprint="+commentForm.getFingerprint();
	}
	
	

}

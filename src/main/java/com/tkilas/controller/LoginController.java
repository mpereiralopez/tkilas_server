package com.tkilas.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.tkilas.Utils.Mailing_Utils;



@Controller
public class LoginController {
	
	@Autowired
	private JavaMailSender mailSender;
	
	
	@RequestMapping(value = { "/", "/index", "/welcome**" }, method = RequestMethod.GET)
	public ModelAndView welcomePage(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView();
		
		
		
		model.setViewName("hello");
		return model;
 
	}
 
	//Spring Security see this :
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(
		@RequestParam(value = "error", required = false) String error,
		@RequestParam(value = "logout", required = false) String logout, Locale locale) {
 
		
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "true");
		}
 
		if (logout != null) {
			model.addObject("msg", "Se ha desconectado con exito.");
		}
		model.setViewName("login");
 
		return model;
 
	}
	
	
	@RequestMapping(value = "/send_comment_index", method = RequestMethod.POST)
	@ResponseBody
	public void sendComment(@RequestParam("body") String body, @RequestParam("to") String to, @RequestParam("from") String from,@RequestParam("subject") String subject) {
		System.out.println("AQUI MANDO EL MAIL");
		mailSender.send(new Mailing_Utils().send_simple_mail(from, to, subject, body)); 
	}
	
	@RequestMapping(value = "/accept_cookies", method = RequestMethod.POST)
	public void acceptCookies(HttpServletResponse response){
		Cookie accpeted = new Cookie("tkilasCookiesAccepted", "true"); //bake cookie
		accpeted.setMaxAge(31556926); //set expire time to 1000 sec
		response.addCookie(accpeted);

	}

}

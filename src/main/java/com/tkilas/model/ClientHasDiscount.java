package com.tkilas.model;

import java.io.Serializable;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.Date;


/**
 * The persistent class for the client_has_discount database table.
 * 
 */
@Entity
@Table(name="client_has_disscount")
//@NamedQuery(name="client_has_discount.findAll", query="SELECT c FROM client_has_discount c")
public class ClientHasDiscount implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ClientHasDiscountPK id;

	@Column(name="size")
	private int size;

	@Temporal(TemporalType.TIME)
	@Column(name="hour")
	private Date hour;
	
	@Column(name="status")
	private int status;
	
	
	//bi-directional many-to-one association to Client
	 
		@ManyToOne
		@JoinColumns({
			@JoinColumn(name="id_client", referencedColumnName="user_id_user",insertable=false, updatable=false),
			})
		private Client client;

		//bi-directional many-to-one association to Discount
	/*
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="date", referencedColumnName="date",insertable=false, updatable=false),
		@JoinColumn(name="id_local", referencedColumnName="local_user_id_user", insertable=false, updatable=false)
		})
	private Discount discount;
	*/
	
	public ClientHasDiscount() {
	}

	public ClientHasDiscountPK getId() {
		return this.id;
	}

	public void setId(ClientHasDiscountPK id) {
		this.id = id;
	}

	public int getSize() {
		return this.size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Date getHour() {
		return this.hour;
	}

	public void setHour(Date hour) {
		this.hour = hour;
	}
	
	
	@JsonBackReference
	public Client getClient() {
		return this.client;
	}
	@JsonBackReference
	public void setClient(Client client) {
		this.client = client;
	}
	/*
	@JsonBackReference
	public Discount getDiscount() {
		return this.discount;
	}
	@JsonBackReference
	public void setDiscount(Discount discount) {
		this.discount = discount;
	}
	*/

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	//Update tkilasDDBB.`pack` set counter = counter + 3 where date = '2014-10-10';

}
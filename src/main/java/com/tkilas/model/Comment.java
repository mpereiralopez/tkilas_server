package com.tkilas.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the comment database table.
 * 
 */
@Entity
@Table(name = "comment")
@NamedQuery(name="Comment.findAll", query="SELECT c FROM Comment c")
public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private CommentPK id;
	
	@Column(name="fingerprint")
	private String fingerprint;
	
	@Column(name="trate")
	private int trate;
	
	@Column(name="drinks")
	private int drinks;
	
	@Column(name="ambient")
	private int ambient;
	
	@Column(name="global")
	private int global;
	
	@Column(name="mean")
	private float mean;
	
	
	@Column(name="comment")
	private String commentBody;
	
	@Column(name="status")
	private int status;

	@Column(name="create_time")
	private Timestamp createTime;

	@Column(name="response_time")
	private Timestamp responsTime;

	//bi-directional many-to-one association to Client
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="client_user_id_user", referencedColumnName="user_id_user",insertable=false, updatable=false),
		})
	private Client client;

	//bi-directional many-to-one association to Local
	@ManyToOne
	@JoinColumns({
		@JoinColumn(name="local_user_id_user", referencedColumnName="user_id_user",insertable=false, updatable=false),
		})
	private Local local;

	public Comment() {
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Local getLocal() {
		return this.local;
	}

	public void setLocal(Local local) {
		this.local = local;
	}

	public String getFingerprint() {
		return fingerprint;
	}

	public void setFingerprint(String fingerprint) {
		this.fingerprint = fingerprint;
	}

	public int getTrate() {
		return trate;
	}

	public void setTrate(int trate) {
		this.trate = trate;
	}

	public int getDrinks() {
		return drinks;
	}

	public void setDrinks(int drinks) {
		this.drinks = drinks;
	}

	public int getAmbient() {
		return ambient;
	}

	public void setAmbient(int ambient) {
		this.ambient = ambient;
	}

	public int getGlobal() {
		return global;
	}

	public void setGlobal(int global) {
		this.global = global;
	}

	public float getMean() {
		return mean;
	}

	public void setMean(float mean) {
		this.mean = mean;
	}

	public String getCommentBody() {
		return commentBody;
	}

	public void setCommentBody(String commentBody) {
		this.commentBody = commentBody;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public Timestamp getResponsTime() {
		return responsTime;
	}

	public void setResponsTime(Timestamp responsTime) {
		this.responsTime = responsTime;
	}
	
	

}
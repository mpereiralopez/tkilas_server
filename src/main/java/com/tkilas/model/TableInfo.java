package com.tkilas.model;

import java.util.Date;

public class TableInfo{
	private Date date;
	private String user;
	private int type;
	private int num_pers;
	private double pvp;
	private double unit_cost;
	private double subtotal;
	private double iva;
	private double total;
	
	public TableInfo(Date date, String user, int type, int num_pers, double pvp, double unit_cost, double subtotal,
			double iva, double total) {
		super();
		this.date = date;
		this.user = user;
		this.type = type;
		this.num_pers = num_pers;
		this.pvp = pvp;
		this.unit_cost = unit_cost;
		this.subtotal = subtotal;
		this.iva = iva;
		this.total = total;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getNum_pers() {
		return num_pers;
	}
	public void setNum_pers(int num_pers) {
		this.num_pers = num_pers;
	}
	public double getPvp() {
		return pvp;
	}
	public void setPvp(double pvp) {
		this.pvp = pvp;
	}
	public double getUnit_cost() {
		return unit_cost;
	}
	public void setUnit_cost(double unit_cost) {
		this.unit_cost = unit_cost;
	}
	public double getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}
	public double getIva() {
		return iva;
	}
	public void setIva(double iva) {
		this.iva = iva;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}

	
}

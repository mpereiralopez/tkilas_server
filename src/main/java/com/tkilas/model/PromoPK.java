package com.tkilas.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Embeddable
public class PromoPK implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Temporal(TemporalType.DATE)
	@Column(name="date", insertable=false, updatable=false)
	private java.util.Date date;

	@Column(name="local_user_id_user", insertable=false, updatable=false)
	private int localUserIdUser;
	
	@Column(name="promo_index", insertable=false, updatable=false)
	private int promo_index;
	
	public PromoPK(){}

	public java.util.Date getDate() {
		return date;
	}

	public void setDate(java.util.Date date) {
		this.date = date;
	}

	public int getLocalUserIdUser() {
		return localUserIdUser;
	}

	public void setLocalUserIdUser(int localUserIdUser) {
		this.localUserIdUser = localUserIdUser;
	}

	public int getPromo_index() {
		return promo_index;
	}

	public void setPromo_index(int promo_index) {
		this.promo_index = promo_index;
	}
	
	
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PromoPK)) {
			return false;
		}
		PromoPK castOther = (PromoPK)other;
		return 
			this.date.equals(castOther.date)
			&& (this.localUserIdUser == castOther.localUserIdUser)
			&& (this.promo_index == castOther.promo_index);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.date.hashCode();
		hash = hash * prime + this.localUserIdUser;
		hash = hash * prime + this.promo_index;

		return hash;
	}
	
}

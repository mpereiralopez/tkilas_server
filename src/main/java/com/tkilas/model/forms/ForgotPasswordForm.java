package com.tkilas.model.forms;

public class ForgotPasswordForm {

	private String email_recovery;

	public String getEmail_recovery() {
		return email_recovery;
	}
	public void setEmail_recovery(String email_recovery) {
		this.email_recovery = email_recovery;
	}
}

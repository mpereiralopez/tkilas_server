package com.tkilas.model.forms;

public class LocalFormUpdateIBAN {
	
	int id_local;
	
	String local_iban_1;
	
	String local_iban_2;
	
	String local_iban_3;
	
	String local_iban_4;
	
	String local_iban_5;

	public int getId_local() {
		return id_local;
	}

	public void setId_local(int id_local) {
		this.id_local = id_local;
	}

	public String getLocal_iban_1() {
		return local_iban_1;
	}

	public void setLocal_iban_1(String local_iban_1) {
		this.local_iban_1 = local_iban_1;
	}

	public String getLocal_iban_2() {
		return local_iban_2;
	}

	public void setLocal_iban_2(String local_iban_2) {
		this.local_iban_2 = local_iban_2;
	}

	public String getLocal_iban_3() {
		return local_iban_3;
	}

	public void setLocal_iban_3(String local_iban_3) {
		this.local_iban_3 = local_iban_3;
	}

	public String getLocal_iban_4() {
		return local_iban_4;
	}

	public void setLocal_iban_4(String local_iban_4) {
		this.local_iban_4 = local_iban_4;
	}

	public String getLocal_iban_5() {
		return local_iban_5;
	}

	public void setLocal_iban_5(String local_iban_5) {
		this.local_iban_5 = local_iban_5;
	}
	
	
	
	

}

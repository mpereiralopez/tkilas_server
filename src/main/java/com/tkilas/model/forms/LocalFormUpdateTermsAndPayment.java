package com.tkilas.model.forms;

public class LocalFormUpdateTermsAndPayment {
	
	private int terms_accepted;
	
	private int reserve_cost;
	
	private int id_local;
	
	private String trade_name;
	
	private String cif;
	
	private String contact_address;
	
	private String contact_cp;
	
	private String contact_city;
	
	private String contact_IBAN;

	
	
	public int getReserve_cost() {
		return reserve_cost;
	}

	public void setReserve_cost(int reserve_cost) {
		this.reserve_cost = reserve_cost;
	}

	public int getTerms_accepted() {
		return terms_accepted;
	}

	public void setTerms_accepted(int terms_accepted) {
		this.terms_accepted = terms_accepted;
	}

	public int getId_local() {
		return id_local;
	}

	public void setId_local(int id_local) {
		this.id_local = id_local;
	}

	public String getTrade_name() {
		return trade_name;
	}

	public void setTrade_name(String trade_name) {
		this.trade_name = trade_name;
	}

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getContact_address() {
		return contact_address;
	}

	public void setContact_address(String contact_address) {
		this.contact_address = contact_address;
	}

	public String getContact_cp() {
		return contact_cp;
	}

	public void setContact_cp(String contact_cp) {
		this.contact_cp = contact_cp;
	}

	public String getContact_city() {
		return contact_city;
	}

	public void setContact_city(String contact_city) {
		this.contact_city = contact_city;
	}

	public String getContact_IBAN() {
		return contact_IBAN;
	}

	public void setContact_IBAN(String contact_IBAN) {
		this.contact_IBAN = contact_IBAN;
	}
	
	

}

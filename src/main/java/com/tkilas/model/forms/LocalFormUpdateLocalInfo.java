package com.tkilas.model.forms;

import java.math.BigDecimal;


public class LocalFormUpdateLocalInfo {
	
	private int local_id;
	
	private int local_type;
	
	private String local_desc;
	
	private int local_pay_way;
	
	private int capacity;
	
	private BigDecimal local_cost_beer;
	
	private BigDecimal local_cost_cocktail;
	
	private BigDecimal local_cost_alch_bottle;
	
	private BigDecimal local_cost_wine_bottle;
	
	private BigDecimal local_cost_coffe;
	
	private BigDecimal local_cost_tonic;
	
	public int getLocal_id() {
		return local_id;
	}

	public void setLocal_id(int localId) {
		this.local_id = localId;
	}

	public int getLocal_type() {
		return local_type;
	}

	public void setLocal_type(int local_type) {
		this.local_type = local_type;
	}

	public String getLocal_desc() {
		return local_desc;
	}

	public void setLocal_desc(String local_desc) {
		this.local_desc = local_desc;
	}

	public int getLocal_pay_way() {
		return local_pay_way;
	}

	public void setLocal_pay_way(int local_pay_way) {
		this.local_pay_way = local_pay_way;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	

	public BigDecimal getLocal_cost_beer() {
		return local_cost_beer;
	}

	public void setLocal_cost_beer(BigDecimal local_cost_beer) {
		this.local_cost_beer = local_cost_beer;
	}

	public BigDecimal getLocal_cost_cocktail() {
		return local_cost_cocktail;
	}

	public void setLocal_cost_cocktail(BigDecimal local_cost_cocktail) {
		this.local_cost_cocktail = local_cost_cocktail;
	}

	public BigDecimal getLocal_cost_alch_bottle() {
		return local_cost_alch_bottle;
	}

	public void setLocal_cost_alch_bottle(BigDecimal local_cost_alch_bottle) {
		this.local_cost_alch_bottle = local_cost_alch_bottle;
	}

	public BigDecimal getLocal_cost_wine_bottle() {
		return local_cost_wine_bottle;
	}

	public void setLocal_cost_wine_bottle(BigDecimal local_cost_wine_bottle) {
		this.local_cost_wine_bottle = local_cost_wine_bottle;
	}

	public BigDecimal getLocal_cost_coffe() {
		return local_cost_coffe;
	}

	public void setLocal_cost_coffe(BigDecimal local_cost_coffe) {
		this.local_cost_coffe = local_cost_coffe;
	}

	public BigDecimal getLocal_cost_tonic() {
		return local_cost_tonic;
	}

	public void setLocal_cost_tonic(BigDecimal local_cost_tonic) {
		this.local_cost_tonic = local_cost_tonic;
	}

}

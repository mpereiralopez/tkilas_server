package com.tkilas.model.forms;

public class LocalFormForUpdate {
	
	private int user_id;
	private String user_name;
	private String user_surname;
	//private String email_in_profile;
	private String local_name;
	private String local_phone;
	private String local_address;
	private String local_cp;
	private String local_city;
	private String local_web;
	
	
	
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id_user) {
		this.user_id = user_id_user;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_surname() {
		return user_surname;
	}
	public void setUser_surname(String user_surname) {
		this.user_surname = user_surname;
	}
	/*public String getEmail_in_profile() {
		return email_in_profile;
	}
	public void setEmail_in_profile(String email_in_profile) {
		this.email_in_profile = email_in_profile;
	}*/
	public String getLocal_name() {
		return local_name;
	}
	public void setLocal_name(String local_name) {
		this.local_name = local_name;
	}
	public String getLocal_phone() {
		return local_phone;
	}
	public void setLocal_phone(String local_phone) {
		this.local_phone = local_phone;
	}
	public String getLocal_address() {
		return local_address;
	}
	public void setLocal_address(String local_address) {
		this.local_address = local_address;
	}
	public String getLocal_cp() {
		return local_cp;
	}
	public void setLocal_cp(String local_cp) {
		this.local_cp = local_cp;
	}
	public String getLocal_city() {
		return local_city;
	}
	public void setLocal_city(String local_city) {
		this.local_city = local_city;
	}
	public String getLocal_web() {
		return local_web;
	}
	public void setLocal_web(String local_web) {
		this.local_web = local_web;
	}
	

}

package com.tkilas.model.forms;

import java.math.BigDecimal;

import javax.persistence.Column;



public class LocalFormAdmin {
	
	private int userIdUser;
	
	private String user_name;
	
	private String user_surname;
	
	private String email;
		
	private String local_name;
	
	private String tlf;

	private String address;
	
	private String local_cp;

	private String city;
	
	private String web;
	
	private int capacity;
	
	private int district;
	
	private int local_type;
	
	private int product_type_local;
	
	private String comercial;
	
	private float local_latitud;
	
	private float local_longitud;
	
	private String url_pic1;
	
	private String url_pic2;
	
	private String url_pic3;
	
	private String local_desc;
	
	private int local_pay_way;
	
	private BigDecimal local_cost_beer;
	
	private BigDecimal local_cost_cocktail;
	
	private BigDecimal local_cost_alch_bottle;
	
	private BigDecimal local_cost_wine_bottle;
	
	private BigDecimal local_cost_coffe;
	
	private BigDecimal local_cost_tonic;
	
	private int terms_accpeted;
	
	private String trade_name;
	
	private String cif;
	
	private String contact_address;

	private String contact_cp;
	
	private String contact_city;
	
	private String local_iban;

	public String getUrl_pic1() {
		return url_pic1;
	}

	
	public int getUserIdUser() {
		return userIdUser;
	}

	public void setUserIdUser(int userIdUser) {
		this.userIdUser = userIdUser;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_surname() {
		return user_surname;
	}

	public void setUser_surname(String user_surname) {
		this.user_surname = user_surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLocal_name() {
		return local_name;
	}

	public void setLocal_name(String local_name) {
		this.local_name = local_name;
	}

	public String getTlf() {
		return tlf;
	}

	public void setTlf(String tlf) {
		this.tlf = tlf;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLocal_cp() {
		return local_cp;
	}

	public void setLocal_cp(String local_cp) {
		this.local_cp = local_cp;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getWeb() {
		return web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getDistrict() {
		return district;
	}

	public void setDistrict(int district) {
		this.district = district;
	}

	public int getLocal_type() {
		return local_type;
	}

	public void setLocal_type(int local_type) {
		this.local_type = local_type;
	}

	public int getProduct_type_local() {
		return product_type_local;
	}

	public void setProduct_type_local(int product_type_local) {
		this.product_type_local = product_type_local;
	}

	public String getComercial() {
		return comercial;
	}

	public void setComercial(String comercial) {
		this.comercial = comercial;
	}

	public float getLocal_latitud() {
		return local_latitud;
	}

	public void setLocal_latitud(float local_latitud) {
		this.local_latitud = local_latitud;
	}

	public float getLocal_longitud() {
		return local_longitud;
	}

	public void setLocal_longitud(float local_longitud) {
		this.local_longitud = local_longitud;
	}
	
	
	public void setUrl_pic1(String url_pic1) {
		this.url_pic1 = url_pic1;
	}

	public String getUrl_pic2() {
		return url_pic2;
	}

	public void setUrl_pic2(String url_pic2) {
		this.url_pic2 = url_pic2;
	}

	public String getUrl_pic3() {
		return url_pic3;
	}

	public void setUrl_pic3(String url_pic3) {
		this.url_pic3 = url_pic3;
	}


	public String getLocal_desc() {
		return local_desc;
	}


	public void setLocal_desc(String local_desc) {
		this.local_desc = local_desc;
	}


	public int getLocal_pay_way() {
		return local_pay_way;
	}


	public void setLocal_pay_way(int local_pay_way) {
		this.local_pay_way = local_pay_way;
	}


	public BigDecimal getLocal_cost_beer() {
		return local_cost_beer;
	}


	public void setLocal_cost_beer(BigDecimal local_cost_beer) {
		this.local_cost_beer = local_cost_beer;
	}


	public BigDecimal getLocal_cost_cocktail() {
		return local_cost_cocktail;
	}


	public void setLocal_cost_cocktail(BigDecimal local_cost_cocktail) {
		this.local_cost_cocktail = local_cost_cocktail;
	}


	public BigDecimal getLocal_cost_alch_bottle() {
		return local_cost_alch_bottle;
	}


	public void setLocal_cost_alch_bottle(BigDecimal local_cost_alch_bottle) {
		this.local_cost_alch_bottle = local_cost_alch_bottle;
	}


	public BigDecimal getLocal_cost_wine_bottle() {
		return local_cost_wine_bottle;
	}


	public void setLocal_cost_wine_bottle(BigDecimal local_cost_wine_bottle) {
		this.local_cost_wine_bottle = local_cost_wine_bottle;
	}


	public BigDecimal getLocal_cost_coffe() {
		return local_cost_coffe;
	}


	public void setLocal_cost_coffe(BigDecimal local_cost_coffe) {
		this.local_cost_coffe = local_cost_coffe;
	}


	public BigDecimal getLocal_cost_tonic() {
		return local_cost_tonic;
	}


	public void setLocal_cost_tonic(BigDecimal local_cost_tonic) {
		this.local_cost_tonic = local_cost_tonic;
	}


	public int getTerms_accpeted() {
		return terms_accpeted;
	}


	public void setTerms_accpeted(int terms_accpeted) {
		this.terms_accpeted = terms_accpeted;
	}
	

	public String getTrade_name() {
		return trade_name;
	}


	public void setTrade_name(String trade_name) {
		this.trade_name = trade_name;
	}


	public String getCif() {
		return cif;
	}


	public void setCif(String cif) {
		this.cif = cif;
	}


	public String getContact_address() {
		return contact_address;
	}


	public void setContact_address(String contact_address) {
		this.contact_address = contact_address;
	}


	public String getContact_cp() {
		return contact_cp;
	}


	public void setContact_cp(String contact_cp) {
		this.contact_cp = contact_cp;
	}


	public String getContact_city() {
		return contact_city;
	}


	public void setContact_city(String contact_city) {
		this.contact_city = contact_city;
	}


	public String getLocal_iban() {
		return local_iban;
	}


	public void setLocal_iban(String local_iban) {
		this.local_iban = local_iban;
	}

}

package com.tkilas.model.forms;

public class RecoveryPasswordForm {

	private String password;
	private String repeated_password;
	
	private int userId;
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRepeated_password() {
		return repeated_password;
	}
	public void setRepeated_password(String repeated_password) {
		this.repeated_password = repeated_password;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	
	
}

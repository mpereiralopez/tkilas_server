package com.tkilas.model.forms;

public class LocalForm {
	
	private String user_name;
	private String user_surname;
	private String email;
	private String user_pass;
	private String local_name;
	private String local_phone;
	private String local_address;
	private String local_cp;
	private String local_city;
	private String local_web;
	
	
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_surname() {
		return user_surname;
	}
	public void setUser_surname(String user_surname) {
		this.user_surname = user_surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUser_pass() {
		return user_pass;
	}
	public void setUser_pass(String user_pass) {
		this.user_pass = user_pass;
	}
	public String getLocal_name() {
		return local_name;
	}
	public void setLocal_name(String local_name) {
		this.local_name = local_name;
	}
	public String getLocal_phone() {
		return local_phone;
	}
	public void setLocal_phone(String local_phone) {
		this.local_phone = local_phone;
	}
	public String getLocal_address() {
		return local_address;
	}
	public void setLocal_address(String local_address) {
		this.local_address = local_address;
	}
	public String getLocal_cp() {
		return local_cp;
	}
	public void setLocal_cp(String local_cp) {
		this.local_cp = local_cp;
	}
	public String getLocal_city() {
		return local_city;
	}
	public void setLocal_city(String local_city) {
		this.local_city = local_city;
	}
	public String getLocal_web() {
		return local_web;
	}
	public void setLocal_web(String local_web) {
		this.local_web = local_web;
	}
	

}

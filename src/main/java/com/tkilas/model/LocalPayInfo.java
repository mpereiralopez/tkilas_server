package com.tkilas.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "localpayinfo")

public class LocalPayInfo implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1130465702812329931L;

	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_local",insertable=false, updatable=false)
	private int id_local;
	
	@Column(name="trade_name")
	private String trade_name;
	
	@Column(name="cif")
	private String cif;
	
	@Column(name="contact_address")
	private String contact_address;

	@Column(name="contact_cp")
	private String contact_cp;
	
	@Column(name="contact_city")
	private String contact_city;
	
	@Column(name="contact_IBAN")
	private String contact_IBAN;
	
	
	public LocalPayInfo(){}

	public int getId_local() {
		return id_local;
	}

	public void setId_local(int id_local) {
		this.id_local = id_local;
	}

	public String getTrade_name() {
		return trade_name;
	}

	public void setTrade_name(String trade_name) {
		this.trade_name = trade_name;
	}

	

	public String getCif() {
		return cif;
	}

	public void setCif(String cif) {
		this.cif = cif;
	}

	public String getContact_address() {
		return contact_address;
	}

	public void setContact_address(String contact_address) {
		this.contact_address = contact_address;
	}

	public String getContact_cp() {
		return contact_cp;
	}

	public void setContact_cp(String contact_cp) {
		this.contact_cp = contact_cp;
	}

	public String getContact_city() {
		return contact_city;
	}

	public void setContact_city(String contact_city) {
		this.contact_city = contact_city;
	}

	public String getContact_IBAN() {
		return contact_IBAN;
	}

	public void setContact_IBAN(String contact_IBAN) {
		this.contact_IBAN = contact_IBAN;
	}
	
	
	
	
}

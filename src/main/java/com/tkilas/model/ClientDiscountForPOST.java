package com.tkilas.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tkilas.Utils.JsonHourSerializer;

public class ClientDiscountForPOST implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private ClientDiscountForPOSTId id;
	private int isAccepted;
	private Date reservedTime;
	
	
	public ClientDiscountForPOST(){}
	
	public ClientDiscountForPOSTId getId() {
		return id;
	}

	public void setId(ClientDiscountForPOSTId id) {
		this.id = id;
	}

	public int getIsAccepted() {
		return isAccepted;
	}

	public void setIsAccepted(int isAccepted) {
		this.isAccepted = isAccepted;
	}
	
	@JsonSerialize(using=JsonHourSerializer.class)
	public Date getReservedTime() {
		return reservedTime;
	}
	@JsonSerialize(using=JsonHourSerializer.class)
	public void setReservedTime(Date reservedTime) {
		this.reservedTime = reservedTime;
	}



	public class ClientDiscountForPOSTId implements Serializable{
		private int clientUserIdUser;
		private Date discountPackDate;
		private int discountPackLocalUserIdUser1;
		
		public ClientDiscountForPOSTId(){}
		
		public int getClientUserIdUser() {
			return clientUserIdUser;
		}

		public void setClientUserIdUser(int clientUserIdUser) {
			this.clientUserIdUser = clientUserIdUser;
		}

		
		public Date getDiscountPackDate() {
			return discountPackDate;
		}

		public void setDiscountPackDate(Date discountPackDate) {
			this.discountPackDate = discountPackDate;
		}
		
		public int getDiscountPackLocalUserIdUser1() {
			return discountPackLocalUserIdUser1;
		}

		public void setDiscountPackLocalUserIdUser1(int discountPackLocalUserIdUser1) {
			this.discountPackLocalUserIdUser1 = discountPackLocalUserIdUser1;
		}
		
	}

}

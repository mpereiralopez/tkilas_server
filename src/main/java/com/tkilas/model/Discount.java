package com.tkilas.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the discount database table.
 * 
 */
@Entity
@Table(name = "discount")

@NamedQuery(name="Discount.findAll", query="SELECT d FROM Discount d")
public class Discount implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PackPK id;

	@Column(name="discount")
	private int discount;

	/*
	//bi-directional many-to-one association to ClientHasDiscount
	 //@JsonIgnore
	@JsonBackReference
	@OneToMany(mappedBy="discount", fetch=FetchType.LAZY)
	private List<ClientHasDiscount> clientHasDiscounts;
*/
	//bi-directional one-to-one association to Pack
	@OneToOne
	//@PrimaryKeyJoinColumns({
	//	@PrimaryKeyJoinColumn(name="pack_date", referencedColumnName="date"),
	//	@PrimaryKeyJoinColumn(name="pack_local_user_id_user1", referencedColumnName="local_user_id_user")
	//	})
	@PrimaryKeyJoinColumn
	private Pack pack;

	public Discount() {
	}

	public PackPK getId() {
		return this.id;
	}

	public void setId(PackPK id) {
		this.id = id;
	}

	public int getDiscount() {
		return this.discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}
	/*
	@JsonBackReference
	public List<ClientHasDiscount> getClientHasDiscounts() {
		return this.clientHasDiscounts;
	}
	@JsonBackReference
	public void setClientHasDiscounts(List<ClientHasDiscount> clientHasDiscounts) {
		this.clientHasDiscounts = clientHasDiscounts;
	}

	
	public ClientHasDiscount addClientHasDiscount(ClientHasDiscount clientHasDiscount) {
		getClientHasDiscounts().add(clientHasDiscount);
		clientHasDiscount.setDiscount(this);

		return clientHasDiscount;
	}

	public ClientHasDiscount removeClientHasDiscount(ClientHasDiscount clientHasDiscount) {
		getClientHasDiscounts().remove(clientHasDiscount);
		clientHasDiscount.setDiscount(null);

		return clientHasDiscount;
	}
*/
	public Pack getPack() {
		return this.pack;
	}

	public void setPack(Pack pack) {
		this.pack = pack;
	}

}
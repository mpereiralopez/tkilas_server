package com.tkilas.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the promo database table.
 * 
 */
@Entity
@Table(name = "promo")

@NamedQuery(name="Promo.findAll", query="SELECT p FROM Promo p")
public class Promo implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PromoPK id;
	
	@Column(name="promo_pvp")
	private float promoPvp;

	@Column(name="promo_size")
	private int promoSize;

	@Column(name="promo_subtype")
	private int promoSubtype;

	@Column(name="promo_type")
	private int promoType;
	
	@Column(name="promo_max_person")
	private int promoMax;

	@ManyToOne
    @JoinColumns({
        @JoinColumn(name="date", referencedColumnName="date", insertable=false, updatable=false),
        @JoinColumn(name="local_user_id_user", referencedColumnName="local_user_id_user",insertable=false, updatable=false)
    })
	private Pack pack;
	
	public Promo() {
	}

	public PromoPK getId() {
		return this.id;
	}

	public void setId(PromoPK id) {
		this.id = id;
	}
	
	public float getPromoPvp() {
		return this.promoPvp;
	}

	public void setPromoPvp(float promoPvp) {
		this.promoPvp = promoPvp;
	}

	public int getPromoSize() {
		return this.promoSize;
	}

	public void setPromoSize(int promoSize) {
		this.promoSize = promoSize;
	}

	public int getPromoSubtype() {
		return this.promoSubtype;
	}

	public void setPromoSubtype(int promoSubtype) {
		this.promoSubtype = promoSubtype;
	}

	public int getPromoType() {
		return this.promoType;
	}

	public void setPromoType(int promoType) {
		this.promoType = promoType;
	}
	
	public int getPromoMax() {
		return this.promoMax;
	}

	public void setPromoMax(int promoMax) {
		this.promoMax = promoMax;
	}

}
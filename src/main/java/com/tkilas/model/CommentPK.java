package com.tkilas.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the comment database table.
 * 
 */
@Embeddable
public class CommentPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;


	@Column(name="client_user_id_user", insertable=false, updatable=false)
	private int clientUserIdUser;

	@Column(name="local_user_id_user", insertable=false, updatable=false)
	private int localUserIdUser;
	
	@Column(name="pack_date", insertable=false, updatable=false)
	private java.util.Date pack_date;

	public CommentPK() {
	}
	
	public int getClientUserIdUser() {
		return this.clientUserIdUser;
	}
	public void setClientUserIdUser(int clientUserIdUser) {
		this.clientUserIdUser = clientUserIdUser;
	}
	public int getLocalUserIdUser() {
		return this.localUserIdUser;
	}
	public void setLocalUserIdUser(int localUserIdUser) {
		this.localUserIdUser = localUserIdUser;
	}
	
	
	public java.util.Date getPack_date() {
		return pack_date;
	}

	public void setPack_date(java.util.Date pack_date) {
		this.pack_date = pack_date;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof CommentPK)) {
			return false;
		}
		CommentPK castOther = (CommentPK)other;
		return 
			 (this.clientUserIdUser == castOther.clientUserIdUser)
			&& (this.localUserIdUser == castOther.localUserIdUser)
		&& (this.pack_date == castOther.pack_date);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.clientUserIdUser;
		hash = hash * prime + this.localUserIdUser;
		hash = hash * prime + this.pack_date.hashCode();
		return hash;
	}
}
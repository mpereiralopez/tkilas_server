package com.tkilas.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the client_has_discount database table.
 * 
 */
@Embeddable
public class ClientHasDiscountPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_client", insertable=false, updatable=false)
	private int id_client;

	@Temporal(TemporalType.DATE)
	@Column(name="date", insertable=false, updatable=false)
	private java.util.Date date;

	@Column(name="id_local", insertable=false, updatable=false)
	private int id_local;

	public ClientHasDiscountPK() {
	}
	public int getClientId() {
		return this.id_client;
	}
	public void setClientId(int clientUserIdUser) {
		this.id_client = clientUserIdUser;
	}
	public java.util.Date getDate() {
		return this.date;
	}
	public void setDate(java.util.Date discountPackDate) {
		this.date = discountPackDate;
	}
	public int getLocalId() {
		return this.id_local;
	}
	public void setLocalId(int discountPackLocalUserIdUser1) {
		this.id_local = discountPackLocalUserIdUser1;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ClientHasDiscountPK)) {
			return false;
		}
		ClientHasDiscountPK castOther = (ClientHasDiscountPK)other;
		return 
			(this.id_client == castOther.id_client)
			&& this.date.equals(castOther.date)
			&& (this.id_local == castOther.id_local);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.id_client;
		hash = hash * prime + this.date.hashCode();
		hash = hash * prime + this.id_local;
		
		return hash;
	}
}
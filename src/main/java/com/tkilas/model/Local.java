package com.tkilas.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the local database table.
 * 
 */
@Entity
@Table(name = "local")

@NamedQuery(name="Local.findAll", query="SELECT l FROM Local l")
public class Local implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id_user",insertable=false, updatable=false)
	private int userIdUser;
	
	@Column(name="local_name")
	private String local_name;
	
	@Column(name="tlf")
	private String tlf;

	@Column(name="address")
	private String address;
	
	@Column(name="local_cp")
	private String local_cp;

	@Column(name="city")
	private String city;
	
	@Column(name="web")
	private String web;
	
	@Column(name="capacity")
	private Integer capacity;

	@Column(name="status")
	private Integer status;
	
	@Column(name="district")
	private Integer district;
	
	@Column(name="local_type")
	private Integer local_type;
	
	@Column(name="product_type_local")
	private Integer product_type_local;
	
	@Column(name="comercial")
	private String comercial;
	
	@Column(name="local_latitud")
	private Float local_latitud;
	
	@Column(name="local_longitud")
	private Float local_longitud;
	
	@Column(name="url_pic1")
	private String url_pic1;
	
	@Column(name="url_pic2")
	private String url_pic2;
	
	@Column(name="url_pic3")
	private String url_pic3;
	
	@Column(name="local_desc")
	private String local_desc;
	
	@Column(name="local_pay_way")
	private int local_pay_way;
	
	@Column(name="local_cost_beer")
	private BigDecimal local_cost_beer;
	
	@Column(name="local_cost_cocktail")
	private BigDecimal local_cost_cocktail;
	
	@Column(name="local_cost_alch_bottle")
	private BigDecimal local_cost_alch_bottle;
	
	@Column(name="local_cost_wine_bottle")
	private BigDecimal local_cost_wine_bottle;
	
	@Column(name="local_cost_coffe")
	private BigDecimal local_cost_coffe;
	
	@Column(name="local_cost_tonic")
	private BigDecimal local_cost_tonic;
	
	@Column(name="terms_accepted")
	private int terms_accpeted;

	//bi-directional one-to-one association to User
	@OneToOne (cascade = CascadeType.ALL)
	@JoinColumn(name="user_id_user", insertable = false, updatable= false)
	private User user;

	//bi-directional many-to-one association to Pack
	@OneToMany(mappedBy="local")
	private List<Pack> packs;

	//bi-directional many-to-one association to Comment
	@OneToMany(mappedBy="local")
	private List<Comment> comments;

	public Local() {
	}


	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getCapacity() {
		return this.capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getUserIdUser() {
		return userIdUser;
	}

	public void setUserIdUser(int userIdUser) {
		this.userIdUser = userIdUser;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getDistrict() {
		return district;
	}

	public void setDistrict(int district) {
		this.district = district;
	}

	public int getLocal_type() {
		return local_type;
	}

	public void setLocal_type(int local_type) {
		this.local_type = local_type;
	}

	public int getProduct_type_local() {
		return product_type_local;
	}

	public void setProduct_type_local(int product_type_local) {
		this.product_type_local = product_type_local;
	}

	public String getComercial() {
		return comercial;
	}

	public void setComercial(String comercial) {
		this.comercial = comercial;
	}

	public float getLocal_latitud() {
		return local_latitud;
	}

	public void setLocal_latitud(float local_latitud) {
		this.local_latitud = local_latitud;
	}

	public float getLocal_longitud() {
		return local_longitud;
	}

	public void setLocal_longitud(float local_longitud) {
		this.local_longitud = local_longitud;
	}

	public String getLocal_cp() {
		return this.local_cp;
	}

	public void setLocal_cp(String localCp) {
		this.local_cp = localCp;
	}

	public String getLocal_name() {
		return this.local_name;
	}

	public void setLocal_name(String localName) {
		this.local_name = localName;
	}

	public String getTlf() {
		return this.tlf;
	}

	public void setTlf(String tlf) {
		this.tlf = tlf;
	}

	public String getWeb() {
		return this.web;
	}

	public void setWeb(String web) {
		this.web = web;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Pack> getPacks() {
		return this.packs;
	}

	public void setPacks(List<Pack> packs) {
		this.packs = packs;
	}

	public Pack addPack(Pack pack) {
		getPacks().add(pack);
		pack.setLocal(this);

		return pack;
	}

	public Pack removePack(Pack pack) {
		getPacks().remove(pack);
		pack.setLocal(null);

		return pack;
	}

	public List<Comment> getComments() {
		return this.comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public Comment addComment(Comment comment) {
		getComments().add(comment);
		comment.setLocal(this);

		return comment;
	}

	public Comment removeComment(Comment comment) {
		getComments().remove(comment);
		comment.setLocal(null);

		return comment;
	}
	
	public String getUrl_pic1() {
		return url_pic1;
	}


	public void setUrl_pic1(String url_pic1) {
		this.url_pic1 = url_pic1;
	}


	public String getUrl_pic2() {
		return url_pic2;
	}


	public void setUrl_pic2(String url_pic2) {
		this.url_pic2 = url_pic2;
	}


	public String getUrl_pic3() {
		return url_pic3;
	}


	public void setUrl_pic3(String url_pic3) {
		this.url_pic3 = url_pic3;
	}


	public String getLocal_desc() {
		return local_desc;
	}


	public void setLocal_desc(String local_desc) {
		this.local_desc = local_desc;
	}


	public int getLocal_pay_way() {
		return local_pay_way;
	}


	public void setLocal_pay_way(int local_pay_way) {
		this.local_pay_way = local_pay_way;
	}


	public BigDecimal getLocal_cost_beer() {
		return local_cost_beer;
	}


	public void setLocal_cost_beer(BigDecimal local_cost_beer) {
		this.local_cost_beer = local_cost_beer;
	}


	public BigDecimal getLocal_cost_cocktail() {
		return local_cost_cocktail;
	}


	public void setLocal_cost_cocktail(BigDecimal local_cost_cocktail) {
		this.local_cost_cocktail = local_cost_cocktail;
	}


	public BigDecimal getLocal_cost_alch_bottle() {
		return local_cost_alch_bottle;
	}


	public void setLocal_cost_alch_bottle(BigDecimal local_cost_alch_bottle) {
		this.local_cost_alch_bottle = local_cost_alch_bottle;
	}


	public BigDecimal getLocal_cost_wine_bottle() {
		return local_cost_wine_bottle;
	}


	public void setLocal_cost_wine_bottle(BigDecimal local_cost_wine_bottle) {
		this.local_cost_wine_bottle = local_cost_wine_bottle;
	}


	public BigDecimal getLocal_cost_coffe() {
		return local_cost_coffe;
	}


	public void setLocal_cost_coffe(BigDecimal local_cost_coffe) {
		this.local_cost_coffe = local_cost_coffe;
	}


	public BigDecimal getLocal_cost_tonic() {
		return local_cost_tonic;
	}


	public void setLocal_cost_tonic(BigDecimal local_cost_tonic) {
		this.local_cost_tonic = local_cost_tonic;
	}


	public int getTerms_accpeted() {
		return terms_accpeted;
	}


	public void setTerms_accpeted(int terms_accpeted) {
		this.terms_accpeted = terms_accpeted;
	}
	
	
	
	

}
package com.tkilas.dao;

import com.tkilas.model.RecoveryPassAttemp;

public interface RecoveryPassAttempDAO {

	public RecoveryPassAttemp addAttemp(RecoveryPassAttemp attemp);
	public RecoveryPassAttemp getAttemp(String fingerPrint);
	public boolean updateStatusAttempByUserId (int userId, String fingerPrint);
}

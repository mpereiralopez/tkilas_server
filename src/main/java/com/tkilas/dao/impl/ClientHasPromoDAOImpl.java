package com.tkilas.dao.impl;


import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.ClientHasPromoDAO;
import com.tkilas.dao.PromoDAO;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientHasPromo;


@Repository
public class ClientHasPromoDAOImpl implements ClientHasPromoDAO {

	@Autowired
	private SessionFactory session;
	
	@Autowired
	private PromoDAO promoDAO;

	@Override
	public List<ClientHasPromo> getClientsOfPromoByLocalId(int localId,
			String today) {
		// TODO Auto-generated method stub
		List<ClientHasPromo> cHPromoList = session.getCurrentSession().createQuery("FROM ClientHasPromo WHERE id_local="+localId+" AND date='"+today+"'").list();
		return cHPromoList;
	}
	
	@Override
	public List<ClientHasPromo> getClientsOfPromoByLocalIdAndDate(int localId, String today) {
		// TODO Auto-generated method stub
		List<ClientHasPromo> cHPromoList = session.getCurrentSession().createQuery("FROM ClientHasPromo WHERE id_local="+localId+" AND date='"+today+"'").list();
		return cHPromoList;
	}

	@Override
	public List<ClientHasPromo> getClientsWithPromoBetweenDates(int localId, String dateIni, String dateFin){
		List<ClientHasPromo> cHdList = session.getCurrentSession().createQuery("FROM ClientHasPromo WHERE id_local="+localId+" AND date BETWEEN '"+dateIni+"' AND '"+dateFin+"' ORDER BY date ASC").list();
		return cHdList;
	}
	
}

package com.tkilas.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.ClientDAO;
import com.tkilas.model.Client;
import com.tkilas.model.Local;

@Repository
public class ClientDAOImpl implements ClientDAO {

	@Autowired
	private SessionFactory session;
	
	@Override
	public Client getClientInfoById(int clientId) {
		// TODO Auto-generated method stub
		return (Client)session.getCurrentSession().get(Client.class, clientId);
	}

}

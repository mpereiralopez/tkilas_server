package com.tkilas.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.DiscountDAO;
import com.tkilas.model.Discount;
import com.tkilas.model.Pack;

@Repository
public class DiscountDAOImpl implements DiscountDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(DiscountDAOImpl.class);

	@Autowired
	private SessionFactory session;

	@Override
	public Discount getDiscountOfDayForLocal(int localId, String dateFormated) {
		// TODO Auto-generated method stub
		return (Discount) session.getCurrentSession().createQuery("FROM Discount WHERE local_user_id_user="+localId+" AND date ='"+dateFormated+"'" ).uniqueResult();
	}
	
	@Override
	public List<Discount> getDiscountListForLocal(int localId){
		
		String today = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		session.getCurrentSession().createQuery("FROM Discount WHERE local_user_id_user="+localId+" AND date>='"+today+"'").list();
		
		return null;
		
	}

	@Override
	public Discount addDiscount(Discount discount) {
		// TODO Auto-generated method stub
		logger.debug("DiscountDAOImpl: addDiscount");
		return (Discount) session.getCurrentSession().save(discount);
	}

}

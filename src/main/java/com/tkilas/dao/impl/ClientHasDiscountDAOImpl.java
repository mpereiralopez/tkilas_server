package com.tkilas.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.ClientDAO;
import com.tkilas.dao.ClientHasDiscountDAO;
import com.tkilas.dao.PackDAO;
import com.tkilas.exceptions.ExceptionSizeLocalLimit;
import com.tkilas.model.Client;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.Pack;

@Repository
public class ClientHasDiscountDAOImpl implements ClientHasDiscountDAO {

	@Autowired
	private SessionFactory session;
	
	@Autowired
	private ClientDAO clientDAO;
	
	@Autowired
	private PackDAO packDAO;
	
	@Override
	public List<ClientHasDiscount> getClientsOfDiscountByLocalId(int localId,
			String today) {
		// TODO Auto-generated method stub
		List<ClientHasDiscount> cHdList = session.getCurrentSession().createQuery("FROM ClientHasDiscount WHERE id_local="+localId+" AND date='"+today+"'").list();
		return cHdList;
	}

	@Override
	public void addDiscountToClient(ClientHasDiscount chd) throws ExceptionSizeLocalLimit {
		// TODO Auto-generated method stub
		//System.out.println(chd.getId().getDiscountPackDate().toString());
		Calendar myCal = new GregorianCalendar();
		myCal.setTime(chd.getId().getDate());
		myCal.get(1);
		String dateForDiscount = new SimpleDateFormat("yyyy-MM-dd").format(chd.getId().getDate());
		Pack p = packDAO.getPackOfLocalDay(chd.getId().getLocalId(), dateForDiscount);
		int ticketsOfLocal = p.getSize();
		
		if(ticketsOfLocal>2){
			p.setSize(ticketsOfLocal-2);
			packDAO.editPack(p.getId());
			session.getCurrentSession().save(chd);
		}else{
			throw new ExceptionSizeLocalLimit("ERROR: YA NO QUEDAN TIKETS");
		}
		
	}
	
	@Override
	public List<ClientHasDiscount> getClientsWithDisscountBetweenDates(int localId, String dateIni, String dateFin){
		List<ClientHasDiscount> cHdList = session.getCurrentSession().createQuery("FROM ClientHasDiscount WHERE id_local="+localId+" AND date BETWEEN '"+dateIni+"' AND '"+dateFin+"' ORDER BY date ASC").list();
		return cHdList;
	}

	@Override
	public List<ClientHasDiscount> getClientsOfDiscountByUserId(int clientId) {
		// TODO Auto-generated method stub
		List<ClientHasDiscount> cHdList = session.getCurrentSession().createQuery("FROM ClientHasDiscount WHERE id_client="+clientId).list();
		return cHdList;
	}

	@Override
	public ClientHasDiscount getDiscountByClientAndLocalAndDate(int clientId,
			int localId, String date) {
		// TODO Auto-generated method stub
		
		ClientHasDiscount chd = (ClientHasDiscount)session.getCurrentSession().createQuery("FROM ClientHasDiscount WHERE id_client="+clientId+" AND id_local="+localId+" AND date='"+date+"'").uniqueResult();
		
		return chd;
	}
	
	

}

package com.tkilas.dao.impl;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.tkilas.dao.LocalDAO;
import com.tkilas.model.ClientHasDiscountPK;
import com.tkilas.model.Local;
import com.tkilas.model.LocalPayInfo;
import com.tkilas.model.User;

@Repository
public class LocalDAOImp implements LocalDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(LocalDAOImp.class);

	
	@Autowired
	private SessionFactory session;

	@Override
	public void addLocal(Local local) {
		// TODO Auto-generated method stub
		logger.debug("LocalDAOimp","Creando local");
		session.getCurrentSession().save(local);
		/** AQUI PREPOPULO LOS DATOS BANCARIOS DEL LOCAL **/
		LocalPayInfo localPayInfo = new LocalPayInfo();
		localPayInfo.setId_local(local.getUserIdUser());
		localPayInfo.setTrade_name(local.getLocal_name());
		localPayInfo.setCif(new String());
		localPayInfo.setContact_address(local.getAddress());
		localPayInfo.setContact_cp(local.getLocal_cp());
		localPayInfo.setContact_city(local.getCity());
		localPayInfo.setContact_IBAN(new String());
		session.getCurrentSession().save(localPayInfo);
	}

	@Override
	public void editLocal(Local local) {
		// TODO Auto-generated method stub
		logger.debug("LocalDAOimp","Editando local");
		session.getCurrentSession().update(local);

	}

	@Override
	public void deleteLocal(int localId) {
		// TODO Auto-generated method stub
		logger.debug("LocalDAOimp","Borrando local");
		session.getCurrentSession().delete(getLocalById(localId));
	}

	@Override
	public Local getLocalById(int localId) {
		// TODO Auto-generated method stub
		logger.debug("LocalDAOimp Buscando Local por ID");

		return (Local)session.getCurrentSession().get(Local.class, localId);
	}

	@Override
	public Local getLocalByName(String localName) {
		// TODO Auto-generated method stub
		logger.debug("LocalDAOimp: Buscando Local por Nombre FROM User WHERE local_name='"+localName+"'");
		return (Local)session.getCurrentSession().createQuery("FROM Local WHERE local_name='"+localName+"'").uniqueResult();
	}

	@Override
	public Local getLocalByUserName(String username) {
		// TODO Auto-generated method stub
		User user = (User)session.getCurrentSession().createQuery("FROM User WHERE email='"+username+"'").uniqueResult();
		return (Local)session.getCurrentSession().createQuery("FROM Local WHERE user_id_user="+user.getIdUser()).uniqueResult();
	}
	
	
	@Override
	public List<Local> getNoConfirmedLocalList(){
		return session.getCurrentSession().createQuery("FROM Local WHERE status=0 OR status=2").list();
	}
	
	@Override
	public List<Local> getAllLocalList(){
		return session.getCurrentSession().createQuery("FROM Local").list();
	}
	
	@Override
	public LocalPayInfo getLocalPayInfoByUserId(int id){
		return (LocalPayInfo)session.getCurrentSession().createQuery("FROM LocalPayInfo WHERE id_local="+id).uniqueResult();
	}
	
	@Override
	public void editLocalPayInfo(LocalPayInfo localPayInfo){
		session.getCurrentSession().update(localPayInfo);
	}
	
	@Override
	public String updateClientReservationStatus (int newStatus, ClientHasDiscountPK pk, int sizeAffected, int type){
		//Update tkilasDDBB.`pack` set counter = counter + 3 where date = '2014-10-10';
		//Update tkilasDDBB.`client_has_disscount` set status =  1 where id_client = 124
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
		String sql = new String();
		Query query = null;	
		int returnValue = -1;
		if(type == 0){
			//TODO PARA DESCUENTOS
			sql = "UPDATE ClientHasDiscount set status = "+newStatus+" where date = '"+		formatoDelTexto.format(pk.getDate())+"' and id_local="+pk.getLocalId()+" and id_client="+pk.getClientId();
			query = session.getCurrentSession().createQuery(sql);
			returnValue = query.executeUpdate();
		}else{
			sql = "UPDATE ClientHasPromo set status = "+newStatus+" where date = '"+		formatoDelTexto.format(pk.getDate())+"' and id_local="+pk.getLocalId()+" and id_client="+pk.getClientId();
			query = session.getCurrentSession().createQuery(sql);
			returnValue = query.executeUpdate();
			
		}

		if(newStatus == 2){
			//Si me mandan un rechazo lo primero es restar al contador global la reserva hecha
			sql="UPDATE Pack set counter = counter - "+sizeAffected+" where date = '"+formatoDelTexto.format(pk.getDate())+"'";
			query = session.getCurrentSession().createQuery(sql);
		}
		
		return returnValue+"";
		
	}
	
	
	//SELECT l.local_name, chd.hour, u.user_name, u.user_surname FROM local l, client_has_disscount chd, user u WHERE chd.id_local = l.user_id_user AND chd.date = '2014-10-10' AND u.id_user = chd.id_client;

	@Override
	public List<Map<String,Object>> getClientDataForMailing(int idClient, String date, int idLocal, int type){
		List<Map<String,Object>> aliasToValueMapList = null;
		Query query = null;
		if(type == 0){
			query = session.getCurrentSession().createSQLQuery("SELECT l.local_name, chd.hour, u.user_name, u.user_surname, u.email FROM local l, client_has_disscount chd, user u WHERE l.user_id_user = "+idLocal+"  AND chd.date = '"+date+"' AND u.id_user = "+idClient+";");
			query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			aliasToValueMapList = query.list();
		}else{
			if(type == 1){
				query = session.getCurrentSession().createSQLQuery("SELECT l.local_name, chpr.hour, u.user_name, u.user_surname, u.email FROM local l, client_has_promo chpr, user u WHERE l.user_id_user = "+idLocal+"  AND chpr.date = '"+date+"' AND u.id_user = "+idClient+";");
				query.setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
				aliasToValueMapList = query.list();
			}
		}
		
		return aliasToValueMapList;
	}
	
	
	public boolean deletePicByPos(int localId, int pos){
		String queryStr = new String();
		switch (pos) {
		case 1:
			queryStr = "UPDATE local SET url_pic1 = NULL WHERE user_id_user = "+localId+";";
			break;
		case 2:
			queryStr = "UPDATE local SET url_pic2 = NULL WHERE user_id_user = "+localId+";";

			break;
		case 3:
			queryStr = "UPDATE local SET url_pic3 = NULL WHERE user_id_user = "+localId+";";

			break;

		default:
			break;
		}
		Query query = session.getCurrentSession().createSQLQuery(queryStr);
		if(query.executeUpdate() == 1){
			return true;
		}else{
			return false;
		}

	}

}

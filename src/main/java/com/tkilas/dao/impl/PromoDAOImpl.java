package com.tkilas.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.Utils.EnumUtils;
import com.tkilas.Utils.EnumUtils.ProductType;
import com.tkilas.Utils.StringTkilasUtils;
import com.tkilas.dao.PackDAO;
import com.tkilas.dao.PromoDAO;
import com.tkilas.model.Pack;
import com.tkilas.model.PackPK;
import com.tkilas.model.Promo;

@Repository
public class PromoDAOImpl implements PromoDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(PromoDAOImpl.class);
	
	@Autowired
	private SessionFactory session;

	@Override
	public Promo addPromo(Promo promo) {
		// TODO Auto-generated method stub
		logger.debug("PackDAOimp: CREO UNA NUEVA PROMO ");
		return (Promo)session.getCurrentSession().save(promo);

	}


	
	

}

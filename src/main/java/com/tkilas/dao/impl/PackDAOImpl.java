package com.tkilas.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.Utils.EnumUtils;
import com.tkilas.Utils.EnumUtils.ProductType;
import com.tkilas.Utils.StringTkilasUtils;
import com.tkilas.dao.ClientDAO;
import com.tkilas.dao.ClientHasDiscountDAO;
import com.tkilas.dao.ClientHasPromoDAO;
import com.tkilas.dao.PackDAO;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientHasPromo;
import com.tkilas.model.Discount;
import com.tkilas.model.Facturacion;
import com.tkilas.model.Pack;
import com.tkilas.model.PackPK;
import com.tkilas.model.PacksCanceled;
import com.tkilas.model.Promo;

@Repository
public class PackDAOImpl implements PackDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(PromoDAOImpl.class);
	
	@Autowired
	private SessionFactory session;
	
	@Autowired
	private ClientHasDiscountDAO clientHasDiscountDAO;
	
	@Autowired
	private ClientHasPromoDAO clientHasPromoDAO;

	@Override
	public void addPack(Pack pack) {
		// TODO Auto-generated method stub
		logger.debug("PackDAOimp: CREO UN PACK NUEVO ");
		session.getCurrentSession().save(pack);


	}

	@Override
	public void deletePack(int localId, String date) {
		// TODO Auto-generated method stub
		logger.debug("PackDAOimp: ELIMINO PACK DE "+localId+" PARA LA FECHA "+date);

		Pack p = getPackOfLocalDay(localId, date);
		
		List<ClientHasDiscount> clientWithDisCount = clientHasDiscountDAO.getClientsOfDiscountByLocalId(localId, date);
		List<ClientHasPromo> clientWithPromo = clientHasPromoDAO.getClientsOfPromoByLocalId(localId, date);
		
		for(int i = 0; i<clientWithDisCount.size(); i++){
			if(clientWithDisCount.get(i).getStatus() != 2){
				PacksCanceled packCancel = new PacksCanceled();
				packCancel.setDate(clientWithDisCount.get(i).getId().getDate());
				packCancel.setId_client(clientWithDisCount.get(i).getId().getClientId());
				packCancel.setId_local(clientWithDisCount.get(i).getId().getLocalId());
				packCancel.setClient_name(clientWithDisCount.get(i).getClient().getUser().getUser_name());
				packCancel.setClient_surname(clientWithDisCount.get(i).getClient().getUser().getUser_surname());
				packCancel.setHour(clientWithDisCount.get(i).getHour());
				packCancel.setSize(clientWithDisCount.get(i).getSize());
				packCancel.setStatus(clientWithDisCount.get(i).getStatus());
			
				packCancel.setDiscount(p.getDiscount().getDiscount());
				session.getCurrentSession().save(packCancel);
			}
		}
		
		for(int i = 0; i<clientWithPromo.size(); i++){
			if(clientWithPromo.get(i).getStatus()!=2){
				PacksCanceled packCancel = new PacksCanceled();
				packCancel.setDate(clientWithPromo.get(i).getId().getDate());
				packCancel.setId_client(clientWithPromo.get(i).getId().getClientId());
				packCancel.setId_local(clientWithPromo.get(i).getId().getLocalId());
				packCancel.setClient_name(clientWithPromo.get(i).getClient().getUser().getUser_name());
				packCancel.setClient_surname(clientWithPromo.get(i).getClient().getUser().getUser_surname());
				packCancel.setHour(clientWithPromo.get(i).getHour());
				packCancel.setSize(clientWithPromo.get(i).getSize());
				packCancel.setStatus(clientWithPromo.get(i).getStatus());
				
				switch (clientWithPromo.get(i).getPromo_index()) {
				case 0:
					packCancel.setPromoType(p.getPromos().get(0).getPromoType());
					packCancel.setPromoSubtype(p.getPromos().get(0).getPromoSubtype());
					packCancel.setPromoSize(p.getPromos().get(0).getPromoSize());
					packCancel.setPromoPvp(p.getPromos().get(0).getPromoPvp());
					packCancel.setPromoMax(p.getPromos().get(0).getPromoMax());

					break;
					
				case 1:
					packCancel.setPromoType(p.getPromos().get(1).getPromoType());
					packCancel.setPromoSubtype(p.getPromos().get(1).getPromoSubtype());
					packCancel.setPromoSize(p.getPromos().get(1).getPromoSize());
					packCancel.setPromoPvp(p.getPromos().get(1).getPromoPvp());
					packCancel.setPromoMax(p.getPromos().get(1).getPromoMax());

					break;

				default:
					break;
				}
				
				session.getCurrentSession().save(packCancel);
			}

		}
		
		session.getCurrentSession().delete(p);

	}

	@Override
	public void editPack(PackPK packId) {
		// TODO Auto-generated method stub

	}

	@Override
	public Pack getPack(PackPK packId) {
		// TODO Auto-generated method stub
		logger.debug("PackDAOimp GETPACK Buscando pack: FROM Pack WHERE local_user_id_user="+packId.getLocalUserIdUser()+" AND date="+packId.getDate());		   
		return (Pack) session.getCurrentSession().createQuery("FROM Pack WHERE local_user_id_user="+packId.getLocalUserIdUser()+" AND date ='"+packId.getDate()+"'" ).uniqueResult();
	}

	@Override
	public List<Pack> getPackList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pack> getPackListOfLocal(int localId, String today) {
		// TODO Auto-generated method stub
		logger.debug("PackDAOimp Buscando pack: FROM Pack WHERE local_user_id_user="+localId+" AND date>"+today);		   
		List<Pack> packList = session.getCurrentSession().createQuery("FROM Pack WHERE local_user_id_user="+localId+" AND date>='"+today+"'").list();
		return packList;
	}

	@Override
	public Pack getPackOfLocalDay(int localId, String today) {
		// TODO Auto-generated method stub
		logger.debug("PackDAOimp Buscando pack: FROM Pack WHERE local_user_id_user="+localId+" AND date="+today);
		Pack pack;
		try{
			pack = (Pack) session.getCurrentSession().createQuery("FROM Pack WHERE local_user_id_user="+localId+" AND date ='"+today+"'" ).uniqueResult();
			
		}catch(java.lang.NullPointerException e){
			logger.debug("PackDAOimp: Exception "+e);
			return null;

		}
		return pack;
	}
	
	@Override
	public Promo getPormoForLocalByDate(int localId, String date){
		logger.debug("getPormoForLocalByDate FROM Pack WHERE local_user_id_user="+localId+" AND date="+date);
		return (Promo)session.getCurrentSession().createQuery("FROM Promo WHERE local_user_id_user="+localId+" AND date ='"+date+"'" ).uniqueResult();
	}
	
	@Override
	public List<PacksCanceled> getListOfCanceledClients (int localId, String date){
		logger.debug("PackDAOimp Buscando pack: FROM PacksCanceled WHERE id_local="+localId+" AND date="+date);		   
		List<PacksCanceled> packList = session.getCurrentSession().createQuery("FROM PacksCanceled WHERE id_local="+localId+" AND date='"+date+"'").list();
		return packList;
	}

	@Override
	public Facturacion getFacturacionDataByDateAndLocalId(int localId, String dateIni, String dateFin) {
		logger.debug("PackDAOimp getFacturacionDataByDateAndLocalId: FROM Facturacion WHERE user_id=" + localId
				+ " AND fecha_emision BETWEEN '" + dateIni + "' AND '" + dateFin + "'");
		Facturacion factura = (Facturacion) session
				.getCurrentSession()
				.createQuery(
						"FROM Facturacion WHERE user_id=" + localId + " AND fecha_emision BETWEEN '" + dateIni
								+ "' AND '" + dateFin + "'").uniqueResult();
		return factura;
	}
	
	
	
	

}

package com.tkilas.dao.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.ParameterMode;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.procedure.ProcedureOutputs;
import org.hibernate.result.Output;
import org.hibernate.result.ResultSetOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.ComercialReportDAO;
import com.tkilas.model.ComercialReportRow;

@Repository
public class ComercialReportDAOImp implements ComercialReportDAO {

	@Autowired
	private SessionFactory session;
	
	@Override
	public List<ComercialReportRow> getComercialReportByFechaIniAndFechaFin(String dateIni, String dateFin) {
		// TODO Auto-generated method stub
		
		
		ProcedureCall procedure = session.getCurrentSession().createStoredProcedureCall( "InformeComercial");
		procedure.registerParameter("fechaIni", String.class, ParameterMode.IN).bindValue(dateIni);
		procedure.registerParameter("fechaFin", String.class, ParameterMode.IN).bindValue(dateFin);

		List<ComercialReportRow> listaFilas = new LinkedList<ComercialReportRow>();
		ProcedureOutputs outputs = procedure.getOutputs();
		Output o = outputs.getCurrent();
		
		List<Object[]> result =((ResultSetOutput) o).getResultList();
		
		//outputs.get
			//List<Object[]> result = query.list();
			SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");

			for(int i=0; i<result.size(); i++){
				Object[] objeto = (Object[])result.get(i);
				ComercialReportRow row = new ComercialReportRow();
				row.setLocalId((int)objeto[0]);
				row.setLocal_name((String)objeto[1]);
				row.setComercial( (String) objeto[2]);
				row.setCreate_time(dateformat.format(objeto[3]));
				row.setNum_reservas(((BigInteger)objeto[4]).intValue());
				row.setNum_total_reservas(((BigInteger)objeto[5]).intValue());
				

				double resultOfDivision=0;
				if(row.getNum_total_reservas() != 0){
					resultOfDivision = (row.getNum_reservas()   / row.getNum_total_reservas()   )*100;
				}
				row.setReservas_percentage(resultOfDivision);
				

				
				row.setIngresos_descuentos_local_periodo(((BigDecimal)objeto[6]).doubleValue());
				row.setIngresos_producto_local_periodo(((BigDecimal)objeto[7]).doubleValue());

				row.setIngresos_local_periodo_total(((BigDecimal)objeto[8]).doubleValue());
				row.setIngresos_periodo_total(((BigDecimal)objeto[9]).doubleValue());
				
				resultOfDivision=0;
				if(row.getNum_total_reservas() != 0){
					resultOfDivision = (row.getIngresos_local_periodo_total() /row.getIngresos_periodo_total())*100;
				}

				row.setIngresos_periodo_percentage_local(resultOfDivision);

				row.setIngresos_acumulado_local_periodo(((BigDecimal)objeto[10]).doubleValue());
				row.setIngresos_acumulado_local_total(((BigDecimal)objeto[11]).doubleValue());
				
				resultOfDivision=0;
				if(row.getNum_total_reservas() != 0){
					resultOfDivision = (row.getIngresos_acumulado_local_periodo()/row.getIngresos_acumulado_local_total())*100;
				}
				
				row.setIngresos_acumulado_local_percentage(resultOfDivision);
				listaFilas.add(row);

			}
		
		return listaFilas;
	}

	
	
	@Override
	public List<Integer>  getRegisteredLocalPerMonthAndHistoricalByDate(String dateIni,
			String dateFin) {
		// TODO Auto-generated method stub
		Query query = session.getCurrentSession().createSQLQuery("SELECT  "
				+ "(SELECT COUNT(DISTINCT id_user) FROM user WHERE create_time BETWEEN '"+dateIni+"' and'"+dateFin+"' AND id_role = 2) as LocalesRegistradosMes,"
				+ "(SELECT COUNT(DISTINCT id_user) FROM user WHERE create_time < '"+dateFin+"' AND id_role = 2) as LocalesRegistradosHistorico");
		List<Object[]> result = query.list();
		List<Integer> returnValues = new LinkedList<Integer>();
		for(int i=0; i<result.size(); i++){
			Object[] objeto = (Object[])result.get(i);
			returnValues.add(((BigInteger)objeto[0]).intValue());
			returnValues.add(((BigInteger)objeto[1]).intValue());

		}
		return returnValues;
	}
	
	@Override
	public List<Integer>  getNumLocalWithOffers(String dateIni, String dateFin) {
		// TODO Auto-generated method stub
		Query query = session.getCurrentSession().createSQLQuery("SELECT  "
				+ "(SELECT COUNT(DISTINCT local_user_id_user) FROM pack WHERE date BETWEEN '"+dateIni+"' and'"+dateFin+"') as LocalWithOfferPeriod,"
				+ "(SELECT COUNT(DISTINCT local_user_id_user) FROM pack WHERE date< '"+dateFin+"') as LocalWithOfferAccumulated");
		List<Object[]> result = query.list();
		List<Integer> returnValues = new LinkedList<Integer>();
		for(int i=0; i<result.size(); i++){
			Object[] objeto = (Object[])result.get(i);
			returnValues.add(((BigInteger)objeto[0]).intValue());
			returnValues.add(((BigInteger)objeto[1]).intValue());

		}
		return returnValues;
	}
	
	@Override
	public List<Integer>  getNumReserves(String dateIni, String dateFin) {
		// TODO Auto-generated method stub
		Query query = session.getCurrentSession().createSQLQuery("SELECT"
				+ "((SELECT COUNT(id_client) FROM client_has_promo WHERE date BETWEEN '"+dateIni+"' and'"+dateFin+"') "
				+ "+ "
				+ "(SELECT COUNT(id_client) FROM client_has_disscount WHERE date BETWEEN '"+dateIni+"' and'"+dateFin+"'))as reservasPeriodo,"
				+ "((SELECT COUNT(id_client) FROM client_has_promo WHERE date <'"+dateFin+"') "
				+ "+ "
				+ "(SELECT COUNT(id_client) FROM client_has_disscount WHERE date <'"+dateFin+"'))as reservasTotal");
		List<Object[]> result = query.list();
		List<Integer> returnValues = new LinkedList<Integer>();
		for(int i=0; i<result.size(); i++){
			Object[] objeto = (Object[])result.get(i);
			returnValues.add(((BigInteger)objeto[0]).intValue());
			returnValues.add(((BigInteger)objeto[1]).intValue());

		}
		return returnValues;
	}
	
	@Override
	public List<Integer>  getNumUsuarios(String dateIni, String dateFin) {
		// TODO Auto-generated method stub
		Query query = session.getCurrentSession().createSQLQuery("SELECT "
				+ "(SELECT COUNT(DISTINCT id_user) FROM user WHERE create_time BETWEEN '"+dateIni+"' and'"+dateFin+"' AND id_role = 3) as usuariosPeriodo,"
				+ "(SELECT COUNT(DISTINCT id_user) FROM user WHERE create_time < '"+dateFin+"' AND id_role = 3) as usuariosTotal");
		List<Object[]> result = query.list();
		List<Integer> returnValues = new LinkedList<Integer>();
		for(int i=0; i<result.size(); i++){
			Object[] objeto = (Object[])result.get(i);
			returnValues.add(((BigInteger)objeto[0]).intValue());
			returnValues.add(((BigInteger)objeto[1]).intValue());

		}
		return returnValues;
	}
	
	
	@Override
	public List<Double> getIngresos(String dateIni, String dateFin) {
		// TODO Auto-generated method stub
		Query query = session.getCurrentSession().createSQLQuery("SELECT("
				+ "SELECT(IFNULL((SELECT SUM(p.promo_pvp*chp.size*0.10) FROM promo p, client_has_promo chp, user u WHERE p.date = chp.date AND p.date BETWEEN '"+dateIni+"' and '"+dateFin+"' AND p.local_user_id_user = chp.id_local AND p.promo_index=chp.promo_index AND p.local_user_id_user = u.id_user AND chp.id_local = u.id_user),0) "
				+ "+ IFNULL((SELECT SUM(CASE WHEN chd.size <= 4 THEN 0.29*chd.size WHEN chd.size > 4 AND chd.size <= 9 THEN 0.39*chd.size WHEN chd.size > 9 THEN 0.49*chd.size END) FROM client_has_disscount chd, user u WHERE chd.date BETWEEN '"+dateIni+"' AND '"+dateFin+"' AND chd.id_local = u.id_user),0))) as IngresosTkilasPorLocalPeriodo	, "
				+ "(SELECT(IFNULL((SELECT SUM(p.promo_pvp*chp.size*0.10) FROM promo p, client_has_promo chp WHERE p.date = chp.date AND p.date < '"+dateFin+"'),0) "
				+ "+ IFNULL((SELECT SUM(CASE WHEN chd.size <= 4 THEN 0.29*chd.size WHEN chd.size > 4 AND chd.size <= 9 THEN 0.39*chd.size WHEN chd.size > 9 THEN 0.49*chd.size END) FROM client_has_disscount chd WHERE chd.date < '"+dateFin+"'),0))) as TotalIngresosPeriodo");
		List<Object[]> result = query.list();
		List<Double> returnValues = new LinkedList<Double>();
		for(int i=0; i<result.size(); i++){
			Object[] objeto = (Object[])result.get(i);
			returnValues.add(((BigDecimal)objeto[0]).doubleValue());
			returnValues.add(((BigDecimal)objeto[1]).doubleValue());

		}
		return returnValues;
	}


	
}

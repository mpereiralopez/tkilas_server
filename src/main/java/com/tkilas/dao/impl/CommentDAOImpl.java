package com.tkilas.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.CommentDAO;
import com.tkilas.model.Comment;
import com.tkilas.model.RecoveryPassAttemp;

@Repository
public class CommentDAOImpl implements CommentDAO {
	
	@Autowired
	private SessionFactory session;

	/**
	 * 
	 */
	private static final long serialVersionUID = -8800206945987073553L;
	
	@Override
	public Comment addAttemp(Comment attemp){
		session.getCurrentSession().save(attemp);
		return attemp;
	}
	
	@Override
	public Comment getAttemp(String fingerPrint){
		return (Comment)session.getCurrentSession().createQuery("FROM Comment WHERE fingerprint='"+fingerPrint+"'").uniqueResult();

	}
	
	@Override
	public boolean updateStatusAttempByUserId(int userId, String fingerPrint){
		return true;
	}
	
	@Override
	public void editComment(Comment c){
		session.getCurrentSession().update(c);
	}
	
	@Override
	public List<Comment> getCommentsByStatus(int status){
		return (List<Comment>)session.getCurrentSession().createQuery("FROM Comment WHERE status="+status).list();
	}
	
	@Override
	public List<Comment> getCommentsOfLocalAndDate(int localId,String date, int from){
		List<Comment> listaComentarios = null;
		if(from == 0)listaComentarios =  (List<Comment>)session.getCurrentSession().createQuery("FROM Comment WHERE local_user_id_user="+localId+" AND response_time <'"+date+"'").list();
		if(from == 1)listaComentarios= (List<Comment>)session.getCurrentSession().createQuery("FROM Comment WHERE local_user_id_user="+localId+" AND response_time >'"+date+"'").list();
		return listaComentarios;
	}


}

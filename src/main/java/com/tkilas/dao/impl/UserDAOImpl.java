package com.tkilas.dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.UserDAO;
import com.tkilas.model.User;

@Repository
public class UserDAOImpl implements UserDAO {

	private static final Logger logger = LoggerFactory.getLogger(UserDAOImpl.class);

	
	@Autowired
	private SessionFactory session;
	
	@Override
	public void addUser(User user) {
		// TODO Auto-generated method stub
		session.getCurrentSession().save(user);
	}
	
	@Override
	public User addUserLocal(User user) {
		// TODO Auto-generated method stub
		session.getCurrentSession().save(user);
		return user;
	}

	@Override
	public void editUser(User user) {
		// TODO Auto-generated method stub
		session.getCurrentSession().update(user);

	}

	@Override
	public void deleteUser(int userId) {
		// TODO Auto-generated method stub
		session.getCurrentSession().delete(getUser(userId));

	}

	@Override
	public User getUser(int userId) {
		// TODO Auto-generated method stub
		logger.debug("##################: Llamada para devolver usuarios por userId en capa DAO");
		return (User)session.getCurrentSession().get(User.class, userId);
	}

	@Override
	public List<User> getUserList() {
		// TODO Auto-generated method stub
		logger.debug("##################: Llamada para devolver todos los usuarios");
		return session.getCurrentSession().createQuery("FROM User").list();
	}

	@Override
	public User getUser(String userLogin) {
		// TODO Auto-generated method stub
		return (User)session.getCurrentSession().createQuery("FROM User WHERE email='"+userLogin+"'").uniqueResult();
	}
	
	
	@Override
	public boolean isEmailFree(String email) {
		// TODO Auto-generated method stub
		boolean aux = false;
		List<Object> list = session.getCurrentSession().createQuery("FROM User WHERE email='"+email+"'").list();
		if(list.size() !=0){
			aux = false;
		}else{
			aux = true;
		}
		return aux;
	}

}

package com.tkilas.dao.impl;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tkilas.dao.RecoveryPassAttempDAO;
import com.tkilas.model.RecoveryPassAttemp;

@Repository
public class RecoveryPassAttempDAOImpl implements RecoveryPassAttempDAO {

	@Autowired
	private SessionFactory session;
	
	@Override
	public RecoveryPassAttemp addAttemp(RecoveryPassAttemp attemp) {
		// TODO Auto-generated method stub
		session.getCurrentSession().save(attemp);
		return attemp;
	}

	@Override
	public RecoveryPassAttemp getAttemp(String fingerPrint) {
		// TODO Auto-generated method stub
		return (RecoveryPassAttemp)session.getCurrentSession().createQuery("FROM RecoveryPassAttemp WHERE fingerprint='"+fingerPrint+"'").uniqueResult();
	}
	
	public boolean updateStatusAttempByUserId (int userId, String fingerPrint){
		String sql = "UPDATE RecoveryPassAttemp set is_done = 1 where id_user = "+userId+" and fingerprint='"+fingerPrint+"'";
		Query query = session.getCurrentSession().createQuery(sql);
		if(query.executeUpdate()==1){
			return true;
		}else{
			return false;
		}
	}

}

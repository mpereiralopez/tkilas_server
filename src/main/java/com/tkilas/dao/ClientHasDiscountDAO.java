package com.tkilas.dao;

import java.util.List;

import com.tkilas.exceptions.ExceptionSizeLocalLimit;
import com.tkilas.model.ClientHasDiscount;

public interface ClientHasDiscountDAO {
	
	public List<ClientHasDiscount> getClientsOfDiscountByLocalId(int localId,String today);
		
	public void addDiscountToClient(ClientHasDiscount chd)throws ExceptionSizeLocalLimit;
	
	public List<ClientHasDiscount> getClientsOfDiscountByUserId(int clientId);
	
	public ClientHasDiscount getDiscountByClientAndLocalAndDate(int clientId, int localId, String date);

	public List<ClientHasDiscount> getClientsWithDisscountBetweenDates(int localId, String dateIni, String dateFin);
}

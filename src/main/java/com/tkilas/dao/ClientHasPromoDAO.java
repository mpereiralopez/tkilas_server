package com.tkilas.dao;

import java.util.List;

import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientHasPromo;

public interface ClientHasPromoDAO {
	
	public List<ClientHasPromo> getClientsOfPromoByLocalId(int localId,String today);
	public List<ClientHasPromo> getClientsOfPromoByLocalIdAndDate(int localId, String today);
	public List<ClientHasPromo> getClientsWithPromoBetweenDates(int localId, String dateIni, String dateFin);
}

package com.tkilas.dao;

import java.util.List;
import java.util.Map;

import com.tkilas.model.ClientHasDiscountPK;
import com.tkilas.model.Local;
import com.tkilas.model.LocalPayInfo;

public interface LocalDAO {
	
	public void addLocal(Local local);
	public void editLocal(Local local);
	public void deleteLocal (int localId);
	public Local getLocalById (int localId);
	public Local getLocalByName (String localName);
	public Local getLocalByUserName (String username);
	
	public List<Local> getNoConfirmedLocalList();
	
	public List<Local> getAllLocalList();
	
	public LocalPayInfo getLocalPayInfoByUserId(int id);
	public void  editLocalPayInfo(LocalPayInfo localPayInfo);
	
	public String updateClientReservationStatus (int newStatus, ClientHasDiscountPK pk,int sizeAffected, int type);
	
	public List<Map<String,Object>> getClientDataForMailing(int idClient, String date, int idLocal, int type);
	
	public boolean deletePicByPos(int localId, int pos);
	
	
}

package com.tkilas.dao;

import java.util.List;

import com.tkilas.model.ComercialReportRow;

public interface ComercialReportDAO {
	
	public List<ComercialReportRow> getComercialReportByFechaIniAndFechaFin(String dateIni,String dateFin);

	public List<Integer> getRegisteredLocalPerMonthAndHistoricalByDate(String dateIni,String dateFin);
	
	public List<Integer>  getNumLocalWithOffers(String dateIni, String dateFin);
	public List<Integer>  getNumReserves(String dateIni, String dateFin);
	public List<Integer>  getNumUsuarios(String dateIni, String dateFin);
	public List<Double> getIngresos(String dateIni, String dateFin);
}

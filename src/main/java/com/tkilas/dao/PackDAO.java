package com.tkilas.dao;

import java.util.Date;
import java.util.List;

import com.tkilas.model.Facturacion;
import com.tkilas.model.Pack;
import com.tkilas.model.PackPK;
import com.tkilas.model.PacksCanceled;
import com.tkilas.model.Promo;

public interface PackDAO {
	
	public void addPack(Pack pack);
	public void deletePack(int localId, String date);
	public void editPack(PackPK packId);
	public Pack getPack(PackPK packId);
	
	public List<Pack> getPackList ();
	
	public List<Pack> getPackListOfLocal (int localId,String today);
	
	public Pack getPackOfLocalDay(int localId, String today);
	public Promo getPormoForLocalByDate(int localId, String date);
	
	public List<PacksCanceled> getListOfCanceledClients (int localId, String date);
	
	
	public Facturacion getFacturacionDataByDateAndLocalId(int localId, String dateIni, String dateFin);
	
}

package com.tkilas.service;

import com.tkilas.model.RecoveryPassAttemp;

public interface RecoveryPassAttempService {

	public RecoveryPassAttemp addAttemp(RecoveryPassAttemp attemp);
	public RecoveryPassAttemp getAttemp(String fingerPrint);
	public boolean updateStatusAttempByUserId(int userId, String fingerPrint);

}

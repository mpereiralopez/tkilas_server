package com.tkilas.service;

import java.util.List;

import com.tkilas.model.User;

public interface UserService {

	public void addUser(User user);
	public User addUserLocal(User user);

	public void editUser(User user);
	public void deleteUser(int userId);
	public User getUser(int userId);
	public User getUser(String userLogin);
	public List<User> getUserList();
	public boolean isEmailFree(String email);
	
}

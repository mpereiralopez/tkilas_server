package com.tkilas.service;

import java.util.List;

import com.tkilas.model.Comment;


public interface SurveyService {

	public Comment addAttemp(Comment attemp);
	public Comment getAttemp(String fingerPrint);
	public boolean updateStatusAttempByUserId(int userId, String fingerPrint);
	public void editComment(Comment c);
	public List<Comment> getCommentsByStatus(int status);
	public List<Comment> getCommentsOfLocalAndDate(int localId,String date, int from);
}

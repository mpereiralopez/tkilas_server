package com.tkilas.service;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import com.tkilas.model.ClientHasDiscountPK;
import com.tkilas.model.Local;
import com.tkilas.model.LocalPayInfo;
import com.tkilas.model.forms.LocalFormAdmin;

public interface LocalServices {
	
	public void addLocal(Local local);
	public void editLocal(Local local);
	public void deleteLocal (int localId);
	public Local getLocalById (int localId);
	public Local getLocalByName (String localName);
	public Local getLocalByUserName (String username);
	public List<Local> getNoConfirmedLocalList();
	public void updateLocal(LocalFormAdmin localFormAdmin);
	public LocalPayInfo getLocalPayInfoByUserId(int id);
	public void editLocalPayInfo(LocalPayInfo localPayInfo);
	public String updateClientReservationStatus (int newStatus, ClientHasDiscountPK pk,int sizeAffected, int type);
	public List<Map<String,Object>> getClientDataForMailing(int idClient, String date, int idLocal, int type);
	public String updateLocalPic(BufferedImage image, int pos, int localId ,HttpServletRequest request);
	public List<Local> getAllLocalList();


}

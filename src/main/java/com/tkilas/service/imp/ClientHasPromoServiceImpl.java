package com.tkilas.service.imp;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.ClientDAO;
import com.tkilas.dao.ClientHasDiscountDAO;
import com.tkilas.dao.ClientHasPromoDAO;
import com.tkilas.dao.DiscountDAO;
import com.tkilas.dao.PackDAO;
import com.tkilas.exceptions.ExceptionSizeLocalLimit;
import com.tkilas.model.Client;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.model.ClientHasDiscountPK;
import com.tkilas.model.ClientHasPromo;
import com.tkilas.model.Discount;
import com.tkilas.model.Pack;
import com.tkilas.model.PackPK;
import com.tkilas.service.ClientHasDiscountService;
import com.tkilas.service.ClientHasPromoService;

@Service
public class ClientHasPromoServiceImpl implements ClientHasPromoService {

	@Autowired
	private ClientHasPromoDAO clientHasPromoDAO;
	
	
	@Transactional
	public List<ClientHasPromo> getClientsOfPromoByLocalId(int localId,String today) {
		// TODO Auto-generated method stub
		return clientHasPromoDAO.getClientsOfPromoByLocalId(localId, today);
	}

	
	@Transactional
	public List<ClientHasPromo> getClientsOfPromoByLocalIdAndDate(int localId,String today) {
		// TODO Auto-generated method stub
		return clientHasPromoDAO.getClientsOfPromoByLocalIdAndDate(localId, today);
	}
	
	@Transactional
	public List<ClientHasPromo> getClientsWithPromoBetweenDates(int localId, String dateIni, String dateFin){
		return clientHasPromoDAO.getClientsWithPromoBetweenDates(localId, dateIni, dateFin);
	}


}

package com.tkilas.service.imp;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.LocalDAO;
import com.tkilas.dao.UserDAO;
import com.tkilas.model.ClientHasDiscountPK;
import com.tkilas.model.Local;
import com.tkilas.model.LocalPayInfo;
import com.tkilas.model.User;
import com.tkilas.model.forms.LocalFormAdmin;
import com.tkilas.service.LocalServices;

@Service
public class LocalServicesImpl implements LocalServices {
	
	private static final Logger logger = LoggerFactory.getLogger(LocalServicesImpl.class);
	
	@Autowired
	private LocalDAO localDAO;
	
	@Autowired
	private UserDAO userDAO;

	@Transactional
	public void addLocal(Local local) {
		// TODO Auto-generated method stub
		localDAO.addLocal(local);

	}

	@Transactional
	public void editLocal(Local local) {
		// TODO Auto-generated method stub
		localDAO.editLocal(local);
	}

	@Transactional
	public void deleteLocal(int localId) {
		// TODO Auto-generated method stub
		localDAO.deleteLocal(localId);
	}

	@Transactional
	public Local getLocalById(int localId) {
		// TODO Auto-generated method stub
		return localDAO.getLocalById(localId);
	}

	@Transactional
	public Local getLocalByName(String localName) {
		// TODO Auto-generated method stub
		return localDAO.getLocalByName(localName);
	}

	@Transactional
	public Local getLocalByUserName(String username) {
		// TODO Auto-generated method stub
		return localDAO.getLocalByUserName(username);
	}
	
	@Transactional
	public List<Local> getNoConfirmedLocalList(){
		return localDAO.getNoConfirmedLocalList();
	}
	
	@Transactional
	public void updateLocal(LocalFormAdmin localFormAdmin){
		int localId = localFormAdmin.getUserIdUser();
		Local local = localDAO.getLocalById(localId);
		local.setLocal_name(localFormAdmin.getLocal_name());
		local.setTlf(localFormAdmin.getTlf());
		local.setAddress(localFormAdmin.getAddress());
		local.setLocal_cp(localFormAdmin.getLocal_cp());
		local.setCity(localFormAdmin.getCity());
		local.setWeb(localFormAdmin.getWeb());
		local.setCapacity(localFormAdmin.getCapacity());
		local.setDistrict(localFormAdmin.getDistrict());
		local.setLocal_type(localFormAdmin.getLocal_type());
		local.setProduct_type_local(localFormAdmin.getProduct_type_local());
 
		local.setComercial(localFormAdmin.getComercial());
		local.setLocal_latitud(localFormAdmin.getLocal_latitud());
		local.setLocal_longitud(localFormAdmin.getLocal_longitud());
		local.setLocal_desc(localFormAdmin.getLocal_desc());
		local.setLocal_pay_way(localFormAdmin.getLocal_pay_way());
		
		local.setLocal_cost_alch_bottle(localFormAdmin.getLocal_cost_alch_bottle());
		local.setLocal_cost_beer(localFormAdmin.getLocal_cost_beer());
		local.setLocal_cost_cocktail(localFormAdmin.getLocal_cost_cocktail());
		local.setLocal_cost_coffe(localFormAdmin.getLocal_cost_coffe());
		local.setLocal_cost_tonic(localFormAdmin.getLocal_cost_tonic());
		local.setLocal_cost_wine_bottle(localFormAdmin.getLocal_cost_wine_bottle());
		LocalPayInfo localPayInfo  = localDAO.getLocalPayInfoByUserId(localId);
		localPayInfo.setContact_IBAN(localFormAdmin.getLocal_iban());
		//local.setUrl_pic1(localFormAdmin.getUrl_pic1());
		//local.setUrl_pic2(localFormAdmin.getUrl_pic2());
		//local.setUrl_pic3(localFormAdmin.getUrl_pic3());
		//local.getUser()
		//Ahora acepto el estatus
		local.setStatus(1);
		localDAO.editLocal(local);
		localDAO.editLocalPayInfo(localPayInfo);
		
		User u = userDAO.getUser(local.getUserIdUser());
		u.setUser_name(localFormAdmin.getUser_name());
		u.setUser_surname(localFormAdmin.getUser_surname());
		userDAO.editUser(u);
		

	}

	@Transactional
	public String updateLocalPic(BufferedImage image, int pos, int localId ,HttpServletRequest request) {
		// TODO Auto-generated method stub
		String name = new String();
		switch (pos) {
		case 1:
			name = "pic1.png";
			break;
		case 2:
			name = "pic2.png";
			break;
		case 3:
			name = "pic3.png";
			break;
		default:
			break;
		}
		
		
		
        if (image!= null) {
            try {
                // Creating the directory to store file
                String rootPath = File.listRoots()[0].getAbsolutePath();
                String sSistemaOperativo = System.getProperty("os.name");
                if(sSistemaOperativo.contains("Windows")){
                	rootPath = new String("C:");
                }
                File dir = new File(rootPath  + File.separator + "usr"+ File.separator +"share"+ File.separator +"tomcat7"+ File.separator+"images"+ File.separator + localId);
                if (!dir.exists())
                    dir.mkdirs();
                // Create the file on server
                File serverFile = new File(dir + File.separator + name);
        		boolean result = ImageIO.write(image, "png", serverFile);
               
                logger.info("Server File Location=" + serverFile.getPath());
                //return "You successfully uploaded file=" + name;
                String aux = request.getContextPath()+"/images/"+localId+'/'+name;
        		
                Local local = localDAO.getLocalById(localId);
                
                switch (pos) {
        		case 1:
                    local.setUrl_pic1(aux);
        			break;
        		case 2:
                    local.setUrl_pic2(aux);
        			break;
        		case 3:
                    local.setUrl_pic3(aux);
        			break;
        		default:
        			break;
        		}
                
                localDAO.editLocal(local);

                
                return aux;

            } catch (Exception e) {
                logger.info("You failed to upload " + name + " => " + e.getMessage());

                //return "You failed to upload " + name + " => " + e.getMessage();
        		return "Error al subir la imagen";

            }
        } else {
            //Si el fichero esta vacio ---> borramos
        	String rootPath = File.listRoots()[0].getAbsolutePath();
        	 String sSistemaOperativo = System.getProperty("os.name");
             if(sSistemaOperativo.contains("Windows")){
             	rootPath = new String("C:");
             }
             
        	File dir = new File(rootPath  + File.separator + "usr"+ File.separator +"share"+ File.separator +"tomcat7"+ File.separator+"images"+ File.separator + localId);
            System.out.println(dir.getAbsolutePath());

        	File serverFile = new File(dir + File.separator + name);
            
            boolean result = localDAO.deletePicByPos(localId, pos);
            
            if(serverFile.delete() && result){
            	return "Imagen borrada correctamente";
            }else{
            	return "Error al borrar la imagen";
            }

            

        }
	}
	
	@Transactional
	public LocalPayInfo getLocalPayInfoByUserId(int id){
		return localDAO.getLocalPayInfoByUserId(id);
	}
	
	@Transactional
	public void editLocalPayInfo(LocalPayInfo localPayInfo){
		localDAO.editLocalPayInfo(localPayInfo);
	}
	
	@Transactional
	public String updateClientReservationStatus (int newStatus, ClientHasDiscountPK pk,int sizeAffected, int type){
		return localDAO.updateClientReservationStatus(newStatus, pk, sizeAffected,  type);
	}
	
	@Transactional
	public List<Map<String,Object>> getClientDataForMailing(int idClient, String date, int idLocal, int type){
		return localDAO.getClientDataForMailing(idClient, date, idLocal, type);
	}

	@Transactional
	public List<Local> getAllLocalList(){
		return localDAO.getAllLocalList();
	}

}

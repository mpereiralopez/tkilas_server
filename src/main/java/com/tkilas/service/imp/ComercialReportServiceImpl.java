package com.tkilas.service.imp;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.ComercialReportDAO;
import com.tkilas.dao.DiscountDAO;
import com.tkilas.model.ComercialReportRow;
import com.tkilas.service.ComercialReportService;

@Service
public class ComercialReportServiceImpl implements ComercialReportService {

	@Autowired
	ComercialReportDAO comercialReportDAO;
	
	@Transactional
	public List<ComercialReportRow> getComercialReportByFechaIniAndFechaFin(String dateIni, String dateFin) {
		// TODO Auto-generated method stub
		return comercialReportDAO.getComercialReportByFechaIniAndFechaFin(dateIni, dateFin);
	}
	
	@Transactional
	public List<Integer> getRegisteredLocalPerMonthAndHistoricalByDate(String dateIni,String dateFin){
		return comercialReportDAO.getRegisteredLocalPerMonthAndHistoricalByDate(dateIni, dateFin);
	}
	
	@Transactional
	public List<Integer>  getNumLocalWithOffers(String dateIni, String dateFin){
		return comercialReportDAO.getNumLocalWithOffers(dateIni, dateFin);
	}
	
	@Transactional
	public List<Integer>  getNumReserves(String dateIni, String dateFin){
		return comercialReportDAO.getNumReserves(dateIni, dateFin);
	}
	
	@Transactional
	public List<Integer>  getNumUsuarios(String dateIni, String dateFin){
		return comercialReportDAO.getNumUsuarios(dateIni, dateFin);
	}
	
	@Transactional
	public List<Double> getIngresos(String dateIni, String dateFin){
		return comercialReportDAO.getIngresos(dateIni, dateFin);
	}

}

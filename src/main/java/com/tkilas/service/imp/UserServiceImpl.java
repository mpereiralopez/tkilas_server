package com.tkilas.service.imp;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.UserDAO;
import com.tkilas.model.User;
import com.tkilas.service.UserService;


@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserDAO userDAO;
	
	@Transactional
	public void addUser(User user) {
		// TODO Auto-generated method stub
		userDAO.addUser(user);
	}

	@Transactional
	public void editUser(User user) {
		// TODO Auto-generated method stub
		userDAO.editUser(user);
	}

	@Transactional
	public void deleteUser(int userId) {
		// TODO Auto-generated method stub
		userDAO.deleteUser(userId);
	}

	@Transactional
	public User getUser(int userId) {
		// TODO Auto-generated method stub
		logger.debug("##################: Llamada para devolver usuarios por userId");
		logger.debug("##################: Llamada para devolver usuarios por userId "+userDAO.getUser(userId).getRole().getName());
		return userDAO.getUser(userId);
	}

	@Transactional
	public List<User> getUserList() {
		// TODO Auto-generated method stub
		return userDAO.getUserList();
	}

	@Transactional
	public User getUser(String userLogin) {
		// TODO Auto-generated method stub
		logger.debug("##################: Llamada para devolver usuarios por login");
		return userDAO.getUser(userLogin);
	}

	@Transactional
	public User addUserLocal(User user) {
		// TODO Auto-generated method stub
		return userDAO.addUserLocal(user);
	}

	@Transactional
	public boolean isEmailFree(String email) {
		// TODO Auto-generated method stub
		return userDAO.isEmailFree(email);
	}
	
	

}

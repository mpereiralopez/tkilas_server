package com.tkilas.service.imp;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.UserDAO;
import com.tkilas.dao.impl.RecoveryPassAttempDAOImpl;
import com.tkilas.model.RecoveryPassAttemp;
import com.tkilas.service.RecoveryPassAttempService;

@Service
public class RecoveryPassAttempServiceImpl implements RecoveryPassAttempService {

	@Autowired
	private RecoveryPassAttempDAOImpl recoveryPassAttempDAO;
	
	
	@Transactional
	public RecoveryPassAttemp addAttemp(RecoveryPassAttemp attemp) {
		// TODO Auto-generated method stub
		return recoveryPassAttempDAO.addAttemp(attemp);
	}

	@Transactional
	public RecoveryPassAttemp getAttemp(String fingerPrint) {
		// TODO Auto-generated method stub
		return recoveryPassAttempDAO.getAttemp(fingerPrint);
	}
	
	@Transactional
	public boolean updateStatusAttempByUserId (int userId, String fingerPrint){
		return recoveryPassAttempDAO.updateStatusAttempByUserId(userId,fingerPrint);
	}

}

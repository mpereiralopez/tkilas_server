package com.tkilas.service.imp;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.CommentDAO;
import com.tkilas.dao.UserDAO;
import com.tkilas.dao.impl.RecoveryPassAttempDAOImpl;
import com.tkilas.model.Comment;
import com.tkilas.model.RecoveryPassAttemp;
import com.tkilas.service.RecoveryPassAttempService;
import com.tkilas.service.SurveyService;

@Service
public class SurveyServiceImpl implements SurveyService {

	@Autowired
	private CommentDAO commentDAO;;
	
	
	
	
	@Transactional
	public boolean updateStatusAttempByUserId (int userId, String fingerPrint){
		return commentDAO.updateStatusAttempByUserId(userId,fingerPrint);
	}

	@Transactional
	public Comment addAttemp(Comment attemp) {
		// TODO Auto-generated method stub
		return commentDAO.addAttemp(attemp);
	}

	@Transactional
	public Comment getAttemp(String fingerPrint) {
		// TODO Auto-generated method stub
		return commentDAO.getAttemp(fingerPrint);
	}
	
	@Transactional
	public void editComment(Comment c){
		commentDAO.editComment(c);
	}
	
	@Transactional
	public List<Comment> getCommentsByStatus(int status){
		return commentDAO.getCommentsByStatus(status);
	}
	
	@Transactional
	public List<Comment> getCommentsOfLocalAndDate(int localId,String date, int from){
		return commentDAO.getCommentsOfLocalAndDate(localId, date, from);
	}


}

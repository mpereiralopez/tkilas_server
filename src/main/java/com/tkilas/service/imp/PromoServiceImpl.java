package com.tkilas.service.imp;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.DiscountDAO;
import com.tkilas.dao.PromoDAO;
import com.tkilas.model.Discount;
import com.tkilas.model.Promo;
import com.tkilas.service.DiscountService;
import com.tkilas.service.PromoService;


@Service
public class PromoServiceImpl implements PromoService{
	
	@Autowired
	PromoDAO promoDAO;
	
	

	@Transactional
	public Promo addPromo(Promo promo) {
		// TODO Auto-generated method stub
		return promoDAO.addPromo(promo);
	}

}

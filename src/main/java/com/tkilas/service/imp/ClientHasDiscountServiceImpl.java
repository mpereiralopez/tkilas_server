package com.tkilas.service.imp;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tkilas.dao.ClientHasDiscountDAO;
import com.tkilas.model.ClientHasDiscount;
import com.tkilas.service.ClientHasDiscountService;

@Service
public class ClientHasDiscountServiceImpl implements ClientHasDiscountService {

	@Autowired
	private ClientHasDiscountDAO clientHasDiscountDAO;
	
	
	@Transactional
	public List<ClientHasDiscount> getClientsOfDiscountByLocalId(int localId,String today) {
		// TODO Auto-generated method stub
		return clientHasDiscountDAO.getClientsOfDiscountByLocalId(localId, today);
	}

	@Transactional
	public List<ClientHasDiscount> getClientsOfDiscountByUserId(int clientId) {
		// TODO Auto-generated method stub
		return clientHasDiscountDAO.getClientsOfDiscountByUserId(clientId);
	}
	
	@Transactional
	public ClientHasDiscount getDiscountByClientAndLocalAndDate(int clientId,
			int localId, String date){
		return clientHasDiscountDAO.getDiscountByClientAndLocalAndDate(clientId, localId, date);
	}

	@Transactional
	public List<ClientHasDiscount> getClientsWithDisscountBetweenDates(int localId, String dateIni, String dateFin) {
		// TODO Auto-generated method stub
		return clientHasDiscountDAO.getClientsWithDisscountBetweenDates(localId, dateIni, dateFin);
	}
	
	

}

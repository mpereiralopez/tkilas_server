package com.tkilas.service;

import java.util.List;

import com.tkilas.model.ClientHasDiscount;

public interface ClientHasDiscountService {
	
	public List<ClientHasDiscount> getClientsOfDiscountByLocalId(int localId,String today);
	public List<ClientHasDiscount> getClientsOfDiscountByUserId(int clientId);
	public ClientHasDiscount getDiscountByClientAndLocalAndDate(int clientId,
			int localId, String date);
	
	public List<ClientHasDiscount> getClientsWithDisscountBetweenDates(int localId, String dateIni, String dateFin);

}

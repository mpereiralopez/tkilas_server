package com.tkilas.config;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.tkilas.model.Role;
import com.tkilas.model.User;

public class SecurityUser extends User implements UserDetails {

	private static final long serialVersionUID = 1L;

	public SecurityUser(User user) {
		if (user != null) {
			this.setIdUser(user.getIdUser());
			this.setEmail(user.getEmail());
			this.setRole(user.getRole());
		}
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		Role userRole = this.getRole();
		if (userRole != null) {
			SimpleGrantedAuthority authority = new SimpleGrantedAuthority(userRole.getName());
			authorities.add(authority);
		}
		return authorities;

	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return super.getPassword();

	}

	@Override
	public String getUsername() {
		return super.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}

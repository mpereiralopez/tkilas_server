package com.tkilas.config;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tkilas.model.Role;
import com.tkilas.model.User;
import com.tkilas.service.UserService;

@Service("userDetailsService")
@Transactional(readOnly = false)
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		User user = userService.getUser(username);
		if (user == null) {
			throw new UsernameNotFoundException("No such user: " + username);
		} else if (user.getRole()==null) {
	        throw new UsernameNotFoundException("User " + username + " has no authorities");
        }
		
		//Modifico el lastTimeAccess
		Date date = new Date();
		Timestamp tms = new Timestamp(date.getTime());
		user.setLastTimeAccess(tms);
		userService.editUser(user);
		
		boolean enabled = true;
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = true;
		
		Set<Role> roles = new HashSet<Role>(0);
		roles.add(user.getRole());
		org.springframework.security.core.userdetails.User u = new org.springframework.security.core.userdetails.User(username, user.getPassword(), enabled, accountNonExpired,credentialsNonExpired, accountNonLocked, getAuthorities(roles));
		return u;
	}
	
	public List<String> getRolesAsList(Set<Role> roles) {
	    List <String> rolesAsList = new ArrayList<String>();
	    for(Role role : roles){
	        rolesAsList.add(role.getName());
	    }
	    return rolesAsList;
	}

	public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
	    List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
	    for (String role : roles) {
	        authorities.add(new SimpleGrantedAuthority(role));
	    }
	    return authorities;
	}

	public Collection<? extends GrantedAuthority> getAuthorities(Set<Role> roles) {
	    List<GrantedAuthority> authList = getGrantedAuthorities(getRolesAsList(roles));
	    return authList;
	}
	

}

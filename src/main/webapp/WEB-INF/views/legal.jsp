<%@ include file="init.jsp" %>

<title><spring:message code="title.normal"/></title>
<link rel="stylesheet" href="resources/css/style.css" />
<script type="text/javascript">

$(function() {
    $( "#dialog_contact_form" ).dialog({
        autoOpen: false,
        modal:true,
        minWidth: 500,
        
    });
    
    
    $( "#contact_box" ).click(function() {
        $( "#dialog_contact_form" ).dialog( "open" );
      });
  });

</script>
<style type="text/css">
h1 {
	padding-left: 5%;
	color: #8be029;
	font-size: 1.5em;
	-webkit-margin-before: 0.75em;
	-webkit-margin-after: 0.75em;
	margin-top: 0.75em;
	margin-bottom: 0.75em;
}
h2 {
	color: gray;
	padding-left: 5%;
	padding-right: 5%;
	font-family: Aller;
	font-size: 1.2em;
	-webkit-margin-before: 0.5em;
	-webkit-margin-after: 0.5em;
	margin-top: 0.5em;
	margin-bottom: 0.5em;
	
}
p {
	padding-left: 5%;
	padding-right: 5%;
	text-align: justify;
	font-size: 1em;
	-webkit-margin-before: 0.3em;
	-webkit-margin-after: 0.3em;
	margin-top: 0.3em;
	margin-bottom: 0.3em;
}
a{
	font-family:Aller;
	font-size: 1em;
}
ul {
	padding-left: 5%;
	padding-right: 5%;
	text-align: justify;
	font-family: Aller;
	font-size: 1em;
}

table{
	padding-left: 5%;
	padding-right: 5%;
	
}

.cookies th {
    border-right: 1px solid #CCCCCC;
    padding: 5px 10px;
    text-align: center;
    width: 50%;
    border-bottom: 1px solid #CCCCCC;
    font-size: 1em;
    font-weight: bold;
    background-color: #F2F2F2;
   	font-family:Aller;
    
}
.cookies tr td{
    border-right: 1px solid #CCCCCC;
    padding: 5px 10px;
    text-align: center;
	width: 50%;
    border-bottom: 1px solid #CCCCCC;
    font-size: 1em;
    font-family: Aller;
    
	
}
/* ============================== Pol�tica de COOKIES =========================== */

.cookies th {    
    width: auto;
}
  
.cookies tr td{
  width: auto;
}
.cookies tr td{
    width: auto;
	text-align: left;
}
.cookies th.propias{
	background-color:rgb(141, 210, 0);
}
.cookies th.terceros{
	background-color:rgb(140, 210, 0);
}
.cookies tr td.necesarias{
	background-color:#F2F2F2;
}
.cookies tr td.analiticas{ 
	background-color:#F2F2F2;
}
.cookies tr td.analiticasBis{
	background-color:#F2F2F2;
}
</style>


</head>
<body>

		<%@ include file="header_normal_template.jsp" %>

	<div id="legal_text_container"
		style="width: 100%; height: 88%; top: 8%; overflow-y: scroll; position: absolute;">
		<h1>MENCIONES LEGALES Y CONDICIONES GENERALES DE UTILIZACI�N DE
			LOS SERVICIOS PROPUESTOS EN www.tkilas.com</h1>

		<p>La web <a href="http://www.tkilas.com" target="_blank">"http://www.tkilas.com"</a> est� editada por:</p>

		<h2>Tkcitysolutions SL</h2>

		<p>Atenci�n: si no est� de acuerdo con las condiciones generales
			de utilizaci�n que aqu� se describen o con una parte de ellas, le
			recomendamos que no utilice el portal www.tkilas.com</p>

		<p>Las presentes menciones legales y condiciones generales tienen
			como objeto definir las condiciones y modalidades de la puesta en
			funcionamiento de un servicio gratuito de b�squeda de promociones en
			bebidas en establecimientos. Las presentes condiciones generales
			pueden ser completadas o modificadas, si fuera necesario, para
			incluir condiciones espec�ficas de utilizaci�n.</p>
		<p>El servicio est� dedicado a personas f�sicas, capaces de
			suscribir un contrato de derecho en espa�ol. Est� considerado como
			usuario de la p�gina Web www.tkilas.com, toda persona que visite la
			Web y/o utilice la Web y los servicios asociados a la misma.</p>

		<h1>ACEPTACI�N DE LAS CONDICIONES GENERALES</h1>

		<p>TK CITY SOLUTIONS.SL propone un servicio gratuito de b�squeda
			de promociones en bebidas, bajo la condici�n de aceptaci�n
			incondicional de las presentes condiciones generales.</p>

		<p>El usuario declara y reconoce haber le�do �ntegramente los
			t�rminos de uso de las presentes condiciones generales. Por otro
			lado, la conexi�n a uno cualquiera de los servidores propuestos est�
			disponible en la siguiente direcci�n: http://www.tkilas.com (de ahora
			en adelante tkilas.com) conlleva la aceptaci�n de las presentes
			condiciones generales por parte del usuario. La sociedad TK CITY
			SOLUTIONS.SL se reserva el derecho a modificar, en su totalidad o en
			parte, las presentes condiciones generales. En consecuencia es el
			usuario el que debe consultar regularmente la �ltima versi�n de las
			mismas publicada en la direcci�n http://www.tkilas.com/legal.</p>

		<p>El usuario aceptar� la versi�n actualizada de las condiciones
			generales en cada nueva conexi�n a la p�gina web. En caso de que el
			usuario no respete las presentes condiciones generales, la sociedad
			TK CITY SOLUTIONS.SL se reserva el derecho de suspender sin previo
			aviso la cuenta del cliente y podr� denegarle el acceso al servicio.</p>

		<h1>DESCRIPCI�N DEL SERVICIO</h1>

		<p>La p�gina web tkilas.com permite al usuario buscar y reservar
			promociones en bebidas a tiempo real en establecimientos. La p�gina
			web tkilas.com permite al usuario beneficiarse de estas ofertas y
			promociones si reserva a trav�s del procedimiento online previsto en
			la web. Las condiciones de validez de estas promociones se
			especifican en la p�gina web.</p>


		<h2>ADVERTENCIA</h2>

		<p>Corresponde al usuario verificar todas las informaciones que
			parezcan necesarias y oportunas antes de proceder a efectuar una
			reserva en cualquiera de los estabecimientos presentes en la p�gina
			web tkilas.com</p>

		<p>La sociedad TK CITY SOLUTIONS.SL no garantiza de ninguna manera
			los productos, servicios y/o pr�cticas comerciales presentes en la
			web. Del mismo modo la sociedad TK CITY SOLUTIONS.SL no garantiza que
			el usuario quede satisfecho con los productos, pr�cticas y/o
			servicios obtenidos a partir de una reserva a trav�s de la p�gina web
			tkilas.com</p>

		<h1>GRATUIDAD DEL SERVICIO</h1>

		<p>Los servicios propuestos en las presentes condiciones generales
			son gratuitos. Eventualmente se pueden aplicar tarifas en funci�n de
			la evoluci�n de los servicios propuestos, la evoluci�n de la red, la
			t�cnica y/o las condiciones legales. El usuario ser� debidamente
			informado de las posibles modificaciones de las presentes condiciones
			generales a trav�s de la p�gina tkilas.com.</p>

		<p>Conforme a las presentes condiciones generales la utilizaci�n
			de la p�gina web tkilas.com es gratuita para los usuarios.</p>

		<p>En cualquier caso, el usuario acepta que la p�gina web
			tkilas.com ofrece a sus usuarios un servicio gratuito de b�squeda y
			reserva de promociones en bebidas. El usuario acepta que el servicio
			ofrecido por el establecimiento se debe pagar.</p>


		<p>La sociedad TKCITY SOLUTIONS.SL contiene informaci�n acerca de
			los precios correspondientes a los prestatarios del servicio
			(establecimientos). Estas informaciones se incluyen a titulo
			informativo. En ning�n caso la sociedad TK CITY SOLUTIONS.SL
			garantiza la exactitud de estas informaciones.</p>


		<p>Por otra parte, la p�gina tkilas.com puede contener enlaces
			hacia otras p�ginas web de terceros, las cuales son explotadas por
			empresas independientes de la sociedad TK CITY SOLUTIONS.SL Puede
			ocurrir que un tercero reclame al usuario un pago por la utilizaci�n
			de sus servicios en su p�gina web. Corresponde al usuario efectuar
			las comprobaciones oportunas para verificar si estos servicios son
			remunerados antes de continuar la transacci�n con un tercero. En
			ning�n caso la sociedad TK CITY SOLUTIONS.SL podr� ser asociada con
			prestaciones efectuadas por terceros y/o a los sitios web de terceros
			en cuesti�n.</p>

		<h1>APERTURA DE CUENTA - IDENTIFICACI�N- INTERCAMBIO DE
			INFORMACIONES</h1>

		<p>Cuando el usuario crea su cuenta se atribuye un nombre y una
			contrase�a (de ahora en adelante identificadores) que le permiten
			acceder a su cuenta privada</p>

		<h2>Confidencialidad de los identificadores</h2>


		<p>Los identificadores son personales y confidenciales. Solo
			pueden ser cambiados bajo petici�n del usuario o por iniciativa de la
			sociedad TK CITY SOLUTIONS.SL El usuario es enteramente responsable
			de la utilizaci�n de sus identificadores y se compromete a conservar
			en secreto los mismos y no divulgarlos de manera alguna.</p>


		<p>En caso de extrav�o o robo de los identificadores, el usuario
			ser� responsable de la p�rdida.</p>

		<h2>Acuerdo sobre la prueba</h2>

		<p>Las partes acuerdan expresamente que:</p>
		<ul>
			<li>La presencia de un c�digo de identificaci�n identifica de
				manera v�lida al autor de un documento o un mensaje y establece la
				autenticidad de ese documento o mensaje.</li>
			<li>Un documento electr�nico que contenga un c�digo de
				identificaci�n equivale a un escrito firmado por la persona el
				emisor</li>

			<li>Las partes pueden valerse de la impresi�n en papel de un
				mensaje electr�nico para poder comprobar los cambios contenidos en
				las presentes condiciones generales.</li>
		</ul>

		<h2>Intercambio de informaciones:</h2>

		<p>El usuario acepta el uso de la mensajer�a electr�nica para la
			transmisi�n de informaciones que le son necesarias y que conciernen a
			la conclusi�n o ejecuci�n de un contrato.</p>

		<h1>PROTECCI�N DE LOS DATOS PERSONALES</h1>

		<p>La sociedad TK CITY SOLUTIONS.SL, dentro del estricto respecto
			a las leyes y reglamentos vigentes, desea recopilar informaciones
			sobre sus usuarios. Estas informaciones son recogidas de forma
			conforme a las disposiciones relativas a la protecci�n de datos
			personales, y est�n destinadas a un uso personalizado y optimizado de
			tkilas.com.</p>

		<h2>Declaraci�n de tratamiento automatizado de las informaciones
			personales de la AGPD</h2>

		<p>La sociedad TK CITY SOLUTIONS.SL que trata datos de car�cter
			personal, tiene su base del sitio web tkilas.com declarada en Agencia
			Nacional de Protecci�n de Datos y hace uso de ella conforme a los
			reglamentos.</p>


		<p>Normas sobre la vida privada</p>

		<p>La recogida de datos de car�cter personal no permite de ning�n
			modo publicar directa o indirectamente, los or�genes �tnicos, las
			opiniones pol�ticas, filos�ficas o religiosas o la pertenencia a
			sindicatos de las personas, ni de los datos relativos a la salud o la
			vida sexual de los mismos.</p>


		<p>Los datos concernientes a los usuarios son recogidos y tratados
			de manera legal y l�cita para una finalidad determinada, explicita y
			legitima, sin ser tratadas de forma incompatible con esas
			finalidades. Esto permite identificar a las personas a las que
			conciernen estas informaciones durante un periodo de tiempo que no
			exceda la duraci�n necesaria de las finalidades para las que son
			recogidas y tratadas.</p>


		<p>Los datos de car�cter personal no ser�n tratados por terceros
			subcontratados o personas que act�en bajo la autoridad del
			responsable del tratamiento de estos datos, a excepci�n de
			instrucciones del responsable de ese tratamiento dentro de la
			sociedad TK CITY SOLUTIONS.SL</p>

		<h2>Utilizaci�n de los datos</h2>

		<p>El usuario es informado por las presentes condiciones generales
			de que los datos de car�cter personal se�alizados como obligatorios
			en los formularios y que son recogidos dentro del cuadro del servicio
			descrito en las presentes condiciones generales, son necesarios para
			la utilizaci�n del servicio y son utilizados �nicamente dentro del
			cuadro de servicio descrito anteriormente y destinados exclusivamente
			a la sociedad TK CITY SOLUTIONS.SL y sus asociados establecimientos
			que pondr�n atenci�n a fin de preservar, en la medida de lo posible,
			la seguridad de los datos personales. El usuario autoriza a la
			sociedad TK CITY SOLUTIONS.SL a proporcionar ciertas informaciones a
			sus servidores t�cnicos, con el fin de que el usuario se pueda
			beneficiar de ciertas funciones de la p�gina Web (foro, opiniones,
			comentarios, etc.)</p>

		<p>El usuario autoriza a la sociedad TK CITY SOLUTIONS.SL a
			facilitar todas las informaciones que le conciernen a un asociado
			establecimiento de la sociedad TK CITY SOLUTIONS.SL Los clientes de
			TK CITY SOLUTIONS.SL ., en lo que se refiere a la funci�n de �
			invitar acompa�antes a una reserva �, pueden comunicar a TK CITY
			SOLUTIONS.SL las direcciones de email de terceros.</p>

		<p>Los clientes de TK CITY SOLUTIONS.SL se comprometen a comunicar
			estas direcciones previo consentimiento expl�cito e informado de sus
			propietarios. En consecuencia, el cliente exime a TK CITY
			SOLUTIONS.SL de toda responsabilidad en lo que se refiere al uso de
			dichos e-mails en las acciones siguientes :</p>

		<ul>
			<li>Env�o de invitaciones del cliente tkilas (e-mail)</li>
			<li>Env�o de e-mail para dejar una opini�n sobre la reserva
				(e-mail)</li>
		</ul>

		<p>Igualmente, el usuario autoriza a la sociedad TK CITY
			SOLUTIONS.SL a utilizar y/o ceder estas informaciones dentro del
			marco de las asociaciones, y que, conformemente a la ley, en
			particular para que el usuario pueda beneficiarse de informaciones y
			servicios personalizados (tales como los puntos de fidelidad o las
			invitaciones gratuitas)</p>

		<h2>Derecho de rectificaci�n y de oposici�n</h2>

		<p>El usuario puede ejercitar su derecho de oposici�n a la cesi�n
			de sus datos personales. Puede ejercitar este derecho marcando o
			desmarcando la casilla dispuesta a ese efecto en el formulario de
			apertura de una cuenta.</p>

		<p>El usuario puede ejercitar, por otra parte, su derecho de
			acceso y rectificaci�n a los datos personales introducidos mandando
			un mensaje a la direcci�n de correo electr�nico siguiente:
			info@tkilas.com</p>

		<p>Los terceros, pueden, de otro lado, ejercer su derecho de
			oposici�n enviando un mensaje con ese fin a la direcci�n de correo
			electr�nico siguiente: info@tkilas.com</p>

		<h2>Advertencia</h2>

		<p>El usuario reconoce que, de manera general y teniendo en cuenta
			las caracter�sticas de la tecnolog�a actual, cada vez que proporciona
			informaciones personales online, estas informaciones pueden ser
			recogidas y utilizadas por terceros. En consecuencia, el usuario
			exime a la sociedad TK CITY SOLUTIONS.SL de toda responsabilidad o
			consecuencia da�ina derivada de la utilizaci�n por parte de terceros,
			de las informaciones personales intercambiadas a trav�s de los medios
			de comunicaci�n de Internet (especialmente los chat, foros o
			anuncios).</p>

		<h2>Tratamiento del � Contenido Usuario � - Opiniones</h2>

		<p>Designa el contenido que los usuarios publican en la web o
			transmiten haciendo uso de ella, sobre todo las opiniones (notas y
			comentarios escritos). No se refiere en ning�n momento a los datos de
			car�cter personal del usuario.</p>

		<p>Solo las opiniones relativas a una reserva cumplida podr�n ser
			publicadas. Cuando un usuario realiza una opini�n tras realizar una
			reserva, la nota numerica se publica autom�ticamente. El comentario
			escrito es revisado por el equipo de tkilas.com. tkilas.com se
			reserva entonces el derecho a no publicar o eliminar el comentario
			escrito sin previo aviso, sobre todo en caso de textos insultantes,
			injuriosos u opiniones destructivas.</p>

		<h2>Utilizaci�n del contenido del usuario:</h2>

		<p>tkilas se reserva el derecho seg�n su criterio a eliminar o
			publicar el contenido del usuario en cualquier momento. tkilas no
			est� obligado a guardar duplicados de este contenido y no se
			responsabiliza de las opiniones de sus usuarios, al ser p�blico y
			opcional no se garantiza ning�n tipo de confidencialidad.</p>

		<p>tkilas se reserva el derecho a utilizar el contenido del
			usuario bajo cualquier forma,puede o no publicarlo en la web,
			modificar el formato, incorporarlo en publicidades u otros
			documentos, crear obras derivadas de este mismo, destacarlo,
			distribuirlo, y permitir a otros que hagan lo mismo en sus paginas
			web y plataformas de internet. En consecuencia, el usuario otorga a
			tkilas su consentimiento irrevocable para utilizar este contenido de
			cualquier forma que sea, y renuncia irrevocablemente a toda
			reclamaci�n y reivindicaci�n relativa a los derechos morales o
			patrimoniales en lo que se refiere a este contenido.</p>

		<h1>LIMITACI�N DE LA RESPONSABILIDAD</h1>

		<h2>Funcionamiento de la red</h2>

		<p>Teniendo en cuenta la especificidad del medio Internet, la
			sociedad TK CITY SOLUTIONS.SL no ofrece ninguna garant�a de
			continuidad del servicio, �nicamente existe una obligaci�n de medios.</p>

		<p>La TK CITY SOLUTIONS.SL no es responsable en caso de da�os
			ligados a la imposibilidad temporal de acceder a alguno de los
			servicios propuestos por el portal tkilas.com</p>

		<h2>Modificaci�n de la p�gina web</h2>
		<p>Todas las informaciones contenidas en la p�gina web tkilas.com
			son susceptibles de ser modificadas en cualquier momento teniendo en
			cuenta el car�cter interactivo de la p�gina, sin que ello sea
			responsabilidad de la sociedad TK CITY SOLUTIONS.SL.</p>

		<h2>Utilizaci�n de la p�gina web</h2>

		<p>La sociedad TK CITY SOLUTIONS.SL declina toda responsabilidad
			por cualquier da�o o p�rdida ligados a la utilizaci�n o imposibilidad
			de utilizaci�n de la p�gina web tkilas.com o su contenido, salvo
			excepci�n prevista por ley.</p>

		<p>La sociedad TK CITY SOLUTIONS.SL no garantiza de ninguna manera
			que las informaciones presentes sean detalladas, completas, ver�dicas
			o exactas. Los documentos, informaciones, fichas descriptivas, y, en
			general, todo contenido presente en la p�gina web tkilas.com es
			correcto, sin que exista ninguna garant�a de ning�n tipo.</p>

		<p>El usuario reconoce expresamente que las fotos presentes en la
			p�gina web tkilas.com no son contractuales.</p>

		<p>De manera general, el usuario acepta y reconoce que la reserva
			no est� garantizada. En ese sentido la sociedad TK CITY SOLUTIONS.SL
			no garantiza la efectividad del servio de reserva. La disponibilidad
			se verifica en tiempo real a trav�s de medios inform�ticos y una
			oferta se bloquea de forma real a trav�s de esos medios. En cualquier
			caso la sociedad TK CITY SOLUTIONS.SL no puede verificar de forma
			material la exactitud de las informaciones recogidas y/o ofrecidas
			por los prestatarios del servicio (establecimientos). Efectivamente,
			los par�metros inform�ticos de las reservas en tiempo real dependen
			en parte de las informaciones ofrecidas y registradas por el
			establecimiento y pueden no corresponderse con la realidad. De ese
			modo, y de manera no exhaustiva, el usuario reconoce y acepta que la
			sociedad TK CITY SOLUTIONS.SL no tiene responsabilidad alguna en caso
			de anulaci�n de la reserva, en caso de que el establecimiento est�
			cerrado o que, por la causa que fuera se rechazara la prestaci�n del
			servicio.
		<p>
		<p>Del mismo modo y por las mismas razones, el usuario acepta que
			la sociedad TK CITY SOLUTIONS.SL no es responsable en caso de que el
			usuario no pueda beneficiarse de las promociones y ofertas especiales
			propuestas por el establecimiento. El usuario reconoce y acepta que
			la sociedad TK CITY SOLUTIONS.SL no ser� responsable de ninguna
			manera en caso de que el establecimiento no respete una promoci�n u
			oferta especial por el caso que sea.</p>


		<h2>Garant�as del usuario</h2>

		<p>El usuario declara que conoce perfectamente las caracter�sticas
			y aspectos de internet. Reconoce especialmente que es imposible
			garantizar que los datos personales que el usuario env�a est�n
			completamente seguros. La sociedad TK CITY SOLUTIONS.SL no ser�
			responsable de los incidentes que puedan derivar de dicha
			transmisi�n.</p>


		<p>Por lo tanto, el usuario las comunica conciente del riesgo y el
			peligro. La sociedad TK CITY SOLUTIONS.SL solo puede asegurar que
			pone todos los medios a su alcance para garantizar la m�xima
			seguridad.</p>

		<p>El usuario se compromete a indemnizar a la sociedad TK CITY
			SOLUTIONS.SL por los costes que la misma deber� soportar a
			consecuencia de cualquier reclamaci�n judicial o extrajudicial
			ligadas a la utilizaci�n de los servicios definidos en las presentes
			condiciones por parte del usuario.</p>


		<p>En cualquier caso, el usuario reconoce expresamente y acepta la
			utilizaci�n de la web tkilas.com exclusivamente bajo su
			responsabilidad y con conocimiento de los riesgos existentes.</p>
		<h2>Enlaces de hipertexto</h2>

		<p>La p�gina web tkilas.com contiene enlaces a p�ginas de terceros
			en Internet.</p>

		<p>Las p�ginas a las que remiten estos enlaces no est�n bajo el
			control de la sociedad TK CITY SOLUTIONS.SL del mismo modo, la
			sociedad no es responsable de los contenidos de esas p�ginas. La
			sociedad TK CITY SOLUTIONS.SL dispone estos enlaces en su p�gina por
			conveniencia, lo cual no implica que la sociedad TK CITY SOLUTIONS.SL
			recomiende la p�gina web en cuesti�n ni que la sociedad TK CITY
			SOLUTIONS.SL est� asociada a la misma. Las p�ginas enlazadas son
			explotadas y dirigidas por prestatarios de servicios independientes y
			de hecho, la sociedad TK CITY SOLUTIONS.SL no puede garantizar que el
			usuario quede satisfecho con los productos, servicios o pr�cticas
			comerciales. Es cometido del usuario llevar a cabo las verificaciones
			que le parezcan necesarias y oportunas antes de acometer una
			transacci�n con un tercero.</p>


		<h1>OBLIGACIONES DEL USUARIO</h1>

		<h2>Aceptar sin restricci�n las presentes condiciones generales</h2>

		<p>Al abrir una cuenta, el usuario acepta, expresamente y sin
			excepci�n, los t�rminos de las presentes condiciones generales y
			eventualmente las condiciones particulares, presentes en la Web.</p>

		<h2>Comunicar las informaciones exactas, sinceras y veraces</h2>

		<p>El usuario est� obligado a transmitir informaciones exactas y
			veraces especialmente sobre su f�rmula de tratamiento de cortes�a,
			sus apellidos, su nombre, su direcci�n de email, su tel�fono,
			necesarios para la una buena identificaci�n a la hora de abrir una
			cuenta.</p>


		<h2>Verificar las condiciones de validez de las promociones y
			ofertas especiales</h2>

		<p>El usuario est� obligado a verificar las condiciones de validez
			de una promoci�n antes de reservar en el portal y no podr�, en ning�n
			caso, reclamar al restaurante una promoci�n de tkilas.com fuera de
			las condiciones de validez especificadas en la Web o fuera del
			procedimiento de reservas con promoci�n u oferta especial.</p>

		<h2>Respetar el derecho nacional e internacional de propiedad
			intelectual</h2>

		<p>El usuario se compromete a no transmitir, copiar, revender,
			reeditar o, en general, facilitar de cualquier forma toda la
			informaci�n o elemento, recibido por parte de tkilas o disponible en
			la Web � tkilas.com �, a cualquier otra persona f�sica o jur�dica, de
			cualquier pa�s. En general, el usuario se compromete a respetar las
			disposiciones que se indican a continuaci�n a prop�sito de la
			propiedad intelectual.</p>

		<h1>PROPIEDAD INTELECTUAL</h1>

		<h2>Propiedad de los derechos</h2>

		<p>Todos los derechos, patrimoniales y morales, de propiedad
			intelectual, referentes a los contenidos y elementos de informaci�n
			de la Web � tkilas.com � son propiedad de La sociedad TK CITY
			SOLUTIONS.SL , bajo reserva de todo derecho patrimonial pudiendo
			pertenecer a un tercero y por los cuales La sociedad TK CITY
			SOLUTIONS.SL ha obtenido la cesi�n de derechos o las autorizaciones
			necesarias.</p>

		<p>Todos los derechos concedidos a los usuarios en vista de la
			utilizaci�n de la Web � tkilas.com � y de los servicios
			proporcionados por la sociedad TK CITY SOLUTIONS.SL no conllevan
			ninguna cesi�n ni ninguna autorizaci�n de explotaci�n o utilizaci�n
			de cualquiera de los elementos del portal � tkilas.com �</p>

		<p>Protecci�n de todos los elementos: Marcas, dise�os, logos,
			enlaces de hipertexto, informaci�n...etc)</p>
		<p>Todos los elementos (marcas, dise�os, textos, enlaces, logos,
			im�genes, v�deos, elementos sonoros, software, bases de datos,
			c�digos.........) contenidos en la p�gina Web � tkilas.com � y en las
			web asociada est�n protegidos por el derecho nacional e internacional
			de propiedad intelectual . Estos elementos son propiedad exclusiva de
			la sociedad TK CITY SOLUTIONS.SL o de sus asociados.</p>

		<h2>Prohibici�n de uso sin autorizaci�n</h2>
		<p>Como consecuencia, salvo autorizaci�n anterior y por escrito de
			la sociedad TK CITY SOLUTIONS.SL o de sus asociados, el usuario no
			puede proceder a ninguna reproducci�n, reedici�n, redistribuci�n,
			adaptaci�n, traducci�n o transformaci�n parcial o �ntegra, o
			transferencia en otra p�gina Web, de todos los elementos que componen
			y se presentan en la Web tkilas.com</p>


		<h2>Sanciones</h2>

		<p>El usuario reconoce y toma conciencia de que si no respeta las
			prohibiciones se�aladas comete un delito castigado tanto civil como
			penalmente.</p>

		<h1>SANCIONES POR FALTAS CONTRACTUALES</h1>

		<h2>Suspensi�n o cese definitivo del servicio o servicios</h2>

		<p>En caso de no ser cumplidas o respetadas, por parte de usuario,
			las obligaciones y estipulaciones previstas en las presentes
			condiciones generales, la sociedad TK CITY SOLUTIONS.SL podr�
			modificar, suspender, limitar o suprimir el acceso al servicio, sin
			que el usuario pueda reclamar ninguna indemnizaci�n.</p>


		<h2>Da�os-Intereses</h2>

		<p>La sociedad TK CITY SOLUTIONS.SL estar� igualmente en derecho
			de reclamar las indemnizaciones correspondientes, destinadas a
			compensar el da�o causado.</p>

		<h1>DISPOSICIONES DIVERSAS</h1>

		<h2>Ley aplicable</h2>

		<p>Las relaciones que se establecen entre La sociedad TK CITY
			SOLUTIONS.SL y el usuario, regidas por las presentes condiciones
			generales, est�n sometidas al derecho espa�ol, excluyendo cualquier
			tipo de legislaci�n estatal. En el caso de traducirse las presentes
			condiciones generales a otras lenguas, s�lo la versi�n espa�ola ser�
			v�lida.</p>

		<h2>Competencia jurisdiccional</h2>
		<p>Toda impugnaci�n o problema de interpretaci�n, ejecuci�n de las
			presentes condiciones generales ser� transmitida a las autoridades
			competentes.</p>

		<h2>Nulidad - Disociaci�n - T�tulos</h2>

		<p>En el hipot�tico caso de que una disposici�n de las presentes
			condiciones generales fuese nula, ilegal o inaplicable de una u otra
			manera, la validez, la legalidad o aplicaci�n de otras de las
			presentes condiciones generales no ser�a afectada o alterada, el
			resto de estipulaciones de las condiciones generales permanecer�an en
			vigor y conservar�an su pleno y entero efecto.</p>

		<p>La sociedad TK CITY SOLUTIONS.SL podr�, en el caso que arriba
			se indica, proceder a la redacci�n de una nueva cl�usula teniendo
			como efecto restablecer la voluntad del conjunto de las partes
			explicadas en la cl�usula inicial y respetando el derecho en vigor
			aplicable a las presentes condiciones generales.</p>
		<p>Los t�tulos de los art�culos no tienen m�s que un valor
			indicativo y no deben ser considerados como parte integrante de las
			condiciones generales.</p>

		<h2>Ausencia de renuncia</h2>

		<p>Salvo que exista una estipulaci�n contraria prevista
			eventualmente en las presentes condiciones generales, ninguna
			tolerancia, inacci�n, abstenci�n u omisi�n, ning�n retraso de la
			sociedad TK CITY SOLUTIONS.SL cambiar�an lo anteriormente expuesto en
			las condicione generales, es decir, no implicar� renuncia alguna a
			los derechos expuestos. Al contrario, estos derechos permanecer�n en
			pleno vigor.</p>
		<h2>Notificaci�n y retraso de un contenido il�cito</h2>

		<p>La sociedad TK CITY SOLUTIONS.SL informa a todos los usuarios
			de la Web � tkilas.com � de que pueden presentar una reclamaci�n o
			objeci�n respecto a los contenidos o elementos de la Web � tkilas.com
			�.</p>


		<p>Si el usuario considera que ciertos elementos o contenidos de
			la Web � tkilas.com � contradicen los derechos de autor que el
			ostenta, el usuario debe dirigir inmediatamente una notificaci�n a la
			sociedad TK CITY SOLUTIONS.SL por correo con acuse de recibo y
			conteniendo todos los elementos que justifiquen la titularidad de
			dichos derechos. Una vez realizado este procedimiento y despu�s de la
			correcta verificaci�n de la exactitud de la notificaci�n, la sociedad
			TK CITY SOLUTIONS.SL se esforzar�, en la medida de lo posible, de
			retirar el contenido il�cito. Siendo precisado que la responsabilidad
			de la sociedad TK CITY SOLUTIONS.SL no puede ser comprometida por los
			contenidos presentes en la Web tkilas.com y modificables por terceros
			(por ejemplo, fichas de los establecimientos, foros, avisos...)</p>

		<p>Sus datos ser�n incorporados en un fichero del que es
			responsable TK CITY SOLUTIONS.SL ., con el fin de gestionar el alta
			en nuestros servicios y remitirle de forma peri�dica por correo
			electr�nico nuestro Newsletter, con noticias de inter�s acerca de
			nuestro website y nuestros servicios y ofertas promocionales de
			nuestros asociados. Igualmente se le informa de que los datos que nos
			proporcione ser�n cedidos a las empresas de los establecimientos para
			la gesti�n de su promoci�n, as� como a aquellas entidades asociadas
			con TK CITY SOLUTIONS.SL . para el env�o de ofertas e informaci�n
			comercial del sector hostelero. Los usuarios cuyos datos sean objeto
			de tratamiento podr�n ejercer gratuitamente los derechos de acceso e
			informaci�n, rectificaci�n, cancelaci�n y oposici�n de sus datos en
			los t�rminos especificados en la Ley Org�nica 15/1999 de Protecci�n
			de Datos de Car�cter Personal, conforme al procedimiento legalmente
			establecido. Estos derechos podr�n ser ejercitados dirigiendo
			comunicaci�n por escrito, debidamente firmada, acompa�ada de
			fotocopia del DNI,a trav�s de la direcci�n de correo electr�nico
			info@tkilas.com</p>


		<h1 id="cookies_section">Pol�tica de cookies</h1>

		<p>
			Una cookie es un fichero que se descarga en el
			ordenador/smartphone/tablet del usuario al acceder a determinadas
			p�ginas web para almacenar y recuperar informaci�n sobre la
			navegaci�n que se efect�a desde dicho equipo. Para conocer m�s
			informaci�n sobre las cookies, TK CITY SOLUTIONS.SL le invita a
			acceder el siguiente <a
				href="https://www.agpd.es/portalwebAGPD/canaldocumentacion/publicaciones/common/Guias/Guia_Cookies.pdf"
				target="_blank">documento</a>.
		</p>

		<p>TK CITY SOLUTIONS.SL utiliza en este sitio web las siguientes
			cookies que se detallan en el cuadro siguiente:</p>
		<ul>
			<li>cookies estrictamente necesarias para la prestaci�n de
				determinados servicios solicitados expresamente por el usuario: si
				se desactivan estas cookies, no podr� recibir correctamente nuestros
				contenidos y servicios; y</li>
			<li>cookies anal�ticas (para el seguimiento y an�lisis
				estad�stico del comportamiento del conjunto de los usuarios), si se
				desactivan estas cookies, el sitio web podr� seguir funcionando sin
				perjuicio de que la informaci�n captada por estas cookies sobre el
				uso de nuestra web.</li>

		</ul>


		<table style="width: 100%;" class="cookies">

			<thead>
				<tr>
					<th colspan="2" style="width: 100px;">Cookies</th>
					<th style="width: 200px;">Informaci�n (*)</th>
					<th style="width: 200px;">Finalidad</th>
					<th style="width: 80px;">Opt-out (*)</th>
				</tr>
				<tr>
					<th colspan="5" class="propias">COOKIES PROPIAS</th>
				</tr>
			</thead>

			<tbody>

				<tr>
					<td colspan="2" class="necesarias"><strong>Estrictamente
							necesarias</strong></td>
					<td>Sesi�n y registro</td>
					<td>Gesti�n del registro<br> Prestar servicios de la
						sociedad de la informaci�n solicitados por el usuario y conforme a
						los t�rminos y condiciones aplicables
					</td>
					<td><strong>N/A</strong></td>
				</tr>
				<tr>
					<th colspan="5" class="terceros"><strong>COOKIES DE
							TERCEROS<br>
					</strong>(la informaci�n recogida a continuaci�n ha sido facilitada por esos
						terceros)</th>
				</tr>
				<tr>
					<td rowspan="3" class="analiticas" style="width: 100px;"><strong>Anal�ticas</strong></td>
					<td style="width: 100px;" class="analiticasBis">
							<a
								href="https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage"
								target="_blank">Google Analytics </a>
						</td>
					<td>N�mero de visitas, p�ginas o secciones visitadas, tiempo
						de navegaci�n, sitios visitados antes de entrar en esta p�gina,
						detalles sobre los navegadores usados</td>
					<td>Informes estad�sticos sobre el tr�fico del sitio web, su
						audiencia total</td>
					<td><a
						href="https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage"
						target="_blank">Ver</a></td>
				</tr>
			</tbody>
		</table>


		<p>
			<b style="color: red">(*)</b> La informaci�n obtenida a trav�s de
			estas cookies, referida al equipo del usuario, podr� ser combinada
			con sus datos personales s�lo si Ud. est� registrado en este sitio
			web. <br>
			<b style="color: red">(**) DESACTIVACI�N DE COOKIES.</b> El usuario
			podr� -en cualquier momento- elegir qu� cookies quiere que funcionen
			en este sitio web mediante:
		</p>


		<ul>
			<li>la configuraci�n del navegador; por ejemplo:</li>
			<ul style="font-size:1em;" >
				<li>Chrome, desde
					<a href="http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647" target="_blank">http://support.google.com/chrome/bin/answer.py?hl=es&answer=95647</a></li>
				<li>Explorer, desde
					<a href="http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9" target="_blank">http://windows.microsoft.com/es-es/windows7/how-to-manage-cookies-in-internet-explorer-9</a></li>
				<li>Firefox, desde
					<a href="http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-web" target="_blank">http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-web</a></li>
				<li>Safari, desde <a href="http://support.apple.com/kb/ph5042" target="_blank">http://support.apple.com/kb/ph5042</a></li>

			</ul>
			<li>otras herramientas de terceros, disponibles on line, que
				permiten a los usuarios detectar las cookies en cada sitio web que
				visita y gestionar su desactivaci�n (por ejemplo, Ghostery:
				<a>http://www.ghostery.com/privacy-statement</a>,
				<a href="http://www.ghostery.com/faq">http://www.ghostery.com/faq</a>).</li>

		</ul>

	</div>

<div id="contact_box" >
		<div id="letter_wrapper"><img src="resources/img/em_landing.png" title="ml" alt="ml" /></div>
		<p><spring:message code="contact.us" /></p>
	</div>
		<%@ include file="footer_template.jsp" %>



</body>
</html>
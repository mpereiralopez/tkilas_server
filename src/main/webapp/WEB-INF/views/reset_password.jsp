<%@ include file="init.jsp"%>

<script type="text/javascript"
	src="resources/js/library/jquery.validate.min.js"></script>

<link href="resources/css/forgot.css" rel="stylesheet" type="text/css">
<link href="resources/css/fonts.css" rel="stylesheet" type="text/css">


<title><spring:message code="password.recovery.title" /></title>
<style>
.input_100{
  margin-bottom: 10px;
 }
  
#forgot-box{
  	max-height: 500px;
}
  
.text_normal{
	color: gray !important;
	font-size:1.5em !important;
	text-align: center !important;
	-webkit-margin-before: 0.5em !important;
	-webkit-margin-after: 0.5em !important;
}

.text_spm{
	font-size: 1em !important;
	text-align: center !important;
	-webkit-margin-before: 0.5em !important;
	-webkit-margin-after: 0.5em !important;
}
</style>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$('#container')
								.css('margin-top', $('#header').height());
						$('#footer h4').css('line-height',
								($('#footer').height() - 1) + 'px');

						$("#contact_box").click(function() {
							$(".modal").css('opacity', '1');
							$(".modal").css('top', '50%');
							$(".modal").css('visibility', 'visible');

						});

						$(window)
								.resize(
										function() {
											$('#container').css('margin-top',
													$('#header').height());
											$('#footer h4').css(
													'line-height',
													($('#footer').height() - 1)
															+ 'px');

										});

						$("#commentForm").bind("submit", manualValidate);

						$("#reset_form")
								.validate(
										{

											rules : {
												password : {
													required : true,
													minlength : 5
												},
												repeated_password : {
													required : true,
													minlength : 5,
													equalTo : "#password"
												}
											},

											messages : {
												password : {
													required : "<spring:message code="validator.pass.required" />",
													minlength : "<spring:message code="validator.pass.minlen" />"
												},
												repeated_password : {
													required : "<spring:message code="validator.pass.required" />",
													minlength : "<spring:message code="validator.pass.minlen" />",
													equalsTo : "<spring:message code="validator.repeatedpass.equals" />"
												}
											},

										});

					});
	function hideModal() {
		$(".modal").css('opacity', '0');
		$(".modal").css('top', '-50%');
		$(".modal").css('visibility', 'hidden');
	}

	function send_comment() {
		var client = new XMLHttpRequest();
		var formData = new FormData();
		formData
				.append("body", $('#cname').val() + ": " + $('#ccomment').val());
		formData.append("to", "info@tkilas.com");
		formData.append("from", $('#cemail').val());
		formData.append("subject", $('#type_selector').val());
		if (window.location.href.indexOf("register") == -1) {
			client.open("post", 'send_comment_index', true);
		} else {
			client.open("post", 'send_comment', true);
		}
		client.send(formData); /* Send to server */
		client.onreadystatechange = function() {
			console.log(client);
			if (client.readyState == 4 && client.status == 200) {
				$("#dialog_contact_form").dialog("close");
			}
		};
	}

	function manualValidate(ev) {
		ev.target.checkValidity();
		if (ev.target.checkValidity())
			send_comment();
		return false;
	}
</script>
</head>
<body>

	<%@ include file="header_normal_template.jsp"%>


	<div class="container">
		<div class="tel_container">
			<img src="resources/img/tlf_ico.png" />
			<p class="tel_number font_green">639 76 43 07</p>
		</div>
		
		<c:if test="${status eq '1'}">
			<div id='tarifas_text_wrapper'>
			<div style='top: 8%;bottom: 4%;margin: auto;position:absolute;left:0;right: 0; max-width: 600px; max-height: 500px;'>
				<h1 style='text-align: center;'>�Se ha cambiado su contrase�a!</h1><br>
				<p class='text_normal'>Acceda de nuevo a su gestor usando la nueva contrase�a desde aqu�: <a href='https://migestor.tkilas.com'>https://migestor.tkilas.com</a></p><br>
			</div>
		</div>
		</c:if>
		
		<c:if test="${status eq '0'}">
				<div id="forgot-box">
			<img src="resources/img/logo_landing.png" alt="Logo Tkilas" />

			<c:if test="${not empty error}">
				<div class="error_box">${error}</div>
			</c:if>

			<c:if test="${empty error}">
				<form:form id="reset_form" commandName="recoveryPasswordForm"
					action="reset_password/send" method='POST'>
					<input class="input_100" type="password" id="password" name='password'
						placeholder=<spring:message code="login.password.placeholder" />
						value='' title="">
					<input class="input_100" type="password" id="repeated_password"
						name='repeated_password'
						placeholder=<spring:message code="password.recovery.repeat" />
						value='' title="">
					<input class="font_white" id="submit_button" name="submit" type="submit"
						value="<spring:message code="password.recovery.confirm" />" />
					<input type="hidden" name="userId" value="${userId}" />
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
				</form:form>
			</c:if>
		</div>
		</c:if>


		
	</div>

	<%@ include file="footer_template.jsp"%>

</body>
</html>
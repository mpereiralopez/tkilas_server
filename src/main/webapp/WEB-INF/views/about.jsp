<%@ include file="init.jsp" %>
<title><spring:message code="title.normal"/></title>
<link rel="stylesheet" href="resources/css/style.css" />
<link href="resources/css/contact_form.css" rel="stylesheet"
	type="text/css">




<script type="text/javascript">
$( document ).ready(function() {
	  $('#footer h4').css('line-height', ($('#footer').height()-1)+'px');
	  $('#footer div').css('line-height', ($('#footer').height()-1)+'px');
	  $( window ).resize(function() {
		  
		  $('#footer h4').css('line-height', ($('#footer').height()-1)+'px');
		  $('#footer div').css('line-height', ($('#footer').height()-1)+'px');
	  });
	  
	  $('#tkila_header_logo').click(function(){
			window.location.href = "http://www.tkilas.com";
		});
	  
	  $( "#dialog_contact_form" ).dialog({
	        autoOpen: false,
	        modal:true,
	        minWidth: 500,
	    });
	    
	    
	    $( "#contact_box" ).click(function() {
	        $( "#dialog_contact_form" ).dialog( "open" );
	      });
});
</script>



<style>
#page1 {
	position: absolute;
	top: 8%;
	bottom: 4%;
	left: 0;
	right: 0;
	overflow-y: scroll;
}

h1 {
	color: rgb(140, 140, 140);
	text-align: center;
}

h2{
	color:rgb(140,210,0);
	padding: 0.5em;
}

p{
	padding-bottom:0.5em; 
}

b{
	color:rgb(140,210,0);
}

ul{
	font-family: Aller;
	margin-top: 0;
	margin-bottom: 0;
}


.wrapper_about {
	position: absolute;
	left: 1em;
	right: 1em;
	text-align: justify;
	max-width: 1440px;
	margin: auto;
}

.logo_wrapper{
	box-shadow:none;
}
</style>
</head>
<body>
	<%@ include file="header_normal_template.jsp" %>
	<section id="page1" class="page current">
		<div class="wrapper_about">
			<div class="logo_wrapper">
				<img src="resources/img/logo_landing.png"
					alt="�Tkilas! Tu bebida con descuento" />
			</div>
			<h1 style="margin-top: 0.5em">�Tkilas! Tu bebida con descuento</h1>

			<h2>�Qu� es Tkilas?</h2>
			<p>
				<b>Los mejores descuentos de bebidas en Madrid</b>
			</p>

			<p>
				Desc�rgate la Aplicaci�n de Tkilas y encontrar�s <b>los mejores
					descuentos en bebida de Madrid</b>; Descuentos en pubs, descuentos en
				bares, descuentos en cervezas, descuentos en copas, botellas al
				mejor precio, cocktails con oferta y <b>descuentos de hasta un
					-50 % en todas las consumiciones</b>. En Tkilas encontrar�s las mejores
				ofertas en copas ,cervezas y todo tipo de bebidas de Madrid. Podr�s
				ver el tipo de Local al que quieres ir, el tipo de musica, su
				ambiente, carta de precios, donde esta y mucha m�s informaci�n.
			</p>

			<img src="http://tkilas.com/images/newsletter/tkilas_nl_1.png"
				alt="�tkilas! Tu bebida con descuento" width="100%" style="padding-top:0.5em;" />


			<h2>�C�mo funciona Tkilas?</h2>
			<p>Tkilas es la primera aplicaci�n donde encontrar�s las mejores
				ofertas en bebidas de tu ciudad. Para utilizar Tkilas solo tendr�s
				que descargarte nuestra aplicacion en App store o en google play es
				totalmente gratuita.</p>

			<p>En tkilas encontrar�s las mejores ofertas de tu ciudad en los
				locales que est�n m�s de moda. Para utilizarla solo tendr�s que:
				<ul style="list-style-type: decimal">
  					<li>Buscar la bebida que quieres en la zona que estes.</li>
  					<li>Reserva tu oferta de forma gratuita</li>
  					<li>�Disfrutar de tu descuento!</li>
				</ul>
			</p>

			<h2>El equipo de Tkilas</h2>

			<p>Tkilas es el proyecto de tres jovenes emprendedores que una
				tarde tomando una copa llegaron a ingeniar esta maravillosa idea.
				Era una tarde cualquiera y no sabian donde ir a tomar una copa a un
				buen precio en Madrid �no es increible?, por lo que decidieron crear
				una aplicaci�n con todas las ofertas en bebidas de Madrid. Esas
				ofertas exist�an y no hab�a ning�n sitio donde los bares pudieran
				publicarlas m�s que su triste pizarra, y fue el d�a que se les
				ocurrio Tkilas.</p>

			<p>
				Sus inicios fueron en 2013 y fueron muy duros hasta llegar a crear
				esta herramienta de Marketing casi perfecta para que los bares de
				Espa�a puedan <b>subir sus ofertas en bebida.</b>
			</p>

			<p>Tkilas ya cuenta con casi 100 bares de Madrid y con algunos de
				otras Comunidades como Burgos y Barcelona. Y sus planes de expansi�n
				preve�n numerosos registros para este 2015.</p>

			<p>Si necesita�s ampliar la informaci�n o quere�s registrar
				vuestra informaci�n pode�s poneros en contacto con nosotros en
				info@tkilas.com o en el tlf. 639764307</p>

			<h2>El equipo de Tkilas �tu bebida con descuento!</h2>
			<h1 style="padding-bottom: 0.5em;">Las mejores ofertas en Bebida de tu ciudad.</h1>


		</div>
	</section>


<div id="contact_box" >
		<div id="letter_wrapper"><img src="resources/img/em_landing.png" title="ml" alt="ml" /></div>
		<p><spring:message code="contact.us" /></p>
	</div>

		<%@ include file="footer_template.jsp" %>


</body>
</html>
<%@ include file="init.jsp" %>

<c:url value="/j_spring_security_check" var="loginUrl" />

<title><spring:message code="title.login"/></title>
<link href="resources/css/login.css" rel="stylesheet" type="text/css">
<script type="text/javascript">

$(function() {
    $( "#dialog_contact_form" ).dialog({
        autoOpen: false,
        modal:true,
        minWidth: 500,
        
    });
    
    
    $( "#contact_box" ).click(function() {
        $( "#dialog_contact_form" ).dialog( "open" );
      });
  });

</script>
</head>
<body onload='document.loginForm.username.focus();'>
	<%@ include file="header_normal_template.jsp"%>
	<div id="container">
		<div class="tel_container">
			<img src="resources/img/tlf_ico.png" />
			<p class="tel_number">639 76 43 07</p>
		</div>
		
		<div id="logo_container">
			<img src="resources/img/logo_landing.png" alt="logo de Tkilas"/>
			<div id="login-box">
				<form id="login_form" name='loginForm'
					action="<c:url value='${request.contextPath}/j_spring_security_check' />"
					method='POST'>
					
					<input class="top" type='text' name='username' placeholder="<spring:message code="login.mail.placeholder" />" value=''> <input
					class="bottom" type='password' name='password' placeholder="<spring:message code="login.password.placeholder" />" />
				
					<p style="text-align: center; color: #8cd200">
						<a href="<%getServletContext().getContextPath();%>forgot_password"><spring:message code="login.password.forgot" /></a>
					</p>

					<input id="submit_button" name="submit" type="submit" value="<spring:message code="login.btn.login" />" /> <input type="hidden"
					name="${_csrf.parameterName}" value="${_csrf.token}" />

					<p style="text-align: center; color: #8cd200">
					<spring:message code="login.register" /> <a href="//www.tkilas.com/register#final"><spring:message code="login.here" /></a>
					</p>



				</form>
			</div>
		</div>
		
		<c:if test="${not empty error}">
						<div class="error"><p><spring:message code="login.error" /></p></div>
					</c:if>
					<c:if test="${not empty msg}">
						<div class="msg"><p><spring:message code="login.logout" /></p></div>
					</c:if>

		

	</div>


<div id="contact_box" >
		<div id="letter_wrapper"><img src="resources/img/em_landing.png" title="ml" alt="ml" /></div>
		<p><spring:message code="contact.us" /></p>
	</div>
	
	<%@ include file="footer_template.jsp"%>

</body>
</html>
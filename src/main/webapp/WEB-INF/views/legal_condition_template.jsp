<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    		<div class="legal_condition">
			
				<h1>CONDICIONES LEGALES</h1>
				<h2>ARTÍCULO 1 - OBJETO DEL CONTRATO</h2>
				<p>Constituye el objeto del presente contrato la cesión de la
					correspondiente licencia de uso de la herramienta propiedad de
					TKCITYSOLUTIONS, S.L. en adelante TKILAS albergada en sus
					servidores. La licencia se otorga por parte de TKILAS a los
					Establecimientos en adelante CLIENTE que deseen utilizar dicha
					aplicación. Esta aplicación está basada en un modelo de herramienta
					de marketing online gratuita, además de otros servicios con coste
					variable (reservas online, BBDD, encuestas de calidad, estadísticas
					y servicios varios). A modo informativo y de manera resumida la
					aplicación de TKILAS es: una herramienta que permite a los
					restauradores gestionar y organizar sus reservas mediante un libro
					de reservas electrónico, sus datos de clientes, y a los clientes
					del establecimiento reservar a través de la aplicación tkilas tanto
					para dispositivos Android como iOS.</p>

				<h2>ARTÍCULO 2 - ACCESO A LA HERRAMIENTA</h2>
				<p>En virtud del presente contrato, TKILAS otorga al CLIENTE una
					licencia intransferible para utilizar su herramienta y la opción de
					abonarse a las aplicaciones de pago, así como la formación y la
					documentación correspondiente para su utilización, obligándose el
					CLIENTE a pagar el precio descrito en el acuerdo de implantación
					adjunto, el cual constituye requisito imprescindible para obtener
					los derechos de uso, bajo los términos y condiciones del presente
					contrato. El acceso puede hacerse desde cualquier « ordenador
					cliente » conectado a Internet con las claves de identificación qué
					TKILAS facilitará al CLIENTE. El CLIENTE es el único y total
					responsable de sus claves de identificación.</p>

				<h2>ARTÍCULO 3 - OBLIGACIONES</h2>
				<p>El CLIENTE se compromete a: no ceder ni transmitir los
					derechos de uso contraídos por el presente contrato. No podrá
					comercializar, copiar, recrear o generar esta herramienta ni
					derivados de la misma. El CLIENTE se comprometen a no utilizar los
					servicios de TKILAS para la realización de actividades contrarias a
					las leyes, a la moral, al orden público ni a utilizar los servicios
					con fines o efectos ilícitos, prohibidos, lesivos de derechos e
					intereses de terceros, declinando TKILAS cualquier responsabilidad
					que de ello se pudieran derivar. Con el fin de confirmar la calidad
					de las prestaciones propuestas por el establecimiento asociado y
					verificar que son las prestaciones estipuladas en el presente
					Contrato, el establecimiento asociado autoriza a TKILAS (o toda
					persona autorizada por TKILAS) a testear/probar gratuitamente las
					prestaciones, antes de la integración de las prestaciones en página
					web, o durante el transcurso de la validez del Contrato. Podrá
					efectuarse de forma anónima y por dos personas, el establecimiento
					será informado al transcurrir la misma. Las partes reconocen que la
					calidad es también un elemento determinante de su compromiso con el
					presente Contrato. Así mismo EL CLIENTE autoriza la utilización de
					datos, fotografías y logotipos del establecimiento para la correcta
					difusión de la información en tkilas.com, así como en las
					aplicaciones para los dispositivos de Android y de iOS y garantiza
					la veracidad de estas informaciones. Todas las fotografías
					realizadas por TKILAS así como las descripciones originales y
					aquellas informaciones que no sean de uso público para la correcta
					visualización del establecimiento o actualización de la ficha,
					serán propiedad de uso en exclusiva de TKILAS. TKILAS se compromete
					a: Aplicar los medios necesarios para asegurar la permanencia, la
					continuidad y la calidad de los servicios que propone. Proporcionar
					un servicio lo más potente posible al usuario, según el estado de
					la técnica y de las proyecciones tecnológicas actuales y a
					proporcionar al usuario la última versión del software albergada.
					Emplearse en dar seguridad al acceso y uso de la herramienta,
					teniendo en cuenta los protocolos, conformes a los usos de
					Internet.</p>

				<h2>ARTÍCULO 4 - PROPIEDAD DE LOS DATOS</h2>
				<p>A los efectos de lo previsto en la Ley Orgánica 15/1999 del
					13 de diciembre de Protección de Datos de Carácter Personal se
					acuerda bajo este contrato lo siguiente: 1. Los datos de carácter
					personal creados por y para CLIENTE a través del uso de las
					aplicaciones y de forma manual son propiedad y responsabilidad de
					este último. TKILAS, se compromete al cumplimiento de su obligación
					de secreto de los datos de carácter personal y de su deber de
					guardarlos y adoptará las medidas necesarias para evitar su
					alteración, pérdida, tratamiento o acceso no autorizado. 2. Los
					datos de carácter personal creados por y para TKILAS a través de su
					página web www.tkilas.com, o de sus asociados son propiedad de
					TKILAS cómo responsable del fichero. TKILAS cederá los datos de
					aquellos usuarios que reserven en el establecimiento CLIENTE para
					el cumplimiento de sus servicios, así mismo informará al usuario de
					sus derechos de acceso, modificación o cancelación de sus datos. El
					CLIENTE se comprometen a cumplir con la normativa legal en lo que a
					protección de datos se refiere, conforme al artículo 11.5 LOPD,
					para lo que declara disponer de las adecuadas medidas técnicas y
					organizativas impuestas por esta misma ley.</p>

				<h2>ARTÍCULO 5 - COSTE DE UTILIZACIÓN Y PAGO</h2>
				<p>Las tarifas vigentes durante el presente contrato son las
					siguientes: El pago se realizará mensualmente y a mes vencido.
					Tarifas establecidas en función de las personas que asistan a las
					reservas:</p>
				<ul>
					<li>1-4 personas: 0,29cts/p</li>
					<li>5-9 personas: 0,39cts/p</li>
					<li>>9 personas:0,49cts/p</li>
				</ul>
				
				<p style="font-style: italic; font-weight: bold;"><b>Mandato de domiciliación de adeudo directo-SEPA B2B.</b></p>
				<p>Mediante la aceptación de condiciones de venta, Ud. autoriza al acreedor (TK CITY SOLUTIONS S.L.) a enviar órdenes a su entidad financiera para adeudar en su cuenta y también a su entidad financiera para efectuar los adeudos en su cuenta de acuerdo con las órdenes del acreedor. Esta orden de domiciliación está prevista exclusivamente para operaciones entre empresas y/o autónomos. Usted no tiene derecho a que su entidad financiera le reembolse una vez que se haya realizado el cargo en cuenta, pero tiene derecho a solicitar a su entidad financiera que no adeude su cuenta hasta la fecha de vencimiento para el cobro del adeudo. Puede obtener información adicional sobre sus derechos en su entidad financiera.</p>
				<p>Ud. se compromete a comunicar a su entidad bancaria el presente pacto, no dirigiendo a ésta instrucciones distintas a las aquí establecidas en cuanto al plazo de devolución de los adeudos girados por el Acreedor sin la previa comunicación al Acreedor. Asimismo, autoriza al Acreedor a notificar dicho acuerdo a su entidad financiera si lo considerase oportuno. Las presentes condiciones se pactan al amparo de lo previsto en el artículo 23 de la Ley 16/2009, de 13 de noviembre, de Servicios de Pago.</p>	
		</div>
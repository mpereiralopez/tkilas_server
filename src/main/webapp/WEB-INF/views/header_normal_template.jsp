<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet" href="resources/css/header.css" />

<nav id="header" class="m-navigation-header">
	<div class="header_footer_container">
		<a href="//www.tkilas.com" target="_self"><img
			id="tkila_header_logo" src="resources/img/tkilas_header.png" /></a>
			
		<!-- <div class="flag_container" style="width: auto"><a href="?lang=es"><img alt="Idioma Español" src="resources/img/es.png"></a><a href="?lang=en"><img alt="Idioma Inglés" src="resources/img/us.png"></a></div> -->
	
			<c:if test="${not empty local_info}">
							<div style="
    display: table;
    float: right;
    width: auto;
    height: 100%;
    margin-right: 1em;
"><h1 style="color:#FFF;vertical-align: middle;display: table-cell;">Bienvenido, ${local_info.getLocal_name()}</h1></div>

				</c:if>
	</div>
</nav>
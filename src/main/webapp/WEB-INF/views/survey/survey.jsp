<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../init.jsp"%>

<script type="text/javascript"
	src="resources/js/library/jquery.validate.min.js"></script>

<link href="resources/css/forgot.css" rel="stylesheet" type="text/css">
<link href="resources/css/fonts.css" rel="stylesheet" type="text/css">

<title><spring:message code="title.normal" /></title>

<style>
.input_100 {
	margin-bottom: 10px;
}

textarea{
	width: 100%;
}

#survey-box {
	max-width: 430px;
	margin: auto;
	max-height: 500px;
	position: absolute;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	
}

h1{
	font-size: 2.5em;
	text-align: center;
}

h2{
	font-size: 1.5em
}

p{
font-size: 100%;}

</style>

<script type="text/javascript">
$( document ).ready(function() {
	  $('#footer h4').css('line-height', ($('#footer').height()-1)+'px');
	  $('#footer div').css('line-height', ($('#footer').height()-1)+'px');
	  $( window ).resize(function() {
		  
		  $('#footer h4').css('line-height', ($('#footer').height()-1)+'px');
		  $('#footer div').css('line-height', ($('#footer').height()-1)+'px');
	  });
	  
	  $('#tkila_header_logo').click(function(){
			window.location.href = "http://www.tkilas.com";
		});
	  
	  $( "#dialog_contact_form" ).dialog({
	        autoOpen: false,
	        modal:true,
	        minWidth: 500,
	    });
	    
	    
	    $( "#contact_box" ).click(function() {
	        $( "#dialog_contact_form" ).dialog( "open" );
	      });
	    

	    $('#survey_form').submit(function () {

	    	if ($('.trate-form-field input:radio', this).is(':checked') &&
	    			$('.drinks-form-field input:radio', this).is(':checked') &&
	    			$('.ambient-form-field input:radio', this).is(':checked') &&
	    			$('.global-form-field input:radio', this).is(':checked')) {
	    	        // everything's fine...
	    	    } else {
	    	        alert('Debes puntuar al local');
	    	        return false;
	    	    }
	    });
});
</script>




</head>
<body>

	<%@ include file="../header_normal_template.jsp"%>

	<div class="container">
		<div class="tel_container">
			<img src="resources/img/tlf_ico.png" />
			<p class="tel_number font_green">639 76 43 07</p>
		</div>

		<c:if test="${status eq '1'}">
			<div id='tarifas_text_wrapper'>
				<div
					style='top: 8%; bottom: 4%; margin: auto; position: absolute; left: 0; right: 0; max-width: 600px; max-height: 500px;'>
					<h1 class="font_green" style='text-align: center;'>¡Se ha enviado su comentario!</h1>
					<br>
					<p class='text_normal'>
						Su comentario ha sido enviado y será validado próximamente.</p>
					<p>Gracias por participar con nosotros. </p>
					<br>
				</div>
			</div>
		</c:if>

		<c:if test="${status eq '2'}">
			<div id='tarifas_text_wrapper'>
				<div
					style='top: 8%; bottom: 4%; margin: auto; position: absolute; left: 0; right: 0; max-width: 600px; max-height: 500px;'>
					<h1 class="font_green" style='text-align: center;'>¡Comentario ya validado!</h1>
					<br>
					<p class='text_normal'>
						Su comentario ya ha sido validado por nuestro equipo y visible por
						todos. </p><p>Gracias por participar con nosotros. </p>
					<br>
				</div>
			</div>
		</c:if>

		<c:if test="${status eq '0'}">
			<div id="survey-box">
				<h1 class="font_green">Hola ${userName}</h1>
				</br>
				<h2>¡Queremos conocer tu experiencia en ${localName}!</h2>
				</br>
				<c:if test="${not empty error}">
					<div class="error_box">${error}</div>
				</c:if>

				<c:if test="${empty error}">
					<form:form id="survey_form" commandName="commentForm"
						action="survey/send" method='POST'>
						
						
	<ol>
		<li>¿Qué valoración darías al trato recibido?
			<div class="trate-form-field">
				<label><input name="trate" type="radio" value=1>1</label>
				 <label><input name="trate" type="radio" value=2>2</label> 
				 <label><input name="trate" type="radio" value=3>3</label>
				<label><input name="trate" type="radio" value=4>4</label> 
				<label><input name="trate" type="radio" value=5>5</label>
			</div>
		</li>
		<li></br>¿Qué te han parecido las bebidas?
			<div class="drinks-form-field">
				<label><input name="drinks" type="radio" value=1>1</label>
				 <label><input name="drinks" type="radio" value=2>2</label>
				 <label><input name="drinks" type="radio" value=3>3</label>
				<label><input name="drinks" type="radio" value=4>4</label>
				<label><input name="drinks" type="radio" value=5>5</label>
			</div>
		</li>
		<li></br>¿Que valoración darías al ambiente del establecimiento?
			<div class="ambient-form-field">
				<label><input name="ambient" type="radio" value=1>1</label>
				<label><input name="ambient" type="radio" value=2>2</label>
				<label><input name="ambient" type="radio" value=3>3</label>
				<label><input name="ambient" type="radio" value=4>4</label>
				<label><input name="ambient" type="radio" value=5>5</label>
			</div>
		</li>
		<li></br>¿Qué puntuación global darías al establecimiento?
			<div class="global-form-field">
				<label><input name="global" type="radio" value=1>1</label>
				<label><input name="global" type="radio" value=2>2</label>
				<label><input name="global" type="radio" value=3>3</label>
				<label><input name="global" type="radio" value=4>4</label>
				<label><input name="global" type="radio" value=5>5</label>
			</div>
		</li>
	</ol>
	</br>
	<p>Escribe tu cometario:</p>
	</br>
	<textarea name="commentBody" rows="5" cols="50" maxlength="140"></textarea>
						
						
						
						<div style="text-align: center;"><input class="font_white" id="submit_button" name="submit"
							type="submit"
							value="Enviar comentario" /></div>
						<input type="hidden" name="userId" value="${userId}" />
						<input type="hidden" name="localId" value="${localId}" />
						<input type="hidden" name="fingerprint" value="${fingerprint}" />
						
						
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
					</form:form>
				</c:if>
			</div>
		</c:if>



	</div>


<div id="contact_box" >
	<div id="letter_wrapper"><img src="resources/img/em_landing.png" title="ml" alt="ml" /></div>
	<p><spring:message code="contact.us" /></p>
</div>
	


	<%@ include file="../footer_template.jsp"%>
</body>
</html>
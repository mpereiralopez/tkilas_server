﻿<%@ include file="init.jsp" %>
<%@page session="false"%>
<title><spring:message code="title.normal"/></title>
<script type="text/javascript" src="resources/js/home.js"></script>
<link href="resources/css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript">

$(function() {
    $( "#dialog_contact_form" ).dialog({
        autoOpen: false,
        modal:true,
        minWidth: 500,
    });
    
    
    $( "#contact_form" ).click(function() {
        $( "#dialog_contact_form" ).dialog( "open" );
      });
  });

</script>
</head>
<body>
	<nav id="header" class="m-navigation ">
		<div id="contact_section">
			<a href="//www.facebook.com/pages/tkilas/1439768839608237" target="_blank"><img
				src="resources/img/fb_landing.png" title="Facebook" alt="Facebook de Tkilas" /></a> 
				<a href="//twitter.com/tkilas" target="_blank"><img src="resources/img/tw_landing.png" title="Twitter" alt="Twitter de Tkilas" /></a> 
				<a href="//instagram.com/tkilas_bebidas" target="_blank"><img src="resources/img/instagram.png" title="Instagram" alt="Instagram de Tkilas" /></a> 
				<a href="//www.youtube.com/channel/UC2jPT8m33lDKrnY1hhczt0w" target="_blank"><img src="resources/img/youtube.png" title="Youtube" alt="Youtube de Tkilas" /></a> 							
				<a href="//plus.google.com/110748576465810074124" target="_blank"><img src="resources/img/gplus.png" title="Google+" alt="Google+ de Tkilas" /></a> 
				<a id="contact_form" ><img src="resources/img/em_landing.png" title="Mail" alt="Escríbenos un mail" /></a>
		</div>
		<a id="local_publish" href="<% getServletContext().getContextPath();%>register" title="Publica tu establecimiento"><spring:message code="home.publish"/></a>
		<div class="flag_container">Ahorra hasta un 50% en tus copas, cervezas , cafes y cocktails</div>
	</nav>
	<div id="page1" class="page current">
		<section class="wrapper" id="wrapper">
			<div id="slider">
				<img src="resources/img/image1.jpg" class="active" title="pic1" alt="Copas con descuento" /> 
				<img src="resources/img/image2.jpg" title="pic2" alt="Bebidas con descuento en tu ciudad"  /> 
				<img src="resources/img/image3.jpg" title="pic3" alt="Descuentos en bebidas"  />
			</div>
			<div class="darker"></div>
			<div class="darkerCenter"></div>

			<div style="width: 100%; height: 100%; position: absolute;">
				<div id="mob_container">
					<a class="download_app appIOS" href="https://itunes.apple.com/es/app/tkilas/id948238250?mt=8&uo=4" target="itunes_store"><spring:message code="available.appstore"/></a> <img id="ios_example" class="mobiles"
						src="resources/img/iph_mob.png" title="iph_mob" alt="Aplicación de Tkilas para iOS"></img>
					<img id="android_example" class="mobiles"
						src="resources/img/and_mob.png" title="and_mob" alt="Aplicación de Tkilas para Android"></img>
					<a class="download_app appAndroid" href="https://play.google.com/store/apps/details?id=com.tksolutions.tkilas&hl=es" target="_blank"><spring:message code="available.google"/></a>

				</div>

			</div>


			<div class="logo_wrapper">
				<img src="resources/img/logo_landing.png" alt="¡Tkilas! Tu bebida con descuento" />
			</div>



			<div id="center_land"
				style="position: relative; z-index: 8; text-align: center;padding-top:1em;">
				<button id="play_intro">
				<h1>
					<spring:message code="home.msg"/>
				</h1>
				<spring:message code="home.play"/>
				
				
				<div id="info">
					<h1><spring:message code="home.now.available"/></h1>
				</div>
				</button>
			</div>
			
			
			
		</section>			
	</div>
	
	<%@ include file="footer_template.jsp" %>

	
	
<div class="modal_container" id="video_container">
	<div id="close_vid" class="custom_close_buttom"  onclick="close_vid();return true;"></div>
	
	
	
	<video id="video" controls="controls" preload="auto">
	<source src="clips/tkilas_ipad.mp4" type="video/mp4" />
	<source src="clips/tkilas.webm" type="video/webm" />
	<source src="clips/tkilas.ogg" type="video/ogg" />
	<object type="application/x-shockwave-flash" data="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" width="640" height="360">
		<param name="movie" value="http://releases.flowplayer.org/swf/flowplayer-3.2.1.swf" />
		<param name="allowFullScreen" value="true" />
		<param name="wmode" value="transparent" />
		<param name="flashVars" value="config={'playlist':[{'url':'clips/tkilas.mp4'}]}" />
	</object>
</video>
</div>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<link rel="icon" href="<%getServletContext().getContextPath(); %>resources/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="user-scalable = yes">
<title>Tkilas ¡Tu bebida con descuento!</title>
<link rel="stylesheet" href="<%getServletContext().getContextPath(); %>resources/css/style.css" />
<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<script type="text/javascript" src="<%getServletContext().getContextPath(); %>resources/js/library/jquery-2.1.0.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53931872-1', 'auto');
  ga('send', 'pageview');

</script>

<style  type="text/css">
.text_normal{
	color: gray !important;
	font-size:1.5em !important;
	text-align: center !important;
	-webkit-margin-before: 0.5em !important;
-webkit-margin-after: 0.5em !important;
}

.text_spm{
	font-size: 1em !important;
	text-align: center !important;
	-webkit-margin-before: 0.5em !important;
-webkit-margin-after: 0.5em !important;
}

.header_footer_container {
	width: 100%;
	height: 100%;
	position: relative;
	max-width: 1500px;
	margin-left: auto;
	margin-right: auto;
	left: 0;
	right: 0;
	bottom: 0;
}

.m-navigation-header {
	position: absolute;
	right: 0;
	z-index: 100;
	height: 8%;
	color: #ccc;
	background-color: rgb(0, 234, 39);
}

.m-navigation-header a {
	font-family: Aller;
	font-size: 13px;
	color: #fff;
	text-decoration: none;
	display: inline-block;
	border-radius: 16px;
	cursor: pointer;
}
#header {
	left: 0;
	top: 0;
}

#tkila_header_logo {
	top:0;
	bottom:0;
	margin-top:auto;
	margin-bottom:auto;
	position: absolute;
	padding-left: 20px;
	cursor: pointer;
}


	
	

}

</style>

</head>
<body>

	<nav id="header" class="m-navigation-header">
	<div class="header_footer_container">
		<a href="//www.tkilas.com" target="_self"><img
			id="tkila_header_logo" src="<%getServletContext().getContextPath(); %>resources/img/tkilas_header.png" /></a>
	</div>
</nav>



	<div id='tarifas_text_wrapper'>
				<div style='top: 8%;bottom: 4%;margin: auto;position:absolute;left:0;right: 0; max-width: 600px; max-height: 500px;'>
					<h1 style='text-align: center;'>¡Ups!</h1><br>
					<p class='text_normal'>Ha habido un pequeño problema y lo solucionaremos pronto</p><br>
				</div>
			</div>

	<%@ include file="../footer_template.jsp" %>

</body>
</html>
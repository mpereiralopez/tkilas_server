<script type="text/javascript">
$('#nav_section').children().removeClass('actual');
$($('#nav_section').children().eq(2)).addClass('actual');
</script>
<style>
p{
font-size:1em;
}
#submit_button {
	font-family: Aller;
	width: 100%;
	color: white !important;
	background-color: #8cd200 !important;
	font-size: 1em;
	border: 1px solid #8cd200 !important;
	height: 45px !important;
	margin-top: 15px;
	cursor: pointer;
	-webkit-border-radius: 15px;
	-webkit-border-radius: 15px;
	-moz-border-radius: 15px;
	-moz-border-radius: 15px;
	border-radius: 15px;
	border-radius: 15px;
}
</style>

<section style="max-width: 1440px;margin:1em auto;">
			
<div id="left_size">		
	<%@ include file="validator_comment_form.jsp" %>
</div>

<div id="right_size" style="width: 50%;">
<div class="table_container">
	<table style="width: 100%">
		<thead>
			<tr>
				<th>Usuario</th>
				<th>Dispositivo</th>
				<th>Local</th>
				<!-- <th>Trato</th>
				<th>Bebidas</th>
				<th>Ambiente</th>
				<th>Global</th>
				<th>Media</th>
				<th>Comentario</th> -->
				<th>Estado</th>
				<th>Fecha</th>
			</tr>
		</thead>

		<c:forEach items="${commentList}" var="comment">




			<c:choose>
				<c:when test="${comment.getStatus() == 1}">
					<tr style="background-color: yellow"
						onclick="viewCommentsToUpdate('${comment.getFingerprint()}', this);">
						<td>${comment.getClient().getUser().getUser_name()}</td>
						<td>${comment.getClient().getClient_SO()}</td>
						<td>${comment.getLocal().getLocal_name()}</td>
						<!-- <td>${comment.getTrate()}</td>
						<td>${comment.getDrinks()}</td>
						<td>${comment.getAmbient()}</td>
						<td>${comment.getGlobal()}</td>
						<td>${comment.getMean()}</td>
						<td>${comment.getCommentBody()}</td> -->
						<td>1</td>
						<td><fmt:formatDate
								value="${comment.getResponsTime()}" pattern="dd/MM/yyyy" /></td>
					</tr>
				</c:when>
				<c:when test="${comment.getStatus() == 2}">
					<tr style="background-color: #00ea27;"
						onclick="viewCommentsToUpdate('${comment.getFingerprint()}',this);">
						<td>${comment.getClient().getUser().getUser_name()}</td>
						<td>${comment.getClient().getClient_SO()}</td>
						<td>${comment.getLocal().getLocal_name()}</td>
						<!-- <td>${comment.getTrate()}</td>
						<td>${comment.getDrinks()}</td>
						<td>${comment.getAmbient()}</td>
						<td>${comment.getGlobal()}</td>
						<td>${comment.getMean()}</td>
						<td>${comment.getCommentBody()}</td> -->
						<td>1</td>
						<td><fmt:formatDate
								value="${comment.getResponsTime()}" pattern="dd/MM/yyyy" /></td>
					</tr>
				</c:when>
			</c:choose>


		</c:forEach>
	</table>
</div>

</div>

</section>
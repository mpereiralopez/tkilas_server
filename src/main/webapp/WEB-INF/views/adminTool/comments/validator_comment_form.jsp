<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>



<c:if test="${MSG_OK != null}">
				<div class="msg_ok">
					<p style="color: white; margin-left: 35px;">${MSG_OK}</p>
				</div>
</c:if>

<form:form id="survey_form" commandName="commentForm"
						action="admin/validate_comment" method='POST'>
	
		<p>Usuario: ${comment.getClient().getUser().getUser_name()}</p>
		</br><p>Local: ${comment.getLocal().getLocal_name()}</p>	
		</br><p>Fecha de comentario: <fmt:formatDate value="${comment.getResponsTime()}" pattern="dd/MM/yyyy" /></p>		
		</br><p>�Qu� valoraci�n dar�as al trato recibido? ${comment.getTrate()}</p>
		</br>
		<p>�Qu� te han parecido las bebidas? ${comment.getDrinks()}</p>
		</br>
		<p>�Que valoraci�n dar�as al ambiente del establecimiento? ${comment.getAmbient()}</p>
		</br>
		<p>�Qu� puntuaci�n global dar�as al establecimiento? ${comment.getGlobal()}</p>
			
	</br>
	
	<p>Media de puntuaci�n: ${comment.getMean()}</p>
			
	</br>
	<p>Cometario:</p>
	</br>
	<textarea name="commentBody" rows="5" cols="50" maxlength="140">${comment.getCommentBody()}</textarea>
						
						
						<div style="text-align: center;">
						<input class="font_white" id="submit_button" name="submit"
							type="submit"
							value="Validar comentario" /></div>
						<input type="hidden" name="userId" value="${userId}" />
						<input type="hidden" name="localId" value="${localId}" />
						<input type="hidden" name="fingerprint" value="${fingerprint}" />
						
						
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
					</form:form>
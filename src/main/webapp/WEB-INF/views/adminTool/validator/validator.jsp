<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

    <script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.
var markerGlobal;
function initialize() {

  var markers = [];
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    zoom: 8,
    center: new google.maps.LatLng(40.4169, -3.7035)
  });
  // Create the search box and link it to the UI element.
  var input = /** @type {HTMLInputElement} */(
      document.getElementById('pac-input'));
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

  // [START region_getplaces]
  // Listen for the event fired when the user selects an item from the
  // pick list. Retrieve the matching places for that item.
  google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }
    for (var i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
    }

    // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      var marker = new google.maps.Marker({
        map: map,
        icon: image,
        title: place.name,
        position: place.geometry.location
      });
      
      markerGlobal = marker;

      markers.push(marker);

      bounds.extend(place.geometry.location);
    }

    map.fitBounds(bounds);
  });
  // [END region_getplaces]

  // Bias the SearchBox results towards places that are within the bounds of the
  // current map's viewport.
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    
    	<section class="wrapper horizontally_move" id="validator">
			<div id="validator_wrapper">
			<c:if test="${MSG_OK != null}">
				<div class="msg_ok">
					<p style="color: white; margin-left: 35px;">${MSG_OK}</p>
				</div>
			</c:if>
				<div id="left_size">
					
				<%@ include file="validator_form.jsp" %>
	
				
					


				</div>
				<div id="right_size">
				 <div class="table_container">
				 	<table style="width: 100%">
						<thead>
							<tr>
								<th>ID</th>
								<th>CONTACTO</th>
								<th>E-MAIL</th>
								<th>TELÉFONO</th>
								<th>LOCAL</th>
								<th>FECHA REGISTRO</th>
							</tr>
						</thead>

						<c:forEach items="${locals}" var="local">
					
	
					
					
					<c:choose>
						<c:when test="${local.getTerms_accpeted() == 0}">
        					<tr style="background-color: yellow" onclick="viewValuesToUpdate(${local.getUserIdUser()}, this);">
								<td>${local.getUserIdUser()}</td>
								<td>${local.getUser().getUser_name()}</td>
								<td>${local.getUser().getEmail()}</td>
								<td>${local.getTlf()}</td>
								<td>${local.getLocal_name()}</td>
								<td><fmt:formatDate value="${local.getUser().getCreateTime()}" pattern="dd/MM/yyyy" /></td>
							</tr>
    					</c:when>
    					<c:when test="${local.getStatus() == 2}">
        					<tr style="background-color: orange" onclick="viewValuesToUpdate(${local.getUserIdUser()}, this);">
								<td>${local.getUserIdUser()}</td>
								<td>${local.getUser().getUser_name()}</td>
								<td>${local.getUser().getEmail()}</td>
								<td>${local.getTlf()}</td>
								<td>${local.getLocal_name()}</td>
								<td><fmt:formatDate value="${local.getUser().getCreateTime()}" pattern="dd/MM/yyyy" /></td>
							</tr>
    					</c:when>
						<c:otherwise>
        					<tr style="background-color: #00ea27;" onclick="viewValuesToUpdate(${local.getUserIdUser()},this);">
								<td>${local.getUserIdUser()}</td>
								<td>${local.getUser().getUser_name()}</td>
								<td>${local.getUser().getEmail()}</td>
								<td>${local.getTlf()}</td>
								<td>${local.getLocal_name()}</td>
								<td><fmt:formatDate value="${local.getUser().getCreateTime()}" pattern="dd/MM/yyyy" /></td>
							</tr>
   						 </c:otherwise>
					</c:choose>
					
							
						</c:forEach>
					</table>
				 </div>
					
					
					<input id="pac-input" class="controls" type="text" placeholder="Search Box">
    				<div id="map-canvas"></div>
    				<input type="button" id="coordenates" value="Coordenadas GPS" /> 

				</div>


			</div>

		</section>
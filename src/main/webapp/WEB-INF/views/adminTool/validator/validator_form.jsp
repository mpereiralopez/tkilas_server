<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>




<form:form commandName="localFormAdmin" method='POST'
	action="admin/validate_local">
	<table id="table_form">
		<tr>
			<td colspan="2" class="table_title">Validar locales</td>
		</tr>
		<tr>
			<td colspan="2" class="table_line"></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="userIdUser">Id Establecimiento:</form:label></td>
			<td class="form_label">${localToUpdate.getUserIdUser()}</td>


			<input id="localId" type='hidden' name='userIdUser'
				value='${localToUpdate.getUserIdUser()}' required>
		</tr>
		<tr>
			<td class="form_label"><form:label path="user_name">Nombre Contacto:</form:label></td>
			<td><input id="user_name" type='text' name='user_name'
				value='${localToUpdate.getUser().getUser_name()}' required></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="user_surname">Apellidos Contacto:</form:label></td>
			<td><input id="user_surname" type='text' name='user_surname'
				value='${localToUpdate.getUser().getUser_surname()}' required /></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="email">E-mail:</form:label></td>
			<td><input id="email" type='email' name='email'
				value='${localToUpdate.getUser().getEmail()}' required /></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="local_name">Nombre del establecimiento:</form:label></td>
			<td><input id="local_name" type='text' name='local_name'
				value='${localToUpdate.getLocal_name()}' required /></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="tlf">Teléfono del establecimiento:</form:label></td>
			<td><input id="tlf" type='tel' name='tlf'
				value='${localToUpdate.getTlf()}' required pattern="[0-9]{9}" /></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="address">Dirección:</form:label></td>
			<td><input id="address" type='text' name='address' required
				value='${localToUpdate.getAddress()}' /></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="local_cp">Código Postal:</form:label></td>
			<td><input id="local_cp" type='text' name='local_cp' required
				value='${localToUpdate.getLocal_cp()}' pattern="[0-9]{5}" /></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="city">Ciudad:</form:label></td>
			<td><select id="city" name="city" size="1">
					<c:forEach var="entry" items="${citiesEnum}">
						<c:if test="${entry.label eq localToUpdate.getCity()}">
							<option selected value='${entry.label}'>${entry.label}</option>
						</c:if>
						<c:if test="${entry.label != localToUpdate.getCity()}">
							<option value='${entry.label}'>${entry.label}</option>
						</c:if>
					</c:forEach>
			</select>
		</tr>
		<tr>
			<td class="form_label"><form:label path="web">Sitio Web (opcional):</form:label></td>
			<td><input id="url" type='url' name='web'
				value='${localToUpdate.getWeb()}' pattern="http?://.+" /></td>
		</tr>

		<tr>
			<td class="form_label"><form:label path="capacity">Capacidad:</form:label></td>
			<td><input id="capacity" type='number' name='capacity'
				value='${localToUpdate.getCapacity()}' required /></td>
		</tr>

		<tr>
			<td class="form_label"><form:label path="district">Distrito:</form:label></td>
			<td><select id="district" name='district' required>
					<c:forEach var="entry" items="${districtEnum}">
						<option value='${entry.code}'>${entry.label}</option>
					</c:forEach>
			</select></td>
		</tr>

		<tr>
			<td class="form_label"><form:label path="local_type">Tipo de establecimiento:</form:label></td>
			<!-- <td><input type='text' name='local_type' required /></td> -->
			<td><select id="local_type" name='local_type' required>
					<c:forEach var="entry" items="${localTypeEnum}">
						<c:if test="${entry.code eq localToUpdate.getLocal_type()}">
							<option selected value='${entry.code}'>${entry.label}</option>
						</c:if>
						<c:if test="${entry.code != localToUpdate.getLocal_type()}">
							<option value='${entry.code}'>${entry.label}</option>
						</c:if>
					</c:forEach>
			</select></td>
		</tr>

		<tr>
			<td class="form_label"><form:label path="product_type_local">Productos del establecimiento:</form:label></td>
			<td><select id="product_type_local" name='product_type_local'
				required>
					<c:forEach var="entry" items="${productTypeLocal}">
						<option value='${entry.code}'>${entry.label}</option>
					</c:forEach>

			</select></td>
		</tr>

		<tr>
			<td class="form_label"><form:label path="local_latitud">Latitud:</form:label></td>
			<td><input id="latInput" type='text' name='local_latitud'
				required value='${localToUpdate.getLocal_latitud()}'/></td>
		</tr>

		<tr>
			<td class="form_label"><form:label path="local_longitud">Longitud:</form:label></td>
			<td><input id="longInput" type='text' name='local_longitud'
				required value='${localToUpdate.getLocal_longitud()}'/></td>
		</tr>

		<tr>
			<td class="form_label"><form:label path="comercial">Comercial:</form:label></td>
			<td><input id="comercial" type='text' name='comercial' required
				value="${localToUpdate.getComercial()}" /></td>
		</tr>


		<tr>
			<td class="form_label"><form:label path="local_desc">Descripción de tu establecimiento</form:label></td>
			<td><textarea maxlength=500 rows=7 name='local_desc' required
					style="min-height: 15em; min-width: 15em;">${localToUpdate.getLocal_desc()}</textarea></td>
		</tr>

		<tr>
			<td class="form_label"><form:label path="local_pay_way">Forma de Pago:</form:label></td>
			<td><select id="local_pay_way_selector" name="local_pay_way"
				size="1" required>
					<c:if test="${localToUpdate.getLocal_pay_way() eq 0 }">
						<option selected value='0'><spring:message
								code="business.payway_cash" /></option>
						<option value='1'><spring:message
								code="business.payway_credit" /></option>
					</c:if>
					<c:if test="${localToUpdate.getLocal_pay_way() eq 1 }">
						<option value='0'><spring:message
								code="business.payway_cash" /></option>
						<option selected value='1'><spring:message
								code="business.payway_credit" /></option>
					</c:if>
			</select>
		</tr>



		<tr>
			<td class="form_label"><form:label path="local_cost_beer">Cerveza</form:label></td>
			<td><input id="local_cost_beer" type='number'
				pattern="[0-9]+([\,][0-9]+)?" step="0.01" name='local_cost_beer'
				value='${localToUpdate.getLocal_cost_beer()}' /><span
				class="profile_span">€ <i>P.V.P. real sin descuento</i></span></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="local_cost_cocktail">Copa</form:label></td>
			<td><input id="local_cost_cocktail" type='number'
				pattern="[0-9]+([\,][0-9]+)?" step="0.01" name='local_cost_cocktail'
				value='${localToUpdate.getLocal_cost_cocktail()}' /><span
				class="profile_span">€ <i>P.V.P. real sin descuento</i></span></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="local_cost_alch_bottle">Botella alcohol</form:label></td>
			<td><input id="local_cost_alch_bottle" type='number' step="1"
				name='local_cost_alch_bottle'
				value='${localToUpdate.getLocal_cost_alch_bottle()}' /><span
				class="profile_span">€ <i>P.V.P. real sin descuento</i></span></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="local_cost_wine_bottle">Botella vino</form:label></td>
			<td><input id="local_cost_wine_bottle" type='number'
				pattern="[0-9]+([\,][0-9]+)?" step="0.01"
				name='local_cost_wine_bottle'
				value='${localToUpdate.getLocal_cost_wine_bottle()}' /><span
				class="profile_span">€ <i>P.V.P. real sin descuento</i></span></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="local_cost_coffe">Café</form:label></td>
			<td><input id="local_cost_coffe" type='number'
				pattern="[0-9]+([\,][0-9]+)?" step="0.01" name='local_cost_coffe'
				value='${localToUpdate.getLocal_cost_coffe()}' /><span
				class="profile_span">€ <i>P.V.P. real sin descuento</i></span></td>
		</tr>
		<tr>
			<td class="form_label"><form:label path="local_cost_tonic">Refresco</form:label></td>
			<td><input id="local_cost_tonic" type='number'
				pattern="[0-9]+([\,][0-9]+)?" step="0.01" name='local_cost_tonic'
				value='${localToUpdate.getLocal_cost_tonic()}' /><span
				class="profile_span">€ <i>P.V.P. real sin descuento</i></span></td>
		</tr>


		<tr>

			<td class="form_label"><form:label path="terms_accpeted">Terminos y condiciones aceptados:</form:label></td>

			<c:if test="${localToUpdate.getTerms_accpeted()==0}">
				<td class="form_label">No</td>
			</c:if>

			<c:if test="${localToUpdate.getTerms_accpeted()==1}">
				<td class="form_label">Sí</td>
			</c:if>



			<input id="terms_accpeted" type='hidden' name='terms_accpeted'
				value="${localToUpdate.getTerms_accpeted()}" />
		</tr>

		<tr>
			<td class="font_gray form_label"><form:label path="trade_name">
					<spring:message code="condition.social" />
				</form:label></td>
			<td><input type='text' name='trade_name'
				value='${localPayInfo.getTrade_name()}' required /></td>
		</tr>

		<tr>
			<td class="font_gray form_label"><form:label path="cif">
					<spring:message code="contact.cif" />
				</form:label></td>
			<td><input type='text' name='cif'
				value='${localPayInfo.getCif()}' /></td>
		</tr>

		<tr>
			<td class="font_gray form_label"><form:label
					path="contact_address">
					<spring:message code="contact.business_addr" />
				</form:label></td>
			<td><input type='text' name='contact_address'
				value='${localPayInfo.getContact_address()}' required /></td>
		</tr>

		<tr>
			<td class="font_gray form_label"><form:label path="contact_cp">
					<spring:message code="contact.business_cp" />
				</form:label></td>
			<td><input type='text' name='contact_cp'
				value='${localPayInfo.getContact_cp()}' maxlength="5"
				onkeypress="return isNumberKey(event)" /></td>
		</tr>

		<tr>
			<td class="font_gray form_label"><form:label path="contact_city">
					<spring:message code="contact.business_city" />
				</form:label></td>
			<td><select id="city_selector_pay" name="contact_city" size="1"
				required>
					<c:forEach var="entry" items="${citiesEnum}">
						<c:if test="${entry.label eq localToUpdate.getCity()}">
							<option selected value='${entry.label}'>${entry.label}</option>
						</c:if>
						<c:if test="${entry.label != localToUpdate.getCity()}">
							<option value='${entry.label}'>${entry.label}</option>
						</c:if>
					</c:forEach>
			</select>
		</tr>



		<tr>
			<td class="form_label"><form:label path="local_iban">Código IBAN:</form:label></td>
			<td><input type='text' id="iban1" pattern="[A-Z]{2}\d{22}$"
				name='local_iban' maxlength="24" style="width: 24em;" /></td>
		</tr>


		<tr>
			<td colspan='2'><input id="submit_btn" name="submit"
				type="submit" value="VALIDAR" /></td>
		</tr>


	</table>

	<input type="hidden" name="${_csrf.parameterName}"
		value="${_csrf.token}" />

</form:form>


<div id="pics_container">
	<%@ include file="pics_section.jsp"%>
</div>
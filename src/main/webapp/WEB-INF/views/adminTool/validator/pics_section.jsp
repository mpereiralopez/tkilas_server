<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


						<form:form id="pic1_form" commandName="localFormUpdateLocalInfo"
			class="profile_forms form_validator localFormUpdateLocalInfo"
			action="admin/update_local_pic" enctype='multipart/form-data' method='POST'>
			<table class="_90">
				<tr>
					<td colspan="2" class="table_title"><spring:message
							code="business.images" /></td>
				</tr>
				<tr>
					<td colspan="2" class="table_line"></td>
				</tr>


				<tr>
					<td>
							<c:if test="${MSG_PIC_UPLOAD_OK_1 != null}">
								<c:if test="${PIC_URL != null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="${PIC_URL}" onclick="chargePic('input_pic1','submit_1');"/>
								</c:if>
								<c:if test="${PIC_URL == null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic1','submit_1');"/>
								</c:if>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_1 == null && localToUpdate.getUrl_pic1() == null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic1','submit_1');"/>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_1 == null && localToUpdate.getUrl_pic1() != null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src='${localToUpdate.getUrl_pic1()}' onclick="chargePic('input_pic1','submit_1');"/>
							</c:if>
						
						
						<input class="picinput" disabled="disabled" id="input_pic1" type="file" name="file" accept="image/gif, image/jpeg, image/png" style="display: none" onchange="readURL(this,1,
						'<spring:message  code="modal.crop.title" />','<spring:message  code="close" />','<spring:message  code="modal.crop" />');">
						</td>
				
					<td colspan='2'>
						<input class="submit_pic" name="save" type="submit" id="submit_1" value="<spring:message  code="business.save" />" disabled="disabled" />
						<input class="del_pic" name="delete" type="submit"  value="<spring:message  code="business.delete" />" />
					
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
					
					<c:if test="${MSG_PIC_UPLOAD_OK_1 != null}">
							<div class="msg_ok">
								<p style="color: white; margin-left: 35px;">${MSG_PIC_UPLOAD_OK_1}</p>
							</div>
					</c:if>
					</td>
				</tr>
			</table>
			<input type='hidden' name='pic_id' id='pic_id' value=1 />
						<input type='hidden' name='x' id='x1' value=0 />
						<input type='hidden' name='y' id='y1' value=0 />
						<input type='hidden' name='w' id='w1' value=0 />
						<input type='hidden' name='h' id='h1' value=0 />
			
			<input type='hidden' name='user_id' id='local_id' value='${localToUpdate.getUserIdUser()}' />
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form:form>
		
		
		
		<form:form commandName="localFormUpdateLocalInfo"
			class="profile_forms form_validator localFormUpdateLocalInfo"
			action="admin/update_local_pic" enctype='multipart/form-data' method='POST'>
			<table class="_90">
				
				<tr>
					<td colspan="2" class="table_line"></td>
				</tr>


				<tr>
					<td>
					<c:if test="${MSG_PIC_UPLOAD_OK_2 != null}">
								<c:if test="${PIC_URL != null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="${PIC_URL}" onclick="chargePic('input_pic2','submit_2');"/>
								</c:if>
								<c:if test="${PIC_URL == null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic2','submit_2');"/>
								</c:if>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_2 == null && localToUpdate.getUrl_pic2() == null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic2','submit_2');"/>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_2 == null && localToUpdate.getUrl_pic2() != null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src='${localToUpdate.getUrl_pic2()}' onclick="chargePic('input_pic2','submit_2');"/>
							</c:if>
						
						<input class="picinput" disabled="disabled" id="input_pic2" type="file" name="file" accept="image/gif, image/jpeg, image/png" style="display: none" onchange="readURL(this,2,
						'<spring:message  code="modal.crop.title" />','<spring:message  code="close" />','<spring:message  code="modal.crop" />');">
						</td>
				
					<td colspan='2'>
						<input class="submit_pic" name="save" type="submit" id="submit_2" value="<spring:message  code="business.save" />"  disabled="disabled"/>
						<input class="del_pic" name="delete" type="submit" value="<spring:message  code="business.delete" />" /></td>
				</tr>
				
				<tr>
					<td colspan="2">
						<c:if test="${MSG_PIC_UPLOAD_OK_2 != null}">
							<div class="msg_ok">
								<p style="color: white; margin-left: 35px;">${MSG_PIC_UPLOAD_OK_2}</p>
							</div>
						</c:if>
						<c:if test="${MSG_ERR != null}">
							<div class="msg_ok">
								<p style="color: white;">${MSG_ERR}</p>
							</div>
						</c:if>
					</td>
				</tr>
			</table>

			<input type='hidden' name='pic_id' id='pic_id' value=2 />
						<input type='hidden' name='x' id='x2' value=0 />
						<input type='hidden' name='y' id='y2' value=0 />
						<input type='hidden' name='w' id='w2' value=0 />
						<input type='hidden' name='h' id='h2' value=0 />
			
			<input type='hidden' name='user_id' id='local_id' value='${localToUpdate.getUserIdUser()}' />
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

			
		</form:form>
		
		
		<form:form commandName="localFormUpdateLocalInfo"
			class="profile_forms form_validator localFormUpdateLocalInfo"
			action="admin/update_local_pic" enctype='multipart/form-data' method='POST'>
			<table class="_90">
				
				<tr>
					<td colspan="2" class="table_line"></td>
				</tr>


				<tr>
					<td>
						
						<c:if test="${MSG_PIC_UPLOAD_OK_3 != null}">
								<c:if test="${PIC_URL != null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="${PIC_URL}" onclick="chargePic('input_pic3','submit_3');"/>
								</c:if>
								<c:if test="${PIC_URL == null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic3','submit_3');"/>
								</c:if>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_3 == null && localToUpdate.getUrl_pic3() == null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic3','submit_3');"/>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_3 == null && localToUpdate.getUrl_pic3() != null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src='${local_info.getUrl_pic3()}' onclick="chargePic('input_pic3','submit_3');"/>
							</c:if>
						
						<input class="picinput" disabled="disabled" id="input_pic3" type="file" name="file" accept="image/gif, image/jpeg, image/png" style="display: none" onchange="readURL(this,3,
						'<spring:message  code="modal.crop.title" />','<spring:message  code="close" />','<spring:message  code="modal.crop" />');">
						</td>
				
					<td colspan='2'><input class="submit_pic"
						name="save" type="submit" id="submit_3"
						value="<spring:message  code="business.save" />" disabled="disabled" />
						
						<input class="del_pic"
						name="delete" type="submit"
						value="<spring:message  code="business.delete" />" /></td>
				</tr>
				
				<tr>
					<td colspan="2">
						<c:if test="${MSG_PIC_UPLOAD_OK_3 != null}">
							<div class="msg_ok">
								<p style="color: white; margin-left: 35px;">${MSG_PIC_UPLOAD_OK_3}</p>
							</div>
						</c:if>
					</td>
				</tr>
			</table>


			<input type='hidden' name='pic_id' id='pic_id' value=3 />
						<input type='hidden' name='x' id='x3' value=0  />
						<input type='hidden' name='y' id='y3' value=0 />
						<input type='hidden' name='w' id='w3' value=0 />
						<input type='hidden' name='h' id='h3' value=0 />

			<input type='hidden' name='user_id' id='local_id'
				value='${localToUpdate.getUserIdUser()}' />

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />

			
		</form:form>
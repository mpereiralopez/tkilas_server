<%@ include file="../init.jsp" %>




 
<link rel="stylesheet" href="resources/css/fonts.css" />
<link rel="stylesheet" href="resources/css/admin.css" />
<link rel="stylesheet" href="resources/css/jquery.Jcrop.min.css" />
<script type="text/javascript" src="resources/js/admin.js"></script>
<script type="text/javascript" src="resources/js/library/jquery.Jcrop.min.js"></script>
<script type="text/javascript" src="resources/js/library/jquery.validate.min.js"></script>
<script type="text/javascript" src="resources/js/library/additional-methods.min.js"></script>

<script type="text/javascript" src="resources/js/library/utils.js"></script>

<script type="text/javascript" src="resources/js/library/form_utils.js"></script>


    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>

<title>Administrador interno local</title>


<script>

function viewValuesToUpdate(local_Id, ele){
	$("#right_size table tr").removeClass("color_gray");
	var url = "<% getServletContext().getContextPath();%>admin/get_local_by_id";
	$(ele).addClass("color_gray");
	$.ajax({
		  type:"GET",
		  url: url,
		  data: {"localId": local_Id,"section":0},
		  success: function(result, textStatus){
			  $("#left_size").html(result);
			 $('.picinput').attr("disabled",false);

		  },
		  error: function (err){
			  console.log(err)
		  }
		});
}

function viewCommentsToUpdate(fingerprint, ele){
	$("#right_size table tr").removeClass("color_gray");
	var url = "<% getServletContext().getContextPath();%>admin/get_comment_by_fingerprint";
	$(ele).addClass("color_gray");
	$.ajax({
		  type:"GET",
		  url: url,
		  data: {"fingerprint": fingerprint},
		  success: function(result, textStatus){
			  $("#left_size").html(result);

		  },
		  error: function (err){
			  console.log(err)
		  }
		});
}



</script>

</head>
<body>
		<%@ include file="../header_normal_template.jsp" %>

	<section class="page">
		<div id="nav_section" class="nav_section">
			<a class="actual manage" href="admin?section=validator">Validar Locales</a>
			<a class="manage"  href="admin?section=viewall">Consultas</a>
			<a class="manage"  href="admin?section=comments">Comentarios</a>
			<a class="manage"  href="admin?section=facturacion">Facturación</a>
			<a class="manage"  href="admin?section=comercial">Informe Comercial</a>
			<a style="float:right; text-decoration: none; padding-right: 15px" href="<c:url value="j_spring_security_logout" />"  target="_self"> <spring:message code="menu.salir"/></a>
			
		</div>

	<c:if test="${template == 0}">
		<%@ include file="validator/validator.jsp" %>
	</c:if>
	<c:if test="${template == 1}">
		<%@ include file="viewall/viewall.jsp" %>
	</c:if>
	<c:if test="${template == 2}">
		<%@ include file="facturacion/search_form.jsp" %>
	</c:if>
	
	<c:if test="${template == 3}">
		<%@ include file="comercial/search_form.jsp" %>
	</c:if>
	
	<c:if test="${template == 4}">
		<%@ include file="comments/commentValidator.jsp" %>
	</c:if>
	

	</section>


		


</body>
</html>
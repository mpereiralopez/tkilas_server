<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<script type="text/javascript">
$('#nav_section').children().removeClass('actual');
$($('#nav_section').children().eq(1)).addClass('actual');
</script>

<style>
.histogram_wrapper {
	position: relative;
	margin: 15px auto;
	max-width: 800px;
	text-align: center;
}

ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
}

li {
	float: left;
}

p {
	display: inline-block;
	-webkit-margin-before: 0;
	-webkit-margin-after: 0;
	margin-bottom: 0;
	margin-top: 0;
}

.box {
	display: inline-block;
	width: 15px;
	height: 15px;
	border: 1px solid gray;
	margin-right: 0.5em;
	margin-left: 0.5em;
}

.gray {
	background-color: gray;
}

.yellow {
	background-color: yellow;
}

.green {
	background-color: #00ea27;
}

.red {
	background-color: red;
}
.orange{
	background-color: orange;
}
</style>

<section class="modal_container" id="modalWaiting">
	<div>
		<img src="resources/img/loading.gif" alt="gif" width="90" height="90"
			style="display: block; margin: auto; position: absolute; top: 0; right: 0; left: 0; bottom: 0;">
	</div>
</section>


<script type="text/javascript">
function getModalWithLocalInfo(localId){
		$("#modalWaiting").css("display","block");
		$("#modalWaiting").css("visibility","visible");
		$("#modalWaiting").css("height","100%");
		$("#search_results_wrapper").empty();
		var url = "<%=getServletContext().getContextPath()%>/admin/get_local_by_id";
			$.ajax({
				type : "GET",
				url : url,
				data : {
					"localId" :localId,"section":1
				},
				success : function(result, textStatus) {
					$("#modalWaiting").css("display","none");
					$("#modalWaiting").css("visibility","hidden");
				
					$("#modal-update-local").empty();
					$("#modal-update-local").html(result);
					
					 $( "#modal-update-local" ).dialog('open');
				},
				error : function(err) {
					console.log(err)
				}
			});
		
}
</script>

<div id="modal_for_update" class="modal">
	<%@ include file="popup/local_info.jsp"%>
</div>


<section class="wrapper">

	<c:if test="${MSG_OK != null}">
		<div class="msg_ok">
			<p style="color: white; margin-left: 35px;">${MSG_OK}</p>
		</div>
	</c:if>

	<div class="histogram_wrapper">
		<ul>
			<li><div class="box gray"></div>
				<p>Validado</p></li>
			<li><div class="box yellow"></div>
				<p>No acepto términos</p></li>
			<li><div class="box green"></div>
				<p>Pendiente de validar</p></li>
			<li><div class="box red"></div>
				<p>Validado sin aceptar términos</p></li>
				<li><div class="box orange"></div>
				<p>Local modificado</p></li>

		</ul>
	</div>


	<div class="table_container" id="viewallTable"
		style="overflow-x: scroll;">


		<table class="report w100">
			<thead>
				<tr>
					<th>ID</th>
					<th>CONTACTO</th>
					<th>E-MAIL</th>
					<th>LOCAL</th>
					<th>Teléfono</th>
					<th>Dirección</th>
					<th>Aforo</th>
					<th>Distrito</th>
					<th>Comercial</th>
					<th>Fecha de registro</th>
					<th>Suplantar</th>
				</tr>
			</thead>

			<c:forEach items="${locals}" var="local">

				<c:if
					test="${local.getTerms_accpeted() == 0 && local.getStatus() == 0}">
					<tr style="background-color: yellow">
				</c:if>
				<c:if
					test="${local.getTerms_accpeted() == 0 && local.getStatus() == 1}">
					<tr style="background-color: red">
				</c:if>
				<c:if
					test="${local.getTerms_accpeted() == 1 && local.getStatus() == 0}">
					<tr style="background-color: #00ea27;">
				</c:if>
				<c:if
					test="${local.getTerms_accpeted() == 1 && local.getStatus() == 1}">
					<tr style="background-color: gray;">
				</c:if>
				<c:if
					test="${local.getTerms_accpeted() == 1 && local.getStatus() == 2}">
					<tr style="background-color: orange;">
				</c:if>
				<td onclick="getModalWithLocalInfo(${local.getUserIdUser()});return true;">${local.getUserIdUser()}</td>
				<td onclick="getModalWithLocalInfo(${local.getUserIdUser()});return true;">${local.getUser().getUser_name()}</td>
				<td onclick="getModalWithLocalInfo(${local.getUserIdUser()});return true;">${local.getUser().getEmail()}</td>
				<td onclick="getModalWithLocalInfo(${local.getUserIdUser()});return true;">${local.getLocal_name()}</td>
				<td onclick="getModalWithLocalInfo(${local.getUserIdUser()});return true;">${local.getTlf()}</td>
				<td onclick="getModalWithLocalInfo(${local.getUserIdUser()});return true;">${local.getAddress()}</td>
				<td onclick="getModalWithLocalInfo(${local.getUserIdUser()});return true;">${local.getCapacity()}</td>
				<td onclick="getModalWithLocalInfo(${local.getUserIdUser()});return true;"><c:forEach var="entry" items="${districtEnum}">
							<c:if test="${entry.code eq local.getDistrict()}">
								${entry.label}
							</c:if>
						</c:forEach></td>
				<td onclick="getModalWithLocalInfo(${local.getUserIdUser()});return true;">${local.getComercial()}</td>
				<td onclick="getModalWithLocalInfo(${local.getUserIdUser()});return true;"><fmt:formatDate value="${local.getUser().getCreateTime()}"
						pattern="dd/MM/yyyy" /></td>
						
				<td><a href="j_spring_security_switch_user?j_username=${local.getUser().getEmail()}"><img src="resources/img/impersonate_user.png" /></a></td>
				</tr>


			</c:forEach>
		</table>
	</div>

</section>



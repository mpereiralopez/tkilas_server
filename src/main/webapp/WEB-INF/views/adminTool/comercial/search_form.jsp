<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<link href="resources/css/jquery-ui.min.css" rel="stylesheet"
	type="text/css">



<style>
.row{
	padding-top: 0.25em;
	padding-bottom: 0.25em;
}
.wrapper {
	position: relative;
	max-width: 1440px;
	width: 100%;
	margin: 0 auto;
	display: table;
}

#search_form {
	margin-top: 1em;
	width: 50%;
	float: left;
}

#help {
	margin-top: 1em;
	width: 49%;
	float: right;
}

#modalWaiting{
	display: none;
	visibility: hidden;
}

label{
	padding-right: 1em;
}
</style>

<script type="text/javascript">
$('#nav_section').children().removeClass('actual');
$($('#nav_section').children().eq(4)).addClass('actual');
</script>
<section class="wrapper">
 <div class="margined">
	<div style="display: table; width: 100%;">
	<section id="search_form">
		<h1 class="report_subtitle">Informe Comercial</h1>
		<h2 class="font_green">Buscador</h2>
		
		<div class="row">

			<label class="font_green" for="mes">Mes:</label> <select id="month" name="mes" size="1"
				onchange="getComercialData();">
				<option value=-1>--</option>
				<option value='0'>Enero</option>
				<option value='1'>Febrero</option>
				<option value='2'>Marzo</option>
				<option value='3'>Abril</option>
				<option value='4'>Mayo</option>
				<option value='5'>Junio</option>
				<option value='6'>Julio</option>
				<option value='7'>Agosto</option>
				<option value='8'>Septiembre</option>
				<option value='9'>Octubre</option>
				<option value='10'>Noviembre</option>
				<option value='11'>Diciembre</option>
			</select>
		</div>
		<div class="row">
			<label class="font_green" for="year">Año:</label> <select id="year" name="year" size="1"
				onchange="getComercialData();">
				<option value=-1>--</option>
				<option value=2014>2014</option>
				<option value=2015>2015</option>
			</select>
		</div>
		
	</section>
	
</div>

	<script type="text/javascript">
	function getComercialData(){
		if($("#month").val()!=-1 && $("#year").val()!=-1){
			$("#modalWaiting").css("display","block");
			$("#modalWaiting").css("visibility","visible");
			$("#search_results_wrapper").empty();
			var url = "<%=getServletContext().getContextPath()%>/admin/get_comercial_info_by_month_year";
				$.ajax({
					type : "GET",
					url : url,
					data : {
						"month" : $("#month").val(),
						"year" : $("#year").val()
					},
					success : function(result, textStatus) {
						$("#modalWaiting").css("display","none");
						$("#modalWaiting").css("visibility","hidden");
						$("#search_results_wrapper").empty();
						console.log(result);
						$("#search_results_wrapper").html(result);
					},
					error : function(err) {
						console.log(err)
					}
				});
			}
		}
	</script>
	
	<div id="down_side" style="position: relative; min-height: 500px;">
	
		<section class="modal_container" id="modalWaiting">
			<div>
				<img src="resources/img/loading.gif" alt="gif" width="90"
					height="90"
					style="display: block; margin: auto; position: absolute; top: 0; right: 0; left: 0; bottom: 0;">
			</div>
		</section>
		
		<section id="search_results_wrapper">
		
		<%@ include file="search_results.jsp"%>
	</section>
	
	</div>
	
</div>
</section>

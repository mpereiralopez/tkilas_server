<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<hr>

<c:if test="${not empty registeredLocals}">
	<div id="resume_data" class="w50">
	
	<h2 class="report_subtitle font_green">Datos Resumen</h2>
	<table class="report w100">
		<thead>
			<tr>
				<th class="w60"></th>
				<th class="w20">MES</th>
				<th class="w20">HISTÓRICO</th>
			</tr>
		</thead>
		<tr class="color_gray_clear">
			<td>Número establecimientos registrados</td>
			<c:forEach items="${registeredLocals}" var="data">
				<td class="centered">${data}</td>
			</c:forEach>
		</tr>
		<tr>
			<td>Número establecimientos con ofertas</td>
			<c:forEach items="${localWithOffers}" var="data">
				<td class="centered">${data}</td>
			</c:forEach>
		</tr><tr class="color_gray_clear">
			<td>Número reservas</td>
			<c:forEach items="${numReserves}" var="data">
				<td class="centered">${data}</td>
			</c:forEach>
		</tr><tr>
			<td>Ingresos</td>
			<c:forEach items="${ingresos}" var="data">
				<td class="centered">${data}€</td>
			</c:forEach>
		</tr><tr class="color_gray_clear">
			<td>Ticket Medio</td>
			<c:forEach items="${listaTicketMedio}" var="data">
				<td class="centered">
					<fmt:formatNumber type="number" minFractionDigits="2"
						maxFractionDigits="2" value="${data}" />€/reserva
					</td>
			</c:forEach>
		</tr><tr>
			<td>Número clientes</td>
			<c:forEach items="${numUsuarios}" var="data">
				<td class="centered">${data}</td>
			</c:forEach>
		</tr>
		
		</table>
	</div>
	
	<div class="w100">
	<h2 class="report_subtitle font_green">Detalle Establecimientos</h2>
	<table class="report w100">
		<thead>
			<tr>
				<th class="w5">Id</th>
				<th class="w25">Establecimiento</th>
				<th class="w10">Comercial</th>
				<th class="w10">Fecha Registro</th>
				<th class="w5">Nº Reservas</th>
				<th class="w10">% Reservas/Periodo</th>
				<th class="w5">Ingresos</th>
				<th class="w10">% Ingresos/Periodo</th>
				<th class="w10">Ingresos Acumulado</th>
				<th class="w10">%Ingresos Acumulado/Periodo</th>

			</tr>
		</thead>
		
		<c:forEach items="${rows}" var="row" varStatus="loop">
			<c:if test="${loop.index % 2 == 0}">
				<tr class="color_gray_clear">
			</c:if>
			
			<c:if test="${loop.index % 2 != 0}">
				<tr>
			</c:if>
				<td class="centered">${row.getLocalId()}</td>
				<td>${row.getLocal_name()}</td>
				<td class="centered">${row.getComercial()}</td>
				<td class="centered">${row.getCreate_time()}</td>
						
				<td class="centered">${row.getNum_reservas()}</td>

				<td class="centered"><fmt:formatNumber type="number" minFractionDigits="2"
						maxFractionDigits="2" value="${row.getReservas_percentage()}" />%</td>
				<td class="centered"><fmt:formatNumber type="number" minFractionDigits="2"
						maxFractionDigits="2" value="${row.getIngresos_local_periodo_total()}" />€</td>
				<td class="centered"><fmt:formatNumber type="number" minFractionDigits="2"
						maxFractionDigits="2" value="${row.getIngresos_periodo_percentage_local()}" />%</td>
				<td class="centered"><fmt:formatNumber type="number" minFractionDigits="2"
						maxFractionDigits="2" value="${row.getIngresos_acumulado_local_periodo()}" />€</td>
				<td class="centered"><fmt:formatNumber type="number" minFractionDigits="2"
						maxFractionDigits="2" value="${row.getIngresos_acumulado_local_percentage()}" />%</td>

			</tr>

		</c:forEach>
		
		<tr class="color_green">
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			
			<td class="centered">${rows[0].getNum_total_reservas()}</td>
			<td class="centered">100%</td>
			<td class="centered">${rows[0].getIngresos_periodo_total()}</td>
			<td class="centered">100%</td>
			<td class="centered">${rows[0].getIngresos_acumulado_local_total()}</td>
			<td class="centered">100%</td>
		</tr>


	</table>

	
</div>
	
</c:if>



<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<hr>

<c:if test="${not empty local_selected}">
	<div id="data_resume" class="w50">
		<h2 class="report_subtitle font_green">Datos</h2>
			<div class="row">
		<label for="contac_name">Razón social:</label><label>${ local_selected.getTrade_name() }</label>
	</div>
	
	<div class="row">
		<label for="cif">CIF:</label>
		<label> ${ local_selected.getTrade_name() }</label> 
	</div>
	
	<div class="row">
		<label for="address">Dirección:</label><label>${ local_selected.getContact_address() }</label>
	</div>
	
	<div class="row">
		<label for="tlf">Teléfono:</label><label>${ local_selected_info.getTlf() }</label>
	</div>
	
	<div class="row">
		<label for="email">Email:</label><label>${ local_selected_info.getUser().getEmail() }</label>
	</div>
	
	<div class="row">
		<label for="comercial">Comercial:</label><label>${ local_selected_info.getComercial() }</label>
	</div>
	
	<div class="row">
		<label for="fecha_emision">Fecha Emision:</label><label><fmt:formatDate value="${ factura.getFecha_emision() }" pattern="dd/MM/yyyy" /></label>
	</div>
	
	<div class="row">
		<label for="factura">Nº Factura:</label><label>${ factura.getNum_factura() }</label>
	</div>
	
	</div>
	
	<hr>

	<div id="table_result_reserves">
	<h2 class="report_subtitle font_green">Detalle de Establecimientos</h2>
	<table class="report w100">
		<thead>
			<tr>
				<th>Nº</th>
				<th>FECHA</th>
				<th>USUARIO</th>
				<th>TIPO</th>
				<th>Nº PERSONAS</th>
				<th>IMPORTE</th>
				<th>COSTE UNITARIO</th>
				<th>SUBTOTAL</th>
				<th>IVA</th>
				<th>TOTAL</th>

			</tr>
		</thead>

		<c:forEach items="${tableInfo}" var="reserva" varStatus="loop">
			<tr>
				<td class="centered">${loop.index}</td>
				<td class="centered"><fmt:formatDate value="${reserva.getDate()}"
						pattern="dd/MM/yyyy" /></td>
				<td class="centered">${reserva.getUser()}</td>

				<c:choose>
					<c:when test="${ reserva.getType()==0}">
						<td class="centered">A</td>
						<td class="centered">--</td>
						<td class="centered">${reserva.getPvp()*reserva.getNum_pers()}</td>

					</c:when>
					<c:otherwise>
						<td class="centered">B</td>
						<td class="centered">${reserva.getNum_pers()}</td>
						<td class="centered">--</td>
					</c:otherwise>
				</c:choose>



				<td class="centered"><fmt:formatNumber type="number" minFractionDigits="2"
						maxFractionDigits="2" value="${reserva.getUnit_cost()}" /></td>
				<td class="centered"><fmt:formatNumber type="number" minFractionDigits="2"
						maxFractionDigits="2" value="${reserva.getSubtotal()}" /></td>
				<td class="centered"><fmt:formatNumber type="number" minFractionDigits="2"
						maxFractionDigits="2" value="${reserva.getIva()}" /></td>
				<td class="centered"><fmt:formatNumber type="number" minFractionDigits="2"
						maxFractionDigits="2" value="${reserva.getTotal()}" /></td>

			</tr>

		</c:forEach>
		<tr>
		
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td class="centered">
			<span>Subtotal: <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${ factura.getSubtotal() }" /></span>
			</td>
			
			<td class="centered">
				<span>IVA:<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${ factura.getTotal()-factura.getSubtotal() }" /> </label> 
			</td>
			
			
			<td class="centered">
				<span>Total con IVA: <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${ factura.getTotal() }" /></span> 
			</td>
		
		</tr>


	</table>

	
</div>
	
</c:if>



<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<style>

.modal_container {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-color: rgba(0, 0, 0, 0.6);
	background: rgba(0, 0, 0, 0.6);
	color: rgba(0, 0, 0, 0.6);
	visibility: hidden;
	display: none;
	z-index: 500;
}

#video_container {
	display: none;
}

.custom_close_buttom {
	width: 31px;
	height: 30px;
	background-image: url(resources/img/close_btn_vid.png);
	position: absolute;
	cursor: pointer;
	z-index: 600;
}

#contact_form_wrapper {
	background-color: white;
	width: auto;
	height: auto;
	position: relative;
	margin: 1.5em auto;
	padding: 1%;
	max-width: 450px;
}


#footer {
	left: 0;
	bottom: 0;
}

#footer h4 {
	margin-left: 0.5em;
	margin-top: 0;
	margin-bottom: 0;
	color: #000;
	display: inline;
}

.header_footer_container div {
	display: inline;
	margin: 0 auto;
}

.header_footer_container a {
	margin-left: 1em;
	margin-top: 0;
	margin-bottom: 0;
	color: white;
	font-size: 13px;
}

.m-navigation-footer {
	position: absolute;
	right: 0;
	z-index: 100;
	height: 8%;
	color: #333;
	background-color: rgba(255, 255, 255, 0.8);
}

.m-navigation-footer a {
	font-family: Aller;
	font-size: 13px;
	color: #333;
	text-decoration: none;
	display: inline-block;
	border-radius: 16px;
	cursor: pointer;
}
</style>

<script type="text/javascript">
	$(document).ready(
			function() {
				$('#footer h4').css('line-height',
						($('#footer').height() - 1) + 'px');
				$('#footer div').css('line-height',
						($('#footer').height() - 1) + 'px');
				$(window).resize(
						function() {

							$('#footer h4').css('line-height',
									($('#footer').height() - 1) + 'px');
							$('#footer div').css('line-height',
									($('#footer').height() - 1) + 'px');
						});
			});
</script>

<nav id="footer" class="m-navigation-footer">
	<div class="header_footer_container">
		<h4>&copy; Tkilas 2015</h4>
		<div>
			<a href="about" target="_self"><spring:message code="footer.who" /></a> 
			<!-- <a href="//blog.tkilas.com" target="_blank"><spring:message code="footer.blog" /></a>  -->
			<a href="legal" target="_self" id="legal_cond"><spring:message code="footer.legal" /></a>

		</div>
	</div>
</nav>

<div id="dialog_contact_form" title="<spring:message code="contact.us" />">
	<%@ include file="contact_form.jsp"%>
</div>
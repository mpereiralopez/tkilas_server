<%@ include file="init.jsp" %>

<% pageContext.setAttribute("citiesEnum", EnumUtils.CitiesEnum.values()); %>
<link rel="stylesheet" href="resources/css/style.css" />
<script type="text/javascript" src="resources/js/home.js"></script>
<script type="text/javascript" src="resources/js/library/jquery.validate.min.js"></script>
<script type="text/javascript" src="resources/js/library/additional-methods.min.js"></script>
<script type="text/javascript" src="resources/js/library/form_utils.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	validateMyForm();
	
	$( "#dialog_contact_form" ).dialog({
        autoOpen: false,
        modal:true,
        minWidth: 500,
    });
    
    
    $( "#contact_box" ).click(function() {
        $( "#dialog_contact_form" ).dialog( "open" );
      });
});
</script>
<title><spring:message code="title.normal" /></title>




<style type="text/css">
input.error{
	border-color: red;
}
label.error{
	color:red;
	font-size: 0.75em;
	padding-left: 0.5em;
	clear: both;
	display: block;
}

</style>

</head>
<body>
		<%@ include file="header_normal_template.jsp" %>

	<div class="wrapper" class="page current">
		<div id="nav_section" class="nav_section">
			<a class="actual"><spring:message code="register.index" /></a>
			 <a><spring:message code="register.why" /></a>
			  <a><spring:message code="register.inscription" /></a> <p>639 76 43 07</p>
		</div>
		
		<section class="wrapper horizontally_move" id="inicio">
			<div id="d1" class="diapo white">
				<img src="resources/img/iph_inicio.png"></img>
				<div id="text_d1_wrapper"><p><spring:message code="register.publish" /></p>
				<div class="free_inscription_claim"><spring:message code="register.freeinscription" /></div></div>
			</div>
			<div id="d2" class="diapo grey">
				<img src="resources/img/inicio_m1.png"></img>
				<div>
				<p class="text_v align_rigth"><spring:message code="register.question" /></p>
				<p class="text_g align_rigth"><spring:message code="register.questionText" /></p>
				</div>
			</div>
			<div id="d3" class="diapo white">
				<div>
				<p class="text_v"><spring:message code="register.answer" /></p>
				<p class="text_g"><spring:message code="register.answerText" /></p>
				</div>
				<img src="resources/img/inicio_m2.png"></img>
			</div>
			<div id="d4" class="diapo grey">
				<img src="resources/img/inicio_m3.png"></img>
				<div>
				<p class="text_v align_rigth"><spring:message code="register.wait" /></p>
				<p class="text_g align_rigth"><spring:message code="register.waittext" /></p>
				</div>
			</div>
			<div id="d5" class="diapo">
				<div id="text_d5_wrapper">
					<img src="resources/img/logo_landing.png" alt="" />
					<p style=" color: white;text-align: center;font-size: 16pt;"><spring:message code="register.d61" /></p>
				<div id="first_inscription" class="free_inscription_claim"><spring:message code="register.freeinscription" /></div></div>
				<a class="download_app appIOS"><spring:message code="available.appstore" /></a>
				<a class="download_app appAndroid"><spring:message code="available.google" /></a>
			</div>
			
		</section>
		
		
		<section class="wrapper horizontally_move" id="tarifas">
			<div id="tarifas_text_wrapper">
			
				<div id="devices_wrapper">
					<img id="ios_example" class="mobiles dev_tarifas_ios"src="resources/img/iph_mob.png" title="iph_mob" alt="iph_mob"></img>
					<img id="android_example" class="mobiles dev_tarifas_android" src="resources/img/and_mob.png" title="and_mob" alt="and_mob"></img>
				</div>
				
				
				
				<div id="t_w">
					<h1><spring:message code="register.beneficio" /></h1>
				
					<p class="tarfia_list"><spring:message code="register.tarfia_list1" /></p>
					<p class="tarfia_list"><spring:message code="register.tarfia_list2" /></p>
					<p class="tarfia_list"><spring:message code="register.tarfia_list3" /></p>
					<p class="tarfia_list"><spring:message code="register.tarfia_list4" /></p>
					<p class="tarfia_list"><spring:message code="register.tarfia_list5" /></p>
					<p class="tarfia_list"><spring:message code="register.tarfia_list6" /></p>
					
				
				
					<div class="free_inscription_claim" style="width: 50%;margin: 0;"><spring:message code="register.freeinscription" /></div>
								<p class="quote"><spring:message code="register.reservecost" /></p>
				
				</div>
				
				
				
			</div>
		</section>
	
		<section class="wrapper horizontally_move" id="inscripcion">
			<div id="inscripcion_wrapper">
				<div id="left_size"><div id="register_side_izq" class="register_side"><h1><spring:message code="register.thanks" /></h1><img src="resources/img/inscripcion.png"></img></div></div>
				<div id="right_size">
					<form:form commandName="localForm"  class="form_validator" action="register" method='POST'>
		  				<div class="register_side">
		  					<table class="table_title">
		  					<tr><td class="table_section"><spring:message code="contact.title" /></td></tr>
		  				</table>
		  				
		  				<table class="table_content">		  				
						<tr>
							<td class="form_label" ><form:label path="user_name"><spring:message code="contact.name" /></form:label></td>
							<td><input type='text' maxlength="20" name='user_name' value='' title=""></td>
						</tr>
						<tr>
							<td class="form_label"><form:label path="user_surname"><spring:message code="contact.surname" /></form:label></td>
							<td><input type='text' maxlength="75" name='user_surname' title=""  /></td>
						</tr>
						<tr>
							<td class="form_label"><form:label path="email"><spring:message code="login.mail.placeholder" /></form:label></td>
							<td><input type='email' name='email' title="" /></td>
						</tr>
						<tr>
							<td class="form_label"><form:label path="user_pass"><spring:message code="login.password.placeholder" /></form:label></td>
							<td><input type='password' id="password" name='user_pass' title="" /></td>
						</tr>
						
						<tr>
							<td class="form_label"><form:label path="user_pass"><spring:message code="register.passwordrepeat" /></form:label></td>
							<td><input type='password' name='user_pass_repeated'  title="" /></td>
						</tr>
						</table>
						<table class="table_title">
							<tr><td class="table_section" style="padding-top: 15px;"><spring:message code="contact.business_title" /></td></tr>
						</table>
						
						<table class="table_content">
						
						
						<tr>
							<td class="form_label"><form:label path="local_name"><spring:message code="contact.business_name" /></form:label></td>
							<td><input type='text' maxlength="50" name='local_name'  /></td>
						</tr>
						<tr>
							<td class="form_label"><form:label path="local_phone"><spring:message code="contact.business_tlf" /></form:label></td>
							<td><input type='tel' maxlength="15" name='local_phone'  /></td>
						</tr>
						<tr>
							<td class="form_label"><form:label path="local_address"><spring:message code="contact.business_addrnormal" /></form:label></td>
							<td><input type='text' maxlength="300" name='local_address' /></td>
						</tr>
						<tr>
							<td class="form_label"><form:label path="local_cp"><spring:message code="contact.business_cp" /></form:label></td>
							<td><input type='text'  onkeypress="return isNumberKey(event);"  maxlength="5" name='local_cp' /></td>
						</tr>
							<tr>
								<td class="form_label"><form:label path="local_city"><spring:message code="contact.business_city" /></form:label></td>
								<td><select name="local_city" size="1">
										<c:forEach var="entry" items="${citiesEnum}">
    										<option value='${entry.label}'>${entry.label}</option>
										</c:forEach>
										
								</select>
							</tr>
						<tr>
							<td colspan='2' style="padding-top: 0.5em;"><input id="submit_btn" name="submit" type="submit" value="REGISTRAR" /></td>
						</tr>
		  				</table>
		  				
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		  			</div>
		  				
 
				</form:form>
				
				
			</div>
		</div>
			
	</section>
		
		
		
</div>
	
<div id="contact_box" >
		<div id="letter_wrapper"><img src="resources/img/em_landing.png" title="ml" alt="ml" /></div>
		<p><spring:message code="contact.us" /></p>
	</div>

	<%@ include file="footer_template.jsp" %>


</body>
</html>
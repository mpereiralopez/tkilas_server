<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="contact_form_wrapper">
	<form class="cmxform" id="commentForm" method="get" action="">
		<div class="legend" >Escríbenos tus dudas, sugerencias o incidencias.
			Pronto contactaremos contigo</div>
		<table style="margin-top: 10%; width: 100%;">
			<tr>
				<td><label class="form_label" for="cname">Nombre: </label></td>
				<td><input id="cname" name="name" type="text" required /></td>
			</tr>
			<tr>
				<td><label class="form_label" for="cemail">E-Mail: </label></td>
				<td><input id="cemail" name="email" type="email" required /></td>
			</tr>
			<tr>
				<td><label class="form_label" for="curl">Tipo de
						consulta</label></td>
				<td><select class="contact_form_select" id="type_selector"
					name="type_selector" size="1" required>
						<option value="Comercial">Comercial</option>
						<option value="Sugerencia">Sugerencia</option>
						<option value="Otros">Otros</option>
				</select></td>
			</tr>
			<tr>
				<td><label class="form_label" for="ccomment">Descripción</label>
				</td>
				<td><textarea id="ccomment" maxlength=500 rows=7 name='comment'
						required></textarea></td>
			</tr>
			<tr>
				<td colspan='2'><input id="send_mail_btn" class="submit"
					type="submit" value="Enviar" /></td>
			</tr>
		</table>
	</form>

</div>
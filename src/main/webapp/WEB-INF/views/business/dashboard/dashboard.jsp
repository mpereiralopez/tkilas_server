<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
var packsDates= new Array();
</script>
<c:forEach items="${local_offers}" var="pack">
	<script type="text/javascript">
	packsDates.push("${pack.getId().getDate()}");
	</script>
</c:forEach>

<script type="text/javascript">
$(document).ready(function() {
	validateMyForm();
	
	$('.radio_disscount').change(function() {
		if ($('#calendar_container').multiDatesPicker('getDates').length > 0) {
			$("#publish_offer").removeClass("oculto");
			$("#publish_offer").removeAttr("disabled");
			$("#clearRadioBtn").attr("disabled",false);
		}

		// $("#delete_offer").removeClass("oculto");
	});

	if("${ not empty pack}"){
		$("#hour_ini > option").each(function() {
		    if(this.text == "${pack.getTimeIni().toString().split(':')[0]}")$("#hour_ini").val(this.value);
		});
		
		$("#min_ini > option").each(function() {
		    if(this.text == "${pack.getTimeIni().toString().split(':')[1]}")$("#min_ini").val(this.value);
		});
		
		$("#hour_fin > option").each(function() {
		    if(this.text == "${pack.getTimeFin().toString().split(':')[0]}")$("#hour_fin").val(this.value);
		});
		
		$("#min_fin > option").each(function() {
		    if(this.text == "${pack.getTimeFin().toString().split(':')[1]}")$("#min_fin").val(this.value);
		});
		
		 $("#offer_section").removeClass("disabled");
		  $("#promo_section").removeClass("disabled");	
	}
	
	
	
	
	$( "#calendar_container" ).multiDatesPicker({
		firstDay: 1,
		minDate:-1,
		dateFormat: "yy-mm-dd",
		monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembere", "Octubre", "Noviembre", "Diciembre" ],
	  	monthNamesShort: [ "Ene", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
		dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
	  	dayNamesMin: [ "Do", "Lu", "Ma", "Mie", "Jue", "Vie", "Sa" ],

		onSelect: function(date, inst) {
	        
            if(packsDates.indexOf(date)!=-1){         	
            	viewPackInfoOfDate(date);
            	return false;
            }else{
            	$("#delete_offer").remove();
            	updateReservesList(date);
            	offuscateAll();				
            	if($('#calendar_container').multiDatesPicker('getDates').length >0){
            		$("#hour_ini").attr("disabled",false);
            		$("#min_ini").attr("disabled",false);
            		

            	}else{
            		$("#hour_ini").attr("disabled",true);
            		$("#min_ini").attr("disabled",true); 		
            	}
            }
            },
	});
	if(packsDates.length>0){
		$('#calendar_container').multiDatesPicker('addDates', packsDates);
	}
	
	
	
	function viewPackInfoOfDate(date){
		var url = "<% getServletContext().getContextPath();%>gestor/get_pack_info_by_date";
		$.ajax({
			  type:"GET",
			  url: url,
			  data: {"localId": $("#localId").val(), "date":date},
			  success: function(result, textStatus){
				  $("#dashboard_wrapper").html(result);
				  $("#offer_section").removeClass("disabled");
				  $("#promo_section").removeClass("disabled");
				  $("#dateforuploadreservelist").val(date);
				  updateReservesList(date);
				  $( "#calendar_container" ).datepicker( "setDate", date );
				  $("#dateValue").val(date);
				

			  },
			  error: function (err){
				  console.log(err)
			  }
			});
	}
	
	$("#delete_offer").click(function(){
		var url = "<% getServletContext().getContextPath();%>gestor/delete_pack";
		var date = $("#dateValue").val();
		$("#modalWaiting").css("display","block");
		$("#modalWaiting").css("visibility","visible");
		$.ajax({
			type:"POST",
			url: url,
			data: {"localId": $("#localId").val(), "date":$("#dateValue").val()},
			  success: function(result, textStatus){
				  window.location.reload();
			  },
			  error: function (err){
				  console.log(err)
			  }
			});
	});
	
});

</script>





<section class="wrapper horizontally_move landscape" id="inicio">
	


	<section id="inicio_left_side">
	
	<c:if test="${not empty MSG_OFERTA_OK}">
			<div class="msg_ok">
				<p style="color: white; margin-left: 35px;">${MSG_OFERTA_OK}</p>
			</div>
	</c:if>
		<div id="date_container">

			<div id="calendar_container"></div>
			<section class="date_size_container">
				<p id="dateToPublish" style="display: none"></p>
				<h1>Selecciona hora y número de ofertas:</h1>
				<div style="width: 100%; display: table; padding-bottom: 0.5em;">
					<p style="display: table-cell; width: 30%">Hora inicio:</p>
					<select id="hour_ini" disabled class="promo_date_input"
						onchange="hourfinstablish();">
						<option selected value="-1">--</option>
						<option value="1">01</option>
						<option value="2">02</option>
						<option value="3">03</option>
						<option value="4">04</option>
						<option value="5">05</option>
						<option value="6">06</option>
						<option value="7">07</option>
						<option value="8">08</option>
						<option value="9">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">00</option>
					</select> <span class="span_time"> hh </span> <select disabled id="min_ini"
						class=promo_date_input>
						<option selected value="-1">--</option>
						<option value="00">00</option>
						<option value="30">30</option>
					</select> <span class="span_time"> mm</span>
				</div>
				<div style="width: 100%; display: table; padding-bottom: 0.5em;">
					<p style="display: table-cell; width: 30%">Hora final:</p>
					<select id="hour_fin" disabled class="promo_date_input">
						<option selected value="-1">--</option>
						<option value="1">01</option>
						<option value="2">02</option>
						<option value="3">03</option>
						<option value="4">04</option>
						<option value="5">05</option>
						<option value="6">06</option>
						<option value="7">07</option>
						<option value="8">08</option>
						<option value="9">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">00</option>

					</select><span class="span_time"> hh </span> <select id="min_fin" disabled
						class=promo_date_input>
						<option selected value="-1">--</option>						
						<option value="00">00</option>
						<option value="30">30</option>
					</select> <span class="span_time"> mm</span>
				</div>
				<div style="width: 100%; padding-bottom: 0.75em">
					<span class="span_time span_quote">(Ofertas validas hasta 30
						minutos más tarde de la hora establecida como fin de oferta)</span>
				</div>
				<div style="width: 100%; display: table; padding-bottom: 0.5em;">
					<p style="display: table-cell;">Máx. reservas:*</p>
					<input id="size" disabled type="number" class="promo_date_input" min=1 max="<%= session.getAttribute("local_capacity") %>"  step="1" value="${pack.getSize()}" onkeypress="return isNumber(event)" onchange="onSizeChange(this,event);return true;" ></input><span class="span_time span_quote">*
						Máx: 80% de la capacidad de su local.</span>
				</div>
				<div style="width: 100%;">
					<span class="span_time span_quote">*Si selecciona más de una
						oferta, el número máximo de personas se distribuirá de forma
						automática en función de la elección de los consumidores</span>
				</div>
			</section>



		</div>

		<div id="pac_container">

			<!-- <h1 id="offerTitle">No hay ofertas publicadas para el día: </h1>
        					<button id="publish_offer_btn">Crear Oferta</button> -->

			<div id="offer_editor_container">
				<div id="offer_section" class="section disabled">

					<h4>A) Descuento en todas las bebidas que consuman los clientes (excepto botellas):</h4>
				<c:choose>
					<c:when test="${pack.getDiscount().getDiscount() == 50}">
						<input checked class="radio_disscount" type="radio" name="discount" value=50><label>50%</label> 
						<input disabled class="radio_disscount" type="radio" name="discount" value=40><label>40%</label>
						<input disabled class="radio_disscount" type="radio" name="discount" value=30><label>30%</label>
					</c:when>
					<c:when test='${pack.getDiscount().getDiscount() == 40}'>
						<input disabled class="radio_disscount" type="radio" name="discount" value=50><label>50%</label> 
						<input checked class="radio_disscount" type="radio" name="discount" value=40><label>40%</label>
						<input disabled class="radio_disscount" type="radio" name="discount" value=30><label>30%</label>
					</c:when>
					<c:when test="${pack.getDiscount().getDiscount() == 30}">
						<input disabled class="radio_disscount" type="radio" name="discount" value=50><label>50%</label> 
						<input disabled class="radio_disscount" type="radio" name="discount" value=40><label>40%</label>
						<input checked class="radio_disscount" type="radio" name="discount" value=30><label>30%</label>
					</c:when>
					<c:when test="${empty pack.getDiscount().getDiscount() && not empty fromGet}">
						<div id="no_disscount"><p>No elegiste descuento para esta fecha</p></div>
						<div id="dis_wrapper" style="display:none"><input disabled class="radio_disscount" type="radio" name="discount" value=50><label>50%</label> 
						<input disabled class="radio_disscount" type="radio" name="discount" value=40><label>40%</label>
						<input disabled class="radio_disscount" type="radio" name="discount" value=30><label>30%</label></div>
					</c:when>
					<c:otherwise>
						<input disabled class="radio_disscount" type="radio" name="discount" value=50><label>50%</label> 
						<input disabled class="radio_disscount" type="radio" name="discount" value=40><label>40%</label>
						<input disabled class="radio_disscount" type="radio" name="discount" value=30><label>30%</label>
					</c:otherwise>
				</c:choose>
					
					
					
					
					<button id="clearRadioBtn" disabled onclick="clearRadio();">Limpiar</button>
				</div>
				
				
				
				<div id="promo_section" class="section disabled">
					<h4>B) Promociones:</h4>
					
					<c:if test="${ empty pack.getPromos() && not empty fromGet }">
							<div id="promos_empty"><p>No hay promociones para la fecha seleccionada</p></div>
							<table id="table_promociones" style="padding: 1em; display:none">
				
					</c:if>
				
					<c:if test="${ not empty pack.getPromos() || empty fromGet }">
							<table id="table_promociones" style="padding: 1em;">
				
					</c:if>
					
						<tr>
							<th></th>
							<th>Bebida</th>
							<th>Tipo</th>
							<th>Unidades</th>
							<th>P.V.P. unitario</th>
							<th>P.V.P. total</th>
							<th id="maxPersonsHeader" class="oculto">Max. personas</th>
						</tr>
						<tr>
							<td>Promoción1:</td>
																	
							
							<td><select disabled id="b1" class="promo_item"
								onchange="changeProductType1();">
									<option value=-1>Selecciona</option>
									<c:forEach var="entry" items="${productType}">
									
										<c:if test="${pack.getPromos().size()>0 && pack.getPromos().get(0).getPromoType() == entry.code}">
											
											
											<option selected value='${entry.code}'>${entry.label}</option>
										</c:if>
										<c:if test="${ pack.getPromos().size()==0 || pack.getPromos().get(0).getPromoType() != entry.code}">
											<option value='${entry.code}'>${entry.label}</option>
										</c:if>
									</c:forEach>

							</select></td>
							<td><select disabled id="t1" class="promo_item"
								disabled="disabled" onchange="changeProductSubtype1()">
									<option value=-1>Selecciona</option>
									<c:forEach var="entry" items="${productSubtypeCopa}">
										<c:if test="${pack.getPromos().size()>0 && pack.getPromos().get(0).getPromoSubtype() == entry.code}">
											<option selected value='${entry.code}'>${entry.label}</option>
										</c:if>
										<c:if test="${ pack.getPromos().size()==0 || pack.getPromos().get(0).getPromoSubtype() != entry.code}">
											<option value='${entry.code}'>${entry.label}</option>
										</c:if>
									</c:forEach>
							</select></td>
							<td style="text-align: center">
								<c:if test="${pack.getPromos().size()>0}">
									<input disabled id="n1" onkeypress="return isNumber(event)" onchange="onChangePromoSize(this,1);return true;" class=promo_item_i type="number" min="1" step="1" value="${pack.getPromos().get(0).getPromoSize()}" ></input>
								</c:if>
								<c:if test="${ empty pack ||  pack.getPromos().size()==0}">
									<input disabled id="n1" onkeypress="return isNumber(event)" onchange="onChangePromoSize(this,1);return true;" class=promo_item_i type="number" min="1" step="1"></input>
								</c:if>
							</td>
							<td style="text-align: center">
								<c:if test="${pack.getPromos().size()>0}">
									<input disabled id="p1" onkeypress="return isNumberKey(event)"  onchange="onChangePVP(this,1)" class=promo_item_i type="number" min="0.01" pattern="[0-9]+([\,|\.][0-9]+)?" step="0.01" value="${pack.getPromos().get(0).getPromoPvp()}" ></input>
								</c:if>
								<c:if test="${ empty pack || pack.getPromos().size() == 0}">
									<input disabled id="p1"  onkeypress="return isNumberKey(event)"  onchange="onChangePVP(this,1)" class=promo_item_i type="number" min="0.01" pattern="[0-9]+([\,|\.][0-9]+)?" step="0.01"></input>
								</c:if>
								<span class="profile_span">€</span></td>
							<td style="text-align: center">
							
							<c:if test="${pack.getPromos().size()>0}">
								<input disabled id="m1"  class=promo_item_i type="number" value="${pack.getPromos().get(0).getPromoPvp()*pack.getPromos().get(0).getPromoSize() }" ></input>
							</c:if>
							<c:if test="${ empty pack || pack.getPromos().size() == 0}">
								<input disabled id="m1" onchange="changeTotal(this,1);return true;" class=promo_item_i type="number"></input>
							</c:if>
							<span class="profile_span">€</span></td>
							
							<c:if test="${pack.getPromos().size()>0 && pack.getPromos().get(0).getPromoType()==3}">
								<td id="maxPersons1" style="text-align: center">
								<input id="max1" disabled class=promo_item_i type="number" min="1" step="1" value="${pack.getPromos().get(0).getPromoMax()}" ></input>
								<img width="20%" src="resources/img/pers.png" style="float: right;"></img></td>
								
							</c:if>
							<c:if test="${ empty pack ||pack.getPromos().size() == 0}">
								<td id="maxPersons1" class="oculto" style="text-align: center">
								<input id="max1" disabled class=promo_item_i type="number" min="1" step="1"></input>
								<img width="20%" src="resources/img/pers.png" style="float: right;"></img></td>
								
							</c:if>
							<c:if test="${ pack.getPromos().size()>0 && pack.getPromos().get(0).getPromoType()!=3}">
								<td id="maxPersons1" class="oculto" style="text-align: center">
								<input id="max1" disabled class=promo_item_i type="number" min="1" step="1"></input>
								<img width="20%" src="resources/img/pers.png" style="float: right;"></img></td>
								
							</c:if>
							

						</tr>

						<tr>
							<td>Promoción2:</td>
							<td><select disabled id="b2" class="promo_item"
								onchange="changeProductType2();">
									<option value=-1>Selecciona</option>
									<c:forEach var="entry" items="${productType}">
										
										<c:if test="${pack.getPromos().size()==2}">
										<c:choose>
											<c:when test="${pack.getPromos().get(1).getId().getPromo_index()==1 && pack.getPromos().get(1).getPromoType() == entry.code}">
													<option selected value='${entry.code}'>${entry.label}</option>
											</c:when>
											<c:otherwise>
													<option value='${entry.code}'>${entry.label}</option>
											</c:otherwise>
											</c:choose>
										</c:if>
										
										<c:if test="${ pack.getPromos().size()<2 || empty pack.getPromos() }">
											<option value='${entry.code}'>${entry.label}</option>
										</c:if>
									</c:forEach>
							</select></td>
							<td><select id="t2" class="promo_item" disabled="disabled" onchange="changeProductSubtype2();">
									<option value=-1>Selecciona</option>
									<c:forEach var="entry" items="${productSubtypeCopa}">
										<c:if test="${pack.getPromos().size()==2 && pack.getPromos().get(1).getPromoSubtype() == entry.code}">
											<option selected value='${entry.code}'>${entry.label}</option>
										</c:if>
										<c:if test="${ pack.getPromos().size()!=2 || pack.getPromos().get(1).getPromoSubtype() != entry.code}">
											<option value='${entry.code}'>${entry.label}</option>
										</c:if>
									</c:forEach>
							</select></td>
							<td style="text-align: center">
							
							<c:if test="${pack.getPromos().size()==2}">
									<input disabled id="n2" onkeypress="return isNumber(event)" onchange="onChangePromoSize(this,2);return true;" class=promo_item_i type="number" min="1" step="1" value="${pack.getPromos().get(1).getPromoSize()}" ></input>
								</c:if>
							<c:if test="${empty pack || pack.getPromos().size() < 2}">
									<input disabled id="n2" onkeypress="return isNumber(event)" onchange="onChangePromoSize(this,2);return true;" class=promo_item_i type="number" min="1" step="1"></input>
							</c:if>
								
							</td>
							<td style="text-align: center">
								
								<c:if test="${pack.getPromos().size()==2}">
									<input disabled id="p2" class="promo_item_i" onkeypress="return isNumberKey(event)"  onchange="onChangePVP(this,2)" type="number" pattern="[0-9]+([\,|\.][0-9]+)?" step="0.01" value="${pack.getPromos().get(1).getPromoPvp()}" ></input>
								</c:if>
								<c:if test="${empty pack || pack.getPromos().size()<2}">
									<input disabled id="p2" class="promo_item_i" onkeypress="return isNumberKey(event)"  onchange="onChangePVP(this,2)" type="number" pattern="[0-9]+([\,|\.][0-9]+)?" step="0.01"></input>
								</c:if>
								
								<span class="profile_span">€</span></td>
							<td style="text-align: center">
							
							<c:if test="${pack.getPromos().size()==2}">
								<input disabled id="m2" class=promo_item_i type="number" value="${pack.getPromos().get(1).getPromoPvp()*pack.getPromos().get(1).getPromoSize() }" ></input>
							</c:if>
							<c:if test="${empty pack || pack.getPromos().size() <2}">
								<input disabled id="m2" onchange="changeTotal(this,2);return true;" class=promo_item_i type="number"></input>
							</c:if>								
								<span class="profile_span">€</span></td>
								
								
								
							<c:if test="${pack.getPromos().size()==2 && pack.getPromos().get(1).getPromoType()==3}">
								<td id="maxPersons2" style="text-align: center">
								<input id="max2" disabled class=promo_item_i type="number" min="1" step="1" value="${pack.getPromos().get(1).getPromoMax()}" ></input>
								<img width="20%" src="resources/img/pers.png" style="float: right;"></img></td>
								
							</c:if>
							<c:if test="${empty pack || pack.getPromos().size() < 2}">
								<td id="maxPersons2" class="oculto" style="text-align: center">
								<input id="max2" disabled class=promo_item_i type="number" min="1" step="1"></input>
								<img width="20%" src="resources/img/pers.png" style="float: right;"></img></td>
								
							</c:if>
							<c:if test="${ pack.getPromos().size()==2 && pack.getPromos().get(1).getPromoType()!=3}">
								<td id="maxPersons2" class="oculto" style="text-align: center">
								<input id="max2" disabled class=promo_item_i type="number" min="1" step="1"></input>
								<img width="20%" src="resources/img/pers.png" style="float: right;"></img></td>
								
							</c:if>	
							

						</tr>

					</table>

					<button id="clearProductBtn" disabled onclick="clearProduct();">Limpiar</button>


				</div>
				<input type='hidden' name='localId' id='localId' value="${localId}" />
				<button class="action_offer" id="publish_offer" disabled
					onclick="publish_offer_input_checker()">Publicar Oferta</button>
				
				<c:if test="${not empty pack}">
				<input type="hidden" id="dateValue" />
					<button class="action_offer" id="delete_offer" >Eliminar Oferta</button>
				</c:if>
					
			</div>

		</div>
	</section>

	<section id="inicio_right_side">
		<%@ include file="reservationList.jsp" %>
	</section>
</section>

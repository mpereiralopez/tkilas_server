<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
function updateReservesList(dateVar){
	var url = "<% getServletContext().getContextPath();%>gestor/update_reserves_list";
	var date;
	(typeof dateVar != undefined )?date = dateVar : date = $("#dateforuploadreservelist").val()
	$.ajax({
		  type:"GET",
		  url: url,
		  data: {"localId": $("#localId").val(), "date":date},
		  success: function(result, textStatus){
			  $("#inicio_right_side").empty();				 
			  $("#inicio_right_side").html(result);				 
		  },
		  error: function (err){
			  console.log(err)
		  }
		});
}

function updateReservationStatusOfClient(clientId, type){
	var URL = "<%getServletContext().getContextPath();%>gestor/update_client_reservation_status";
    $.ajax({
    	  type: "POST",
    	  url: URL,
    	  data: {"localId": $("#localId").val(), "date":$("#dateforuploadreservelist").val()
    		  ,"clientId": clientId,"type":type,"newStatus":$("#"+clientId+" select").val()},
    	  success: function(result, textStatus){
    		  console.log(result);
    		  $("#inicio_right_side").empty();				 
			  $("#inicio_right_side").html(result);

		  },
		  error: function (err){
			  console.log(err)
		  }
    	});

}

</script>

<c:if test="${pack == null}">

	<h1 id="confirmed_title">Reservas:</h1>
	<div class="progress_bar" id="bar_actual">
		<div ></div>
	</div>
	
</c:if>

<c:if test="${pack != null}">



	<h1 id="confirmed_title">Reservas: ${totalReservedForDay} / ${pack.getSize()}</h1>
	<div class="progress_bar" id="bar_actual">
		<div style="width: ${(totalReservedForDay*100) / pack.getSize()}%"></div>
		
	
	</div>
	<input type="hidden" id="dateforuploadreservelist" />
	<input onclick="updateReservesList()" type="image" src="resources/img/ic_action_refresh2.png" style="display:inline-block; border:none; max-height: 30px;"/>
	
</c:if>

<h1>Lista de reservas:</h1>
<table>
	<thead>
		<tr>
			<th>CLIENTE</th>
			<th>DIA</th>
			<th>HORA</th>
			<th>PERS</th>
			<th>RESERVA</th>
			<th>ESTADO</th>
		</tr>
	</thead>


	<tbody id="listOfclientsOfDayBody">
	
	
	<c:forEach var="reserve" items="${client_canceled}">

			<c:if test="${reserve.getStatus() == 0}">
				<tr id="${reserve.getId_client()}"
					style="background-color: orange;">
					<td>${reserve.getClient_name()}
						${reserve.getClient_surname()}</td>
					<td>${reserve.getDate().toString()}</td>
					<td>${reserve.getHour().toString()}</td>
					<td>${reserve.getSize()}</td>
					<c:if test="${reserve.getPromoType() != -1} ">
							<td>Promo ${reserve.getPromoType()}</td>
					</c:if>
					<c:if test="${reserve.getDiscount() != -1} ">
							<td>${reserve.getDiscount()} %</td>
					</c:if>
					<td><select
						onchange="updateReservationStatusOfClient(${reserve.getId_client()},1)">
							<option selected style="color: gray" value=0>Pendiente</option>
							<option style="color: green" value=1>Confirmado</option>
							<option style="color: red" value=2>Denegado</option>
					</select></td>
				</tr>

			</c:if>
			
			<c:if test="${reserve.getStatus() == 1}">
				<tr id="${reserve.getId_client()}"
					style="background-color: #8cd200;">
					<td>${reserve.getClient_name()}
						${reserve.getClient_surname()}</td>
					<td>${reserve.getDate().toString()}</td>
					<td>${reserve.getHour().toString()}</td>
					<td>${reserve.getSize()}</td>
					<c:if test="${reserve.getPromoType() != -1} ">
							<td>Promo ${reserve.getPromoType()}</td>
					</c:if>
					<c:if test="${reserve.getDiscount() != -1} ">
							<td>${reserve.getDiscount()} %</td>
					</c:if>
					<td><select
						onchange="updateReservationStatusOfClient(${reserve.getId_client()},1)">
							<option selected style="color: green" value=1>Confirmado</option>
							<option style="color: red" value=2>Denegado</option>
					</select></td>
				</tr>

			</c:if>
			
		</c:forEach>
	
	

	<c:forEach var="reserve" items="${listaClientesConDescuento}">

			<c:if test="${reserve.getStatus() == 0}">
				<tr id="${reserve.getClient().getUserIdUser()}"
					style="background-color: orange;">
					<td>${reserve.getClient().getUser().getUser_name()}
						${reserve.getClient().getUser().getUser_surname()}</td>
					<td>${reserve.getId().getDate().toString()}</td>
					<td>${reserve.getHour().toString()}</td>
					<td>${reserve.getSize()}</td>
					<td>${pack.getDiscount().getDiscount()}%</td>
					<td><select
						onchange="updateReservationStatusOfClient(${reserve.getClient().getUserIdUser()},0)">
							<option selected style="color: gray" value=0>Pendiente</option>
							<option style="color: green" value=1>Confirmado</option>
							<option style="color: red" value=2>Denegado</option>
					</select></td>
				</tr>

			</c:if>
			
			<c:if test="${reserve.getStatus() == 1}">
				<tr id="${reserve.getClient().getUserIdUser()}"
					style="background-color: #8cd200;">
					<td>${reserve.getClient().getUser().getUser_name()}
						${reserve.getClient().getUser().getUser_surname()}</td>
					<td>${reserve.getId().getDate().toString()}</td>
					<td>${reserve.getHour().toString()}</td>
					<td>${reserve.getSize()}</td>
					<td>${pack.getDiscount().getDiscount()}%</td>
					<td><select
						onchange="updateReservationStatusOfClient(${reserve.getClient().getUserIdUser()},0)">
							<option selected style="color: green" value=1>Confirmado</option>
							<option style="color: red" value=2>Denegado</option>
					</select></td>
				</tr>

			</c:if>
			
			<c:if test="${reserve.getStatus() == 2}">
				<tr id="${reserve.getClient().getUserIdUser()}"
					style="background-color: red;">
					<td>${reserve.getClient().getUser().getUser_name()}
						${reserve.getClient().getUser().getUser_surname()}</td>
					<td>${reserve.getId().getDate().toString()}</td>
					<td>${reserve.getHour().toString()}</td>
					<td>${reserve.getSize()}</td>
					<td>${pack.getDiscount().getDiscount()}%</td>
					<td><select>
							<option selected style="color: red" value=2>Denegado</option>
					</select></td>
				</tr>

			</c:if>




		</c:forEach>
		
		
		<c:forEach var="reserve" items="${listaClientesConPromo}">

			<c:if test="${reserve.getStatus() == 0}">
				<tr id="${reserve.getClient().getUserIdUser()}"
					style="background-color: orange;">
					<td>${reserve.getClient().getUser().getUser_name()}
						${reserve.getClient().getUser().getUser_surname()}</td>
					<td>${reserve.getId().getDate().toString()}</td>
					<td>${reserve.getHour().toString()}</td>
					<td>${reserve.getSize()}</td>
					<td>Promo ${reserve.getPromo_index()}</td>
					<td><select
						onchange="updateReservationStatusOfClient(${reserve.getClient().getUserIdUser()},1)">
							<option selected style="color: gray" value=0>Pendiente</option>
							<option style="color: green" value=1>Confirmado</option>
							<option style="color: red" value=2>Denegado</option>
					</select></td>
				</tr>

			</c:if>
			
			<c:if test="${reserve.getStatus() == 1}">
				<tr id="${reserve.getClient().getUserIdUser()}"
					style="background-color: #8cd200;">
					<td>${reserve.getClient().getUser().getUser_name()}
						${reserve.getClient().getUser().getUser_surname()}</td>
					<td>${reserve.getId().getDate().toString()}</td>
					<td>${reserve.getHour().toString()}</td>
					<td>${reserve.getSize()}</td>
					<td>Promo ${reserve.getPromo_index()}</td>
					<td><select
						onchange="updateReservationStatusOfClient(${reserve.getClient().getUserIdUser()},1)">
							<option selected style="color: green" value=1>Confirmado</option>
							<option style="color: red" value=2>Denegado</option>
					</select></td>
				</tr>

			</c:if>
			
			<c:if test="${reserve.getStatus() == 2}">
				<tr id="${reserve.getClient().getUserIdUser()}"
					style="background-color: red;">
					<td>${reserve.getClient().getUser().getUser_name()}
						${reserve.getClient().getUser().getUser_surname()}</td>
					<td>${reserve.getId().getDate().toString()}</td>
					<td>${reserve.getHour().toString()}</td>
					<td>${reserve.getSize()}</td>
					<td>Promo ${reserve.getPromo_index()}</td>
					<td><select>
							<option selected style="color: red" value=2>Denegado</option>
					</select></td>
				</tr>

			</c:if>




		</c:forEach>
		


	</tbody>
</table>
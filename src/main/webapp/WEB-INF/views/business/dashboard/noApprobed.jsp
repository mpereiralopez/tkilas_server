<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<style  type="text/css">
.text_normal{
	color: gray !important;
	font-size:1.5em !important;
	text-align: center !important;
	-webkit-margin-before: 0.5em !important;
-webkit-margin-after: 0.5em !important;
}

.text_spm{
	font-size: 1em !important;
	text-align: center !important;
	-webkit-margin-before: 0.5em !important;
-webkit-margin-after: 0.5em !important;
}

.text_wrapper {
	position: absolute;
	left: 0;
	right: 0;
	margin-left: auto;
	margin-right: auto;
	width: 100%;
	height: 95%;
}

</style>

<div class='text_wrapper'>
	<div style='top: 8%;bottom: 4%;margin: auto;position:absolute;left:0;right: 0; max-width: 600px; max-height: 500px;'>
		<spring:message code="dash.noapprobed"/>
	</div>
</div>
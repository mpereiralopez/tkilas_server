
<%@page session="true"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script type="text/javascript">
$('#nav_section').children().removeClass('actual');
$($('#nav_section').children().eq(2)).addClass('actual');
</script>


<style>



article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section { display: block; }
ol, ul { list-style: none; }

blockquote, q { quotes: none; }
blockquote:before, blockquote:after, q:before, q:after { content: ''; content: none; }
strong { font-weight: bold; } 
table { border-collapse: collapse; border-spacing: 0; }
img { border: 0; max-width: 100%; }
h1 {text-align:center;line-height: 1.6em; font-weight: normal; text-shadow: 0px 1px 1px #fff; margin-bottom: 21px; }

p { line-height: 1.42em; margin-bottom: 12px; font-weight: normal; color: #656565; }

a { color: #896dc6; text-decoration: none; }
a:hover { text-decoration: underline; }

/* page layout structure */ 
#w { display: block; width: 700px; margin: 0 auto; padding-top: 35px; }

#container { 
  display: block; 
  width: 100%; 
  background: #fff; 
  padding: 14px 20px; 
  -webkit-border-radius: 4px; 
  -moz-border-radius: 4px; 
  border-radius: 4px; 
  -webkit-box-shadow:1px 1px 1px 1px rgb(140,210,0);
  -moz-box-shadow:1px 1px 1px 1px rgb(140,210,0);
  box-shadow:1px 1px 1px 1px rgb(140,210,0);
}


/* comments area */
#comments { display: block; }

#comments .cmmnt, ul .cmmnt, ul ul .cmmnt { display: block; position: relative; padding-left: 65px; border-top: 1px solid rgb(140,210,0); }

#comments .cmmnt .avatar  { position: absolute; top: 8px; left: 0; }
#comments .cmmnt .avatar img { 
  -webkit-border-radius: 3px; 
  -moz-border-radius: 3px; 
  border-radius: 3px; 
  -webkit-box-shadow: 1px 1px 2px rgba(0,0,0,0.44);
  -moz-box-shadow: 1px 1px 2px rgba(0,0,0,0.44);
  box-shadow: 1px 1px 2px rgba(0,0,0,0.44);
  -webkit-transition: all 0.4s linear;
  -moz-transition: all 0.4s linear;
  -ms-transition: all 0.4s linear;
  -o-transition: all 0.4s linear;
  transition: all 0.4s linear;
}

#comments .cmmnt .avatar a:hover img { opacity: 0.77; }

#comments .cmmnt .cmmnt-content { padding: 0px 3px; padding-bottom: 12px; padding-top: 8px; }

#comments .cmmnt .cmmnt-content header { display: block; margin-bottom: 8px; }
#comments .cmmnt .cmmnt-content header .pubdate { color: #777; }
#comments .cmmnt .cmmnt-content header .userlink { font-weight: bold; } 

#comments .cmmnt .replies { margin-bottom: 7px; }

</style>

<title><spring:message code="title.manager" /></title>
<div id="w">
    <h1 class="font_green">Comentarios sobre ${local_info.getLocal_name()}</h1>
    
    <div id="container">
    
     <c:if test="${listaComentarios.size() == 0}">
      		<div><h1>Actualmente no existen comentarios de los clientes</h1></div>
      </c:if>
      
      
      <c:if test="${listaComentarios.size() > 0}">
      	<ul id="comments">
      
     
				
        <c:forEach var="comment" items="${listaComentarios}">
        	<li class="cmmnt">
          		<div class="avatar"><a href="javascript:void(0);"><img src="resources/img/default.png" width="55" height="55" alt="${comment.getClient().getUser().getUser_name()} ${comment.getClient().getUser().getUser_surname()} photo avatar"></a></div>
          		<div class="cmmnt-content">
            			<header><a href="javascript:void(0);" class="userlink">${comment.getClient().getUser().getUser_name()} ${comment.getClient().getUser().getUser_surname()}</a> - <span class="pubdate"><fmt:formatDate value="${comment.getResponsTime()}" pattern="dd/MM/yyyy" /></span></header>
            			<p>${comment.getCommentBody()}</p>
          		</div>
        	</li>
        </c:forEach>
        
       
      </ul>
      </c:if>
      
      
    </div>
  </div>
<%@ include file="../init.jsp"%>

<%@page import="com.tkilas.controller.LocaleLandingController"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<title><spring:message code="title.manager" /></title>

<link rel="stylesheet" href="resources/css/fonts.css" />
<link rel="stylesheet" href="resources/css/local.css" />
<link rel="stylesheet" href="resources/css/jquery.Jcrop.min.css" />

<script type="text/javascript" src="resources/js/local.js"></script>
<script type="text/javascript"
	src="resources/js/library/jquery.Jcrop.min.js"></script>
<script type="text/javascript"
	src="resources/js/library/jquery.validate.min.js"></script>
<script type="text/javascript"
	src="resources/js/library/additional-methods.min.js"></script>
<script type="text/javascript" src="resources/js/library/utils.js"></script>
<script type="text/javascript" src="resources/js/library/form_utils.js"></script>
<script type="text/javascript" src="resources/js/local_ws_utils.js"></script>
<script type="text/javascript"
	src="resources/js/library/jquery-ui.min.js"></script>

<script src="resources/js/library/jquery.plugin.min.js"></script>
<script src="resources/js/library/jquery.datepick.min.js"></script>
<script src="resources/js/library/datepicker.js"></script>
<script src="resources/js/library/jquery.datepick.ext.min.js"></script>
<script src="resources/js/library/jquery-ui.multidatespicker.js"></script>
<script type="text/javascript">
	$(document).ready(
			function() {
				var minW = ($(document).width() * 0.80);
				$("#dialog_legal_conditions").dialog({
					autoOpen : false,
					modal : true,
					minWidth : minW,
					maxHeight : 600
				});

				$("#dialog_contact_form").dialog({
					autoOpen : false,
					modal : true,
					minWidth : 500,
				});
				

				$("#dialog_crop").dialog({
					autoOpen : false,
					modal : true,
					minWidth : 750,
					maxHeight : 420
				});
				

				// on browser resize...
				if (readCookie("showModalOnStart") == 'true'
						|| readCookie("showModalOnStart") == null) {
					var showModalInfoIni = true
				} else {
					var showModalInfoIni = false;
				}

				$("#dialog_multiple_day").dialog({
					autoOpen : false,
					modal : true,
					minWidth : 300,
					maxHeight : 200,
					buttons : {
						'<spring:message code="modal.show.nomore" />' : function() {
							noShowMore();
							$(this).dialog("close");
						},
						Cancel : function() {
							$(this).dialog("close");
						}
					}
				});

				if (showModalInfoIni) {
					$("#dialog_multiple_day").dialog("open");
				}

				$("#contact_box").click(function() {
					$("#dialog_contact_form").dialog("open");
				});
			});
</script>

<style type="text/css">
input.error {
	border-color: red;
}

label.error {
	color: red;
	font-size: 0.75em;
	padding-left: 0.5em;
	clear: both;
	display: block;
}
</style>

</head>
<body>
	<fmt:setLocale value="es_ES" />
	<%@ include file="../header_normal_template.jsp"%>


	<div id="manager" class="page current horizontal">
		<div id="nav_section" class="nav_section">
			<a class="actual manage" href="gestor?section=dashboard"><spring:message code="menu.gestor" /></a> 
			<a class="manage" href="gestor?section=profile&subsection=personal"><spring:message code="menu.perfil" /></a>
			<c:if test="${local_info.getStatus() eq 1}">
				<a class="manage" href="gestor?section=comments">Comentarios</a>
			</c:if>
			
			<sec:authorize access="hasRole('ROLE_PREVIOUS_ADMINISTRATOR')">
                         <a style="float: right; text-decoration: none; padding-right: 15px" href="<c:url value='/j_spring_security_exit_user' />">
                            Volver a Admin</a>
                            <script type="text/javascript">
                            
                            window.onbeforeunload = function() {
                            	   // This template uses no error checking, it's just concept code to be
                            	   // expanded on.
                            		 
                            	   // Create a new XMLHttpRequest object
                            	   var AJAX=new XMLHttpRequest();  
                            		 
                            	   // Handle ready state changes ( ignore them until readyState = 4 )
                            	   AJAX.onreadystatechange= function() { if (AJAX.readyState!=4) return false; }
                            		 
                            	   // we're passing false so this is a syncronous request.
                            	   // The script will stall until the document has been loaded.
                            	   // the open statement depends on a global variable titled _userID.
                            	   AJAX.open("GET", "<c:url value='/j_spring_security_exit_user' />", false);
                            	   AJAX.send(null);
                            	}
                            
                            
                            </script>
			</sec:authorize>
			
			<a style="float: right; text-decoration: none; padding-right: 15px" href="<c:url value="j_spring_security_logout" />" target="_self">
				<spring:message code="menu.salir" />
			</a>
		</div>


		<section class="wrapper horizontally_move portrait inicio_vert">
			<div id="msg_portrait_wrapper">
				<spring:message code="msg.error.portratit" />
			</div>
		</section>


		<c:if test="${template == 0}">
			<div id="dashboard_wrapper"><%@ include
					file="dashboard/dashboard.jsp"%></div>
		</c:if>

		<c:if test="${template == 1}">
			<%@ include file="dashboard/noApprobed.jsp"%>
		</c:if>

		<c:if test="${template == 2}">

			<%@ include file="profile/profile.jsp"%>
		</c:if>
		
		<c:if test="${template == 3}">

			<%@ include file="comments/comments.jsp"%>
		</c:if>



	</div>

	<div id="contact_box">
		<div id="letter_wrapper">
			<img src="resources/img/em_landing.png" title="ml" alt="ml" />
		</div>
		<p>
			<spring:message code="contact.us" />
		</p>
	</div>


	<%@ include file="../footer_template_no_links.jsp"%>

	<section class="modal_container" id="modalWaiting">
		<div>
			<img src="resources/img/loading.gif" alt="gif" width="90" height="90"
				style="display: block; margin: auto; position: absolute; top: 0; right: 0; left: 0; bottom: 0;">
		</div>
	</section>




	<div id="dialog_multiple_day" title="<spring:message code="modal.show.multiple" />">
		<p><spring:message code="modal.show.multiple.text" /></p>
	</div>


	<div id="modal_for_delete" class="modal">
		<div class="modal-content">
			<div class="header">
				<h2 style="color: white">¡Atención!</h2>
			</div>
			<div class="copy">
				<p>Existen reservas activas realizadas para este día y Tkilas le
					recuerda que tiene que atender a todas estas reservas aunque
					elimine la oferta de su gestor. Si no desea atender estas reservas
					debe cancelar cada una de ellas.</p>
			</div>
			<div class="cf footer">
				<button id="confirToDeleteBtn" class="btn left_btn">Confirmar</button>
				<button onclick="hideModalForDelete();return true;" class="btn">Cerrar</button>
			</div>
		</div>
		<div class="overlay"></div>
	</div>


	<div id="dialog_legal_conditions" title="<spring:message code="footer.legal" />">
		<%@ include file="../legal_condition_template.jsp"%>
	</div>

	<div id="dialog_contact_form"
		title="<spring:message code="contact.us" />">
		<%@ include file="../contact_form.jsp"%>
	</div>

</body>
</html>
<script type="text/javascript">
$(document).ready(function() {
	validateMyForm();
});
</script>
<style>
#datos_condiciones{
overflow: scroll;
}
#localFormUpdateTermsAndPayment{
	max-width: 550px;
  position: relative;
  display: block;
}
</style>
<div class="subsection horizontally_move" id="datos_condiciones">
<c:if test="${not empty isAccepted }">
		<div class="progress-wrap progress" data-progress-percent="75">
			<div class="progress-bar progress"></div>
		</div>
	</c:if>

		<div id="info_msg">
		<h1><spring:message code="condition.title" /></h1>
	</div>
	
	<form:form commandName="localFormUpdateTermsAndPayment"
		class="profile_forms form_validator"
		action="gestor/update_local_terms_pay" method='POST'>
		

		<table>
			<tr>
				<td class="table_title" colspan="2"><spring:message code="condition.general" /></td>
			</tr>
			<tr>
				<td colspan="2" class="table_line"></td>
			</tr>

			<tr>
				<td colspan="2"><input style="width: 35px;" type="checkbox"
					name="reserve_cost" value=1 required /><label class="terms"><spring:message code="condition.terms" /></label></td>
			</tr>

			<tr>
				<td colspan="2"><p class="conditions_list"><spring:message code="condition.list.1" /></p></td>
			</tr>
			<tr>
				<td colspan="2"><p class="conditions_list"><spring:message code="condition.list.2" /></p></td>
			</tr>
			<tr>
				<td colspan="2"><p class="conditions_list"><spring:message code="condition.list.3" /></p></td>
			</tr>
			<tr>
				<td colspan="2" style="border-top: 1px solid #8c8c8c;"><p class="conditions_list" style="padding-top: 0.5em;"><spring:message code="condition.list.4" /></p></td>
			</tr>

			<tr>
				<td colspan="2"><input style="width: 35px;" type="checkbox"
					name="terms_accepted" value=1 required /><label
					id="check_label_terms" class="terms"><spring:message code="condition.term.accept" />	</label></td>
			</tr>

			<tr>
				<td colspan="2" class="table_title"><spring:message code="condition.facture.data" /></td>
			</tr>
			
			<tr>
				<td colspan="2" class="table_line"></td>
			</tr>

			<tr>
				<td class="font_gray form_label"><form:label path="trade_name"><spring:message code="condition.social" /></form:label></td>
				<td><input type='text' name='trade_name'
					value='${localPayInfo.getTrade_name()}' required /></td>
			</tr>

			<tr>
				<td class="font_gray form_label"><form:label path="cif"><spring:message code="contact.cif" /></form:label></td>
				<td><input type='text' name='cif'
					value='${localPayInfo.getCif()}' /></td>
			</tr>

			<tr>
				<td class="font_gray form_label"><form:label path="contact_address"><spring:message code="contact.business_addr" /></form:label></td>
				<td><input type='text' name='contact_address'
					value='${localPayInfo.getContact_address()}' required /></td>
			</tr>

			<tr>
				<td class="font_gray form_label"><form:label path="contact_cp"><spring:message code="contact.business_cp" /></form:label></td>
				<td><input type='text' name='contact_cp'
					value='${localPayInfo.getContact_cp()}' maxlength="5"
					onkeypress="return isNumberKey(event)" /></td>
			</tr>

			<tr>
				<td class="font_gray form_label"><form:label path="contact_city"><spring:message code="contact.business_city" /></form:label></td>
				<td><select id="city_selector_pay" name="contact_city" size="1"
					required>
						<c:forEach var="entry" items="${citiesEnum}">
							<option value='${entry.label}'>${entry.label}</option>
						</c:forEach>
				</select>
			</tr>





			<tr>
				<td colspan='2'><input id="submit_btn" name="submit"
					type="submit" class="submit" value="<spring:message code="profile.btn_next" />" /></td>
			</tr>
		</table>
		<input type='hidden' name='id_local' id='id_local'
			value='${local_info.getUserIdUser()}' />

		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />

	</form:form>




</div>

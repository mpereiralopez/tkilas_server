
<div class="subsection horizontally_move" id="datos_contacto">
<script type="text/javascript">
$(document).ready(function() {
	validateMyForm();
});
</script>
<script type="text/javascript">
	$('#nav_section_sub').children().removeClass('actual');
	$($('#nav_section_sub').children().eq(0)).addClass('actual');
</script>




	<c:if test="${not empty isAccepted }">
		<div class="progress-wrap progress" data-progress-percent="25">
			<div class="progress-bar progress"></div>
		</div>
	</c:if>


	<form:form commandName="localFormUpdate" id="personal_info_form"
		class="profile_forms form_validator"
		action="gestor/update_contact_info" method='POST'>
		<table>
			<tr>
				<td colspan="2" class="table_title"><spring:message
						code="contact.title" /></td>
			</tr>
			<tr>
				<td colspan="2" class="table_line"></td>
			</tr>



			<tr>
				<td class="font_gray form_label"><form:label path="user_name">
						<spring:message code="contact.name" />
					</form:label></td>
				<td><input type='text' name='user_name'
					value='${local_info.getUser().getUser_name()}' required></td>
			</tr>
			<tr>
				<td class="font_gray form_label"><form:label
						path="user_surname">
						<spring:message code="contact.surname" />
					</form:label></td>
				<td><input type='text' name='user_surname'
					value='${local_info.getUser().getUser_surname()}' required /></td>
			</tr>


			<tr>
				<td colspan="2" class="table_title"><spring:message
						code="contact.business_title" /></td>
			</tr>
			<tr>
				<td colspan="2" class="table_line"></td>
			</tr>


			<tr>
				<td class="font_gray form_label"><form:label path="local_name">
						<spring:message code="contact.business_name" />
					</form:label></td>
				<td><input type='text' name='local_name'
					value='${local_info.getLocal_name()}' required /></td>
			</tr>
			<tr>
				<td class="font_gray form_label"><form:label path="local_phone">
						<spring:message code="contact.business_tlf" />
					</form:label></td>
				<td><input type='tel' name='local_phone'
					value='${local_info.getTlf()}' required pattern="[0-9]{9}" /></td>
			</tr>
			<tr>
				<td class="font_gray form_label"><form:label
						path="local_address">
						<spring:message code="contact.business_addr" />
					</form:label></td>
				<td><input type='text' name='local_address'
					value='${local_info.getAddress()}' required /></td>
			</tr>
			<tr>
				<td class="font_gray  form_label"><form:label path="local_cp">
						<spring:message code="contact.business_cp" />
					</form:label></td>
				<td><input type='text' name='local_cp'
					value='${local_info.getLocal_cp()}' required pattern="[0-9]{5}" /></td>
			</tr>
			<tr>
				<td class="font_gray form_label"><form:label path="local_city">
						<spring:message code="contact.business_city" />
					</form:label></td>
				<td><select id="city_selector" name="local_city" size="1"
					required style="width: 100%;">
						<c:forEach var="entry" items="${citiesEnum}">
							<c:if test="${local_info.getCity() eq entry.label }">
								<option selected value='${entry.label}'>${entry.label}</option>
							</c:if>
							<c:if test="${local_info.getCity() != entry.label }">
								<option value='${entry.label}'>${entry.label}</option>
							</c:if>
						</c:forEach>

				</select>
			</tr>
			<tr>
				<td class="font_gray form_label"><form:label path="local_web">
						<spring:message code="contact.business_web" />
					</form:label></td>
				<td><input type='url' name='local_web'
					value='${local_info.getWeb()}' maxlength="64" pattern="http?://.+" /></td>
			</tr>






			<tr style="text-align: center">
				<td colspan='2'>
					
					<c:if test="${empty isAccepted }">
						<input id="submit_btn" class="submit" name="submit" type="submit" value="<spring:message code="profile.btn_update"/>" />
					</c:if>
					<c:if test="${not empty isAccepted }">
						<input id="submit_btn" class="submit" name="submit" type="submit" value="<spring:message code="profile.btn_next"/>" />
					</c:if>
					
					</td>
			</tr>
			
		</table>



		<input type='hidden' name='user_id' id='local_id'
			value='${local_info.getUserIdUser()}' />

		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />

		<c:if test="${MSG_OK != null}">
			<div class="msg_ok">
				<p style="color: white; margin-left: 35px;">${MSG_OK}</p>
			</div>
		</c:if>

	</form:form>



	<label id="check_label_terms" class="terms"
		style="text-align: center; display: block;"><spring:message code="contact.consult" /> </label>


</div>
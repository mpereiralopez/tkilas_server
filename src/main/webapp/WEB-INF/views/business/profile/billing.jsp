<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    
    <div class="subsection horizontally_move" id="datos_pago">
	<!--  <div id="left_side">-->
	<c:if test="${not empty isAccepted }">
		<div class="progress-wrap progress" data-progress-percent="100">
			<div class="progress-bar progress"></div>
		</div>
	</c:if>
	
	<div id="iban_wrapper">
	<form:form commandName="localFormUpdateIBAN"
		class="profile_forms form_validator"
		action="gestor/update_iban" method='POST'>
		<table>
			<tr>
				<td colspan="2" class="table_title"><spring:message code="billing.info.title" /></td>
			</tr>
			
			<tr>
				<td colspan="2" class="table_line"></td>
			</tr>

			<tr>
				<td><h2><spring:message code="billing.info.account" /></h2></td>
			</tr>
			<tr>
				<td><label class="form_label"><spring:message code="billing.info.IBAN" /></label>
				<input type='text' id="local_iban_1" class="_4L" name='local_iban_1' onkeypress="return isFirstFieldIban(event,4)"  /> 
				<input type='text' id="local_iban_2" class="_4L" name='local_iban_2' onkeypress="return isNumberForIban(event,4)" />
				 <input type='text' id="local_iban_3" class="_4L" name='local_iban_3' onkeypress="return isNumberForIban(event,4)" />
				 <input type='text' id="local_iban_4" class="_2L" name='local_iban_4' onkeypress="return isNumberForIban(event,2)" /> 
				<input type='text' id="local_iban_5"  class="_10L" name='local_iban_5' onkeypress="return isNumberForIban(event,10)" /></td>
			</tr>

			<tr>
				<td><label class="form_label"><spring:message code="billing.info.IBAN.example" /></label><input
					type='text' class="_4L" disabled="disabled" value="ESXX" /> <input
					type='text' disabled="disabled" class="_4L" value="0123" /> <input
					type='text' disabled="disabled" class="_4L" value="4567" /> <input
					type='text' disabled="disabled" class="_2L" value="89" /> <input
					type='text' disabled="disabled" class="_10L" value="0123456789" />
				</td>
			</tr>

			<tr>
				<td><br>
				<label class="form_label"><spring:message code="billing.info.IBAN.info" />
				</label></td>
			</tr>

			<tr>
				<td colspan='2'><input id="submit_btn" name="submit"
					class="submit" type="submit" value="<spring:message code="profile.btn_next" />" /></td>
			</tr>
		</table>
		<input type='hidden' name='id_local' id='id_local'
			value='${local_info.getUserIdUser()}' />

		
		</form:form>
		<div id="gif_container"
			style="width: 100%; margin: 2em auto; display: none;">
			<p style="text-align: center; color: #808080"><spring:message code="billing.procesing"/></p>
			<img src="resources/img/loading.gif" alt="gif" width="65" height="65"
				style="display: block; margin: 2em auto; position: relative;">

		</div>
	</div>

	<div id="free_ad">
		<h1><spring:message code="condition.title" /></h1>
	</div>
</div>
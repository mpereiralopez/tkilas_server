<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<script type="text/javascript">
$(document).ready(function() {
	validateMyForm();
});
</script>
<script type="text/javascript">
$('#nav_section').children().removeClass('actual');
$($('#nav_section').children().eq(1)).addClass('actual');
</script>

<style>
	table tr td{
		padding-bottom: 0.5em;
		vertical-align: middle;
		}
</style>

    <div id="nav_section_sub">
		<c:if test="${empty isAccepted }">
			<a class="actual"  href="gestor?section=profile&subsection=personal" ><spring:message code="submenu.personal.info"/></a> 
			<a href="gestor?section=profile&subsection=business" ><spring:message code="submenu.business.info"/></a>		
		</c:if>
	</div>

    <section class="wrapper horizontally_move" id="perfil">
    
    <c:if test="${subTemplate == 0}">
		<%@ include file="personal.jsp" %>
	</c:if>
	
	<c:if test="${subTemplate == 1}">
		<%@ include file="business.jsp" %>
	</c:if>
	
	<c:if test="${subTemplate == 2}">
		<%@ include file="legal_conditions.jsp" %>
	</c:if>
	
	<c:if test="${subTemplate == 3}">
		<%@ include file="billing.jsp" %>
	</c:if>
			
	</section>
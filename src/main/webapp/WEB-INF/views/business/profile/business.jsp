<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	
<script type="text/javascript">
	$('#nav_section_sub').children().removeClass('actual');
	$($('#nav_section_sub').children().eq(1)).addClass('actual');
</script>

<script type="text/javascript">
$(document).ready(function() {
	validateMyForm();
	
});
</script>

<div class="subsection horizontally_move" id="datos_local">
	<!-- SEGUNDA PAGINA -->
	
	<c:if test="${not empty isAccepted }">
		<div class="progress-wrap progress" data-progress-percent="50">
			<div class="progress-bar progress"></div>
		</div>
	</c:if>
	
	<div id="left_side">
		<form:form commandName="localFormUpdateLocalInfo"
			id="update_local_info_form" class="profile_forms form_validator"
			action="gestor/update_local_info" method='POST'>
			<table class="_90">
				<tr>
					<td colspan="2" class="table_title"><spring:message
							code="business.title" /></td>
				</tr>
				<tr>
					<td colspan="2" class="table_line"></td>
				</tr>

				<tr>
					<td class="font_gray form_label"><form:label path="local_type">
							<spring:message code="business.type" />
						</form:label></td>
					<td><select name='local_type' required>
							<c:forEach var="entry" items="${localTypeEnum}">
								<option value='${entry.code}'>${entry.label}</option>
							</c:forEach>

					</select></td>
				</tr>

				<tr>
					<td class="font_gray form_label"><form:label path="local_desc">
							<spring:message code="business.desc" />
						</form:label></td>
					<td><textarea maxlength=300 rows=3 name='local_desc'>${local_info.getLocal_desc()}</textarea></td>
				</tr>
				<tr>
					<td class="font_gray form_label"><form:label
							path="local_pay_way">
							<spring:message code="business.payway" />
						</form:label></td>
					<td><select id="local_pay_way_selector" name="local_pay_way"
						size="1" required>
						<c:if test="${local_info.getLocal_pay_way() eq 0 }">
								<option selected value='0'><spring:message code="business.payway_cash" /></option>
								<option value='1'><spring:message code="business.payway_credit" /></option>
							</c:if>
							<c:if test="${local_info.getLocal_pay_way() eq 1 }">
								<option value='0'><spring:message code="business.payway_cash" /></option>
								<option selected value='1'><spring:message code="business.payway_credit" /></option>
							</c:if>
							
					</select>
				</tr>

				<tr>
					<td class="font_gray form_label"><form:label path="capacity">
							<spring:message code="business.capacity" />
						</form:label></td>
					<td><input type='number' size=4 max="1500" min="1"
						name='capacity' value='${local_info.getCapacity()}'
						onkeypress="return isNumberKey(event)" /></td>

				</tr>

				
			
	<tr>
		<td colspan="2" class="table_title"><spring:message code="prices.title"/></td>
	</tr>
	<tr>
		<td colspan="2" class="table_line"></td>
	</tr>

	<tr>
		<td class="font_gray form_label"><form:label path="local_cost_beer"><spring:message code="prices.beer"/></form:label></td>
		<td colspan="2"><input  type='number' lang="en" min="0" step="0.01"
			name='local_cost_beer' value='${local_info.getLocal_cost_beer()}'
			onkeypress="return isNumberKey(event)" /><span class="profile_span">€
				<i><spring:message code="prices.pvp"/></i>
		</span></td>
	</tr>
	<tr>
		<td class="font_gray form_label"><form:label
				path="local_cost_cocktail"><spring:message code="prices.coktail"/></form:label></td>
		<td colspan="2"><input type='number' min="0" step="0.01"
			name='local_cost_cocktail'
			value='${local_info.getLocal_cost_cocktail()}'
			onkeypress="return isNumberKey(event)" /><span class="profile_span">€
				<i><spring:message code="prices.pvp"/></i>
		</span></td>
	</tr>
	<tr>
		<td class="font_gray form_label"><form:label
				path="local_cost_alch_bottle"><spring:message code="prices.alcohol"/></form:label></td>
		<td colspan="2"><input type='number' min="0" step="1"
			name='local_cost_alch_bottle'
			value='${local_info.getLocal_cost_alch_bottle()}'
			onkeypress="return isNumberKey(event)" /><span class="profile_span">€
				<i><spring:message code="prices.pvp"/></i>
		</span></td>
	</tr>
	<tr>
		<td class="font_gray form_label"><form:label
				path="local_cost_wine_bottle"><spring:message code="prices.wine"/></form:label></td>
		<td colspan="2"><input type='number' min="0" step="0.01"
			name='local_cost_wine_bottle'
			value='${local_info.getLocal_cost_wine_bottle()}'
			onkeypress="return isNumberKey(event)" /><span class="profile_span">€
				<i><spring:message code="prices.pvp"/></i>
		</span></td>
	</tr>
	<tr>
		<td class="font_gray form_label"><form:label
				path="local_cost_coffe"><spring:message code="prices.coffee"/></form:label></td>
		<td colspan="2"><input type='number' min="0" step="0.01"
			name='local_cost_coffe' value='${local_info.getLocal_cost_coffe()}'
			onkeypress="return isNumberKey(event)" /><span class="profile_span">€
				<i><spring:message code="prices.pvp"/></i>
		</span></td>
	</tr>
	<tr>
		<td class="font_gray form_label"><form:label
				path="local_cost_tonic"><spring:message code="prices.tonic"/></form:label></td>
		<td colspan="2"><input type='number' min="0" max="1500" step="0.01"
			name='local_cost_tonic' value='${local_info.getLocal_cost_tonic()}'
			onkeypress="return isNumberKey(event)" /><span class="profile_span">€
				<i><spring:message code="prices.pvp"/></i>
		</span></td>
	</tr>

	<tr style="text-align: center">
					<td colspan='2'>
					<c:if test="${empty isAccepted }">
						<input id="submit_btn" class="submit" name="submit" type="submit" value="<spring:message code="profile.btn_update"/>" />
					</c:if>
					<c:if test="${not empty isAccepted }">
						<input id="submit_btn" class="submit" name="submit" type="submit" value="<spring:message code="profile.btn_next"/>" />
					</c:if>
						
					</td>
				</tr>
			</table>


			<input type='hidden' name='local_id' id='local_id'
				value='${local_info.getUserIdUser()}' />

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />

			<c:if test="${MSG_OK != null}">
				<div class="msg_ok">
					<p style="color: white; margin-left: 35px;">${MSG_OK}</p>
				</div>
			</c:if>

		</form:form>
	</div>





	<div id="right_side">



		

<script type="text/javascript">
    function doDeletePost(elem) {

    	$(elem).attr("disabled","disabled");
    	
    	
			};
		</script>
    
    
<form:form id="pic1_form" commandName="localFormUpdateLocalInfo"
			class="profile_forms form_validator localFormUpdateLocalInfo"
			action="gestor/update_local_pic" enctype='multipart/form-data' method='POST'>
			<table class="_90">
				<tr>
					<td colspan="2" class="table_title"><spring:message
							code="business.images" /></td>
				</tr>
				<tr>
					<td colspan="2" class="table_line"></td>
				</tr>


				<tr>
					<td>
							<c:if test="${MSG_PIC_UPLOAD_OK_1 != null}">
								<c:if test="${PIC_URL != null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="${PIC_URL}" onclick="chargePic('input_pic1','submit_1');"/>
								</c:if>
								<c:if test="${PIC_URL == null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic1','submit_1');"/>
								</c:if>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_1 == null && local_info.getUrl_pic1() == null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic1','submit_1');"/>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_1 == null && local_info.getUrl_pic1() != null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src='${local_info.getUrl_pic1()}' onclick="chargePic('input_pic1','submit_1');"/>
							</c:if>
						
						
						<input class="picinput" id="input_pic1" type="file" name="file" accept="image/gif, image/jpeg, image/png" style="display: none" 
							onchange="readURL(this,1);">
						</td>
				
					<td colspan='2'>
						<input class="submit_pic" name="save" type="submit" id="submit_1" value="<spring:message  code="business.save" />" disabled="disabled" />
						<input class="del_pic" name="delete" type="submit" id="del_1"  value="<spring:message  code="business.delete" />" />
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
					
					<c:if test="${MSG_PIC_UPLOAD_OK_1 != null}">
							<div class="msg_ok">
								<p style="color: white; margin-left: 35px;">${MSG_PIC_UPLOAD_OK_1}</p>
							</div>
					</c:if>
					</td>
				</tr>
			</table>
			<input type='hidden' name='pic_id' id='pic_id' value=1 />
						<input type='hidden' name='x' id='x1' value=0 />
						<input type='hidden' name='y' id='y1' value=0 />
						<input type='hidden' name='w' id='w1' value=0 />
						<input type='hidden' name='h' id='h1' value=0 />
			
			<input type='hidden' name='user_id' id='local_id' value='${local_info.getUserIdUser()}' />
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form:form>
		
		
		
		<form:form commandName="localFormUpdateLocalInfo"
			class="profile_forms form_validator localFormUpdateLocalInfo"
			action="gestor/update_local_pic" enctype='multipart/form-data' method='POST'>
			<table class="_90">
				
				<tr>
					<td colspan="2" class="table_line"></td>
				</tr>


				<tr>
					<td>
					<c:if test="${MSG_PIC_UPLOAD_OK_2 != null}">
								<c:if test="${PIC_URL != null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="${PIC_URL}" onclick="chargePic('input_pic2','submit_2');"/>
								</c:if>
								<c:if test="${PIC_URL == null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic2','submit_2');"/>
								</c:if>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_2 == null && local_info.getUrl_pic2() == null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic2','submit_2');"/>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_2 == null && local_info.getUrl_pic2() != null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src='${local_info.getUrl_pic2()}' onclick="chargePic('input_pic2','submit_2');"/>
							</c:if>
						
						<input class="picinput"  id="input_pic2" type="file" name="file" accept="image/gif, image/jpeg, image/png" style="display: none" onchange="readURL(this,2);">
						</td>
				
					<td colspan='2'>
						<input class="submit_pic" name="save" type="submit" id="submit_2" value="<spring:message  code="business.save" />"  disabled="disabled"/>
						<input class="del_pic" name="delete" type="submit" value="<spring:message  code="business.delete" />" /></td>
				</tr>
				
				<tr>
					<td colspan="2">
						<c:if test="${MSG_PIC_UPLOAD_OK_2 != null}">
							<div class="msg_ok">
								<p style="color: white; margin-left: 35px;">${MSG_PIC_UPLOAD_OK_2}</p>
							</div>
						</c:if>
						<c:if test="${MSG_ERR != null}">
							<div class="msg_ok">
								<p style="color: white;">${MSG_ERR}</p>
							</div>
						</c:if>
					</td>
				</tr>
			</table>

			<input type='hidden' name='pic_id' id='pic_id' value=2 />
						<input type='hidden' name='x' id='x2' value=0 />
						<input type='hidden' name='y' id='y2' value=0 />
						<input type='hidden' name='w' id='w2' value=0 />
						<input type='hidden' name='h' id='h2' value=0 />
			
			<input type='hidden' name='user_id' id='local_id' value='${local_info.getUserIdUser()}' />
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

			
		</form:form>
		
		
		<form:form commandName="localFormUpdateLocalInfo"
			class="profile_forms form_validator localFormUpdateLocalInfo"
			action="gestor/update_local_pic" enctype='multipart/form-data' method='POST'>
			<table class="_90">
				
				<tr>
					<td colspan="2" class="table_line"></td>
				</tr>


				<tr>
					<td>
						
						<c:if test="${MSG_PIC_UPLOAD_OK_3 != null}">
								<c:if test="${PIC_URL != null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="${PIC_URL}" onclick="chargePic('input_pic3','submit_3');"/>
								</c:if>
								<c:if test="${PIC_URL == null}">
									<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic3','submit_3');"/>
								</c:if>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_3 == null && local_info.getUrl_pic3() == null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src="resources/img/pic_no_disponible.png" onclick="chargePic('input_pic3','submit_3');"/>
							</c:if>
							<c:if test="${MSG_PIC_UPLOAD_OK_3 == null && local_info.getUrl_pic3() != null}">
								<img class="local_img" alt="Imagen de tu establecimiento" src='${local_info.getUrl_pic3()}' onclick="chargePic('input_pic3','submit_3');"/>
							</c:if>
						
						<input class="picinput" id="input_pic3" type="file" name="file" accept="image/gif, image/jpeg, image/png" style="display: none" onchange="readURL(this,3);">
						</td>
				
					<td colspan='2'><input class="submit_pic"
						name="save" type="submit" id="submit_3"
						value="<spring:message  code="business.save" />" disabled="disabled" />
						
						<input class="del_pic"
						name="delete" type="submit"
						value="<spring:message  code="business.delete" />" /></td>
				</tr>
				
				<tr>
					<td colspan="2">
						<c:if test="${MSG_PIC_UPLOAD_OK_3 != null}">
							<div class="msg_ok">
								<p style="color: white; margin-left: 35px;">${MSG_PIC_UPLOAD_OK_3}</p>
							</div>
						</c:if>
					</td>
				</tr>
			</table>


			<input type='hidden' name='pic_id' id='pic_id' value=3 />
						<input type='hidden' name='x' id='x3' value=0  />
						<input type='hidden' name='y' id='y3' value=0 />
						<input type='hidden' name='w' id='w3' value=0 />
						<input type='hidden' name='h' id='h3' value=0 />

			<input type='hidden' name='user_id' id='local_id'
				value='${local_info.getUserIdUser()}' />

			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />

			
		</form:form>


	</div>
	
	</div>
	
	<div id="dialog_crop" title="<spring:message code="modal.crop.title" />">
		
	</div>
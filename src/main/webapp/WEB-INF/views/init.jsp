<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page import="com.tkilas.Utils.EnumUtils" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE">
<html lang="es">
<head>
<link rel="icon" href="resources/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=UFT-8">
<meta name="viewport" content="user-scalable = yes">
<meta name="description" content="¡Tkilas! Tu bebida con descuento." />
<meta name="keywords" content="Tkilas, bebidas baratas, bebidas con descuento, copas con descuento, copas baratas, copas lowcost" />
<meta name="robots" content="index, follow">
<meta name="description" content="Con Tkilas encontrarás los mejores descuentos y las mejores promociones en bebidas de tu ciudad">


<link rel="canonical" href="http://www.tkilas.com/">
<meta property="og:title" content="¡Tkilas! Tu bebida con descuento." />
<meta property="og:type" content="website" />
<meta property="og:url" content="http://www.tkilas.com/" />
<meta property="og:image" content="http://www.tkilas.com/resources/img/logo_landing.png" />



<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<link href="resources/css/contact_form.css" rel="stylesheet" type="text/css">
<link href="resources/css/modals.css" rel="stylesheet" type="text/css">



<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<script type="text/javascript" src="resources/js/library/jquery-2.1.0.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53931872-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
</script>
<%@ include file="init.jsp" %>


<link href="resources/css/forgot.css" rel="stylesheet" type="text/css">
<link href="resources/css/fonts.css" rel="stylesheet" type="text/css">

<title><spring:message code="password.recovery.title" /></title>

<script type="text/javascript" src="resources/js/library/jquery.validate.min.js"></script>
<script type="text/javascript" src="resources/js/library/additional-methods.min.js"></script>
<script type="text/javascript" src="resources/js/library/form_utils.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	validateMyForm();
});
</script>
</head>
<body>

	<%@ include file="header_normal_template.jsp"%>


	<div class="container">
		<div class="tel_container">
			<img src="resources/img/tlf_ico.png" />
			<p class="tel_number font_green">639 76 43 07</p>
		</div>

		<div id="forgot-box">

			<img src="resources/img/logo_landing.png" alt="Logo Tkilas" />

			<c:if test="${not empty error}">
				<div class="error_box"><p><spring:message code="forgot.error" /></p></div>
			</c:if>
			<c:if test="${not empty success}">
				<div class="success_box"><p><spring:message code="forgot.msg" /></p></div>
			</c:if>

			<form:form id="forgot_form" class="form_validator" commandName="forgotPasswordForm"
				action="forgot_password/send" method='POST'>

				<input class="input_100" name='email_recovery' placeholder="<spring:message code="login.mail.placeholder" />" value=''>

				<input class="font_white" id="submit_button" name="submit" type="submit" value="<spring:message code="input.search" />" />
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />




			</form:form>
		</div>

	</div>




	<%@ include file="footer_template.jsp"%>

</body>
</html>
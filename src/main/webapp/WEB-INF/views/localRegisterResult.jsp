<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<link rel="icon" href="resources/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="user-scalable = yes">
<title>Tkilas ¡Tu bebida con descuento!</title>
<link rel="stylesheet" href="resources/css/style.css" />
<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]--> 
<script type="text/javascript" src="resources/js/library/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="resources/js/home.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53931872-1', 'auto');
  ga('send', 'pageview');

</script>

<style  type="text/css">
.text_normal{
	color: gray !important;
	font-size:1.5em !important;
	text-align: center !important;
	-webkit-margin-before: 0.5em !important;
-webkit-margin-after: 0.5em !important;
}

.text_spm{
	font-size: 1em !important;
	text-align: center !important;
	-webkit-margin-before: 0.5em !important;
-webkit-margin-after: 0.5em !important;
}

</style>

</head>
<body>

<nav id="header" class="m-navigation ">
		<div id="header_logo_container"><img id="tkila_header_logo" src="resources/img/tkilas_header.png" /></div>
	</nav>


	${returnValue}

	<nav id="footer" class="m-navigation">
		<h4>&copy; Tkilas 2015 <a href="legal" target="_blank" id="legal_cond">Condiciones Legales</a></h4>
	</nav>
</body>
</html>
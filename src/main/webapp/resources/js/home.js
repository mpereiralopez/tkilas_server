$( document ).ready(function() {
	
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	//	 console.log("MOVIL");
		$('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', 'resources/css/mobile.css') );
	}
  var fh = $('#header').height() + $('#footer').height() ;
  /*$('#local_publish').css('line-height',$('#header').height()+'px');*/
  $('#wrapper').css('height',($('body').height()-fh)+'px');
  $('.wrapper').css('height',($('body').height()-fh)+'px');

  $('#mob_container').css('height',($('body').height()-fh)+'px');
  $('#footer h4').css('line-height', ($('#footer').height()-1)+'px');
  $('#footer div').css('line-height', ($('#footer').height()-1)+'px');

  

		if(Boolean(readCookie("tkilasCookiesAccepted")) != true){
			$("body").append('<section id="cookies"><div style="width:100%; max-width: 1440px; position:relative; margin:0.3em auto;"><p style="word-wrap: break-word">Utilizamos cookies propias y de terceros para mejorar nuestros servicios. Si continúa navegando, consideramos que acepta su uso. Puede obtener más información, o bien conocer cómo cambiar la configuración, en nuestra <a href="legal#cookies" target="_blank">Política de cookies</a> <br><button id="cookies_accept_button" class="brand-action">Estoy de acuerdo</button></p></div></section>');
		}

	

  setInterval('cycleImages()', 7000);
  var init = 0;
  $( window ).resize(function() {
		var fh = $('#header').height() + $('#footer').height() ;
		 /*$('#local_publish').css('line-height',$('#header').height()+'px');*/
		  $('.wrapper').css('height',($('body').height()-fh)+'px');

		  $('#wrapper').css('height',($('body').height()-fh)+'px');
		  $('#mob_container').css('height',($('body').height()-fh)+'px');
		  $('#footer h4').css('line-height', ($('#footer').height()-1)+'px');
		  $('#footer div').css('line-height', ($('#footer').height()-1)+'px');
		  //$('#cookies p').css('line-height', ($('#cookies').height()-1)+'px');

		  $('#close_vid').css('top',($($("#video").get(0)).offset().top)-15);
			var leftPos = $($("#video").get(0)).offset().left + $('video').width() - 15;
			$('#close_vid').css('left',leftPos);
			
			
		


	});

	$($('#nav_section').children().eq(0)).click(function(){
		$('#nav_section').children().removeClass('actual');
		$($('#nav_section').children().eq(0)).addClass('actual');

		if(init ==1){
			$('#tarifas').css('left','+100%');
		}
		if(init ==2){
			$('#inscripcion').css('left','+100%');
		}
		$('#inicio').css('left','0');
		init = 0;
	});
	
	$($('#nav_section').children().eq(1)).click(function(){
		$('#nav_section').children().removeClass('actual');
		$($('#nav_section').children().eq(1)).addClass('actual');
		if(init == 0){
			$('#tarifas').css('left','0');
			$('#inicio').css('left','-100%');
		}
		if(init == 2){
			$('#tarifas').css('left','0%');
			$('#inscripcion').css('left','+100%');
		}
		init = 1;
	});
	
	
	$($('#nav_section').children().eq(2)).click(function(){
		$('#nav_section').children().removeClass('actual');
		$($('#nav_section').children().eq(2)).addClass('actual');
		if(init == 0){
			$('#inscripcion').css('left','0');
			$('#inicio').css('left','-100%');
		}
		if(init == 1){
			$('#inscripcion').css('left','0%');
			$('#tarifas').css('left','-100%');
		}
		init = 2;
		
	});
	
	
	
	
	$('#tkila_header_logo').click(function(){
		window.location.href = "http://www.tkilas.com";
	});
	
	$("#play_intro").click(function(){
		$("#video").get(0).play();
		$('#video_container').css('display','block');	
		$('#video_container').css('visibility','visible');		
		$('video').attr('width','75%');
		$('video').attr('height','75%');
		$('video').attr('autoplay','autoplay');
		$('#close_vid').css('top',($($("#video").get(0)).offset().top)-15);
		var leftPos = $($("#video").get(0)).offset().left + $('video').width() - 15;
		$('#close_vid').css('left',leftPos);
	});
		
	
	
	
	
	    
	    
	 $('.free_inscription_claim').click(function(){
	    	$($('#nav_section').children().eq(2)).click();
	    	
	    });

				
		$("#commentForm").bind("submit", manualValidate);
		$("#cookies_accept_button").bind("click", accept_cookies);
		
		
		//Metemos el hash para controlar llegada desde login.
		

				switch (window.location.hash) {

					case '#final':
						$($('#nav_section').children().eq(2)).click();
						break;

					default:
						
						break;
					}

});

function manualValidate(ev) {
    ev.target.checkValidity();
   if(ev.target.checkValidity())send_mail();
    return false;
}


function cycleImages(){
    var $active = $('#slider .active');
    $("#slider img").css("display","block");
    var $next = ($active.next().length > 0) ? $active.next() : $('#slider img:first');
    $next.css('z-index',2);//move the next image up the pile
    $active.fadeOut(1500,function(){//fade out the top image
	  $active.css('z-index',1).show().removeClass('active');//reset the z-index and unhide the image
        $next.css('z-index',3).addClass('active');//make the next image the top one
    });
  }


function send_mail(){
	console.log ( $('#cname').val());
	console.log ( $('#cemail').val());
	console.log ( $('#type_selector').val());
	console.log ( $('#ccomment').val());
	send_comment();
}

function accept_cookies(){
	var client = new XMLHttpRequest();
	client.open("post", 'accept_cookies', true);
	client.send();
	client.onreadystatechange = function() {
		console.log(client);
		if (client.readyState == 4 && client.status == 200) {
		     $( "#cookies" ).css( "display","none" );
		}
	};
}

function send_comment(){
	var client = new XMLHttpRequest();
	var formData = new FormData();
	formData.append("body", $('#cname').val()+": "+$('#ccomment').val());
	formData.append("to", "info@tkilas.com");
	formData.append("from", $('#cemail').val());
	formData.append("subject", $('#type_selector').val());
	if(window.location.href.indexOf("register")==-1){
		client.open("post", 'send_comment_index', true);
	}else{
		client.open("post", 'send_comment', true);
	}
	client.send(formData); /* Send to server */
	client.onreadystatechange = function() {
		console.log(client);
		if (client.readyState == 4 && client.status == 200) {
		     $( "#dialog_contact_form" ).dialog( "close" );
		}
	};
}

function close_vid(){
	$('#video_container').css('visibility' ,'hidden');
	$('#video_container').css('display' ,'none');
	$("video")[0].pause();
}

function close_contact_form(){
	$('#dialog_contact_form').css('visibility' ,'hidden');
	$('#dialog_contact_form').css('display' ,'none');
}

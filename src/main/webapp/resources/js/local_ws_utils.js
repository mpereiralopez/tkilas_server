/**
 * 
 *
 */

function publish_offer_input_checker(){
	/*console.log("VAMOS A PUBLICAR");*/
	time_ini = $('#hour_ini').val() + ":"+$('#min_ini').val()+":00";
	time_fin = $('#hour_fin').val() + ":"+$('#min_fin').val()+":00";
	offer_size = $('#size').val();
	
	var datesToSend = new Array();
	var datesInCalendar = $('#calendar_container').multiDatesPicker('getDates');
	for(var i=0;i<datesInCalendar.length ; i++){
		if(  $.inArray( datesInCalendar[i], packsDates) == -1){
			datesToSend.push(datesInCalendar[i]);
		}
	}
	manage_publish_discount_product_post(datesToSend,time_ini,time_fin,offer_size);
}


function manage_publish_discount_product_post (dates, time_ini,time_fin,offer_size){
	//LIBERO EL MODAL
	$("#modalWaiting").css("display","block");
	$("#modalWaiting").css("visibility","visible");

	//
	var disscount = $('input[type="radio"]:checked', '#offer_section').val();
	
	if(disscount == undefined){
		disscount = -1;
	}
	
	var localId= $('#localId').val();
	var product_type1 = $('#b1').val();
	var product_subtype1 = $('#t1').val();
	var product_number1 = $('#n1').val();
	var product_pvp1 = $('#p1').val();
	var product_max1 = $('#max1').val();
	
	var product_type2 = $('#b2').val();
	var product_subtype2 = $('#t2').val();
	var product_number2 = $('#n2').val();
	var product_pvp2 = $('#p2').val();
	var product_max2 = $('#max2').val();
	
	if(product_number1=="" ||product_number1==undefined )product_number1=-1;
	if(product_pvp1=="" || product_pvp1==undefined)product_pvp1=-1;
	if(product_max1=="" || product_max1==undefined)product_max1=-1;
	
	
	if(product_number2=="" || product_number2==undefined)product_number2=-1;
	if(product_pvp2=="" || product_pvp2==undefined)product_pvp2=-1;
	if(product_max2=="" || product_max2==undefined)product_max2=-1;

	
	
	var client = new XMLHttpRequest();
	var URL = new String();
	if(window.location.href.indexOf("?")!=0){
		URL = window.location.href.substring(0, window.location.href.indexOf("?"))+'/upload_pack_discount_and_product';

	}else{
		URL = window.location.href+'/upload_pack_discount_and_product';

	}
	
	var formData = new FormData();
	formData.append("localId", localId);
	formData.append("dates", dates);
	formData.append("time_ini",time_ini);
    formData.append("time_fin",time_fin);
    formData.append("offer_size",offer_size);
    
    formData.append("disscount",disscount);
    
    formData.append("product_type",product_type1);
    formData.append("product_subtype",product_subtype1);
    formData.append("product_number",product_number1);
    formData.append("product_pvp",product_pvp1);
    formData.append("product_max",product_max1);
    
    formData.append("product_type2",product_type2);
    formData.append("product_subtype2",product_subtype2);
    formData.append("product_number2",product_number2);
    formData.append("product_pvp2",product_pvp2);
    formData.append("product_max2",product_max2);

    
    client.open("post", URL, true);
    client.send(formData);  /* Send to server */ 

	    client.onreadystatechange = function() {
		if (client.readyState == 4 && client.status == 200) {
				$("#dashboard_wrapper").html(client.responseText);
				$("#modalWaiting").css("display","none");
				$("#modalWaiting").css("visibility","hidden");

		}
	};
}



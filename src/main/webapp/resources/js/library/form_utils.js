/**
 * Usin name to check and title to put msg
 */

function isNumberForIban(evt,len){
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
		return false;
	}else{
		return evt.target.value == "" || evt.target.value.length < len;
	}
}

function isFirstFieldIban(evt,len){
	return evt.target.value == "" || evt.target.value.length < len;
}

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
		return false;
	}
	
	var partsOfStr = evt.target.value.split('.');
	var indexOfComma = evt.target.value.indexOf('.')	
	if (partsOfStr[1].length > 2){
		evt.target.value = evt.target.value.slice(0,-1); 
	}
	

	return true;
}

function validateMyForm() {

	$(':input').change(function() {
		$(this).val($(this).val().trim());
	});

	$('textarea').change(function() {
		$(this).val($(this).val().trim());
	});
	

	$(".form_validator").each(function() {
						var form = $(this);
						form.validate(
										{

											rules : {

												user_name : {
													required : true,
													minlength : 2,
													maxlength : 20
												},

												user_surname : {
													required : true,
													minlength : 2,

												},

												email : {
													required : true,
													email : true,
													remote: {
													      url: "register/check_email_free",
													      type: "post",
													      complete: function(data) {
											                    if (data.responseText == false || data.responseText == 'false'){
											                        message: {
											                        	email: 'Este email esta siendo utilizado.'
											                        }
											                    	
											                }
													      }
													  }

												},
												email_recovery : {
													required : true,
													email : true,
												},

												user_pass : {
													required : true,
													minlength : 5,
													nowhitespace : true
												},
												user_pass_repeated : {
													required : true,
													minlength : 5,
													equalTo : "#password"
												},

												local_name : {
													required : true,
													minlength : 1,

												},
												local_phone : {
													required : true,
													minlength : 9,
													nowhitespace : true,
													digits : true
												},

												local_address : {
													required : true,
													maxlength: 500,
												},

												local_cp : {
													required : true,
													minlength : 5,
													maxlength : 5,
													nowhitespace : true,
													digits : true
												},

												local_desc : {
													required : true,
													maxlength : 400
												},
												capacity : {
													maxlength : 4,
													minlength : 1,
													required : true,
													digits : true
												},
												
												local_cost_beer:{
													required:true,
													number:true,
													maxlength : 5,
												},
												local_cost_cocktail:{
													required:true,
													number:true,
													maxlength : 5,
												},
												local_cost_alch_bottle:{
													required:true,
													number:true,
													maxlength : 5,
												},
												
												local_cost_wine_bottle:{
													required:true,
													number:true,
													maxlength : 5,
												},
												
												local_cost_coffe:{
													required:true,
													number:true,
													maxlength : 5,
												},
												
												local_cost_tonic:{
													required:true,
													number:true,
													maxlength : 5,
												},
												
												reserve_cost:{
													required:true
												},
												
												terms_accepted:{
													required : true,
												},
												
												trade_name:{
													required:true,
													minlength : 2,
													maxlength : 20
												},
												
												cif:{
													required:false,
													//cifES : true
												},
												
												contact_address:{
													required:true,
													maxlength: 500,
												},
												
												contact_cp:{
													required:true,
													minlength : 5,
													maxlength : 5,
													nowhitespace : true,
													digits : true
												},
												
												contact_city:{
													required:true
												},
												
												local_iban_1:{
													nowhitespace : true,
													maxlength : 4,

												},
												local_iban_2:{
													digits : true,
													nowhitespace : true,
													maxlength : 4,



												},
												local_iban_3:{
													digits : true,
													nowhitespace : true,
													maxlength : 4,


												},
												local_iban_4:{
													digits : true,
													nowhitespace : true,
													maxlength : 2,


												},
												local_iban_5:{
													digits : true,
													nowhitespace : true,
													maxlength : 10,


												}
											},
											
											
											
											
											
											

											messages : {

												user_name : {
													required : "Se requiere un nombre",
													minlength : "Debe contener al menos dos caracteres",
													maxlength : "No debe sobrepasar 20 caracteres"
												},

												user_surname : {
													required : "Se requieren los apellidos",
													minlength : "Debe contener al menos dos caracteres",
													nowhitespace : "No debe contener espacios al principio ni final"
												},

												email : {
													required : "Se requiere una dirección de correo electrónico",
													email : 'Debe cumplir el formato "nombre@dominio.com"',
						                        	remote: 'Este email esta siendo utilizado.'
												},
												email_recovery:{
													required : "Se requiere una dirección de correo electrónico",
													email : 'Debe cumplir el formato "nombre@dominio.com"',
												},

												user_pass : {
													required : "Debe introducir una contraseña",
													minlength : "La contraseña debe contener al menos 5 caracteres",
													nowhitespace : "No debe contener espacios en blanco al principio ni final"
												},
												user_pass_repeated : {
													required : "Debe introducir una contraseña",
													minlength : "La contraseña debe contener al menos 5 caracteres",
													equalsTo : "Introduzca la misma contraseña que arriba."
												},
												local_name : {
													required : "Se requiere un nombre para el establecimiento",
													minlength : "Debe contener al menos un caracter",
													nowhitespace : "No debe contener espacios en blanco al principio ni final"
												},
												local_phone : {
													required : "Debe introducir un telefono de contacto",
													minlength : "Debe introducir un número de telefono válido",
													nowhitespace : "No debe contener espacios en blanco al principio ni final",
													digits : "Debe introducir un número de telefono válido"
												},

												local_address : {
													required : "Debe introducir la dirección del establecimiento",
													maxlength: "Debe ocupar menos de 500 caracteres",

												},

												local_cp : {
													required : "Debe introducir el código postal del establecimiento",
													minlength : "Debe contener 5 dígitos",
													maxlength : "Debe contener 5 dígitos",
													nowhitespace : "No debe contener espacios en blanco al principio ni final",
													digits : "Debe contener 5 dígitos"
												},
												
												reserve_cost:{
													required:"Es necesario aceptar los terminos de costes"
												},
												
												terms_accepted:{
													required : "Es necesario aceptar los terminos de uso",
												},
												
												trade_name:{
													required:"Se requiere un nombre",
													minlength : "El nombre debe contener al menos 2 caracteres",
													maxlength : "El nombre debe contener menos de 20 caracteres"
												},
												
												cif:{
													cifES:"Introduzca un número de CIF válido"
												},
												contact_address:{
													required:"Se requiere una dirección de contacto",
													maxlength: "Debe ocupar menos de 500 caracteres",
												},
												
												contact_cp:{
													required : "Debe introducir el código postal del establecimiento",
													minlength : "Debe contener 5 dígitos",
													maxlength : "Debe contener 5 dígitos",
													nowhitespace : "No debe contener espacios en blanco al principio ni final",
													digits : "Debe contener 5 dígitos"
												},
												
												contact_city:{
													required:"Debe seleccionar una ciudad"
												}
											},

										});
					});

}
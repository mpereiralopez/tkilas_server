$(document)
		.ready(
				function() {

					moveProgressBar();
					

					
					if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i
							.test(navigator.userAgent)) {
						// console.log("MOVIL");
						$('#contact_box p').text('');
					}
					var fh = $('.m-navigation').height() * 2;
					$('#wrapper').css('height',
							($('body').height() - fh) + 'px');
					$('#footer h4').css('line-height',
							($('#footer').height() - 1) + 'px');
					$('#contact_box').css('top', '91%');

					$('.local_name').css('line-height',
							$('#header').css('height'));


					

					$(window).resize(
							function() {
								$('.local_name').css('line-height',
										$('#header').css('height'));
								$('#footer h4').css('line-height',
										($('#footer').height() - 1) + 'px');
								$('#contact_box').css('top', '91%');
								moveProgressBar();

							});

					

					
						
					

				});



function changeProductType1() {
	var myselect = document.getElementById("b1");
	if (myselect.options[myselect.selectedIndex].value == 2
			|| myselect.options[myselect.selectedIndex].value == 3) {
		$("#t1").removeAttr("disabled");
		if (myselect.options[myselect.selectedIndex].value == 3) {
			$("#t1 option[value='3']").prop('disabled', true);
		} else {
			$("#t1 option[value='3']").prop('disabled', false);
		}
	} else {
		$("#t1").prop("disabled", true);
		$("#n1").removeAttr("disabled");
	}

	if (myselect.options[myselect.selectedIndex].value == 3) {
		showMaxPersons(1);
	} else {
		hideMaxPersons(1);
	}
	
	
	if (myselect.options[myselect.selectedIndex].value == -1){
		$("#publish_offer").prop("disabled",true);
	}else{
		$("#b1 option[value=-1]").remove();
		$("#clearRadioBtn").attr("disabled",false);
		$("#clearProductBtn").attr("disabled",false);
	}

	

}

function changeProductType2() {
	var myselect = document.getElementById("b2");
	if (myselect.options[myselect.selectedIndex].value == 2
			|| myselect.options[myselect.selectedIndex].value == 3) {
		$("#t2").removeAttr("disabled");
		if (myselect.options[myselect.selectedIndex].value == 3) {
			$("#t2 option[value='3']").prop('disabled', true);
		} else {
			$("#t2 option[value='3']").prop('disabled', false);
		}
	} else {
		$("#t2").prop("disabled", true);
		$("#n2").removeAttr("disabled");
	}

	if (myselect.options[myselect.selectedIndex].value == 3) {
		showMaxPersons(2);
	} else {
		hideMaxPersons(2);
	}
	
	if (myselect.options[myselect.selectedIndex].value == -1){
		$("#publish_offer").prop("disabled",true);
	}else{
		$("#b2 option[value=-1]").remove();
		$("#clearRadioBtn").attr("disabled",false);
		$("#clearProductBtn").attr("disabled",false);
	}

}


function changeProductSubtype1(){
	var myselect = document.getElementById("t1");
	if (myselect.options[myselect.selectedIndex].value != -1){
		$("#n1").removeAttr("disabled");
		$("#t1 option[value=-1]").remove();
	}
}

function changeProductSubtype2(){
	var myselect = document.getElementById("t2");
	if (myselect.options[myselect.selectedIndex].value != -1){
		$("#n2").removeAttr("disabled");
		$("#t2 option[value=-1]").remove();
	}
}

function isNumber(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
		return false;
	}else{
		return true
	}
	
}


function onSizeChange(ele,evt){
		if(ele.value > 0){
			if(ele.value <= parseInt($(ele).prop("max"))){
				$(ele).css("border-color","#8cd200");
				$("#offer_section").removeClass("disabled");
				$("#promo_section").removeClass("disabled");
				$(".radio_disscount").prop("disabled",false);
				$("#b1").prop("disabled",false);
				$("#b2").prop("disabled",false);	
			}else{
				$(ele).css("border-color","red");
				$("#offer_section").addClass("disabled");
				$("#promo_section").addClass("disabled");
				$(".radio_disscount").prop("disabled",true);
				$("#b1").prop("disabled",true);
				$("#b2").prop("disabled",true);
			}
			
		}
	
	
}

function onChangePromoSize(ele,index){
	if(ele.value > 0){
		if(index == 1){
			$("#p1").prop("disabled",false);
			var result = ($("#n1").val()) *($("#p1").val())
			$("#m1").val(result)
		}
		if(index == 2){
			$("#p2").prop("disabled",false);
			var result = ($("#n2").val())*($("#p2").val())
			$("#m2").val(result)
		}
	}
}

function onChangePVP(ele,index){
	if(ele.value > 0){
		var result;
		if(index == 1){
			result = ($("#n1").val()) *($("#p1").val());
			$("#m1").val(result);
			changeTotal($("#m1")[0],1);
		}
		if(index == 2){
			result = ($("#n2").val())*($("#p2").val());
			$("#m2").val(result);
			changeTotal($("#m2")[0],2);

		}
		
	}
}

function changeTotal(ele,index){
	console.log(ele);
	console.log(ele.value);
	if(ele.value > 0){
		$("#publish_offer").removeAttr("disabled");
	}else{
		
		if(index == 1){
			if($("#m2").val() > 0){
				$("#publish_offer").removeAttr("disabled");
			}
		}else{
			if($("#m1").val() > 0){
				$("#publish_offer").removeAttr("disabled");
			}
		}
		
	}
}


function showMaxPersons(index) {

	$("#maxPersonsHeader").removeClass("oculto");
	switch (index) {
	case 1:
		$("#maxPersons1").removeClass("oculto");
		$("#max1").prop("disabled", false);
		break;

	case 2:
		$("#maxPersons2").removeClass("oculto");
		$("#max2").prop("disabled", false);

		break;
	default:
		break;
	}
}

function hideMaxPersons(index) {
	$("#maxPersonsHeader").addClass("oculto");
	switch (index) {
	case 1:
		$("#maxPersons1").addClass("oculto");
		$("#max1").prop("disabled", true);

		break;

	case 2:
		$("#maxPersons2").addClass("oculto");
		$("#max2").prop("disabled", true);

		break;
	default:
		break;
	}
}

function clearRadio() {
	$('.radio_disscount').removeAttr('checked');
	if($('#m1').val()=='' && $('#m2').val()=='')$("#publish_offer").attr('disabled', 'disabled');
}

function clearProduct() {
	
	$('#b1').val('-1');
	$("#b1 option[value=-1]").val("Selecciona");

	
	
	$('#t1').val('-1');
	$("#t1").attr('disabled', 'disabled');
	$('#n1').val('');
	$("#n1").attr('disabled', 'disabled');
	$('#p1').val('');
	$("#p1").attr('disabled', 'disabled');
	$('#m1').val('');
	$("#m1").attr('disabled', 'disabled');
	$('#maxPersons1').addClass('oculto');
	$('#b2').val('-1');
	$('#t2').val('-1');
	$("#t2").attr('disabled', 'disabled');
	$('#n2').val('');
	$("#n2").attr('disabled', 'disabled');
	$('#p2').val('');
	$("#p2").attr('disabled', 'disabled');
	$('#m2').val('');
	$("#m2").attr('disabled', 'disabled');
	$('#maxPersons2').addClass('oculto');
	
	var disscount = $('input[type="radio"]:checked', '#offer_section').val();
	
	if(disscount == undefined){
		$("#publish_offer").attr('disabled', 'disabled');
	}

}

function offuscateAll() {
	
	$("#hour_ini").val(-1);
	$("#min_ini").val(-1);
	$("#hour_fin").val(-1);	
	$("#min_fin").val(-1);
	
	
	
	$("#size").val("--");
	
	$(".radio_disscount").prop("checked",false);
	$(".radio_disscount").prop("disabled",true);

	$("#promos_empty").empty();
	$("#table_promociones").css("display","table");

	$("#no_disscount").css("display","none");
	$("#dis_wrapper").css("display","block");
	
	
	$('#b1').val('-1');
	$('#t1').val('-1');
	$('#b2').val('-1');
	$('#t2').val('-1');
	$('#n1').val('');
	$('#n2').val('');
	$('#p1').val('');
	$('#p2').val('');
	$('#m1').val('');
	$('#m2').val('');
	$('#max1').val(1);
	$('#max2').val(1);

	$('#b1').prop('disabled', true);
	$('#t1').prop('disabled', true);
	$('#b2').prop('disabled', true);
	$('#t2').prop('disabled', true);
	$('#n1').prop('disabled', true);
	$('#n2').prop('disabled', true);
	$('#p1').prop('disabled', true);
	$('#p2').prop('disabled', true);
	$('#m1').prop('disabled', true);
	$('#m2').prop('disabled', true);
	$('#max1').prop('disabled', true);
	$('#max2').prop('disabled', true);

	$("#maxPersons1").addClass("oculto");
	$("#maxPersons2").addClass("oculto");

}


function translateType(type) {
	var returnedString = new String();
	switch (parseInt(type)) {
	case 0:
		returnedString = "Cerveza";
		break;
	case 1:
		returnedString = "Café";
		break;
	case 2:
		returnedString = "Copa";
		break;
	case 3:
		returnedString = "Botella";
		break;

	default:
		break;
	}
	return returnedString;
}

function translateSubType(subtype) {
	var returnedString = new String();
	switch (subtype) {
	case 0:
		returnedString = "Básico";
		break;
	case 1:
		returnedString = "Premium";
		break;
	case 2:
		returnedString = "Vino";
		break;
	default:
		returnedString = "";
		break;
	}

	return returnedString;
}

function costOfferTranslated(pvp, size) {
	return " " + size + "*" + pvp + "€ = " + size * pvp + "€";
}

function noShowMore() {
	document.cookie = "showModalOnStart=false";
}

function hourfinstablish() {
	$("#hour_fin option").prop('disabled', false);
	$("#min_ini").val("00");
	var valIni = $("#hour_ini").val();
	for (var i = valIni; i > 0; i--) {
		$("#hour_fin option[value=" + i + "]").prop('disabled', true);
	}
	valIni = parseInt(valIni) + 1
	$("#hour_fin").val(valIni);
	$("#min_fin").val("00");
	$("#hour_fin").prop("disabled", false);
	$("#min_fin").prop("disabled", false);
	$("#size").prop("disabled", false);
}

function openLegalConditions() {
    $( "#dialog_legal_conditions" ).dialog( "open" );

}

//SIGNATURE PROGRESS
function moveProgressBar() {
	var getPercent = ($('.progress-wrap').data('progress-percent') / 100);
	var getProgressWrapWidth = $('.progress-wrap').width();
	var progressTotal = getPercent * getProgressWrapWidth;
	var animationLength = 1500;
	$('.progress-bar').css("left", progressTotal)
}



